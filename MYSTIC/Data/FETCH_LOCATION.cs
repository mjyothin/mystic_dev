//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MYSTIC.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class FETCH_LOCATION
    {
        public int LOCATION_MST_PK { get; set; }
        public string LOCATION_ID { get; set; }
        public string LOCATION_NAME { get; set; }
        public int LOCATION_TYPE_FK { get; set; }
        public string LOCATION_TYPE_ID { get; set; }
        public string LOCATION_TYPE_DESC { get; set; }
        public string ADDRESS_LINE1 { get; set; }
        public string CITY { get; set; }
        public string ZIP { get; set; }
        public Nullable<int> COUNTRY_MST_FK { get; set; }
        public string COUNTRY_ID { get; set; }
        public string COUNTRY_NAME { get; set; }
        public string GST_ARN_VAT_NO { get; set; }
        public string TELE_PHONE_NO { get; set; }
        public string FAX_NO { get; set; }
        public string E_MAIL_ID { get; set; }
        public byte ACTIVE_FLAG { get; set; }
        public string FREIGHT_FORWARDER { get; set; }
    }
}
