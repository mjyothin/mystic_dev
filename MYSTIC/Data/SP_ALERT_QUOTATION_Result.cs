//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MYSTIC.Data
{
    using System;
    
    public partial class SP_ALERT_QUOTATION_Result
    {
        public string QUOTATION_REF_NO { get; set; }
        public Nullable<System.DateTime> VALID_TILL { get; set; }
        public string CUSTOMER_ID { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public string POLID { get; set; }
        public string POLNAME { get; set; }
        public string PODID { get; set; }
        public string PODNAME { get; set; }
        public string OPERATOR_ID { get; set; }
        public string SHIPPINGLINE { get; set; }
        public Nullable<System.DateTime> QUOTATION_DATE { get; set; }
        public Nullable<System.DateTime> SHIPPING_DATE { get; set; }
        public string FF { get; set; }
    }
}
