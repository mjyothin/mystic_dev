//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MYSTIC.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class FETCH_PLACE
    {
        public int PLACE_PK { get; set; }
        public string PLACE_CODE { get; set; }
        public string PLACE_NAME { get; set; }
        public bool ACTIVE_FLAG { get; set; }
        public string LOCATION_ID { get; set; }
        public string LOCATION_NAME { get; set; }
    }
}
