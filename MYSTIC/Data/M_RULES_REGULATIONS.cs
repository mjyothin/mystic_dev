//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MYSTIC.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class M_RULES_REGULATIONS
    {
        public int M_RULES_REGULATIONS_PK { get; set; }
        public string RULES_ID { get; set; }
        public string RULES_DESC { get; set; }
        public Nullable<int> COUNTRY_MST_FK { get; set; }
        public Nullable<byte> IS_ACTIVE { get; set; }
        public Nullable<System.DateTime> CREATED_DATE { get; set; }
        public string COUNTRY_NAME { get; set; }
    }
}
