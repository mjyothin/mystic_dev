//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MYSTIC.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class M_CORPORATE_MST_TBL
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public M_CORPORATE_MST_TBL()
        {
            this.M_DROP_DOWN_TBL = new HashSet<M_DROP_DOWN_TBL>();
            this.M_MESSAGES_MST_TBL = new HashSet<M_MESSAGES_MST_TBL>();
            this.M_USER_MST_TBL = new HashSet<M_USER_MST_TBL>();
            this.M_USER_CORPORATE_TBL = new HashSet<M_USER_CORPORATE_TBL>();
            this.ACTIVITY_LOG_TRN_TBL = new HashSet<ACTIVITY_LOG_TRN_TBL>();
        }
    
        public int M_CORPORATE_MST_PK { get; set; }
        public Nullable<int> E_APPLICATION_MST_FK { get; set; }
        public string CORPORATE_ID { get; set; }
        public string CORPORATE_NAME { get; set; }
        public string ADDRESS_LINE1 { get; set; }
        public string ADDRESS_LINE2 { get; set; }
        public string ADDRESS_LINE3 { get; set; }
        public string CITY { get; set; }
        public Nullable<int> COUNTRY_MST_FK { get; set; }
        public string COUNTRY { get; set; }
        public string CONTACT_PERSON { get; set; }
        public string HOME_PAGE { get; set; }
        public string PHONE { get; set; }
        public string FAX { get; set; }
        public string EMAIL { get; set; }
        public string COMPANY_REG_NO { get; set; }
        public string GST_NO { get; set; }
        public string POST_CODE { get; set; }
        public Nullable<bool> ACTIVE_FLAG { get; set; }
        public string FMC_NO { get; set; }
        public string ACCOUNT_NO { get; set; }
        public string IATA_CODE { get; set; }
        public Nullable<bool> EXCH_RATE_BASIS { get; set; }
        public Nullable<bool> BUSINESS_TYPE { get; set; }
        public Nullable<int> CREATED_BY_FK { get; set; }
        public Nullable<System.DateTime> CREATED_DT { get; set; }
        public Nullable<int> LAST_MODIFIED_BY_FK { get; set; }
        public Nullable<System.DateTime> LAST_MODIFIED_DT { get; set; }
        public Nullable<short> VERSION_NO { get; set; }
        public string DATABASE_NAME { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<M_DROP_DOWN_TBL> M_DROP_DOWN_TBL { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<M_MESSAGES_MST_TBL> M_MESSAGES_MST_TBL { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<M_USER_MST_TBL> M_USER_MST_TBL { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<M_USER_CORPORATE_TBL> M_USER_CORPORATE_TBL { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ACTIVITY_LOG_TRN_TBL> ACTIVITY_LOG_TRN_TBL { get; set; }
    }
}
