//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MYSTIC.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class RATING_REASON_MST_TBL
    {
        public int RATING_REASON_MST_PK { get; set; }
        public string RATING_REASON_ID { get; set; }
        public string RATING_REASON_DESC { get; set; }
        public Nullable<byte> ISACTIVE { get; set; }
        public Nullable<System.DateTime> CREATED_DATE { get; set; }
    }
}
