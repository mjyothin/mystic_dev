//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MYSTIC.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class ALERT_SETTLEMENT_ACCOUNT
    {
        public int COLPK { get; set; }
        public bool PROCESS { get; set; }
        public bool BIZTYPE { get; set; }
        public string COLREFNO { get; set; }
        public System.DateTime COLDATE { get; set; }
        public Nullable<System.DateTime> CLOSINGDATE { get; set; }
        public Nullable<int> CURFK { get; set; }
        public string REMARKS { get; set; }
        public Nullable<int> CUSTFK { get; set; }
        public string CURRENCY { get; set; }
        public string CUSTID { get; set; }
        public short VERSION_NO { get; set; }
        public Nullable<System.DateTime> FROMDATE { get; set; }
        public Nullable<System.DateTime> TODATE { get; set; }
    }
}
