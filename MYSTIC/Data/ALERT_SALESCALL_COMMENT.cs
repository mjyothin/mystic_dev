//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MYSTIC.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class ALERT_SALESCALL_COMMENT
    {
        public Nullable<decimal> SLNR { get; set; }
        public string SALES_CALL_ID { get; set; }
        public int SALES_CALL_COMMENTS_PK { get; set; }
        public Nullable<System.DateTime> COMMENTS_DATE { get; set; }
        public Nullable<int> EXECUTIVE_MST_FK { get; set; }
        public string EMPLOYEE_NAME { get; set; }
        public string COMMENTS { get; set; }
        public Nullable<decimal> DEL { get; set; }
    }
}
