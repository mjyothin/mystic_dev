//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MYSTIC.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class FETCH_SHIPMENT_CARGO_DETAILS
    {
        public string BOOKING_REF_NO { get; set; }
        public string MOVEMENT_CODE { get; set; }
        public string CONTAINER_TYPE_MST_ID { get; set; }
        public string COMMODITY_NAME { get; set; }
        public Nullable<decimal> WEIGHT_MT { get; set; }
        public Nullable<decimal> VOLUME_CBM { get; set; }
        public string PACK_TYPE_ID { get; set; }
        public string PACK_TYPE_DESC { get; set; }
        public Nullable<decimal> QUANTITY { get; set; }
    }
}
