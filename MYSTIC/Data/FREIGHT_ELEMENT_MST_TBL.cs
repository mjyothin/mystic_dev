//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MYSTIC.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class FREIGHT_ELEMENT_MST_TBL
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public FREIGHT_ELEMENT_MST_TBL()
        {
            this.FREIGHT_CONFIG_TRN_TBL = new HashSet<FREIGHT_CONFIG_TRN_TBL>();
            this.QUOTATION_FREIGHT_TRN = new HashSet<QUOTATION_FREIGHT_TRN>();
        }
    
        public int FREIGHT_ELEMENT_MST_PK { get; set; }
        public string FREIGHT_ELEMENT_ID { get; set; }
        public string FREIGHT_ELEMENT_NAME { get; set; }
        public bool ACTIVE_FLAG { get; set; }
        public int CREATED_BY_FK { get; set; }
        public System.DateTime CREATED_DT { get; set; }
        public Nullable<int> LAST_MODIFIED_BY_FK { get; set; }
        public Nullable<System.DateTime> LAST_MODIFIED_DT { get; set; }
        public short VERSION_NO { get; set; }
        public bool APPLICABLE_ON { get; set; }
        public bool CHARGE_BASIS { get; set; }
        public bool BY_DEFAULT { get; set; }
        public bool BUSINESS_TYPE { get; set; }
        public Nullable<int> UOM_MST_FK { get; set; }
        public Nullable<bool> FREIGHT_TYPE { get; set; }
        public Nullable<decimal> BASIS_VALUE { get; set; }
        public Nullable<byte> PREFERENCE { get; set; }
        public string FORMULA { get; set; }
        public Nullable<bool> CREDIT { get; set; }
        public Nullable<bool> BASIC_CHRAGE { get; set; }
        public Nullable<bool> LOCAL_CHARGE { get; set; }
        public bool CHARGE_TYPE { get; set; }
        public string FIN_CODE { get; set; }
        public string COA_CODE { get; set; }
        public bool PAYMENT_TYPE { get; set; }
        public string AIF_FREIGHT_FKS { get; set; }
        public Nullable<bool> SERVICE_TYPE_FK { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FREIGHT_CONFIG_TRN_TBL> FREIGHT_CONFIG_TRN_TBL { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<QUOTATION_FREIGHT_TRN> QUOTATION_FREIGHT_TRN { get; set; }
        public virtual DIMENTION_UNIT_MST_TBL DIMENTION_UNIT_MST_TBL { get; set; }
    }
}
