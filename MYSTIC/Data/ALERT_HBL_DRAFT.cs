//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MYSTIC.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class ALERT_HBL_DRAFT
    {
        public Nullable<decimal> SR_NO { get; set; }
        public int HBL_EXP_TBL_PK { get; set; }
        public string HBL_REF_NO { get; set; }
        public System.DateTime HBL_DATE { get; set; }
        public string CUSTOMER_ID { get; set; }
        public string CONSIGNEE { get; set; }
        public string POL { get; set; }
        public string POD { get; set; }
        public string OPERATOR_ID { get; set; }
        public string VESSEL_NAME { get; set; }
        public string VOYAGE { get; set; }
        public Nullable<System.DateTime> ATD_POL { get; set; }
        public Nullable<System.DateTime> POL_CUT_OFF_DATE { get; set; }
        public Nullable<System.DateTime> POL_ETD { get; set; }
        public string CARGO_TYPE { get; set; }
        public string STATUS { get; set; }
    }
}
