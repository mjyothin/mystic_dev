//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MYSTIC.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class FETCH_AIRFREIGHTSLAB
    {
        public int AIRFREIGHT_SLABS_TBL_PK { get; set; }
        public string BREAKPOINT_ID { get; set; }
        public Nullable<decimal> BREAKPOINT_RANGE { get; set; }
        public string BREAKPOINT_DESC { get; set; }
        public byte ACTIVE_FLAG { get; set; }
        public byte BREAKPOINT_TYPE { get; set; }
        public byte BASIS { get; set; }
        public byte SEQUENCE_NO { get; set; }
        public int CREATED_BY_FK { get; set; }
        public System.DateTime CREATED_DT { get; set; }
        public Nullable<int> LAST_MODIFIED_BY_FK { get; set; }
        public Nullable<System.DateTime> LAST_MODIFIED_DT { get; set; }
        public short VERSION_NO { get; set; }
        public Nullable<decimal> BREAKPOINT_RANGE_TO { get; set; }
    }
}
