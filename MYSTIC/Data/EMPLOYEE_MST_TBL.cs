//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MYSTIC.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class EMPLOYEE_MST_TBL
    {
        public int EMPLOYEE_MST_PK { get; set; }
        public string EMPLOYEE_ID { get; set; }
        public string EMPLOYEE_NAME { get; set; }
        public string EMPLOYEE_ADDRESS1 { get; set; }
        public string EMPLOYEE_ADDRESS2 { get; set; }
        public string EMAIL_ID { get; set; }
        public int DESIGNATION_MST_FK { get; set; }
        public int DEPARTMENT_MST_FK { get; set; }
        public int CREATED_BY_FK { get; set; }
        public System.DateTime CREATED_DT { get; set; }
        public Nullable<int> LAST_MODIFIED_BY_FK { get; set; }
        public Nullable<System.DateTime> LAST_MODIFIED_DT { get; set; }
        public short VERSION_NO { get; set; }
        public string CITY { get; set; }
        public Nullable<int> STATE_MST_FK { get; set; }
        public string ZIP { get; set; }
        public string PHONE_NO { get; set; }
        public string MOBILE_NO { get; set; }
        public Nullable<int> LOCATION_MST_FK { get; set; }
        public Nullable<System.DateTime> JOIN_DATE { get; set; }
        public Nullable<bool> TERMINATED { get; set; }
        public Nullable<bool> BUSINESS_TYPE { get; set; }
        public Nullable<bool> KEY_CONTACT { get; set; }
        public Nullable<bool> EMP_TYPE { get; set; }
        public string FIRST_NAME { get; set; }
        public string LAST_NAME { get; set; }
        public Nullable<bool> SUPER_USER { get; set; }
    
        public virtual DEPARTMENT_MST_TBL DEPARTMENT_MST_TBL { get; set; }
        public virtual DESIGNATION_MST_TBL DESIGNATION_MST_TBL { get; set; }
        public virtual LOCATION_MST_TBL LOCATION_MST_TBL { get; set; }
    }
}
