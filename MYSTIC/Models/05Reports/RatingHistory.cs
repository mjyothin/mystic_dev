﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MYSTIC.Data;

namespace MYSTIC.Models
{
    public class RatingHistory
    {
        public int User_PK { get; set; }
        public int BusinessType { get; set; }
        public int CargoType { get; set; }
        public int Document { get; set; }
        public int Location_PK { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string Customer { get; set; }
        public int POL_AOL_PK { get; set; }
        public int POD_AOD_PK { get; set; }
        public int Currency_PK { get; set; }
        public List<SP_RPT_RATING_HISTORY_Result> RptRateHistory { get; set; }
    }
}