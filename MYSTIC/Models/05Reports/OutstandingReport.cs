﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MYSTIC.Data;

namespace MYSTIC.Models
{
    public class OutstandingReport
    {     
        public int UserPK { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string Customer { get; set; }
        public string Location { get; set; }
        public string Sector { get; set; }
        public string Vessel { get; set; }
        public int BusinessType { get; set; }
        public int ProcessType { get; set; }
        public int JobType { get; set; }
        public int BaseCurrency { get; set; }
        public List<SP_RPT_OUTSTANDING_Result> OutstandingResult { get; set; }
    }
}