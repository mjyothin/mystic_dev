﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MYSTIC.Data;

namespace MYSTIC.Models
{
    public class CustomsStatus
    {
        public int UserPK { get; set; }
        public int BusinessType { get; set; }
        public int CargoType { get; set; }
        public string JC_PK { get; set; }     
        public int JobType { get; set; }
        public int ProcessType { get; set; }
        public string Vessel_Voyage { get; set; }
        public string Customer_PK { get; set; }
        public string JCRefNo { get; set; }
        public string CHA_PK { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string Commodity_PK { get; set; }
        public int Status { get; set; }
        public List<SP_RPT_CUSTOMS_STATUS_Result> LstCustomsStatus { get; set; }
    }
    public class VendorList
    {
        public decimal CHA_PK { get; set; }
        public string CHA_Name { get; set; }
    }
    public class JobCardList
    {
        public decimal? JC_PK { get; set; }
        public string JCNumber { get; set; }
    }

    public class CustomerList
    {
        public decimal Customer_PK { get; set; }
        public string Customer_Name { get; set; }
    }
    public class VesVoyList
    {
        public string Vessel_Voyage { get; set; }
    }
}