﻿using System;
using System.Collections.Generic;

namespace MYSTIC.Models
{
    public class CustomerSOA
    {
        public decimal? Overall_Outstanding { get; set; }
        public List<CustomerSOAList> CustomerSOALists { get; set; }
    }
    public class CustomerSOAList
    {
        public decimal CUSTOMER_MST_PK { get; set; }
        public decimal? LOC_PK { get; set; }
        public DateTime REF_DATE { get; set; }
        public string REFDATE { get; set; }
        public string PROCESS { get; set; }
        public string TRANSACTION { get; set; }
        public string SHIPMENT_REF_NO { get; set; }
        public string DOCREFNR { get; set; }
        public decimal? DEBIT { get; set; }
        public decimal? CREDIT { get; set; }
        public decimal? BALANCE { get; set; }
        public string OUTSTANDING_DAYS { get; set; }
        public decimal? REF_PK { get; set; }
        public decimal? CARGO_TYPE { get; set; }
        public decimal? BIZ_TYPE { get; set; }
    }
    public class CustomerSOASearch
    {
        public int UserPK { get; set; }
        public string FreightForwarder { get; set; }
        public string Fromdate { get; set; }
        public string Todate { get; set; }
        public string POLPK { get; set; }
        public string PODPK { get; set; }
        public string CountryPK { get; set; }
        public string LocPK { get; set; }
        public string CustPK { get; set; }
        public int BizType { get; set; }
        public int ProcessType { get; set; }
        public string CustGroupPK { get; set; }
        public string CarrierPK { get; set; }
        public string VslVoyPK { get; set; }
        public string FlightID { get; set; }
        public int CargoType { get; set; }
        public int CurrPK { get; set; }
        public int BaseCurrPK { get; set; }
    }
}