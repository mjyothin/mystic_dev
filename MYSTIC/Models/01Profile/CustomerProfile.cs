﻿using MYSTIC.Data;
using System.Collections.Generic;

namespace MYSTIC.Models
{
    public class CustomerProfile
    {
        public FETCH_CUSTOMER_PROFILE CustomerDetails { get; set; }
        public List<FETCH_CUSTOMER_CATEGORY> CustomerCategoryDetails { get; set; }
        public List<FETCH_CUSTOMER_CONTACT> CustomerContactDetails { get; set; }
        //public string CompanyID { get; set; }
        //public string CompanyName { get; set; }
        //public List<string> CustomerCategory { get; set; }
        //public AddressClass AdminAddress { get; set; }
        //public AddressClass CorresAddress { get; set; }
        //public AddressClass BillingAddress { get; set; }
        //public string CustomerRegNo { get; set; }
        //public string AccountNumber { get; set; }
        //public string TaxNumber { get; set; }
        //public string BusinessType { get; set; }
        //public CreditDetails CreditDetails { get; set; }
        //public List<ContactDetails> contactDetails { get; set; }
        //public BankDetailsClass BankDetails { get; set; }

    }
    public class AddressClass
    {
        public string Address { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string PhoneNumber { get; set; }
        public string MobileNumbre { get; set; }
        public string FaxNumber { get; set; }
        public string EmailID { get; set; }
    }
    public class BankDetailsClass
    {
        public string BankName { get; set; }
        public string AccountNumber { get; set; }
        public string Address { get; set; }
        public string Branch { get; set; }
        public string Location { get; set; }
        public string Country { get; set; }
    }
    public class CreditDetails
    {
        public decimal? CreditLimit { get; set; }
        public short? CreditDays { get; set; }
        public decimal? CreditUsed { get; set; }
        public decimal? CreditBalance { get; set; }
    }
    public class ContactDetails
    {
        public string ContactPerson { get; set; }
        public string Designation { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
    }
}