﻿using MYSTIC.Data;

namespace MYSTIC.Models
{
    public class UserPreference
    {
       public int? ENVIRONMENT_FK { get; set; }
        public short? USER_FONT_SIZE { get; set; }
        public short? USER_DATE_FORMAT { get; set; }
        public short? USER_NUMBER_FORMAT { get; set; }
        public short? USER_STYLE_SHEET { get; set; }
    }
}