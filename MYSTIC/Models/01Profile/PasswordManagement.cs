﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MYSTIC.Models
{
    public class PasswordManagement
    {
        public string UserID { get; set; }
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
    }
}