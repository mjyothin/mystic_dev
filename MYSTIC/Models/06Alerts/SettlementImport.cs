﻿using MYSTIC.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MYSTIC.Models
{
    public class SettlementImport
    {
        public List<ALERT_SETTLEMENT> LstSettlement { get; set; }
        public ALERT_SETTLEMENT_ACCOUNT SettlementAccount { get; set; }
        public List<ALERT_SETTLEMENT_OUTSTANDING> SettlementOutstanding { get; set; }
        public List<ALERT_SETTLEMENT_COLLECTION> SettlementCollection { get; set; }
    }
}