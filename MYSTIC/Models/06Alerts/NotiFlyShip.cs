﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MYSTIC.Data;

namespace MYSTIC.Models
{
    public class NotiFlyShip
    {
        public string BookingRefNo{get;set ;}
        public string BookingDate { get; set; }
        public string ShipmentDate { get; set; }
        public decimal? Volume { get; set; }
        public string Customer { get; set; }
        public string Commodity { get; set; }
    }
}