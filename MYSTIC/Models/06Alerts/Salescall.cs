﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MYSTIC.Data;

namespace MYSTIC.Models
{
    public class Salescall
    {
        public List<ALERT_SALES_CALL> SalesCall { get; set; }
        public ALERT_SALESCALL_HEADER SalesCallHeader { get; set; }
        public List<ALERT_SALESCALL_COMMENT> SalesCallComment { get; set; }
        public List<ALERT_SALESCALL_HISTORY> SalesCallHistory { get; set; }
        public ALERT_SALESCALL_FOLLOWUP SalesCallFollowUp { get; set; }
        public ALERT_SALESCALL_ESCALATE SalesCallEscalate { get; set; }
        public ALERT_SALESCALL_DELEGATE SalesCallDelegate { get; set; }
        public ALERT_SALESCALL_REVISIT SalesCallRevisit { get; set; }
    }
}