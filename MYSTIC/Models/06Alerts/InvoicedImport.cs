﻿using MYSTIC.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MYSTIC.Models
{
    public class InvoicedImport
    {
        public List<ALERT_INVOICE_IMP_EXP> LstInvoiceImport { get; set; }
        public List<ALERT_INVOICE_DTL> LstInvoiceDetail { get; set; }
        public ALERT_INVOICED_HEADER InvoiceHeader { get; set; }
        public List<ALERT_INVOICE_JOB_JC> InvoiceJC { get; set; }
        public List<ALERT_INVOICE_JOB_CBJC> InvoiceCBJC { get; set; }
        public List<ALERT_INVOICE_JOB_TPT> InvoiceTPT { get; set; }
        public List<ALERT_INVOICE_JOB_DEM> InvoiceDEM { get; set; }
    }
}