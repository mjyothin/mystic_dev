﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MYSTIC.Data;

namespace MYSTIC.Models
{
    public class InvoicedExport
    {
        public List<ALERT_INVOICE_IMP_EXP> LstInvoiceExport { get; set; }
        public List<ALERT_INVOICE_DTL> LstInvoiceDetail { get; set; }
        public ALERT_INVOICED_HEADER InvoiceHeader { get; set; }
        public List<ALERT_INVOICE_JOB_JC> InvoiceJC { get; set; }
        public List<ALERT_INVOICE_JOB_CBJC> InvoiceCBJC { get; set; }
        public List<ALERT_INVOICE_JOB_TPT> InvoiceTPT { get; set; }
        public List<ALERT_INVOICE_JOB_DEM> InvoiceDEM { get; set; }
    }
}