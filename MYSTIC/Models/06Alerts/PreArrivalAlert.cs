﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MYSTIC.Models
{
    public class PreArrivalAlert
    {
        public string Vsl_Voy{get;set;}
        public string HBLNr { get; set; }
        public string MBLNr { get; set; }
        public string POL { get; set; }
        public string ContainerNo { get; set; }
        public string POD { get; set; }
        public string DPAgent { get; set; }
    }
}