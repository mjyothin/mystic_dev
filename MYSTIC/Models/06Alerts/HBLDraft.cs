﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MYSTIC.Data;

namespace MYSTIC.Models
{
    public class HBLDraft
    {
        public List<ALERT_HBL_DRAFT> LstHBLDraft { get; set; }
        public ALERT_HBL_DTL HBLDraftDetail { get; set; }
    }
}