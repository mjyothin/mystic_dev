﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MYSTIC.Data;

namespace MYSTIC.Models
{
    public class Quotation
    {
        public List<SP_ALERT_QUOTATION_Result> LstQuotationAlert { get; set; }
        // public List<ALERT_QUOTATION> LstQuotationAlert { get; set; }
        public ALERT_QUOTATION_DTL QuotationDetail { get; set; }
        public List<ALERT_QUOTATION_FRT> QuotationFrtDet { get; set; }
        public List<ALERT_QUOTATION_OTH_FRT> QuotationOtherFrtDet { get; set; }
    }
}