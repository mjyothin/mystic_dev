﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MYSTIC.Data;

namespace MYSTIC.Models
{
    public class OrderReceived
    {
        public List<SP_ALERT_ORDER_RCVD_Result> LstOrderReceived { get; set; }
        //public List<ALERT_ORDER_RECEIVED> LstOrderReceived { get; set; }
    }
}