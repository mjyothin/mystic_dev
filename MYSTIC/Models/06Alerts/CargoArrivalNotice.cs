﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MYSTIC.Data;

namespace MYSTIC.Models
{
    public class CargoArrivalNotice
    {
        public List<ALERT_CAN_AIR> LstCANAir { get; set; }
        public List<ALERT_CAN_SEA> LstCANSea { get; set; }
        public List<ALERT_CAN_AIR_DTL> CANAirDetails { get; set; }
        public List<ALERT_CAN_SEA_DTL> CANSeaDetails { get; set; }
        public List<ALERT_CAN_CLAUSE_DTL> CANClauseDetail { get; set; }
    }
}