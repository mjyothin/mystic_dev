﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MYSTIC.Data;

namespace MYSTIC.Models
{
    public class VesselDelay
    {
        public List<ALERT_VESSEL_DELAY> LstVesselDelay { get; set; }
        public List<ALERT_VESSEL_DELAY_DTL> VesselDelayDetail { get; set; }
    }
    public class VesselDelaySearch
    {
        public int VesselPK { get; set; }
        public string VesselId { get; set; }
        public string VesselName { get; set; }
        public string Status { get; set; }
        public int ProcessType { get; set; }
    }
}