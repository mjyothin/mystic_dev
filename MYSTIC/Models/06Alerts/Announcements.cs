﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MYSTIC.Data;

namespace MYSTIC.Models
{
    public class Announcements
    {
        // public List<ALERT_ANNOUNCEMENT> LstAnnouncement { get; set; }
        public List<AnnouncementList> LstAnnouncement { get; set; }
        public ALERT_ANNOUNCEMENT_DETAILS AnnouncementDetails { get; set; }
        public List<ALERT_ANNOUNCEMENT_ATTACHMENT> AnnouncementAttachments { get; set; }
    }
    public class AnnouncementList
    {
        public string Sender { get; set; }
        public string Subject { get; set; }
        public string Description { get; set; }
    }
}