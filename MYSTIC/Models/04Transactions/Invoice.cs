﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MYSTIC.Data;

namespace MYSTIC.Models
{
    public class Invoice
    {
        public List<InvoiceListing> Invoices { get; set; }
    }
    public class SearchInvoice
    {
        public int UserPK { get; set; }
        public string InvoiceNumber { get; set; }
        public DateTime InvoiceDate { get; set; }
        public DateTime DueDate { get; set; }
        public string POLID { get; set; }
        public string PODID { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string Customer { get; set; }
        public string FF { get; set; }
    }
    public class InvoiceListing
    {
        public string InvoiceNumber { get; set; }
        public string InvoiceDate { get; set; }
        public string DueDate { get; set; }
        public string InvoiceCurrency { get; set; }
        public decimal InvoiceAmount { get; set; }     
        public string Customer { get; set; }
        public string FF { get; set; }
    }
}