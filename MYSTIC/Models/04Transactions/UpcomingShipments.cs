﻿using MYSTIC.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MYSTIC.Models
{
    public class UpcomingShipments
    {
        public int UserPk { get; set; }
        public string CorporateName { get; set; }
        public int BusinessType { get; set; }
        public bool IsExport { get; set; } 
        public int IsHistory { get; set; }
        public int CargoType { get; set; }
        public int Process_Type { get; set; }
        public string POL { get; set; }
        public string POD { get; set; }
        public int CustomerPK { get; set; }
        public string ShipmentNo { get; set; }
        public int VesselVoyagePK { get; set; }
        public string ShippingLine { get; set; }
        public string Freight_forwarder { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public List<UpcomingShipmentList> Shipments { get; set; }
    }
    public class ShipmentDetails
    {
        public SP_UPCOMING_SHIPMENTS_DTL_Result ShipmentDetail { get; set; }
        public List<SP_UPCOMING_SHIP_CARGO_DTL_Result> CargoDetails { get; set; }
        public List<SP_UPCOMING_SHIP_FRT_DTL_Result> FreightDetails { get; set; }
    }
    public class UpcomingShipmentList
    {
        public decimal JOB_CARD_TRN_PK { get; set; }
        public Nullable<decimal> BUSINESS_TYPE { get; set; }
        public decimal CARGO_TYPE { get; set; }
        public string POLID { get; set; }
        public string POL { get; set; }
        public string PODID { get; set; }
        public string POD { get; set; }
        public string SHIPPER_NAME { get; set; }
        public string CONSIGNEE_NAME { get; set; }
        public string SHIPMENT_NO { get; set; }
        public string SHIPMENT_DATE { get; set; }
        public string VES_VOY_NO { get; set; }
        public Nullable<decimal> COL_PLACE_MST_FK { get; set; }
        public string PLACE_RECEIPT { get; set; }
        public Nullable<decimal> DEL_PLACE_MST_FK { get; set; }
        public string PLACE_LOADING { get; set; }
        public string ETA_DATE { get; set; }
        public string ETD_DATE { get; set; }
        public string LOAD_AGENT { get; set; }
        public string DISCHARGE_AGENT { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public string SHIPPING_LINE { get; set; }
        public string ARRIVAL_DATE { get; set; }
        public string DEPARTURE_DATE { get; set; }
        public string FF { get; set; }
    }
}