﻿using MYSTIC.Data;
using System;
using System.Collections.Generic;

namespace MYSTIC.Models
{
    public class ActiveQuotations
    {
        public int BIZ_TYPE { get; set; }
        public string BIZTYPE { get; set; }
        public int CARGO_TYPE { get; set; }
        public string CARGOTYPE { get; set; }
        public int isActive { get; set; }
        public int UserPK { get; set; }
        public string QUOTATION_REF_NO { get; set; }
        public string POLID { get; set; }
        public string POLNAME { get; set; }
        public string PODID { get; set; }
        public string PODNAME { get; set; }
        public string CUSTOMER_ID { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public Nullable<decimal> STATUS { get; set; }
        public string QUOTATION_DATE { get; set; }
        public string VALID_FOR { get; set; }
        public string FreightForwarder { get; set; }
        public DateTime Validity { get; set; }
        //  public List<SP_FETCH_QUOTATION_Result> Quotations { get; set; }
        // public AcQuotationDetails QuotationDetails { get; set; }
        // public SP_FETCH_QUOTATION_DTL_Result QuotationCargodetails { get; set; }
        // public List<SP_FETCH_QUOTATION_FRT_DTL_Result> QuotationFrtDtl { get; set; }
        public string STATUSNAME { get; internal set; }
    }
    public class ActiveQuotationDetails
    {
        public AcQuotationDetails QuotationDetails { get; set; }
        public List<QuotationCargodetails> QuotationCargodetails { get; set; }
    }
    public class AcQuotationDetails
    {
        public Nullable<decimal> QUOTE_DTL_PK { get; set; }
        public Nullable<decimal> QUOTATION_MST_FK { get; set; }
        public Nullable<decimal> BIZ_TYPE { get; set; }
        public Nullable<decimal> CARGO_TYPE { get; set; }
        public string QUOTATION_REF_NO { get; set; }
        public string QUOTATION_DATE { get; set; }
        public string CUSTOMER_ID { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public string VALID_FOR { get; set; }
        public string STATUS { get; set; }
        public string POLID { get; set; }
        public string POLNAME { get; set; }
        public string PODID { get; set; }
        public string PODNAME { get; set; }

    }
    public class QuotationCargodetails
    {
        public Nullable<decimal> QUOTE_DTL_PK { get; set; }
        public Nullable<decimal> QUOTATION_MST_FK { get; set; }
        public string COMMODITY_GROUP_CODE { get; set; }
        public string COMMODITY_GROUP_DESC { get; set; }
        public string COMMODITY_ID { get; set; }
        public string COMMODITY_NAME { get; set; }
        public string CONTAINER_TYPE_MST_ID { get; set; }
        public string CONTAINER_TYPE_NAME { get; set; }
        public Nullable<decimal> CONTAINER_TYPE_MST_FK { get; set; }
        public Nullable<decimal> COMMODITY_GROUP_FK { get; set; }
        public Nullable<decimal> COMMODITY_MST_FK { get; set; }
        public Nullable<decimal> BASIS { get; set; }
        public Nullable<decimal> EXPECTED_VOLUME { get; set; }
        public Nullable<decimal> EXPECTED_WEIGHT { get; set; }
        public List<QuotationFreightDetails> QuotationFreightDetails { get; set; }
    }
    public class QuotationFreightDetails
    {
        public Nullable<decimal> QUOTION_FREIGHT_PK { get; set; }
        public Nullable<decimal> QUOTATION_DTL_FK { get; set; }
        public Nullable<decimal> FREIGHT_ELEMENT_MST_FK { get; set; }
        public Nullable<decimal> CURRENCY_MST_FK { get; set; }
        public string FREIGHT_ELEMENT_NAME { get; set; }
        public string CURRENCY_ID { get; set; }
        public Nullable<decimal> QUOTED_RATE { get; set; }
    }

}