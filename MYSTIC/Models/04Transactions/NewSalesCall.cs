﻿using MYSTIC.Data;
using System;

namespace MYSTIC.Models
{
    public class NewSalesCall
    {
        public SALES_CALL_TRN SalesCall { get; set; }
    }
}