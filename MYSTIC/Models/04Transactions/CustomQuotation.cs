﻿using MYSTIC.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MYSTIC.Models
{
    public class CustomQuotation
    {
        public List<FETCH_CUSTOM_QUOTATION> lstCustomQuotation { get; set; }
        public FETCH_CUSTOMQUOTE_DETAILS CustomQuoteDetails { get; set; }
        public List<FETCH_CUSTOMQUOTE_CONT_DETAILS> CustomQuoteContDetails { get; set; }
    }
}