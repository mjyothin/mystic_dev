﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MYSTIC.Data;

namespace MYSTIC.Models
{
    public class NewBooking
    {
        public int Status { get; set; }
        public int UserPK { get; set; }
        public string FreightForwarder { get; set; }
        public List<string> FreightForwarderList { get; set; }
        public BOOKING_MST_TBL Booking { get; set; }
        public List<BookingDetails> BookingDetails { get; set; }
        public List<AirlineSchedule> AirCarrierDetails { get; set; }
        public List<SeaSchedule> SeaCarrierDetails { get; set; }
        public List<SP_FETCH_SHIPPER_DETAILS_Result> ShipperList { get; set; }
        public List<SP_FETCH_CONSIGNEE_DETAILS_Result> ConsigneeList { get; set; }
    }
    public class BookingDetails
    {
        public BOOKING_TRN BOOKING_TRN { get; set; }
        public List<BOOKING_TRN_FRT_DTLS> BookingFreightDetails { get; set; }
        public List<SP_FETCH_FRT_DTL_BOOKING_Result> FreightDetails { get; set; }
    }
    public class ListBooking
    {
        public List<BookingCargoDetails> BookingCargoDetails { get; set; }
    }
    public class BookingCargoDetails
    {
        public decimal BOOKING_TRN_PK { get; set; }
        public string BOOKING_REF_NO { get; set; }
        public string MOVEMENT_CODE { get; set; }
        public string CONTAINER_TYPE_MST_ID { get; set; }
        public string COMMODITY_NAME { get; set; }
        public Nullable<decimal> WEIGHT_MT { get; set; }
        public Nullable<decimal> VOLUME_CBM { get; set; }
        public string PACK_TYPE_ID { get; set; }
        public string PACK_TYPE_DESC { get; set; }
        public Nullable<decimal> QUANTITY { get; set; }
        public List<SP_FETCH_BOOKING_FRT_DTL_Result> BookingFreightDetails { get; set; }
    }
    public class BookingList
    {
        public decimal BUSINESS_TYPE { get; set; }
        public Nullable<decimal> CARGO_TYPE { get; set; }
        public string POL { get; set; }
        public string POD { get; set; }
        public string SHIPPER_NAME { get; set; }
        public string CONSIGNEE_NAME { get; set; }
        public string BOOKING_NO { get; set; }
        public string VES_VOY_NO { get; set; }
        public string SHIPMENT_DATE { get; set; }
        public DateTime SHIPMENTDATE { get; set; }
        public string PLACE_RECEIPT { get; set; }
        public string PLACE_LOADING { get; set; }
        public string ETA_DATE { get; set; }
        public string ETD_DATE { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public string SHIPPING_LINE { get; set; }
        public string FF { get; set; }
    }
}