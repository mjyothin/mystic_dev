﻿using System;
using System.Collections.Generic;
using MYSTIC.Data;
using Newtonsoft.Json;

namespace MYSTIC.Models
{
    public class QuotationRequest
    {
        public int Status { get; set; }
        public int UserPK { get; set; }
        public List<FreightForwarder> FreightForwarder { get; set; }
        public Quotation_Master Quotation { get; set; }
        public List<QuotationDetails> QuotationDetails { get; set; }
    }  
    public class Quotation_List
    {
        public Quotation_Master QUOTATION { get; set; }
        public List<QuotationDetails> QUOTATIONDETAILS { get; set; }
    }  
    public class QuotationResponse
    {
        public string RateRequestNumber { get; set; }
        public List<QuotationResponseDtl> quotations { get; set; }
    }
    public class QuotationResponseDtl
    {
        public string FREIGHT_FORWARDER { get; set; }
        public string QuotationNumber { get; set; }
        public string Status { get; set; }
    }
    //Added for listing
    public class Quotation_Master
    {
        public decimal QUOTATION_MST_PK { get; set; }
        public string QUOTATION_REF_NO { get; set; }
        public string QUOTATION_DATE { get; set; }
        public string EXPECTED_SHIPMENT_DT { get; set; }
        public string VALID_FOR { get; set; }
        public string BIZTYPE { get; set; }
        public Nullable<byte> BIZ_TYPE { get; set; }
        public Nullable<byte> PROCESS_TYPE { get; set; }
        public string PROCESSTYPE { get; set; }
        public string CARGOTYPE { get; set; }
        public Nullable<byte> CARGO_TYPE { get; set; }
        public Nullable<int> CUSTOMER_MST_FK { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public string STATUS { get; set; }
        public Nullable<int> PORT_MST_POL_FK { get; set; }
        public Nullable<int> PORT_MST_POD_FK { get; set; }
        public string POL_ID { get; set; }
        public string POD_ID { get; set; }
        public string PORT_OF_LOADING { get; set; }
        public string PORT_OF_DISCHARGE { get; set; }
        public Nullable<int> FREIGHT_FORWARDER_PK { get; set; }
        public string FREIGHT_FORWARDER { get; set; }
        public string COM_REFERENCE_NO { get; set; }
        public int COMMODITY_GROUP_MST_FK { get; set; }
    }
    //Added for listing
    public class QuotationDetails
    {
        public int QUOTE_DTL_PK { get; set; }
        public int QUOTATION_MST_FK { get; set; }
        public int? PORT_MST_POD_FK { get; set; }
        public int? PORT_MST_POL_FK { get; set; }
        public Nullable<int> CONTAINER_TYPE_MST_FK { get; set; }
        public string CONTAINER_TYPE { get; set; }
        public Nullable<int> COMMODITY_GROUP_FK { get; set; }
        public string COMMODITY_GROUP_NAME { get; set; }
        public Nullable<int> COMMODITY_MST_FK { get; set; }
        public string COMMODITY_NAME { get; set; }
        public Nullable<int> BASIS { get; set; }
        public Nullable<decimal> EXPECTED_VOLUME { get; set; }
        public Nullable<decimal> EXPECTED_WEIGHT { get; set; }
    }
}