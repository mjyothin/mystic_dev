﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MYSTIC.Data;

namespace MYSTIC.Models
{
    public class Collections
    {
        public int UserPK { get; set; }
        public string InvoiceNumber { get; set; }
        public string CollectionNumber { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public List<ListCollection> LstCollection { get; set; }
    }  
    public class ListCollection
    {
        public string INVOICE_REF_NO { get; set; }
        public string INVOICE_DATE { get; set; }
        public string INVOICE_DUE_DATE { get; set; }
        public string CURRENCY_ID { get; set; }
        public Nullable<decimal> INVOICE_AMOUNT { get; set; }
        public string COLLECTIONS_REF_NO { get; set; }
        public string COLLECTIONS_DATE { get; set; }
        public Nullable<decimal> COLLECTION_AMOUNT { get; set; }
        public Nullable<decimal> BALANCE { get; set; }
    }
}