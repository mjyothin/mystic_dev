﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MYSTIC.Data;

namespace MYSTIC.Models
{
    public class Feedback
    {
        public RATE_BOOKING_TRN_TBL RateBooking { get; set; }
        public RATE_FF_TRN_TBL RateFreightForwarder { get; set; }
    }
    public class BookingFeedback
    {
        public string BookingNumber { get; set; }
        public string CorporateName { get; set; }
    }
    public class FFFeedback
    {
        public int FF_PK { get; set; }
        public int FF { get; set; }
    }
}