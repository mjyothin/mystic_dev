﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MYSTIC.Data;

namespace MYSTIC.Models
{
    public class ScheduleEnquiry
    {
        public List<AirlineSchedule> ScheduleAir { get; set; }
        public List<SeaSchedule> ScheduleSea { get; set; }
        public int BusinessType { get; set; }
        public int USER_PK { get; set; }
        public string Freight_forwarder { get; set; }
        public int? CARRIER_MST_PK { get; set; }
        public string Carrier_ID { get; set; }
        public string Carrier_Name { get; set; }
        public int AOO_PK { get; set; }
        public int AOD_PK { get; set; }
        public int POL_PK { get; set; }
        public int POD_PK { get; set; }
        public int FLAG { get; set; }  //1-Departure On,2-Departure Before,3-Departure After,4-Departure Between,5-Arrival On,6-Arrival Before,7-Arrival After,8-Arrival Between,
        public string DEP_DATE { get; set; }
        public string DEP_FROM_DATE { get; set; }
        public string DEP_TO_DATE { get; set; }
        public string ARR_DATE { get; set; }
        public string ARR_FROM_DATE { get; set; }
        public string ARR_TO_DATE { get; set; }
        public string TRANS_TIME_TO { get; set; }
        public string TRANS_TIME_FROM { get; set; }
        public string P_DAY { get; set; }
    }
    public class Carrier_Mst
    {
        public List<FETCH_CARRIER_MST_Result> lstCarrier { get; set; }
    }
    public class AirlineSchedule
    {
        public string AIRLINE_ID { get; set; }
        public decimal CARRIER_MST_PK { get; set; }
        public string CARRIER_ID { get; set; }
        public string CARRIER_NAME { get; set; }
        public string FLIGHTNUMBER { get; set; }
        public string CUT_OFF_DATE { get; set; }
        public string DEPARTURE { get; set; }
        public string DEPARTUREDAY { get; set; }
        public string TARNSIT_TIME { get; set; }
        public string ARRIVAL { get; set; }
        public string ARRIVALDAY { get; set; }
    }
    public class SeaSchedule
    {
        public decimal CARRIER_MST_PK { get; set; }
        public string CARRIER_ID { get; set; }
        public string CARRIER { get; set; }
        public string VESSEL { get; set; }
        public string VOYAGENUMBER { get; set; }
        public string POL_CUT_OFF_DATE { get; set; }
        public string DEPARTURE { get; set; }
        public string DEPARTURE_DAY { get; set; }
        public string TARNSIT_TIME { get; set; }
        public string ARRIVAL { get; set; }
        public string ARRIVAL_DAY { get; set; }
        public decimal VESSEL_VOYAGE_TBL_PK { get; set; }
        public decimal VOYAGE_TRN_PK { get; set; }
        public decimal OPERATOR_MST_PK { get; set; }
    }
}