﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MYSTIC.Data;

namespace MYSTIC.Models
{
    public class Contact
    {
        public M_CONTACT_ME_TRN contact { get; set; }
    }
    public class SubjectMaster
    {
        public int M_SUBJECT_MST_PK { get; set; }
        public string SUBJECT_ID { get; set; }
        public string SUBJECT_DESC { get; set; }
    }
}