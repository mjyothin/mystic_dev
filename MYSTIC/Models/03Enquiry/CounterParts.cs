﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MYSTIC.Data;

namespace MYSTIC.Models
{
    public class CounterParts
    {
        public List<SP_FETCH_COUNTER_PARTS_Result> lstCounterParts { get; set; }
    }
    public class CounterPartsSearch
    {
        public int UserPK { get; set; }
        public string Customer_PK { get; set; }
        public string CUSTOMER_ID { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public string ADDRESS { get; set; }
        public string COUNTRY { get; set; }
        public string CITY { get; set; }
        public string LOCATION { get; set; }
        public string CONTACT_PERSON { get; set; }
        public string PHONE { get; set; }
        public string EMAIL { get; set; }
    }
}