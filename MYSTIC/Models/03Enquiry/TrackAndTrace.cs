﻿using MYSTIC.Data;
using System;
using System.Collections.Generic;

namespace MYSTIC.Models
{
    public class TrackAndTrace
    {
        public int UserPK { get; set; }
        public int BusinessType { get; set; }
        public int CargoType { get; set; }
        public int ProcessType { get; set; }
        public string Corporate { get; set; }
        public string ShipmentNumber { get; set; }
        public string ContainerNumber { get; set; }
        public List<SP_TRACK_TRACE_LIST_Result> TrackNTraceList { get; set; }
    }
    public class TrackNTraceDetail
    {
        public decimal JOB_CARD_TRN_PK { get; set; }
        public Nullable<decimal> BUSINESS_TYPE { get; set; }
        public decimal CARGO_TYPE { get; set; }
        public string POLID { get; set; }
        public string POL { get; set; }
        public string PODID { get; set; }
        public string POD { get; set; }
        public string SHIPPER_NAME { get; set; }
        public string CONSIGNEE_NAME { get; set; }
        public string SHIPMENT_NO { get; set; }
        public string SHIPMENT_DATE { get; set; }
        public string VES_VOY_NO { get; set; }
        public string ETA_DATE { get; set; }
        public string ETD_DATE { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public string SHIPPING_LINE { get; set; }
        public string ARRIVAL_DATE { get; set; }
        public string DEPARTURE_DATE { get; set; }
        public List<SP_TRACK_TRACE_DTL_Result> TrackNTraceStatus { get; set; }
    }
}