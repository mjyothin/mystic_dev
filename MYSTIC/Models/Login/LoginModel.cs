﻿using MYSTIC.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace MYSTIC.Models
{
    public class LoginModel
    {
        public UserMaster UserDetails { get; set; }       
        public FETCH_USER_CORP_DTL_Result UserCorporateDetails { get; set; }
        public FETCH_USER_CUST_DTL_Result UserCustomerDetails { get; set; }
        public List<FreightForwarder> UserFFDetails { get; set; }
        public string SuccessMessage { get; set; }
        public bool BookingFeedbackRequired { get; set; }
        public bool FFFeedbackRequired { get; set; }
    }
    public class FreightForwardersDetail
    {
        public int M_CORPORATE_MST_PK { get; set; }
        public string CORPORATE_ID { get; set; }
        public string CORPORATE_NAME { get; set; }
        public Nullable<int> COUNTRY_MST_FK { get; set; }
        public string COUNTRY { get; set; }
        public string CONTACT_PERSON { get; set; }
        public string HOME_PAGE { get; set; }
        public string PHONE { get; set; }
        public string FAX { get; set; }
        public string EMAIL { get; set; }
    }
    public class RegisterUser
    {
        public M_USER_MST_TBL UserDetails { get; set; }
        public List<int> FreightForwarderPK { get; set; } 
    }
    public class UserMaster
    {
        public int M_USER_MST_PK { get; set; }
        public Nullable<int> M_CORPORATE_MST_FK { get; set; }
        public string USER_ID { get; set; }
        public string PASS_WORD { get; set; }
        public Nullable<System.DateTime> PASS_WORD_CHANGE_DT { get; set; }
        public string USER_NAME { get; set; }
        public string FIRST_NAME { get; set; }
        public string LAST_NAME { get; set; }
        public Nullable<byte> USER_TYPE { get; set; }
        public string USER_ADDRESS1 { get; set; }
        public string USER_ADDRESS2 { get; set; }
        public string USER_ADDRESS3 { get; set; }
        public string CITY { get; set; }
        public string ZIP { get; set; }
        public string PHONE_NO { get; set; }
        public string MOBILE_NO { get; set; }
    }
    public class CorporateDetails
    {
        public Nullable<decimal> CORPORATE_MST_PK { get; set; }
        public string CORPORATE_ID { get; set; }
        public string CORPORATE_NAME { get; set; }
        public string ADDRESS_LINE1 { get; set; }
        public string ADDRESS_LINE2 { get; set; }
        public string ADDRESS_LINE3 { get; set; }
        public string CITY { get; set; }
        public Nullable<decimal> COUNTRY_MST_FK { get; set; }
        public string PHONE { get; set; }
        public string EMAIL { get; set; }
        public string COMPANY_REG_NO { get; set; }
        public string GST_NO { get; set; }
        public Nullable<decimal> CURRENCY_MST_FK { get; set; }
        public Nullable<decimal> STATE_MST_FK { get; set; }
        public string POST_CODE { get; set; }
        public Nullable<decimal> ACTIVE_FLAG { get; set; }
        public string ACCOUNT_NO { get; set; }
        public Nullable<decimal> EXCH_RATE_BASIS { get; set; }
        public Nullable<decimal> VAT_PERCENTAGE { get; set; }
        public string A8CURRENCY_MST_FK { get; set; }
        public Nullable<decimal> BIZ_TYPE { get; set; }
    }
    public class LoginCustomerDetails
    {
        public decimal CUSTOMER_MST_PK { get; set; }
        public string CUSTOMER_ID { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public string ADMIN_ADDRESS { get; set; }
        public string ADM_CITY { get; set; }
        public string ADM_ZIP_CODE { get; set; }
        public string ADMIN_COUNTRY { get; set; }
        public string ADM_PHONE_NO_1 { get; set; }
        public string ADM_PHONE_NO_2 { get; set; }
        public string ADM_FAX_NO { get; set; }
        public string ADM_EMAIL_ID { get; set; }
        public string COR_ADDRESS { get; set; }
        public string COR_CITY { get; set; }
        public string COR_ZIP_CODE { get; set; }
        public string COR_COUNTRY { get; set; }
        public string COR_PHONE_NO_1 { get; set; }
        public string COR_PHONE_NO_2 { get; set; }
        public string COR_FAX_NO { get; set; }
        public string COR_EMAIL_ID { get; set; }
        public string BILL_ADDRESS { get; set; }
        public string BILL_CITY { get; set; }
        public string BILL_ZIP_CODE { get; set; }
        public string BILL_COUNTRY { get; set; }
        public string BILL_PHONE_NO_1 { get; set; }
        public string BILL_PHONE_NO_2 { get; set; }
        public string BILL_FAX_NO { get; set; }
        public string BILL_EMAIL_ID { get; set; }
        public string CUST_REG_NO { get; set; }
        public string ACCOUNT_NO { get; set; }
        public string TAXNR { get; set; }
        public string BIZ_TYPE { get; set; }
        public Nullable<decimal> CREDIT_DAYS { get; set; }
        public Nullable<decimal> CREDIT_LIMIT { get; set; }
        public Nullable<decimal> CREDIT_LIMIT_USED { get; set; }
        public Nullable<decimal> CREDIT_BALANCE { get; set; }
        public string BANK_NAME { get; set; }
        public string BANK_ACCOUNT_NO { get; set; }
        public string BANK_ADDRESS { get; set; }
        public string BRANCH { get; set; }
        public string LOC_NAME { get; set; }
        public string COUNTRY { get; set; }
    }
}