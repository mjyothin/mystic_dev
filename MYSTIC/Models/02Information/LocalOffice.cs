﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MYSTIC.Data;

namespace MYSTIC.Models
{
    public class LocalOffice:Common
    {
        public FETCH_LOCATION Location { get; set; }
        public List<LocationWorkingPorts> WorkingPorts { get; set; }        
    }
    public class Location
    {
        public int LOCATION_MST_PK { get; set; }
        public string LOCATION_ID { get; set; }
        public string LOCATION_NAME { get; set; }
    }
    public class LocationWorkingPorts
    {
        public int PORT_MST_FK { get; set; }
        public string PORT_ID { get; set; }
        public string PORT_NAME { get; set; }
    }
}