﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MYSTIC.Data;

namespace MYSTIC.Models
{
    public class PlaceMaster
    {
        public List<Places> PlaceMasters { get; set; }
    }
    public class Places
    {
        public int PLACE_PK { get; set; }
        public string PLACE_CODE { get; set; }
        public string PLACE_NAME { get; set; }
    }
}