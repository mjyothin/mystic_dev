﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MYSTIC.Data;

namespace MYSTIC.Models
{
    public class ContainerType
    {
        public FETCH_CONTAINER_TYPE ContainerTypes { get; set; }
        public List<SP_FETCH_CONTAINER_MST_Result> ContainerMaster { get; set; }
    }
}