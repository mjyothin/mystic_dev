﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MYSTIC.Data;

namespace MYSTIC.Models
{
    public class Commodity
    {
        public List<FETCH_COMMODITY> CommodityDetails { get; set; }
        public List<SP_FETCH_COMMODITY_MST_Result> CommodityMaster { get; set; }
    }
}