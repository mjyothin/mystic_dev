﻿using MYSTIC.Data;
using System;
using System.Collections.Generic;

namespace MYSTIC.Models
{
    public class AirFreightSlab
    {
        public List<AirFreightSlabList> AirFreightDetails { get; set; }
    }
    public class AirFreightSlabList
    {
        public int AIRFREIGHT_SLABS_TBL_PK { get; set; }
        public string BREAKPOINT_ID { get; set; }
        public Nullable<decimal> BREAKPOINT_RANGE { get; set; }
        public string BREAKPOINT_DESC { get; set; }
    }

}