﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MYSTIC.Data;

namespace MYSTIC.Models
{
    public class CargoMoveCode
    {
        public List<SP_FETCH_MOVECODE_MST_Result> CargoMoveCodeList { get; set; }
    }
}