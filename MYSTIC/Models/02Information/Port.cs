﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MYSTIC.Data;

namespace MYSTIC.Models
{
    public class Port
    {
        public List<Ports> Ports{ get; set; }
    }
    public class Ports
    {
        public decimal PORT_MST_PK { get; set; }
        public string PORT_ID { get; set; }
        public string PORT_NAME { get; set; }
        public string BIZ_TYPE { get; set; }
    }
}