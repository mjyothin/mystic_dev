﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MYSTIC.Data;

namespace MYSTIC.Models
{
    public class PackType
    {
        //public List<FETCH_PACKTYPE> PackTypes { get; set; }
        public List<PackTypes> PackTypeList { get; set; }
    }
    public class PackTypes
    {
        public decimal PACK_TYPE_MST_PK { get; set; }
        public string PACK_TYPE_ID { get; set; }
        public string PACK_TYPE_DESC { get; set; }
        public string UOM { get; set; }
    }
}