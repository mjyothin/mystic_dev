﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MYSTIC.Data;

namespace MYSTIC.Models
{
    public class SalesCallMaster
    {
        public SalesCallType CallType { get; set; }
        public SalesCallReason CallReason { get; set; }
    }
    public class SalesCallType
    {
            public int SALES_CALL_TYPE_PK { get; set; }
            public string SALES_CALL_TYPE_ID { get; set; }
            public string SALES_CALL_TYPE_DESC { get; set; }
            public bool ACTIVE_FLAG { get; set; }
    }
    public class SalesCallReason
    {
        public int SAL_CAL_REASON_MST_TBL_PK { get; set; }
        public string SAL_CAL_ID { get; set; }
        public string SAL_CAL_REASON { get; set; }
    }
}