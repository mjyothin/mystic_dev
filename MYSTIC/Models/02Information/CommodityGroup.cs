﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MYSTIC.Data;

namespace MYSTIC.Models
{
    public class CommodityGroup
    {
        public List<FETCH_COMMODITY_GROUP> CommodityGroupDetails { get; set; }
        public List<SP_FETCH_COMMODITY_GRP_MST_Result> CommodityGroupList { get; set; }
    }
}