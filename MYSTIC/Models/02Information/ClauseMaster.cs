﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MYSTIC.Data;

namespace MYSTIC.Models
{
    public class ClauseMaster
    {
      public List<ListClause> Clauses { get; set; }
    }
    public class ClauseDetails
    {
        public int BL_CLAUSE_PK { get; set; }
        public string REFERENCE_NR { get; set; }
        public string BL_DESCRIPTION { get; set; }
        public string BIZ_TYPE { get; set; }
        public string CLAUSE_TYPE { get; set; }

        public static implicit operator ClauseDetails(ClauseMaster v)
        {
            throw new NotImplementedException();
        }
    }
    public class ClauseCommodity
    {
        public int COMMODITY_MST_PK { get; set; }
        public string COMMODITY_ID { get; set; }
        public string COMMODITY_NAME { get; set; }
    }
    public class ClausePort
    {
        public int PORT_MST_PK { get; set; }
        public string PORT_ID { get; set; }
        public string PORT_NAME { get; set; }
    }
    public class ListClause
    {
        public ClauseDetails Clause { get; set; }
        public List<ClauseCommodity> ClauseCommodity { get; set; }
        public List<ClausePort> ClausePort { get; set; }
    }
}