﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MYSTIC.Data;

namespace MYSTIC.Models
{
    public class ExchangeRate
    {
        public List<ListExchangeRate> ExchangeRates { get; set; }
    }
    public class ListExchangeRate
    {
        public int EXCHANGE_RATE_PK { get; set; }
        public string FROM_CURR_ID { get; set; }
        public string FROM_CURR_NAME { get; set; }
        public string TO_CURR_ID { get; set; }
        public string TO_CURR_NAME { get; set; }
        public decimal EXCHANGE_RATE { get; set; }
        public string EXRATE_TYPE { get; set; }
        public Nullable<decimal> ROE_BUY { get; set; }
    }
}