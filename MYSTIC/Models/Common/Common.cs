﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MYSTIC.Data;

namespace MYSTIC.Models
{
    public class Common
    {
        
    }
    public class DropdownClass
    {
        public int ID { get; set; }
        public string Text { get; set; }
        public string Value { get; set; }
    }
    public class CustomerDetails
    {
        public List<SP_GET_CUSTOMERS_Result> CutomerList { get; set; }
    }
    public class Basis
    {
        public List<Dimension> Dimensions { get; set; }
    }
    public class Dimension
    {
        public int DIMENTION_UNIT_MST_PK { get; set; }
        public int DIMENTION_TYPE { get; set; }
        public string DIMENTION_ID { get; set; }
    }
    public class FreightForwarder
    {
        public int? FREIGHT_FORWARDER_PK { get; set; }
        public string FREIGHT_FORWARDER { get; set; }
    }
}