﻿using MYSTIC.ControllerLogic;
using MYSTIC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MYSTIC.Controllers
{
    public class SalesCallMasterController : ApiController
    {
        SalesCallMasterLogic salesCallMasterLogic = new SalesCallMasterLogic();
        [HttpGet]
        public IHttpActionResult FetchSalesCallType()
        {
            List<SalesCallType> lstSalesCallType = salesCallMasterLogic.FetchSalesCallType();
            return Ok(lstSalesCallType);
        }
        [HttpGet]
        public IHttpActionResult FetchSalesCallReason()
        {
            List<SalesCallReason> lstSalesCallReason = salesCallMasterLogic.FetchSalesCallReason();
            return Ok(lstSalesCallReason);
        }
    }
}
