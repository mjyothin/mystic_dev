﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MYSTIC.Models;
using MYSTIC.ControllerLogic;

namespace MYSTIC.Controllers
{
    public class LocalOfficeController : ApiController
    {
        LocalOfficeLogic localOfficeLogic = new LocalOfficeLogic();
        [HttpGet,HttpPost]
        public IHttpActionResult FetchLocalOffice(int LocationPK)
        {
            LocalOffice localOffice = new LocalOffice();
            localOffice = localOfficeLogic.FetchLocalOfficeDetails(LocationPK);
            return Ok(localOffice);
        }
        [HttpGet]
        public IHttpActionResult FetchLocation()
        {
            List<Location> location = new List<Location>();
            location = localOfficeLogic.FetchLocalOfficeMaster();
            return Ok(location);
        }
    }
}
