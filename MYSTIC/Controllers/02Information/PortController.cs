﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MYSTIC.Models;
using MYSTIC.ControllerLogic;

namespace MYSTIC.Controllers
{
    public class PortController : ApiController
    {
        PortLogic portLogic = new PortLogic();
        [HttpGet,HttpPost]
        public IHttpActionResult FetchPort(string PortID=null,string BusinessType=null, string DBName=null)
        {
            Port port = new  Port();
            port = portLogic.FetchPort(PortID,DBName,BusinessType);
            return Ok(port);
        }
    }
}
