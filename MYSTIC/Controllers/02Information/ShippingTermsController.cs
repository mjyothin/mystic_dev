﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MYSTIC.Models;
using MYSTIC.ControllerLogic;

namespace MYSTIC.Controllers
{
    public class ShippingTermsController : ApiController
    {
        ShippingTermsLogic shippingTermsLogic = new ShippingTermsLogic();
        [HttpGet,HttpPost]
        public IHttpActionResult FetchShippingTerms(string Terms=null)
        {
            ShippingTerms shippingTerms = new  ShippingTerms();
            shippingTerms = shippingTermsLogic.FetchShippingTerms(Terms);
            return Ok(shippingTerms);
        }
    }
}
