﻿using MYSTIC.ControllerLogic;
using MYSTIC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MYSTIC.Controllers
{
    public class ClauseMasterController : ApiController
    {
        ClauseMasterLogic clauseMasterLogic = new ClauseMasterLogic();
        [HttpGet,HttpPost]
        public IHttpActionResult FetchClauseMaster(string ClauseType=null)
        {
            ClauseMaster clauseMaster = clauseMasterLogic.FetchClauseMaster(ClauseType);
            return Ok(clauseMaster);
        }
        [HttpGet]
        public IHttpActionResult FetchClauseType()
        {
            List<string> clauseList = clauseMasterLogic.FetchClauseType();
            return Ok(clauseList);
        }
    }
}
