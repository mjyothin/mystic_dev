﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MYSTIC.ControllerLogic;
using MYSTIC.Models;

namespace MYSTIC.Controllers
{
    public class ExchangeRateController : ApiController
    {
        ExchangeRateLogic exchangeRateLogic = new ExchangeRateLogic();
        [HttpGet]
        public IHttpActionResult FetchExchangeRate(string ExchangeRateType=null)
        {
            ExchangeRate exchangeRate = new ExchangeRate();
            exchangeRate = exchangeRateLogic.FetchExchangeRate(ExchangeRateType);
            return Ok(exchangeRate);
        }
        [HttpGet]
        public IHttpActionResult FetchExchangeRateType()
        {
            List<string> exchangeRatetype = new List<string>();
            exchangeRatetype = exchangeRateLogic.FetchExchangeRateType();
            return Ok(exchangeRatetype);
        }
    }
}
