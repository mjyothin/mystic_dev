﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MYSTIC.ControllerLogic;
using MYSTIC.Models;

namespace MYSTIC.Controllers
{
    public class CargoMoveCodeController : ApiController
    {
        CargoMoveCodeLogic moveCodeLogic = new CargoMoveCodeLogic();
        [HttpGet]
        public IHttpActionResult FetchCargoMoveCode(string CargoMoveCodeID,string DB_Name=null)
        {
            CargoMoveCode cargoMoveCode = new CargoMoveCode();
            cargoMoveCode = moveCodeLogic.FetchCargoMoveCode(CargoMoveCodeID,DB_Name);
            return Ok(cargoMoveCode.CargoMoveCodeList);
        }
    }
}
