﻿using MYSTIC.ControllerLogic;
using MYSTIC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MYSTIC.Controllers._02Information
{
    public class CommodityGroupController : ApiController
    {
        CommodityGroupLogic commodityGroupLogic = new CommodityGroupLogic();
        [HttpGet, HttpPost]
        public IHttpActionResult FetchCommodityGroup( string CommodityGroup=null, string DB_Name = null)
        {
            CommodityGroup commodityGroup = new CommodityGroup();
            // commodityGroup = commodityGroupLogic.FetchCommodityGroupDetails(CommodityGroup);
            commodityGroup = commodityGroupLogic.FetchCommodityGroupMaster(CommodityGroup,DB_Name);
            return Ok(commodityGroup.CommodityGroupList);
        }
    }
}
