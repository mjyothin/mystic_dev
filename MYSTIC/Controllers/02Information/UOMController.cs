﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MYSTIC.Models;
using MYSTIC.ControllerLogic;

namespace MYSTIC.Controllers
{
    public class UOmController : ApiController
    {
        UOMLogic uOMLogic = new  UOMLogic();
        [HttpGet,HttpPost]
        public IHttpActionResult FetchUOM()
        {
            UOM uom = new UOM();
            uom = uOMLogic.FetchUOM();
            return Ok(uom);
        }
    }
}
