﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MYSTIC.Models;
using MYSTIC.ControllerLogic;

namespace MYSTIC.Controllers
{
    public class LocalChargesController : ApiController
    {
        LocalChargeLogic localChargeLogic = new LocalChargeLogic();
        [HttpGet,HttpPost]
        public IHttpActionResult FetchLocalCharges()
        {
            LocalCharges localCharges = new LocalCharges();
            localCharges = localChargeLogic.FetchLocalCharges();
            return Ok(localCharges);
        }
    }
}
