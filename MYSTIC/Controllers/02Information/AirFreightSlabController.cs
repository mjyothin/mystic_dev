﻿using MYSTIC.ControllerLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MYSTIC.Models;

namespace MYSTIC.Controllers
{
    public class AirFreightSlabController : ApiController
    {
        AirFreightSlabLogic airFreightSlabLogic = new AirFreightSlabLogic();
        [HttpGet]
        [HttpPost]
        public IHttpActionResult FetchAirFreightSlab(string SearchParameter=null)
        {
            AirFreightSlab airFreightSlab = new AirFreightSlab();
            airFreightSlab = airFreightSlabLogic.FetchAirFreightSlab(SearchParameter);
            return Ok(airFreightSlab.AirFreightDetails);
        }
    }
}
