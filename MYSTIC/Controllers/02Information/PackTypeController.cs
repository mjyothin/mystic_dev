﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MYSTIC.Models;
using MYSTIC.ControllerLogic;

namespace MYSTIC.Controllers
{
    public class PackTypeController : ApiController
    {
        PackTypeLogic packTypeLogic = new PackTypeLogic();
        [HttpGet,HttpPost]
        public IHttpActionResult FetchPackTypes(string PackTypeID=null,string DB_Name = null)
        {
            PackType packType = new PackType();
            //packType = packTypeLogic.FetchPackType();
            packType = packTypeLogic.FetchPackTypeMaster(PackTypeID, DB_Name);
            return Ok(packType.PackTypeList);
        }
    }
}
