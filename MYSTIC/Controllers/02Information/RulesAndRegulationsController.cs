﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MYSTIC.ControllerLogic;
using MYSTIC.Data;

namespace MYSTIC.Controllers
{
    public class RulesAndRegulationsController : ApiController
    {
        RulesAndRegulationsLogic regulationsLogic = new RulesAndRegulationsLogic();
        [HttpGet]
        public IHttpActionResult FetchRulesAndRegulations(string RulesID=null,string RulesDesc=null,string Country=null)
        {
            List<M_RULES_REGULATIONS> lstRules = regulationsLogic.FetchRulesAndRegulations(RulesID, RulesDesc, Country);
            return Ok(lstRules);
        }
    }
}
