﻿using MYSTIC.ControllerLogic;
using MYSTIC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MYSTIC.Controllers
{
    public class CommodityController : ApiController
    {
        CommodityLogic commodityLogic = new CommodityLogic();
        [HttpGet,HttpPost]
        public IHttpActionResult FetchCommodity(string CommodityID=null,string CommodityGroup=null,string DB_Name=null)
        {
            Commodity commodity = new Commodity();
            // commodity = commodityLogic.FetchCommodityDetails(CommodityID, CommodityGroup);
            commodity = commodityLogic.FetchCommodityMaster(CommodityID, CommodityGroup,DB_Name);
            return Ok(commodity);
        }
    }
}
