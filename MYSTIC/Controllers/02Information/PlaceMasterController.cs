﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MYSTIC.Models;
using MYSTIC.ControllerLogic;

namespace MYSTIC.Controllers
{
    public class PlaceMasterController : ApiController
    {
        PlaceMasterLogic placeMasterLogic = new PlaceMasterLogic();
        [HttpGet,HttpPost]
        public IHttpActionResult FetchPlaceMaster(string Place=null)
        {
            PlaceMaster placeMaster = new PlaceMaster();
            placeMaster = placeMasterLogic.FetchPlaceMaster(Place);
            return Ok(placeMaster);
        }
    }
}
