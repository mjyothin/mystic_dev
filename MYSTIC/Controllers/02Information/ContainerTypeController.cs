﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MYSTIC.Models;
using MYSTIC.ControllerLogic;

namespace MYSTIC.Controllers
{
    public class ContainerTypeController : ApiController
    {
        ContainerTypeLogic containerTypeLogic = new ContainerTypeLogic();
        [HttpGet, HttpPost]
        public IHttpActionResult FetchContainerType(string ContainerTypeID=null, string DB_Name = null)
        {
            ContainerType containerType = new ContainerType();
           // containerType= containerTypeLogic.FetchContainerType(ContainerTypeID);
            containerType = containerTypeLogic.FetchContainerMaster(ContainerTypeID,DB_Name);
            return Ok(containerType.ContainerMaster);
        }
    }
}
