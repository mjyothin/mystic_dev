﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MYSTIC.Models;
using MYSTIC.ControllerLogic;

namespace MYSTIC.Controllers
{
    public class OutstandingReportController : ApiController
    {
        OutstandingReportLogic ReportLogic = new OutstandingReportLogic();
        [HttpGet]
        public IHttpActionResult FetchOutstandingReport([FromUri]OutstandingReport outstandingReport)
        {
            outstandingReport = ReportLogic.FetchOutstandingReport(outstandingReport);
            return Ok(outstandingReport.OutstandingResult);
        }
    }
}
