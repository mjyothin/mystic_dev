﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MYSTIC.ControllerLogic;
using MYSTIC.Models;

namespace MYSTIC.Controllers
{   
    public class CustomsStatusController : ApiController
    {
        CustomsStatusLogic customsStatusLogic = new CustomsStatusLogic();
        [HttpGet]
        public IHttpActionResult FetchCustomsStatus([FromUri]CustomsStatus customsStatus)
        {
            customsStatus = customsStatusLogic.FetchCustomsStatus(customsStatus);
            return Ok(customsStatus.LstCustomsStatus);
        }
        [HttpGet]
        public IHttpActionResult FetchVendors([FromUri]CustomsStatus customsStatus)
        {
            List<VendorList> vendors = customsStatusLogic.FetchVendorsDD(customsStatus);
            return Ok(vendors);
        }
        [HttpGet]
        public IHttpActionResult FetchCustomers([FromUri]CustomsStatus customsStatus)
        {
            List<CustomerList> customers = customsStatusLogic.FetchCustomersDD(customsStatus);
            return Ok(customers);
        }
        [HttpGet]
        public IHttpActionResult FetchJobCards([FromUri]CustomsStatus customsStatus)
        {
            List<JobCardList> jobCards = customsStatusLogic.FetchJobCardsDD(customsStatus);
            return Ok(jobCards);
        }
        [HttpGet]
        public IHttpActionResult FetchVesVoy([FromUri]CustomsStatus customsStatus)
        {
            List<VesVoyList> vesVoys = customsStatusLogic.FetchVesVoyDD(customsStatus);
            return Ok(vesVoys);
        }
    }
}
