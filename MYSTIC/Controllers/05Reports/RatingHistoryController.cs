﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MYSTIC.Models;
using MYSTIC.ControllerLogic;

namespace MYSTIC.Controllers
{
    public class RatingHistoryController : ApiController
    {
        RatingHistoryLogic ratingHistoryLogic = new RatingHistoryLogic();
        [HttpGet]
        public IHttpActionResult FetchRatingHistory([FromUri]RatingHistory ratingHistory)
        {
            ratingHistory = ratingHistoryLogic.FetchRatingHistory(ratingHistory);
            return Ok(ratingHistory.RptRateHistory);
        }
    }
}
