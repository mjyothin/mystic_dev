﻿using MYSTIC.ControllerLogic;
using MYSTIC.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MYSTIC.Controllers
{
    public class CustomerSOAController : ApiController
    {
        CustomerSOALogic customerSOALogic = new CustomerSOALogic();
        [HttpGet]
        public IHttpActionResult FetchCustomerSOA([FromUri]CustomerSOASearch search)
        {
            CustomerSOA ds = customerSOALogic.FetchCustomerSOA(search.UserPK, search.FreightForwarder, search.CustPK, search.Fromdate, search.Todate,
                search.POLPK, search.POLPK, search.CountryPK, search.LocPK,  search.BizType, search.ProcessType, search.CustGroupPK,
                search.CarrierPK, search.VslVoyPK, search.FlightID, search.CargoType, search.CurrPK);
            return Ok(ds);
        }

    }
}
