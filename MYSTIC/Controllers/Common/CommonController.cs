﻿using MYSTIC.ControllerLogic;
using MYSTIC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MYSTIC.Controllers
{
    public class CommonController : ApiController
    {
        CommonLogic commonLogic = new CommonLogic();
        [HttpGet]
        public IHttpActionResult GetCustomerList(int UserPK)
        {
            CustomerDetails customer = new CustomerDetails();
            customer = commonLogic.GetCustomerList(UserPK);
            return Ok(customer);
        }
        [HttpGet]
        public IHttpActionResult GetBasisList()
        {
            Basis basis = new Basis();
            basis = commonLogic.GetBasisList();
            return Ok(basis);
        }
        [HttpGet]
        public IHttpActionResult GetFreightForwarderForUser(int UserPK)
        {
            List<FreightForwarder> freightForwarders = new List<FreightForwarder>();
            freightForwarders = commonLogic.Get_FreightForwarder(UserPK);
            return Ok(freightForwarders);
        }
    }
}
