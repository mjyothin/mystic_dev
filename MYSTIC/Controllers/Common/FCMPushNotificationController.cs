﻿using MYSTIC.ControllerLogic;
using MYSTIC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MYSTIC.Controllers
{
    public class FCMPushNotificationController : ApiController
    {
        FCMPushNotificationLogic notificationLogic = new FCMPushNotificationLogic();
        [HttpGet]
        public IHttpActionResult SendNotification(string DeviceID, string title="Insight", string message="This is Test Notification", string topic="Insight")
        {
           FCMPushNotification fCM= notificationLogic.SendNotification(DeviceID,title, message, topic);
            return Ok(fCM);
        }
    }
}
