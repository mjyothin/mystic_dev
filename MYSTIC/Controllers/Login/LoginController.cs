﻿using MYSTIC.ControllerLogic;
using MYSTIC.Data;
using MYSTIC.Models;
using System.Collections.Generic;
using System.Web.Http;

namespace FUTURA
{

    public class LoginController : ApiController
    {
        private LoginLogic loginLogic = new LoginLogic();
        [HttpGet]
        [Route("api/Login/Login")]
        public IHttpActionResult Login([FromUri]M_USER_MST_TBL UserMaster)
        {
            LoginModel loginModel = new LoginModel();
            loginModel = loginLogic.IsUserValid(UserMaster.USER_ID, UserMaster.PASS_WORD);
            if (loginModel.SuccessMessage == null)
            {
                return Ok(loginModel);
            }
            else
            {
                return BadRequest(loginModel.SuccessMessage);
            }
        }
        [HttpPost]
        [Route("api/Login/RegisterUser")]
        public IHttpActionResult RegisterUser(RegisterUser registerUser)
        {
            string Message = loginLogic.RegisterNewUser(registerUser);
            return Ok(Message);

        }
        [HttpGet]
        public IHttpActionResult FetchAllFreightForwarders()
        {
            List<FreightForwardersDetail> FF = loginLogic.FetchAllFreightForwarders();
            return Ok(FF);
        }
        public IHttpActionResult FetchAllCustomers(List<string> CorporateName)
        {
            List<SP_FETCH_CUSTOMERS_Result> Customers = loginLogic.FetchAllCustomers(CorporateName);
            return Ok(Customers);
        }

    }

}
