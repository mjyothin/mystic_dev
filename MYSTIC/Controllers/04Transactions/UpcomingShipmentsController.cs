﻿using MYSTIC.ControllerLogic;
using MYSTIC.Models;
using System.Web.Http;

namespace MYSTIC.Controllers
{
    public class UpcomingShipmentsController : ApiController
    {
        private UpcomingShipmentsLogic upcomingShipmentsLogic = new UpcomingShipmentsLogic();
        [Route("api/UpcomingShipments/GetShipmentDropdownDetails")]
        [HttpGet]
        public IHttpActionResult GetShipmentDropdownDetails([FromUri] UpcomingShipments upcomingShipments)
        {
            upcomingShipments.Shipments = upcomingShipmentsLogic.FetchShipments(upcomingShipments);
            return Ok(upcomingShipments.Shipments);
        }
        [Route("api/UpcomingShipments/FetchShipments")]
        [HttpGet]
        public IHttpActionResult FetchShipments([FromUri]UpcomingShipments upcomingShipments)
        {
            upcomingShipments.Shipments = upcomingShipmentsLogic.FetchShipments(upcomingShipments);
            return Ok(upcomingShipments.Shipments);
        }
        [Route("api/UpcomingShipments/FetchShipmentDetails")]
        [HttpGet]
        public IHttpActionResult FetchShipmentDetails([FromUri]UpcomingShipments upcomingShipments)
        {
            ShipmentDetails shipmentDetails = upcomingShipmentsLogic.FetchShipmentDetails(upcomingShipments);
            return Ok(shipmentDetails);
        }
        
    }
}
