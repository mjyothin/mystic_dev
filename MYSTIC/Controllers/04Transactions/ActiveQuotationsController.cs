﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MYSTIC.Models;
using MYSTIC.ControllerLogic;
using MYSTIC.Data;

namespace MYSTIC.Controllers
{
    public class ActiveQuotationsController : ApiController
    {
        ActiveQuotationsLogic activeQuotationsLogic = new ActiveQuotationsLogic();
        [HttpGet,HttpPost]
        public IHttpActionResult GetDropDownForQuote([FromUri]ActiveQuotations activeQuotations)
        {
            List<ActiveQuotations> returnData = new List<ActiveQuotations>();
           returnData = activeQuotationsLogic.GetDropdownForQuotation(activeQuotations);
            return Ok(returnData);
        }
        [HttpGet, HttpPost]
        [Route("api/ActiveQuotations/FetchQuotations")]
        public IHttpActionResult FetchQuotations([FromUri]ActiveQuotations activeQuotations)
        {
            List<ActiveQuotations> returnData = new List<ActiveQuotations>();
            returnData = activeQuotationsLogic.FetchQuotations(activeQuotations);
            return Ok(returnData);
        }
        [HttpGet, HttpPost]
        [Route("api/ActiveQuotations/FetchQuotationDetails")]
        public IHttpActionResult FetchQuotationDetails(int UserPK,string QUOTATION_REF_NO)
        {
            ActiveQuotationDetails returnData = new ActiveQuotationDetails();
            returnData = activeQuotationsLogic.FetchQuotationsDetails(UserPK,QUOTATION_REF_NO);
            return Ok(returnData);
        }
       
    }
}
