﻿using MYSTIC.ControllerLogic;
using MYSTIC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MYSTIC.Controllers
{
    public class CollectionController : ApiController
    {
        CollectionLogic collectionLogic = new CollectionLogic();
        [HttpGet]
        public IHttpActionResult FetchCollections([FromUri]Collections collection)
        {
            collection = collectionLogic.FetchCollection(collection);
            return Ok(collection.LstCollection);
        }
    }
}
