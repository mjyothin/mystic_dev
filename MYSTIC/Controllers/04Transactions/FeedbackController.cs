﻿using MYSTIC.ControllerLogic;
using MYSTIC.Data;
using MYSTIC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MYSTIC.Controllers
{
    public class FeedbackController : ApiController
    {
        FeedbackLogic feedbackLogic = new FeedbackLogic();
        [HttpGet]
        public IHttpActionResult IsFeedBackRequiredForFF(int UserPK,int UserType=1)
        {
            FreightForwarder isRequired = UserType==2? feedbackLogic.IsFeedBackRequiredForFF(UserPK):null;
            return Ok(isRequired);
        }
        [HttpGet]
        public IHttpActionResult BookingFeedbackRequired(int UserPK, int UserType = 1)
        {
            BookingFeedback bookingDetails = UserType == 2 ? feedbackLogic.BookingFeedbackRequired(UserPK):new BookingFeedback();
            return Ok(bookingDetails);
        }
        [HttpGet]
        public IHttpActionResult RatingReasonMaster()
        {
            List<RATING_REASON_MST_TBL> ratingReason = feedbackLogic.RatingReasonMaster();
            return Ok(ratingReason);
        }
        [HttpPost]
        public IHttpActionResult SaveBookingRating(RATE_BOOKING_TRN_TBL RateBooking)
        {
            string save = feedbackLogic.SaveBookingRating(RateBooking);
            return Ok(save);
        }
        [HttpPost]
        public IHttpActionResult SaveFFRating(RATE_FF_TRN_TBL RateFF)
        {
            string save = feedbackLogic.SaveFFRating(RateFF);
            return Ok(save);
        }
    }
}
