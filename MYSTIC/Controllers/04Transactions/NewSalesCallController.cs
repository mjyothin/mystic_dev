﻿using MYSTIC.ControllerLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MYSTIC.Data;

namespace MYSTIC.Controllers
{
    public class NewSalesCallController : ApiController
    {
        NewSalesCallLogic salesCallLogic = new NewSalesCallLogic();
        [HttpPost]
        public IHttpActionResult SaveSalesCall(SALES_CALL_TRN SalesCall)
        {
            string message= salesCallLogic.SaveSalesCall(SalesCall);
            return Ok(message);
        }
        [HttpGet]
        public IHttpActionResult FetchSalesCall(int UserPK)
        {
            List<SP_FETCH_SALES_CALL_Result> salescall= salesCallLogic.FetchSalesCall(UserPK);
            return Ok(salescall);
        }
        [HttpGet]
        public IHttpActionResult FetchSalesCallDetails(int UserPK,string FreightForwarder,string SalesCallID)
        {
            SP_FETCH_SALESCALL_DTL_Result salescallDtl = salesCallLogic.FetchSalesCallDetails(UserPK,FreightForwarder,SalesCallID);
            return Ok(salescallDtl);
        }
    }
}
