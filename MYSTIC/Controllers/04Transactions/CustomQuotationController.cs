﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MYSTIC.Models;
using MYSTIC.ControllerLogic;

namespace MYSTIC.Controllers
{
    public class CustomQuotationController : ApiController
    {
        CustomQuotationLogic customQuotationLogic = new CustomQuotationLogic();
        [HttpGet]
        public IHttpActionResult FetchCustomQuotations()
        {
            CustomQuotation customQuotation = customQuotationLogic.FetchCustomQuotation();
            return Ok(customQuotation.lstCustomQuotation);
        }
        [HttpGet]
        public IHttpActionResult FetchCustomQuotationsDetails(string QuotationNo)
        {
            CustomQuotation customQuotation = new CustomQuotation();
            customQuotation.CustomQuoteDetails= customQuotationLogic.FetchCustomQuotationDetails(QuotationNo);
            customQuotation.CustomQuoteContDetails = customQuotationLogic.FetchCustomQuoteContainerDetails(QuotationNo);
            return Ok(customQuotation);
        }
    }
}
