﻿using MYSTIC.ControllerLogic;
using MYSTIC.Data;
using MYSTIC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MYSTIC.Controllers
{
    public class NewBookingController : ApiController
    {
        NewBookingLogic newBookingLogic = new NewBookingLogic();
        public IHttpActionResult SaveBooking(NewBooking booking)
        {
            string Message = newBookingLogic.SaveBooking(booking);
            return Ok(Message);
        }
        [HttpGet]
        public IHttpActionResult FetchCarrierMaster(int UserPK,string FF,int BusinessType,DateTime ShipmentDate,int CarrierPK=0)
        {
            NewBooking booking = newBookingLogic.FetchCarrierMaster(ShipmentDate, BusinessType, FF,UserPK);
            if(BusinessType==1)
            {
                if (CarrierPK == 0)
                    return Ok(booking.AirCarrierDetails);
                else
                    return Ok(booking.AirCarrierDetails.Where(A => A.CARRIER_MST_PK == CarrierPK).ToList());
            }
            else
            {
                if (CarrierPK == 0)
                    return Ok(booking.SeaCarrierDetails);
                else
                    return Ok(booking.SeaCarrierDetails.Where(a => a.CARRIER_MST_PK == CarrierPK).ToList());
            }
        }
        [HttpPost]
        public IHttpActionResult FetchFreightDetails(NewBooking booking)
        {
             booking.BookingDetails = newBookingLogic.FetchFreightDetails(booking);
                return Ok(booking.BookingDetails);
            
        }
        [HttpGet]
        public IHttpActionResult FetchShipperOrConsigneeDetails(string FF, int UserPK, int UserType, int CustomerPK, bool isShipper = false)
        {
            NewBooking booking = new NewBooking();
            booking = newBookingLogic.FetchShipperOrConsignee(FF,UserPK,UserType,CustomerPK,isShipper);
            if (isShipper == true)
                return Ok(booking.ShipperList);
            else
                return Ok(booking.ConsigneeList);
        }
        [HttpGet]
        public IHttpActionResult FetchBooking(int UserPK)
        {
            List<BookingList> bookings = new List<BookingList>();
            bookings = newBookingLogic.FetchBookings(UserPK);
            return Ok(bookings);

        }
        [HttpGet]
        public IHttpActionResult FetchBookingDetails(int UserPK,string CorporateName,string BookingNo)
        {
            ListBooking bookingDetails = new ListBooking();
            bookingDetails = newBookingLogic.FetchBookingDetails(BookingNo,UserPK,CorporateName);
            return Ok(bookingDetails);

        }
    }
}
