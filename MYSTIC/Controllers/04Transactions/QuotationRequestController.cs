﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MYSTIC.Models;
using MYSTIC.ControllerLogic;

namespace MYSTIC.Controllers
{
    public class QuotationRequestController : ApiController
    {
        QuotationRequestLogic RequestLogic = new QuotationRequestLogic();
        [HttpPost]
        public IHttpActionResult SaveQuotation(QuotationRequest quotation)
        {
            QuotationResponse response = new QuotationResponse();
            string CommonReferenceNumber = null;
            if (quotation.Quotation.COM_REFERENCE_NO==null) {
              CommonReferenceNumber=  RequestLogic.GetCommonReferenceNumber();
            }
            else
            {
                CommonReferenceNumber = quotation.Quotation.COM_REFERENCE_NO;
                quotation.FreightForwarder = new List<FreightForwarder>() { new FreightForwarder() { FREIGHT_FORWARDER = quotation.Quotation.FREIGHT_FORWARDER, FREIGHT_FORWARDER_PK = quotation.Quotation.FREIGHT_FORWARDER_PK } };
            }
            response.RateRequestNumber =  CommonReferenceNumber;
            response.quotations = new List<QuotationResponseDtl>();
            foreach (FreightForwarder FreightForwarder in quotation.FreightForwarder)
            {      
                QuotationResponseDtl qdt= RequestLogic.SaveQuotation(quotation.Quotation, quotation.QuotationDetails, CommonReferenceNumber, FreightForwarder, quotation.UserPK, quotation.Status);               
                response.quotations.Add(qdt);
            }
            return Ok(response);
        }
        [HttpPost]
        [Route("api/QuotationRequest/GetFreightForwarders")]
        public IHttpActionResult GetFreightForwarders(QuotationRequest quotation)
        {
            List<FreightForwarder> FreightForwarders = RequestLogic.CheckFreightForwarders(quotation);
            return Ok(FreightForwarders);
        }
        [HttpGet]
        [Route("api/QuotationRequest/FetchQuotations")]
        public IHttpActionResult FetchQuotations(int UserPK)
        {
            List<Quotation_Master> Quotations = RequestLogic.FetchQuotations(UserPK);
            return Ok(Quotations);
        }
        [HttpGet]
        public IHttpActionResult FetchQuotationDetails(int UserPK, int QUOTATION_MST_PK)
        {
            List<Quotation_List> Quotations = RequestLogic.FetchQuotationsDetails(UserPK, QUOTATION_MST_PK);
            return Ok(Quotations);
        }

    }
}
