﻿using MYSTIC.ControllerLogic;
using MYSTIC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MYSTIC.Controllers
{
    public class InvoiceController : ApiController
    {
        InvoiceLogic InvoiceLogic = new InvoiceLogic();
        [HttpGet]
        public IHttpActionResult FetchInvoice([FromUri] SearchInvoice search)
        {
            Invoice invoice = new Invoice();
            invoice = InvoiceLogic.FetchInvoice(search);
            return Ok(invoice.Invoices);
        }
    }
}
