﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MYSTIC.Models;
using MYSTIC.ControllerLogic;

namespace MYSTIC.Controllers
{
    public class AnnouncementsController : ApiController
    {
        Announcements announcements = new Announcements();
        AnnouncementsLogic announcementsLogic = new AnnouncementsLogic();
        [HttpGet]
        public IHttpActionResult FetchAnnouncements(int UserPK)
        {
            announcements = announcementsLogic.FetchAnnouncements(UserPK);
            return Ok(announcements.LstAnnouncement);
        }
        [HttpGet]
        public IHttpActionResult FetchAnnouncementDetails(string AnnouncementID)
        {
            announcements = announcementsLogic.FetchAnnouncementDetails(AnnouncementID);
            return Ok(announcements);
        }
    }
}
