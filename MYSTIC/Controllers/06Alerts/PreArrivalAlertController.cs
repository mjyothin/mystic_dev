﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MYSTIC.ControllerLogic;
using MYSTIC.Models;

namespace MYSTIC.Controllers
{
    public class PreArrivalAlertController : ApiController
    {
        PreArrivalAlertLogic preArrivalAlertLogic = new PreArrivalAlertLogic();
        [HttpGet]
        public IHttpActionResult FetchPreArrivalAlert(int UserPK,string HBLNr,int ProcessType,string FF)
        {
            PreArrivalAlert preArrivalAlert = new PreArrivalAlert();
            preArrivalAlert = preArrivalAlertLogic.PreArrivalAlert(UserPK, HBLNr, ProcessType, FF);
            return Ok(preArrivalAlert);
        }
    }
}
