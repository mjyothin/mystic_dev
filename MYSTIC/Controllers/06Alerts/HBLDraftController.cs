﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MYSTIC.Models;
using MYSTIC.ControllerLogic;

namespace MYSTIC.Controllers
{
    public class HBLDraftController : ApiController
    {
        HBLDraftLogic hBLDraftLogic = new HBLDraftLogic();
        [HttpGet]
        public IHttpActionResult FetchHBLDraft()
        {
            HBLDraft hBLDraft = new HBLDraft();
            hBLDraft = hBLDraftLogic.FetchHBLDraft();
            return Ok(hBLDraft);
        }
        [HttpGet]
        public IHttpActionResult FetchHBLDraftDetails(string HBLNo)
        {
            HBLDraft hBLDraft = new HBLDraft();
            hBLDraft = hBLDraftLogic.FetchHBLDraftDetails(HBLNo);
            return Ok(hBLDraft);
        }
    }
}
