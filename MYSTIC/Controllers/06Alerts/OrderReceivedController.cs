﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MYSTIC.Models;
using MYSTIC.ControllerLogic;

namespace MYSTIC.Controllers
{
    public class OrderReceivedController : ApiController
    {
        OrderReceivedLogic orderReceivedLogic = new OrderReceivedLogic();
        [HttpGet]
        public IHttpActionResult FetchOrderReceived(int UserPK)
        {
            OrderReceived orderReceived = new OrderReceived();
            orderReceived = orderReceivedLogic.FetchOrderReceived(UserPK);
            return Ok(orderReceived);
        }
    }
}
