﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MYSTIC.Models;
using MYSTIC.ControllerLogic;

namespace MYSTIC.Controllers
{
    public class QuotationController : ApiController
    {
        QuotationLogic quotationLogic = new QuotationLogic();
        [HttpGet]
        public IHttpActionResult FetchQuotations(int UserPK)
        {
            Quotation quotation = new Quotation();
            quotation = quotationLogic.FetchQuotations(UserPK);
            return Ok(quotation.LstQuotationAlert);
        }
        [HttpGet]
        public IHttpActionResult FetchQuotationDetails(string QuotationNo)
        {
            Quotation quotation = new Quotation();
            quotation = quotationLogic.FetchQuotationDetails(QuotationNo);
            return Ok(quotation);
        }
    }
}
