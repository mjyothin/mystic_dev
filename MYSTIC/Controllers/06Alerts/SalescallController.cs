﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MYSTIC.Models;
using MYSTIC.ControllerLogic;

namespace MYSTIC.Controllers
{
    public class SalescallController : ApiController
    {
        SalescallLogic salescallLogic = new SalescallLogic();
        [HttpGet]
        public IHttpActionResult FetchSalesCall()
        {
            Salescall salescall = new Salescall();
            salescall.SalesCall = salescallLogic.FetchSalesCall();
            return Ok(salescall.SalesCall);
        }
        [HttpGet]
        public IHttpActionResult FetchSalesCallDetails(string SalesCallID)
        {
            Salescall salescall = new Salescall();
            salescall.SalesCallHeader = salescallLogic.FetchSalesCallHeader(SalesCallID);
            salescall.SalesCallComment = salescallLogic.FetchSalesCallComment(SalesCallID);
            salescall.SalesCallHistory = salescallLogic.FetchSalesCallHistory(SalesCallID);
            salescall.SalesCallFollowUp = salescallLogic.FetchSalesCallFollowup(SalesCallID);
            salescall.SalesCallEscalate = salescallLogic.FetchSalesCallEscalate(SalesCallID);
            salescall.SalesCallDelegate = salescallLogic.FetchSalesCallDelegate(SalesCallID);
            salescall.SalesCallRevisit = salescallLogic.FetchSalesCallRevisit(SalesCallID);
            return Ok(salescall);
        }
    }
}
