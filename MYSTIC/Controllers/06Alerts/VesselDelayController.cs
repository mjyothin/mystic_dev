﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MYSTIC.Models;
using MYSTIC.ControllerLogic;

namespace MYSTIC.Controllers
{
    public class VesselDelayController : ApiController
    {
        VesselDelayLogic delayLogic = new VesselDelayLogic();
        [HttpGet]
        [Route("api/VesselDelay/FetchVesselDelay")]
        public IHttpActionResult FetchVesselDelay([FromUri]VesselDelaySearch delaySearch)
        {
            try
            {
                VesselDelay vesselDelay = new VesselDelay();
                vesselDelay = delayLogic.FetchVesselDelay(delaySearch);
                return Ok(vesselDelay.LstVesselDelay);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpGet]
        [Route("api/VesselDelay/FetchVesselDelayDetails")]
        public IHttpActionResult FetchVesselDelayDetails([FromUri]VesselDelaySearch delaySearch)
        {
            try
            {
                VesselDelay vesselDelay = new VesselDelay();
                vesselDelay = delayLogic.FetchVesselDelayDetails(delaySearch);
                return Ok(vesselDelay.VesselDelayDetail);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
