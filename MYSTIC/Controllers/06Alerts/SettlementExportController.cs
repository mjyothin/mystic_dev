﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MYSTIC.Models;
using MYSTIC.ControllerLogic;

namespace MYSTIC.Controllers
{
    public class SettlementExportController : ApiController
    {
        SettlementExportLogic exportLogic = new SettlementExportLogic();
        [HttpGet]
        public IHttpActionResult FetchSettlementExport()
        {
            SettlementExport settlementExport = new SettlementExport();
            settlementExport = exportLogic.FetchSettlementExport();
            return Ok(settlementExport.LstSettlement);
        }
        [HttpGet]
        public IHttpActionResult FetchSettlementExportDetails(string ColRefNo)
        {
            SettlementExport settlementExport = new SettlementExport();
            settlementExport = exportLogic.FetchSettlementExportDetails(ColRefNo);
            return Ok(settlementExport);
        }
    }
}
