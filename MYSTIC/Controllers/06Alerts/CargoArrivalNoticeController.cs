﻿using MYSTIC.ControllerLogic;
using MYSTIC.Models;
using System;
using System.Web.Http;

namespace MYSTIC.Controllers
{
    public class CargoArrivalNoticeController : ApiController
    {
        private readonly CargoArrivalNoticeLogic ArrivalNoticeLogic = new CargoArrivalNoticeLogic();
        [HttpGet]
        public IHttpActionResult FetchCargoArrival(int BusinessType)
        {
            CargoArrivalNotice cargoArrivalNotice = new CargoArrivalNotice();
            try
            {
                cargoArrivalNotice = ArrivalNoticeLogic.FetchCargoArrival(BusinessType);
                if (BusinessType == 1)
                {
                    return Ok(cargoArrivalNotice.LstCANAir);
                }
                else
                {
                    return Ok(cargoArrivalNotice.LstCANSea);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpGet]
        public IHttpActionResult FetchCargoArrivalDetails(string CANRefNo, int BusinessType)
        {
            CargoArrivalNotice cargoArrivalNotice = new CargoArrivalNotice();
            try
            {
                cargoArrivalNotice = ArrivalNoticeLogic.FetchCargoArrivalDetails(CANRefNo, BusinessType);
                if (BusinessType == 1)
                {
                    return Ok(cargoArrivalNotice.CANAirDetails);
                }
                else
                {
                    return Ok(cargoArrivalNotice.CANSeaDetails);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpGet]
        public IHttpActionResult FetchCargoArrivalClauseDetails(string CANRefNo)
        {
            CargoArrivalNotice cargoArrivalNotice = new CargoArrivalNotice();
            try
            {
                cargoArrivalNotice = ArrivalNoticeLogic.FetchCargoArrivalClauseDetails(CANRefNo);
                return Ok(cargoArrivalNotice.CANClauseDetail);

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}
