﻿using MYSTIC.ControllerLogic;
using MYSTIC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MYSTIC.Controllers
{
    public class SettlementImportController : ApiController
    {
        SettlementImportLogic importLogic = new SettlementImportLogic();
        [HttpGet]
        public IHttpActionResult FetchSettlementImport()
        {
            SettlementImport settlementImport = new SettlementImport();
            settlementImport = importLogic.FetchSettlementImport();
            return Ok(settlementImport.LstSettlement);
        }
        [HttpGet]
        public IHttpActionResult FetchSettlementImportDetails(string ColRefNo)
        {
            SettlementImport settlementImport = new SettlementImport();
            settlementImport = importLogic.FetchSettlementImportDetails(ColRefNo);
            return Ok(settlementImport);
        }
    }
}
