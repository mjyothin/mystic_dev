﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MYSTIC.Models;
using MYSTIC.ControllerLogic;

namespace MYSTIC.Controllers
{
    public class InvoicedExportController : ApiController
    {
        InvoicedExportLogic invoicedExportLogic = new InvoicedExportLogic();
        [HttpGet]
        public IHttpActionResult FetchInvoicedExport()
        {
            InvoicedExport invoicedExport = new InvoicedExport();
            invoicedExport = invoicedExportLogic.FetchInvoicedExport();
            return Ok(invoicedExport);
        }
        [HttpGet]
        public IHttpActionResult FetchInvoicedExportDetails(string InvoiceNumber)
        {
            InvoicedExport invoicedExport = new InvoicedExport();
            invoicedExport = invoicedExportLogic.FetchInvoicedExportDetails(InvoiceNumber);
            return Ok(invoicedExport);
        }
    }
}
