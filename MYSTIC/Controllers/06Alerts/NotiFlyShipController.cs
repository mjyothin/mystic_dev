﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MYSTIC.Models;
using MYSTIC.ControllerLogic;

namespace MYSTIC.Controllers
{
    public class NotiFlyShipController : ApiController
    {
        NotiFlyShipLogic notiFlyShipLogic = new NotiFlyShipLogic();
        [HttpGet]
        public IHttpActionResult GetNotifFlyShip(int UserPK,string BookingNo,string FF)
        {
            NotiFlyShip notiFlyShip = new NotiFlyShip();
            notiFlyShip = notiFlyShipLogic.GetNotiFlyShip(UserPK, BookingNo, FF);
            return Ok(notiFlyShip);
        }
    }
}
