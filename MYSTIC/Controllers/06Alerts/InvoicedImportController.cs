﻿using MYSTIC.ControllerLogic;
using MYSTIC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MYSTIC.Controllers
{
    public class InvoicedImportController : ApiController
    {
        InvoicedImportLogic invoicedImportLogic = new InvoicedImportLogic();
        [HttpGet]
        public IHttpActionResult FetchInvoicedImport()
        {
            InvoicedImport invoicedImport = new InvoicedImport();
            invoicedImport = invoicedImportLogic.FetchInvoicedImport();
            return Ok(invoicedImport);
        }
        [HttpGet]
        public IHttpActionResult FetchInvoicedImportDetails(string InvoiceNumber)
        {
            InvoicedImport invoicedImport = new InvoicedImport();
            invoicedImport = invoicedImportLogic.FetchInvoicedImportDetails(InvoiceNumber);
            return Ok(invoicedImport);
        }
    }
}
