﻿using MYSTIC.ControllerLogic;
using MYSTIC.Data;
using MYSTIC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MYSTIC.Controllers
{
    public class CounterPartsController : ApiController
    {
        CounterPartsLogic counterPartsLogic = new CounterPartsLogic();
        [HttpGet]
        public IHttpActionResult FetchCounterParts([FromUri]CounterPartsSearch counterParts)
        {
            List<SP_FETCH_COUNTER_PARTS_Result> lstCounterParts = counterPartsLogic.FetchCounterParts(counterParts);
            return Ok(lstCounterParts);
        }
    }
}
