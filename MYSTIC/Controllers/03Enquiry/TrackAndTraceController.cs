﻿using MYSTIC.ControllerLogic;
using MYSTIC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MYSTIC.Controllers
{
    public class TrackAndTraceController : ApiController
    {
        TrackAndTraceLogic TrackAndTraceLogic = new TrackAndTraceLogic();
        [HttpGet]
        public IHttpActionResult FetchTrackAndTrace([FromUri]TrackAndTrace track)
        {
            track = TrackAndTraceLogic.FetchTrackAndTrace(track);
            return Ok(track.TrackNTraceList);
        }
        [HttpGet]
        public IHttpActionResult FetchTrackAndTraceDetail([FromUri]TrackAndTrace track)
        {
            TrackNTraceDetail traceDetail = TrackAndTraceLogic.FetchTrackAndTraceDetail(track);
            return Ok(traceDetail);
        }
    }
}
