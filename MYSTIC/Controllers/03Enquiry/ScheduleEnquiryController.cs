﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MYSTIC.ControllerLogic;
using MYSTIC.Models;

namespace MYSTIC.Controllers
{
    public class ScheduleEnquiryController : ApiController
    {
        ScheduleEnquiryLogic scheduleEnquiryLogic = new ScheduleEnquiryLogic();
        [HttpGet]
        public IHttpActionResult FetchScheduleEnquiry([FromUri]ScheduleEnquiry schedule)
        {
            ScheduleEnquiry scheduleEnquiry = new ScheduleEnquiry();
            scheduleEnquiry = scheduleEnquiryLogic.FetchScheduleEnquiry(schedule);
            if (schedule.BusinessType == 1)
                return Ok(scheduleEnquiry.ScheduleAir);
            else
                return Ok(scheduleEnquiry.ScheduleSea);
        }
        [HttpGet]
        [Route("api/ScheduleEnquiry/FetchCarrierMaster")]
        public IHttpActionResult FetchCarrierMaster([FromUri]ScheduleEnquiry schedule)
        {
            Carrier_Mst carrier = new Carrier_Mst();
            carrier = scheduleEnquiryLogic.FetchCarrier(schedule.USER_PK,schedule.BusinessType,schedule.Freight_forwarder);
                return Ok(carrier.lstCarrier);
        }
    }
}
