﻿using MYSTIC.ControllerLogic;
using MYSTIC.Data;
using MYSTIC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MYSTIC.Controllers
{
    public class ContactController : ApiController
    {
        ContactLogic contactLogic = new ContactLogic();
        [HttpPost]
        public IHttpActionResult ContactMe(M_CONTACT_ME_TRN contact)
        {
            string message = contactLogic.ContactMe(contact);
            return Ok(message);
        }
        [HttpGet]
        public IHttpActionResult GetSubject()
        {
            List<SubjectMaster> Subject = contactLogic.SubjectDropdown();
            return Ok(Subject);
        }
    }
}
