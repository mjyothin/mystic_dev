﻿using MYSTIC.ControllerLogic;
using MYSTIC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MYSTIC.Controllers
{
    public class CustomerProfileController : ApiController
    {
        [HttpGet,HttpPost]
        public IHttpActionResult GetCustomerProfile(int CustomerPK)
        {
            CustomerProfile customerProfile = new CustomerProfile();
            CustomerProfileLogic customerProfileLogic = new CustomerProfileLogic();
            customerProfile = customerProfileLogic.GetCustomerProfile(CustomerPK);
            return Ok(customerProfile);
        }
    }
}
