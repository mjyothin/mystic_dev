﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MYSTIC.Models;
using MYSTIC.ControllerLogic;

namespace MYSTIC.Controllers
{
    public class PasswordManagementController : ApiController
    {
        PasswordManagementLogic passwordManagementLogic = new PasswordManagementLogic();
        public IHttpActionResult ChangePassword(PasswordManagement password)
        {
            string Message = passwordManagementLogic.ChangePassword(password);
            return Ok(Message);
        }
    }
}
