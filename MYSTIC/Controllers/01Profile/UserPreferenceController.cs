﻿using MYSTIC.ControllerLogic;
using MYSTIC.Data;
using MYSTIC.Models;
using System.Web.Http;

namespace MYSTIC.Controllers
{
    public class UserPreferenceController : ApiController
    {
        private UserPreferenceLogic userPreferenceLogic = new UserPreferenceLogic();

        public IHttpActionResult UpdateUserPreference(M_USER_PREFERENCE_TBL USER_PREFERENCE_TBL)
        {
            bool Message = userPreferenceLogic.UpdateUserPreference(USER_PREFERENCE_TBL);
            if (Message == true)
                return Ok("Updated Successfully");
            else
                return Ok("Error In Updating Data");
        }
        [HttpGet]
        public IHttpActionResult FetchUserPreference(int User_Mst_PK)
        {
            UserPreference userPreference = userPreferenceLogic.FetchUserPreferences(User_Mst_PK);
            return Ok(userPreference);
        }
    }
}
