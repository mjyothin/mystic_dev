﻿using Microsoft.Ajax.Utilities;
using MYSTIC.Data;
using MYSTIC.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MYSTIC.ControllerLogic
{
    public class CustomsStatusLogic:CommonLogic
    {
        private Entities entities = new Entities();
        public CustomsStatus FetchCustomsStatus(CustomsStatus customsStatus)
        {
            string frDt = null;
            string toDt = null;
            if (customsStatus.FromDate != new DateTime())
            {
               frDt= customsStatus.FromDate.ToString("dd/MM/yyyy");
            }
            if (customsStatus.ToDate != new DateTime())
            {
               toDt= customsStatus.ToDate.ToString("dd/MM/yyyy");
            }
            customsStatus.LstCustomsStatus = entities.SP_RPT_CUSTOMS_STATUS(customsStatus.UserPK, customsStatus.JC_PK, customsStatus.Customer_PK, frDt,
                toDt, customsStatus.Vessel_Voyage, customsStatus.BusinessType, customsStatus.ProcessType, customsStatus.CargoType, customsStatus.Commodity_PK,
                customsStatus.JobType, customsStatus.Status, customsStatus.CHA_PK).ToList();
            return customsStatus;
        }
        public List<VendorList> FetchVendorsDD(CustomsStatus customsStatus)
        {
            customsStatus = FetchCustomsStatus(customsStatus);
            List<VendorList> vendors = customsStatus.LstCustomsStatus.Select(a => new VendorList { CHA_PK = a.VENDOR_MST_PK, CHA_Name = a.CHA_NAME }).DistinctBy(a=>a.CHA_PK).ToList();
            return vendors;
        }
        public List<CustomerList> FetchCustomersDD(CustomsStatus customsStatus)
        {
            customsStatus = FetchCustomsStatus(customsStatus);
            List<CustomerList> customers = customsStatus.LstCustomsStatus.Select(a => new CustomerList { Customer_PK = a.CUSTOMER_MST_FK, Customer_Name = a.CUSTOMER_NAME }).DistinctBy(a=>a.Customer_PK).ToList();
            return customers;
        }
        public List<JobCardList> FetchJobCardsDD(CustomsStatus customsStatus)
        {
            customsStatus = FetchCustomsStatus(customsStatus);
            List<JobCardList> jobCards = customsStatus.LstCustomsStatus.Select(a => new JobCardList { JC_PK = a.JC_FK, JCNumber = a.FILE_NO }).Where(a=>a.JC_PK!=null).DistinctBy(a=>a.JC_PK).ToList();
            return jobCards;
        }
        public List<VesVoyList> FetchVesVoyDD(CustomsStatus customsStatus)
        {
            customsStatus = FetchCustomsStatus(customsStatus);
            List<VesVoyList> vesVoys = customsStatus.LstCustomsStatus.Select(a => new VesVoyList { Vessel_Voyage= a.VOY_FLT }).DistinctBy(a=>a.Vessel_Voyage).ToList();
            return vesVoys;
        }
    }
}