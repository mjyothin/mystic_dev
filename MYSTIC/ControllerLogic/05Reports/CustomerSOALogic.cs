﻿using MYSTIC.Data;
using MYSTIC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MYSTIC.ControllerLogic
{
    public class CustomerSOALogic : CommonLogic
    {
        private Entities entities = new Entities();
        public CustomerSOA FetchCustomerSOA(int UserPK, string FF, string CustPK, string Fromdate, string Todate, string POLPK, string PODPK, string CountryPK, string LocPK
            , int BizType, int ProcessType, string CustGroupPK, string CarrierPK, string VslVoyPK, string FlightID, int CargoType, int CurrPK)
        {

            StringBuilder strQuery = new StringBuilder(5000);
            StringBuilder sb = new StringBuilder(50000);
            System.Text.StringBuilder sbNew1 = new System.Text.StringBuilder(5000), sbNew2 = new System.Text.StringBuilder(5000), sbNew3 = new System.Text.StringBuilder(5000);
            CustomerSOA customerSOA = new CustomerSOA();
            try
            {
                string ParentDB = Get_FreightForwarder_DataBase(UserPK, FF);
                if (string.IsNullOrEmpty(Fromdate))
                {
                    Fromdate = DateTime.Now.AddMonths(-3).ToString("dd/MM/yyyy");
                }
                if (string.IsNullOrEmpty(Todate))
                {
                    Todate = DateTime.Now.ToString("dd/MM/yyyy");
                }
                if (BizType == 0)
                {
                    BizType = 3;
                }
                sbNew1.Append("SELECT DISTINCT  C.CUSTOMER_MST_PK,");
                sbNew1.Append("       0 LOC_PK,");
                sbNew1.Append("       TO_DATE('" + Fromdate + "') REF_DATE,");
                sbNew1.Append("       '' PROCESS,");
                sbNew1.Append("       'OPENING BALANCE' TRANSACTION,");
                sbNew1.Append("       '' SHIPMENT_REF_NO,");
                sbNew1.Append("       '' DOCREFNR,");
                sbNew1.Append("       0 DEBIT,");
                sbNew1.Append("       0 CREDIT,");
                sbNew1.Append("       SUM(B.DEBIT - B.CREDIT) BALANCE,'' OUTSTANDING_DAYS,");
                sbNew1.Append("        0 REF_PK,");
                sbNew1.Append("        0 BIZ_TYPE,");
                sbNew1.Append("        0 CARGO_TYPE ");

                sbNew1.Append("  FROM " + ParentDB + ".CUSTOMER_MST_TBL C,");
                sbNew1.Append("       (SELECT DISTINCT CMT.CUSTOMER_MST_PK,");
                sbNew1.Append("                        CCD.ADM_LOCATION_MST_FK,");
                sbNew1.Append("                        INV.INVOICE_DATE REF_DATE,");
                sbNew1.Append("                        (CASE");
                sbNew1.Append("                          WHEN INV.PROCESS_TYPE = 1 THEN");
                sbNew1.Append("                           'EXPORT'");
                sbNew1.Append("                          ELSE");
                sbNew1.Append("                           'IMPORT'");
                sbNew1.Append("                        END) PROCESS,");
                sbNew1.Append("                        'INVOICE' TRANSACTION,");
                sbNew1.Append("  ( CASE ");
                sbNew1.Append("    WHEN JOB.BUSINESS_TYPE = 2 AND JOB.PROCESS_TYPE = 1 THEN ");
                sbNew1.Append("      HBL.HBL_REF_NO ");
                sbNew1.Append("     WHEN JOB.BUSINESS_TYPE = 1 AND JOB.PROCESS_TYPE = 1 THEN ");
                sbNew1.Append("         HAWB.HAWB_REF_NO ");
                sbNew1.Append("        ELSE ");
                sbNew1.Append("       JOB.HBL_HAWB_REF_NO  ");
                sbNew1.Append("      END) HBL_REF_NO , ");
                sbNew1.Append("                        INV.INVOICE_REF_NO DOCREFNR,");
                sbNew1.Append("                        INV.CONSOL_INVOICE_PK REF_PK,");
                sbNew1.Append("                        INV.BUSINESS_TYPE BIZ_TYPE,");
                if (ProcessType == 1)
                {
                    sbNew1.Append("                        BKG.CARGO_TYPE CARGO_TYPE,");
                }
                else if (BizType == 2)
                {
                    sbNew1.Append("                        JOB.CARGO_TYPE CARGO_TYPE, ");
                }
                else
                {
                    sbNew1.Append("                        0 CARGO_TYPE, ");
                }

                sbNew1.Append("                        ROUND(INV.NET_RECEIVABLE * " + ParentDB + ".GET_EX_RATE(INV.CURRENCY_MST_FK," + CurrPK + ", INV.INVOICE_DATE),2) DEBIT,");
                sbNew1.Append("                        0 CREDIT,");
                sbNew1.Append("                        0 BALANCE,");
                sbNew1.Append("(SELECT DISTINCT TO_CHAR(TO_DATE(SYSDATE,");
                sbNew1.Append("                             'DD/MM/YYYY') -");
                sbNew1.Append("                              TO_DATE(INV.INVOICE_DATE)) OUTSTANDING_DAYS");


                sbNew1.Append("                                   FROM " + ParentDB + ".COLLECTIONS_TRN_TBL COLL,");
                sbNew1.Append("                                        " + ParentDB + ".CONSOL_INVOICE_TBL  INV1");
                sbNew1.Append("                                  WHERE INV1.INVOICE_REF_NO =");
                sbNew1.Append("                                        COLL.INVOICE_REF_NR(+)");
                sbNew1.Append("                                    AND INV1.INVOICE_REF_NO =");
                sbNew1.Append("                                        INV.INVOICE_REF_NO");
                sbNew1.Append("                                    AND NVL((INV1.NET_RECEIVABLE -");
                sbNew1.Append("                                            NVL((SELECT SUM(CTRN.RECD_AMOUNT_HDR_CURR)");
                sbNew1.Append("                                                   FROM " + ParentDB + ".COLLECTIONS_TRN_TBL CTRN");
                sbNew1.Append("                                                  WHERE CTRN.INVOICE_REF_NR LIKE");
                sbNew1.Append("                                                        INV1.INVOICE_REF_NO),");
                sbNew1.Append("                                                 0.00)),");
                sbNew1.Append("                                            0) > 0)");
                sbNew1.Append("          FROM " + ParentDB + ".JOB_CARD_TRN   JOB,");
                if (ProcessType == 1)
                {
                    sbNew1.Append("               " + ParentDB + ".BOOKING_MST_TBL        BKG,");
                }

                sbNew1.Append("               " + ParentDB + ".HBL_EXP_TBL            HBL,");
                sbNew1.Append("               " + ParentDB + ".HAWB_EXP_TBL            HAWB,");
                sbNew1.Append("               " + ParentDB + ".CUSTOMER_MST_TBL       CMT,");
                sbNew1.Append("               " + ParentDB + ".CUSTOMER_CONTACT_DTLS  CCD,");
                sbNew1.Append("               " + ParentDB + ".LOCATION_MST_TBL       LMT,");
                sbNew1.Append("               " + ParentDB + ".CONSOL_INVOICE_TBL     INV,");
                sbNew1.Append("               " + ParentDB + ".CONSOL_INVOICE_TRN_TBL INVTRN");
                sbNew1.Append("         WHERE INVTRN.JOB_CARD_FK = JOB.JOB_CARD_TRN_PK(+)");
                sbNew1.Append("         AND INV.IS_FAC_INV <> 1");
                sbNew1.Append("           AND INVTRN.JOB_TYPE = 1");
                sbNew1.Append("           AND JOB.HBL_HAWB_FK = HBL.HBL_EXP_TBL_PK(+)");
                sbNew1.Append("           AND JOB.HBL_HAWB_FK = HAWB.HAWB_EXP_TBL_PK(+)");
                if (ProcessType == 1)
                {
                    sbNew1.Append("           AND CMT.CUSTOMER_MST_PK = BKG.CUST_CUSTOMER_MST_FK");
                    sbNew1.Append("           AND JOB.BOOKING_MST_FK = BKG.BOOKING_MST_PK");
                    sbNew1.Append("           AND INV.PROCESS_TYPE = " + ProcessType);
                    if (CargoType != 0)
                    {
                        sbNew1.Append("     AND BKG.CARGO_TYPE=" + CargoType);
                    }

                    if (!string.IsNullOrEmpty(POLPK) & POLPK != "0")
                    {
                        sbNew1.Append("       AND BKG.PORT_MST_POL_FK IN(" + POLPK + ")");
                    }

                    if (!string.IsNullOrEmpty(POLPK) & PODPK != "0")
                    {
                        sbNew1.Append("       AND BKG.PORT_MST_POD_FK IN( " + PODPK + ")");
                    }
                }
                else if (ProcessType == 2)
                {
                    sbNew1.Append("        AND JOB.CUST_CUSTOMER_MST_FK=CMT.CUSTOMER_MST_PK");
                    sbNew1.Append("           AND INV.PROCESS_TYPE = 2");
                    if (CargoType != 0)
                    {
                        sbNew1.Append("     AND JOB.CARGO_TYPE=" + CargoType);
                    }

                    if (!string.IsNullOrEmpty(POLPK) & POLPK != "0")
                    {
                        sbNew1.Append("         AND JOB.PORT_MST_POL_FK IN(" + POLPK + ")");
                    }

                    if (!string.IsNullOrEmpty(POLPK) & PODPK != "0")
                    {
                        sbNew1.Append("         AND JOB.PORT_MST_POD_FK IN(" + PODPK + ")");
                    }
                }
                sbNew1.Append("           AND INV.CONSOL_INVOICE_PK = INVTRN.CONSOL_INVOICE_FK");
                sbNew1.Append("           AND CMT.CUSTOMER_MST_PK = CCD.CUSTOMER_MST_FK");
                sbNew1.Append("           AND LMT.LOCATION_MST_PK = CCD.ADM_LOCATION_MST_FK");
                sbNew1.Append("           AND NVL((SELECT SUM(WMT.WRITEOFF_AMOUNT)");
                sbNew1.Append("             FROM " + ParentDB + ".WRITEOFF_MANUAL_TBL WMT");
                sbNew1.Append("            WHERE WMT.CONSOL_INVOICE_FK = INV.CONSOL_INVOICE_PK),");
                sbNew1.Append("           0.00) = 0");
                if (BizType == 1)
                {
                    sbNew1.Append("           AND INV.BUSINESS_TYPE = 1");
                    if (!string.IsNullOrEmpty(FlightID))
                    {
                        sbNew1.Append("            AND JOB.VOYAGE_FLIGHT_NO IN (" + FlightID + ")");
                    }
                }
                else if (BizType == 2)
                {
                    sbNew1.Append("           AND INV.BUSINESS_TYPE = 2");
                    if (!string.IsNullOrEmpty(VslVoyPK))
                    {
                        sbNew1.Append("            AND JOB.VOYAGE_TRN_FK=" + VslVoyPK + "");
                    }
                }
                sbNew1.Append("           AND INV.CHK_INVOICE<>2 ");
                if (!string.IsNullOrEmpty(CountryPK) & CountryPK != "0")
                {
                    sbNew1.Append("       AND LMT.COUNTRY_MST_FK IN(" + CountryPK + ")");
                }

                if (!string.IsNullOrEmpty(LocPK) & LocPK != "0")
                {
                    sbNew1.Append("        AND LMT.LOCATION_MST_PK IN (" + LocPK + ") ");
                }

                if (!string.IsNullOrEmpty(CustPK) & CustPK != "0")
                {
                    sbNew1.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustPK + ")");
                }

                if (!string.IsNullOrEmpty(CustGroupPK) & CustGroupPK != "0")
                {
                    sbNew1.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustGroupPK + ")");
                }

                if (Fromdate != "" & Todate != "")
                {
                    sbNew1.Append("       AND INV.invoice_date BETWEEN TO_DATE('" + Fromdate + "') AND");
                    sbNew1.Append("               TO_DATE('" + Todate + "')");
                }
                sbNew1.Append("        UNION");
                sbNew1.Append("        SELECT DISTINCT CMT.CUSTOMER_MST_PK,");
                sbNew1.Append("                        CCD.ADM_LOCATION_MST_FK,");
                sbNew1.Append("                        COL.COLLECTIONS_DATE REF_DATE,");
                sbNew1.Append("                        (CASE");
                sbNew1.Append("                          WHEN COL.PROCESS_TYPE = 1 THEN");
                sbNew1.Append("                           'EXPORT'");
                sbNew1.Append("                          ELSE");
                sbNew1.Append("                           'IMPORT'");
                sbNew1.Append("                        END) PROCESS,");
                sbNew1.Append("                        'COLLECTION' TRANSACTION,");
                sbNew1.Append("  ( CASE ");
                sbNew1.Append("    WHEN JOB.BUSINESS_TYPE = 2 AND JOB.PROCESS_TYPE = 1 THEN ");
                sbNew1.Append("      HBL.HBL_REF_NO ");
                sbNew1.Append("     WHEN JOB.BUSINESS_TYPE = 1 AND JOB.PROCESS_TYPE = 1 THEN ");
                sbNew1.Append("         HAWB.HAWB_REF_NO ");
                sbNew1.Append("        ELSE ");
                sbNew1.Append("       JOB.HBL_HAWB_REF_NO  ");
                sbNew1.Append("      END) HBL_REF_NO , ");
                sbNew1.Append("                        COL.COLLECTIONS_REF_NO DOCREFNR,");
                sbNew1.Append("                        COL.COLLECTIONS_TBL_PK REF_PK,");
                sbNew1.Append("                        COL.BUSINESS_TYPE BIZ_TYPE,");
                if (ProcessType == 1)
                {
                    sbNew1.Append("                        BKG.CARGO_TYPE CARGO_TYPE,");
                }
                else if (BizType == 2)
                {
                    sbNew1.Append("                        JOB.CARGO_TYPE CARGO_TYPE, ");
                }
                else
                {
                    sbNew1.Append("                        0 CARGO_TYPE, ");
                }

                sbNew1.Append("                        0 DEBIT,");
                sbNew1.Append("                        ROUND(CLT.RECD_AMOUNT_HDR_CURR * " + ParentDB + ".GET_EX_RATE(COL.CURRENCY_MST_FK," + CurrPK + ",COL.COLLECTIONS_DATE),2) CREDIT,");
                sbNew1.Append("                        0 BALANCE,''OUTSTANDING_DAYS");
                sbNew1.Append("          FROM " + ParentDB + ".JOB_CARD_TRN   JOB,");
                if (ProcessType == 1)
                {
                    sbNew1.Append("               " + ParentDB + ".BOOKING_MST_TBL        BKG,");
                }

                sbNew1.Append("               " + ParentDB + ".HBL_EXP_TBL            HBL,");
                sbNew1.Append("               " + ParentDB + ".HAWB_EXP_TBL            HAWB,");
                sbNew1.Append("               " + ParentDB + ".CUSTOMER_MST_TBL       CMT,");
                sbNew1.Append("               " + ParentDB + ".CUSTOMER_CONTACT_DTLS  CCD,");
                sbNew1.Append("               " + ParentDB + ".LOCATION_MST_TBL       LMT,");
                sbNew1.Append("               " + ParentDB + ".CONSOL_INVOICE_TBL     INV,");
                sbNew1.Append("               " + ParentDB + ".CONSOL_INVOICE_TRN_TBL INVTRN,");
                sbNew1.Append("               " + ParentDB + ".COLLECTIONS_TRN_TBL    CLT,");
                sbNew1.Append("               " + ParentDB + ".COLLECTIONS_TBL        COL");
                sbNew1.Append("         WHERE INVTRN.JOB_CARD_FK = JOB.JOB_CARD_TRN_PK(+)");
                sbNew1.Append("         AND INV.IS_FAC_INV <> 1");
                sbNew1.Append("           AND INVTRN.JOB_TYPE = 1");
                sbNew1.Append("           AND JOB.HBL_HAWB_FK = HBL.HBL_EXP_TBL_PK(+)");
                sbNew1.Append("           AND JOB.HBL_HAWB_FK = HAWB.HAWB_EXP_TBL_PK(+)");
                if (ProcessType == 1)
                {
                    sbNew1.Append("           AND CMT.CUSTOMER_MST_PK = BKG.CUST_CUSTOMER_MST_FK");
                    sbNew1.Append("           AND JOB.BOOKING_MST_FK = BKG.BOOKING_MST_PK");
                    sbNew1.Append("           AND COL.PROCESS_TYPE = " + ProcessType);
                    if (CargoType != 0)
                    {
                        sbNew1.Append("     AND BKG.CARGO_TYPE=" + CargoType);
                    }

                    if (!string.IsNullOrEmpty(POLPK) & POLPK != "0")
                    {
                        sbNew1.Append("       AND BKG.PORT_MST_POL_FK IN(" + POLPK + ")");
                    }

                    if (!string.IsNullOrEmpty(POLPK) & PODPK != "0")
                    {
                        sbNew1.Append("       AND BKG.PORT_MST_POD_FK IN( " + PODPK + ")");
                    }
                }
                else if (ProcessType == 2)
                {
                    sbNew1.Append("        AND JOB.CUST_CUSTOMER_MST_FK=CMT.CUSTOMER_MST_PK");
                    sbNew1.Append("           AND COL.PROCESS_TYPE = 2");
                    if (CargoType != 0)
                    {
                        sbNew1.Append("     AND JOB.CARGO_TYPE=" + CargoType);
                    }

                    if (!string.IsNullOrEmpty(POLPK) & POLPK != "0")
                    {
                        sbNew1.Append("         AND JOB.PORT_MST_POL_FK IN(" + POLPK + ")");
                    }

                    if (!string.IsNullOrEmpty(POLPK) & PODPK != "0")
                    {
                        sbNew1.Append("         AND JOB.PORT_MST_POD_FK IN(" + PODPK + ")");
                    }
                }
                sbNew1.Append("           AND INV.CONSOL_INVOICE_PK = INVTRN.CONSOL_INVOICE_FK");
                sbNew1.Append("           AND CLT.INVOICE_REF_NR = INV.INVOICE_REF_NO");
                sbNew1.Append("           AND COL.COLLECTIONS_TBL_PK = CLT.COLLECTIONS_TBL_FK");
                sbNew1.Append("           AND CMT.CUSTOMER_MST_PK = CCD.CUSTOMER_MST_FK");
                sbNew1.Append("           AND LMT.LOCATION_MST_PK = CCD.ADM_LOCATION_MST_FK");
                sbNew1.Append("           AND NVL((SELECT SUM(WMT.WRITEOFF_AMOUNT)");
                sbNew1.Append("             FROM " + ParentDB + ".WRITEOFF_MANUAL_TBL WMT");
                sbNew1.Append("            WHERE WMT.CONSOL_INVOICE_FK = INV.CONSOL_INVOICE_PK),");
                sbNew1.Append("           0.00) = 0");
                if (BizType == 1)
                {
                    sbNew1.Append("           AND COL.BUSINESS_TYPE = 1");
                    if (!string.IsNullOrEmpty(FlightID))
                    {
                        sbNew1.Append("            AND JOB.VOYAGE_FLIGHT_NO IN (" + FlightID + ")");
                    }
                }
                else if (BizType == 2)
                {
                    sbNew1.Append("           AND COL.BUSINESS_TYPE = 2");
                    if (!string.IsNullOrEmpty(VslVoyPK))
                    {
                        sbNew1.Append("            AND JOB.VOYAGE_TRN_FK=" + VslVoyPK + "");
                    }
                }
                sbNew1.Append("           AND INV.CHK_INVOICE<>2 ");
                if (!string.IsNullOrEmpty(CountryPK) & CountryPK != "0")
                {
                    sbNew1.Append("       AND LMT.COUNTRY_MST_FK IN(" + CountryPK + ")");
                }

                if (!string.IsNullOrEmpty(LocPK) & LocPK != "0")
                {
                    sbNew1.Append("        AND LMT.LOCATION_MST_PK IN (" + LocPK + ") ");
                }

                if (!string.IsNullOrEmpty(CustPK) & CustPK != "0")
                {
                    sbNew1.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustPK + ")");
                }

                if (!string.IsNullOrEmpty(CustGroupPK) & CustGroupPK != "0")
                {
                    sbNew1.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustGroupPK + ")");
                }

                if (Fromdate != "" & Todate != "")
                {
                    sbNew1.Append("           AND COL.COLLECTIONS_DATE BETWEEN TO_DATE('" + Fromdate + "') AND");
                    sbNew1.Append("               TO_DATE('" + Todate + "')");
                }
                sbNew1.Append("        UNION");
                sbNew1.Append("        SELECT DISTINCT CMT.CUSTOMER_MST_PK,");
                sbNew1.Append("                        CCD.ADM_LOCATION_MST_FK,");
                sbNew1.Append("                        CNT.CREDIT_NOTE_DATE REF_DATE,");
                sbNew1.Append("                        (CASE");
                sbNew1.Append("                          WHEN CNT.PROCESS_TYPE = 1 THEN");
                sbNew1.Append("                           'EXPORT'");
                sbNew1.Append("                          ELSE");
                sbNew1.Append("                           'IMPORT'");
                sbNew1.Append("                        END) PROCESS,");
                sbNew1.Append("                        'CREDITNOTE' TRANSACTION,");
                sbNew1.Append("  ( CASE ");
                sbNew1.Append("    WHEN JOB.BUSINESS_TYPE = 2 AND JOB.PROCESS_TYPE = 1 THEN ");
                sbNew1.Append("      HBL.HBL_REF_NO ");
                sbNew1.Append("     WHEN JOB.BUSINESS_TYPE = 1 AND JOB.PROCESS_TYPE = 1 THEN ");
                sbNew1.Append("         HAWB.HAWB_REF_NO ");
                sbNew1.Append("        ELSE ");
                sbNew1.Append("       JOB.HBL_HAWB_REF_NO  ");
                sbNew1.Append("      END) HBL_REF_NO , ");
                sbNew1.Append("                        CNT.CREDIT_NOTE_REF_NR DOCREFNR,");
                sbNew1.Append("                         CNT.CRN_TBL_PK REF_PK,");
                sbNew1.Append("                        CNT.BIZ_TYPE BIZ_TYPE,");
                if (ProcessType == 1)
                {
                    sbNew1.Append("                        BKG.CARGO_TYPE CARGO_TYPE,");
                }
                else if (BizType == 2)
                {
                    sbNew1.Append("                        JOB.CARGO_TYPE CARGO_TYPE, ");
                }
                else
                {
                    sbNew1.Append("                        0 CARGO_TYPE, ");
                }

                sbNew1.Append("                        0 DEBIT,");
                sbNew1.Append("                        ROUND(CNT.CRN_AMMOUNT * " + ParentDB + ".GET_EX_RATE(CNT.CURRENCY_MST_FK," + CurrPK + ",CNT.CREDIT_NOTE_DATE),2) CREDIT,");
                sbNew1.Append("                        0 BALANCE,''OUTSTANDING_DAYS");
                sbNew1.Append("          FROM " + ParentDB + ".JOB_CARD_TRN   JOB,");
                if (ProcessType == 1)
                {
                    sbNew1.Append("               " + ParentDB + ".BOOKING_MST_TBL        BKG,");
                }

                sbNew1.Append("               " + ParentDB + ".HBL_EXP_TBL            HBL,");
                sbNew1.Append("               " + ParentDB + ".HAWB_EXP_TBL            HAWB,");
                sbNew1.Append("               " + ParentDB + ".CUSTOMER_MST_TBL       CMT,");
                sbNew1.Append("               " + ParentDB + ".CUSTOMER_CONTACT_DTLS  CCD,");
                sbNew1.Append("               " + ParentDB + ".LOCATION_MST_TBL       LMT,");
                sbNew1.Append("               " + ParentDB + ".CONSOL_INVOICE_TBL     INV,");
                sbNew1.Append("               " + ParentDB + ".CONSOL_INVOICE_TRN_TBL INVTRN,");
                sbNew1.Append("               " + ParentDB + ".CREDIT_NOTE_TBL        CNT,");
                sbNew1.Append("               " + ParentDB + ".CREDIT_NOTE_TRN_TBL    CNTT");
                sbNew1.Append("         WHERE INVTRN.JOB_CARD_FK = JOB.JOB_CARD_TRN_PK(+)");
                sbNew1.Append("         AND INV.IS_FAC_INV <> 1");
                sbNew1.Append("           AND INVTRN.JOB_TYPE = 1");
                sbNew1.Append("           AND JOB.HBL_HAWB_FK = HBL.HBL_EXP_TBL_PK(+)");
                sbNew1.Append("           AND JOB.HBL_HAWB_FK = HAWB.HAWB_EXP_TBL_PK(+)");
                if (ProcessType == 1)
                {
                    sbNew1.Append("           AND CMT.CUSTOMER_MST_PK = BKG.CUST_CUSTOMER_MST_FK");
                    sbNew1.Append("           AND JOB.BOOKING_MST_FK = BKG.BOOKING_MST_PK");
                    sbNew1.Append("           AND CNT.PROCESS_TYPE = " + ProcessType);
                    if (CargoType != 0)
                    {
                        sbNew1.Append("     AND BKG.CARGO_TYPE=" + CargoType);
                    }

                    if (!string.IsNullOrEmpty(POLPK) & POLPK != "0")
                    {
                        sbNew1.Append("       AND BKG.PORT_MST_POL_FK IN(" + POLPK + ")");
                    }

                    if (!string.IsNullOrEmpty(POLPK) & PODPK != "0")
                    {
                        sbNew1.Append("       AND BKG.PORT_MST_POD_FK IN( " + PODPK + ")");
                    }
                }
                else if (ProcessType == 2)
                {
                    sbNew1.Append("        AND JOB.CUST_CUSTOMER_MST_FK=CMT.CUSTOMER_MST_PK");
                    sbNew1.Append("           AND CNT.PROCESS_TYPE = 2");
                    if (CargoType != 0)
                    {
                        sbNew1.Append("     AND JOB.CARGO_TYPE=" + CargoType);
                    }

                    if (!string.IsNullOrEmpty(POLPK) & POLPK != "0")
                    {
                        sbNew1.Append("         AND JOB.PORT_MST_POL_FK IN(" + POLPK + ")");
                    }

                    if (!string.IsNullOrEmpty(POLPK) & PODPK != "0")
                    {
                        sbNew1.Append("         AND JOB.PORT_MST_POD_FK IN(" + PODPK + ")");
                    }
                }
                sbNew1.Append("           AND INV.CONSOL_INVOICE_PK = INVTRN.CONSOL_INVOICE_FK");
                sbNew1.Append("           AND CNT.CRN_TBL_PK = CNTT.CRN_TBL_FK");
                sbNew1.Append("           AND CNTT.CONSOL_INVOICE_TRN_FK = INVTRN.CONSOL_INVOICE_TRN_PK");
                sbNew1.Append("           AND CMT.CUSTOMER_MST_PK = CCD.CUSTOMER_MST_FK");
                sbNew1.Append("           AND LMT.LOCATION_MST_PK = CCD.ADM_LOCATION_MST_FK");
                sbNew1.Append("           AND NVL((SELECT SUM(WMT.WRITEOFF_AMOUNT)");
                sbNew1.Append("             FROM " + ParentDB + ".WRITEOFF_MANUAL_TBL WMT");
                sbNew1.Append("            WHERE WMT.CONSOL_INVOICE_FK = INV.CONSOL_INVOICE_PK),");
                sbNew1.Append("           0.00) = 0");
                if (BizType == 1)
                {
                    sbNew1.Append("           AND CNT.BIZ_TYPE = 1");
                    if (!string.IsNullOrEmpty(FlightID))
                    {
                        sbNew1.Append("            AND JOB.VOYAGE_FLIGHT_NO IN (" + FlightID + ")");
                    }
                }
                else if (BizType == 2)
                {
                    sbNew1.Append("           AND CNT.BIZ_TYPE = 2");
                    if (!string.IsNullOrEmpty(VslVoyPK))
                    {
                        sbNew1.Append("            AND JOB.VOYAGE_TRN_FK=" + VslVoyPK + "");
                    }
                }
                sbNew1.Append("           AND INV.CHK_INVOICE<>2 ");
                if (!string.IsNullOrEmpty(CountryPK) & CountryPK != "0")
                {
                    sbNew1.Append("       AND LMT.COUNTRY_MST_FK IN(" + CountryPK + ")");
                }

                if (!string.IsNullOrEmpty(LocPK) & LocPK != "0")
                {
                    sbNew1.Append("        AND LMT.LOCATION_MST_PK IN (" + LocPK + ") ");
                }

                if (!string.IsNullOrEmpty(CustPK) & CustPK != "0")
                {
                    sbNew1.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustPK + ")");
                }

                if (!string.IsNullOrEmpty(CustGroupPK) & CustGroupPK != "0")
                {
                    sbNew1.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustGroupPK + ")");
                }

                if (Fromdate != "" & Todate != "")
                {
                    sbNew1.Append("           AND CNT.CREDIT_NOTE_DATE BETWEEN TO_DATE('" + Fromdate + "') AND");
                    sbNew1.Append("               TO_DATE('" + Todate + "')");
                }

                // -----Added By Sushama----
                // ' ----CBJC(Invoice)--'
                sb.Append(sbNew1.ToString() + "        UNION");
                sb.Append("           SELECT DISTINCT CMT.CUSTOMER_MST_PK,");
                sb.Append("                        CCD.ADM_LOCATION_MST_FK,");
                sb.Append("                        INV.INVOICE_DATE REF_DATE,");
                sb.Append("                        (CASE");
                sb.Append("                          WHEN INV.PROCESS_TYPE = 1 THEN");
                sb.Append("                           'EXPORT'");
                sb.Append("                          ELSE");
                sb.Append("                           'IMPORT'");
                sb.Append("                        END) PROCESS,");
                sb.Append("                        'INVOICE' TRANSACTION,");
                sb.Append("                        CBJC.HBL_NO HBL_REF_NO,");
                sb.Append("                        INV.INVOICE_REF_NO DOCREFNR,");
                sb.Append("                        INV.CONSOL_INVOICE_PK REF_PK,");
                sb.Append("                        INV.BUSINESS_TYPE BIZ_TYPE,");
                sb.Append("                        CBJC.CARGO_TYPE CARGO_TYPE,");
                sb.Append("                        ROUND(INV.NET_RECEIVABLE *");
                sb.Append("                              " + ParentDB + ".GET_EX_RATE(INV.CURRENCY_MST_FK,");
                sb.Append(CurrPK + ",");
                sb.Append("                                          INV.INVOICE_DATE),");
                sb.Append("                              2) DEBIT,");
                sb.Append("                        0 CREDIT,");
                sb.Append("                        0 BALANCE,");
                sb.Append("                        (SELECT DISTINCT TO_CHAR(TO_DATE(SYSDATE,");
                sb.Append("                                                         'DD/MM/YYYY') -");
                sb.Append("                                                 TO_DATE(INV.INVOICE_DATE)) OUTSTANDING_DAYS");
                sb.Append("                           FROM " + ParentDB + ".COLLECTIONS_TRN_TBL COLL,");
                sb.Append("                                " + ParentDB + ".CONSOL_INVOICE_TBL  INV1");
                sb.Append("                          WHERE INV1.INVOICE_REF_NO = COLL.INVOICE_REF_NR(+)");
                sb.Append("                            AND INV1.INVOICE_REF_NO = INV.INVOICE_REF_NO");
                sb.Append("                            AND NVL((INV1.NET_RECEIVABLE -");
                sb.Append("                                    NVL((SELECT SUM(CTRN.RECD_AMOUNT_HDR_CURR)");
                sb.Append("                                           FROM " + ParentDB + ".COLLECTIONS_TRN_TBL CTRN");
                sb.Append("                                          WHERE CTRN.INVOICE_REF_NR LIKE");
                sb.Append("                                                INV1.INVOICE_REF_NO),");
                sb.Append("                                         0.00)),");
                sb.Append("                                    0) > 0)");
                sb.Append("          FROM " + ParentDB + ".CBJC_TBL   CBJC,");
                sb.Append("              " + ParentDB + ".CUSTOMER_MST_TBL       CMT,");
                sb.Append("               " + ParentDB + ".CUSTOMER_CONTACT_DTLS  CCD,");
                sb.Append("               " + ParentDB + ".LOCATION_MST_TBL       LMT,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TBL     INV,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TRN_TBL INVTRN");
                sb.Append("         WHERE INVTRN.JOB_CARD_FK = CBJC.CBJC_PK(+)");
                sb.Append("           AND INV.IS_FAC_INV <> 1");
                sb.Append("           AND INV.CUSTOMER_MST_FK = CMT.CUSTOMER_MST_PK ");
                sb.Append("           AND INV.PROCESS_TYPE = " + ProcessType);
                sb.Append("           AND INV.CONSOL_INVOICE_PK = INVTRN.CONSOL_INVOICE_FK");
                sb.Append("           AND CMT.CUSTOMER_MST_PK = CCD.CUSTOMER_MST_FK");
                sb.Append("           AND LMT.LOCATION_MST_PK = CCD.ADM_LOCATION_MST_FK");
                sb.Append("           AND NVL((SELECT SUM(WMT.WRITEOFF_AMOUNT)");
                sb.Append("                     FROM " + ParentDB + ".WRITEOFF_MANUAL_TBL WMT");
                sb.Append("                    WHERE WMT.CONSOL_INVOICE_FK = INV.CONSOL_INVOICE_PK),");
                sb.Append("                   0.00) = 0");
                if (Fromdate != "" & Todate != "")
                {
                    sb.Append("           AND INV.invoice_date BETWEEN TO_DATE('" + Fromdate + "') AND");
                    sb.Append("               TO_DATE('" + Todate + "')");
                }
                sb.Append("          AND CBJC.PROCESS_TYPE =" + ProcessType);
                if (BizType != 3)
                {
                    sb.Append("          AND CBJC.BIZ_TYPE =" + BizType);
                }

                if (CargoType != 0)
                {
                    sb.Append("     AND CBJC.CARGO_TYPE=" + CargoType);
                }

                if (!string.IsNullOrEmpty(CountryPK) & CountryPK != "0")
                {
                    sb.Append("       AND LMT.COUNTRY_MST_FK IN(" + CountryPK + ")");
                }

                if (!string.IsNullOrEmpty(LocPK) & LocPK != "0")
                {
                    sb.Append("        AND LMT.LOCATION_MST_PK IN (" + LocPK + ") ");
                }

                if (!string.IsNullOrEmpty(CustPK) & CustPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustPK + ")");
                }

                if (!string.IsNullOrEmpty(CustGroupPK) & CustGroupPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustGroupPK + ")");
                }

                if (BizType == 1)
                {
                    if (!string.IsNullOrEmpty(FlightID))
                    {
                        sb.Append("     AND   CBJC.FLIGHT_NO IN (" + FlightID + ")");
                    }
                }
                else if (BizType == 2)
                {
                    if (!string.IsNullOrEmpty(VslVoyPK))
                    {
                        sb.Append("     AND   CBJC.VOYAGE_TRN_FK=" + VslVoyPK + "");
                    }
                }
                sb.Append("           AND INVTRN.JOB_TYPE = 2       ");
                sb.Append("           AND INV.CHK_INVOICE<>2 ");
                // '    ----TPT(Tranport Note)(Invoice)--
                sb.Append("           UNION");
                sb.Append("           SELECT DISTINCT CMT.CUSTOMER_MST_PK,");
                sb.Append("                        CCD.ADM_LOCATION_MST_FK,");
                sb.Append("                        INV.INVOICE_DATE REF_DATE,");
                sb.Append("                        (CASE");
                sb.Append("                          WHEN INV.PROCESS_TYPE = 1 THEN");
                sb.Append("                           'EXPORT'");
                sb.Append("                          ELSE");
                sb.Append("                           'IMPORT'");
                sb.Append("                        END) PROCESS,");
                sb.Append("                        'INVOICE' TRANSACTION,");
                sb.Append("                        (SELECT CONT.BL_NUMBER FROM " + ParentDB + ".TRANSPORT_TRN_CONT CONT WHERE CONT.TRANSPORT_INST_FK = TIST.TRANSPORT_INST_SEA_PK AND ROWNUM=1) HBL_REF_NO,");
                sb.Append("                        INV.INVOICE_REF_NO DOCREFNR,");
                sb.Append("                        INV.CONSOL_INVOICE_PK REF_PK,");
                sb.Append("                        INV.BUSINESS_TYPE BIZ_TYPE,");
                sb.Append("                        TIST.CARGO_TYPE CARGO_TYPE,");
                sb.Append("                        ROUND(INV.NET_RECEIVABLE *");
                sb.Append("                              " + ParentDB + ".GET_EX_RATE(INV.CURRENCY_MST_FK,");
                sb.Append(CurrPK + ",");
                sb.Append("                                          INV.INVOICE_DATE),");
                sb.Append("                              2) DEBIT,");
                sb.Append("                        0 CREDIT,");
                sb.Append("                        0 BALANCE,");
                sb.Append("                        (SELECT DISTINCT TO_CHAR(TO_DATE(SYSDATE,");
                sb.Append("                                                         'DD/MM/YYYY') -");
                sb.Append("                                                 TO_DATE(INV.INVOICE_DATE)) OUTSTANDING_DAYS");
                sb.Append("                           FROM " + ParentDB + ".COLLECTIONS_TRN_TBL COLL,");
                sb.Append("                                " + ParentDB + ".CONSOL_INVOICE_TBL  INV1");
                sb.Append("                          WHERE INV1.INVOICE_REF_NO = COLL.INVOICE_REF_NR(+)");
                sb.Append("                            AND INV1.INVOICE_REF_NO = INV.INVOICE_REF_NO");
                sb.Append("                            AND NVL((INV1.NET_RECEIVABLE -");
                sb.Append("                                    NVL((SELECT SUM(CTRN.RECD_AMOUNT_HDR_CURR)");
                sb.Append("                                           FROM " + ParentDB + ".COLLECTIONS_TRN_TBL CTRN");
                sb.Append("                                          WHERE CTRN.INVOICE_REF_NR LIKE");
                sb.Append("                                                INV1.INVOICE_REF_NO),");
                sb.Append("                                         0.00)),");
                sb.Append("                                    0) > 0)");
                sb.Append("          FROM " + ParentDB + ".TRANSPORT_INST_SEA_TBL TIST  ,");
                sb.Append("               " + ParentDB + ".CUSTOMER_MST_TBL       CMT,");
                sb.Append("               " + ParentDB + ".CUSTOMER_CONTACT_DTLS  CCD,");
                sb.Append("               " + ParentDB + ".LOCATION_MST_TBL       LMT,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TBL     INV,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TRN_TBL INVTRN");
                sb.Append("         WHERE INVTRN.JOB_CARD_FK = TIST.TRANSPORT_INST_SEA_PK(+)");
                sb.Append("           AND INV.IS_FAC_INV <> 1");
                sb.Append("           AND INV.CUSTOMER_MST_FK = CMT.CUSTOMER_MST_PK(+)");
                sb.Append("           AND INV.PROCESS_TYPE = " + ProcessType);
                sb.Append("           AND INV.CONSOL_INVOICE_PK = INVTRN.CONSOL_INVOICE_FK");
                sb.Append("           AND CMT.CUSTOMER_MST_PK = CCD.CUSTOMER_MST_FK");
                sb.Append("           AND LMT.LOCATION_MST_PK = CCD.ADM_LOCATION_MST_FK");
                sb.Append("           AND NVL((SELECT SUM(WMT.WRITEOFF_AMOUNT)");
                sb.Append("                     FROM " + ParentDB + ".WRITEOFF_MANUAL_TBL WMT");
                sb.Append("                    WHERE WMT.CONSOL_INVOICE_FK = INV.CONSOL_INVOICE_PK),");
                sb.Append("                   0.00) = 0");
                if (Fromdate != "" & Todate != "")
                {
                    sb.Append("           AND INV.invoice_date BETWEEN TO_DATE('" + Fromdate + "') AND");
                    sb.Append("               TO_DATE('" + Todate + "')");
                }
                sb.Append("          AND TIST.PROCESS_TYPE =" + ProcessType);
                if (BizType != 3)
                {
                    sb.Append("          AND TIST.BUSINESS_TYPE =" + BizType);
                }

                if (CargoType != 0)
                {
                    sb.Append("     AND TIST.CARGO_TYPE=" + CargoType);
                }

                if (!string.IsNullOrEmpty(CountryPK) & CountryPK != "0")
                {
                    sb.Append("       AND LMT.COUNTRY_MST_FK IN(" + CountryPK + ")");
                }

                if (!string.IsNullOrEmpty(LocPK) & LocPK != "0")
                {
                    sb.Append("        AND LMT.LOCATION_MST_PK IN (" + LocPK + ") ");
                }

                if (!string.IsNullOrEmpty(CustPK) & CustPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustPK + ")");
                }

                if (!string.IsNullOrEmpty(CustGroupPK) & CustGroupPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustGroupPK + ")");
                }

                if (BizType == 1)
                {
                    if (!string.IsNullOrEmpty(FlightID))
                    {
                        sb.Append("     AND   TIST.FLIGHT_NO IN (" + FlightID + ")");
                    }
                }
                else if (BizType == 2)
                {
                    if (!string.IsNullOrEmpty(VslVoyPK))
                    {
                        sb.Append("     AND   TIST.VSL_VOY_FK=" + VslVoyPK + "");
                    }
                }
                sb.Append("           AND INVTRN.JOB_TYPE = 3");
                sb.Append("           AND INV.CHK_INVOICE<>2 ");
                // '       ----DET & DEM-TPT (Invoice)-
                sb.Append("        UNION");
                sb.Append("           SELECT DISTINCT CMT.CUSTOMER_MST_PK,");
                sb.Append("                        CCD.ADM_LOCATION_MST_FK,");
                sb.Append("                        INV.INVOICE_DATE REF_DATE,");
                sb.Append("                        (CASE");
                sb.Append("                          WHEN INV.PROCESS_TYPE = 1 THEN");
                sb.Append("                           'EXPORT'");
                sb.Append("                          ELSE");
                sb.Append("                           'IMPORT'");
                sb.Append("                        END) PROCESS,");
                sb.Append("                        'INVOICE' TRANSACTION,");
                sb.Append("                        (SELECT CONT.BL_NUMBER FROM " + ParentDB + ".TRANSPORT_TRN_CONT CONT WHERE CONT.TRANSPORT_INST_FK = TIST.TRANSPORT_INST_SEA_PK AND ROWNUM=1) HBL_REF_NO,");
                sb.Append("                        INV.INVOICE_REF_NO DOCREFNR,");
                sb.Append("                        INV.CONSOL_INVOICE_PK REF_PK,");
                sb.Append("                        INV.BUSINESS_TYPE BIZ_TYPE,");
                sb.Append("                        DCH.CARGO_TYPE CARGO_TYPE,");
                sb.Append("                        ROUND(INV.NET_RECEIVABLE *");
                sb.Append("                              " + ParentDB + ".GET_EX_RATE(INV.CURRENCY_MST_FK,");
                sb.Append(CurrPK + ",");
                sb.Append("                                          INV.INVOICE_DATE),");
                sb.Append("                              2) DEBIT,");
                sb.Append("                        0 CREDIT,");
                sb.Append("                        0 BALANCE,");
                sb.Append("                        (SELECT DISTINCT TO_CHAR(TO_DATE(SYSDATE,");
                sb.Append("                                                         'DD/MM/YYYY') -");
                sb.Append("                                                 TO_DATE(INV.INVOICE_DATE)) OUTSTANDING_DAYS");
                sb.Append("                           FROM " + ParentDB + ".COLLECTIONS_TRN_TBL COLL,");
                sb.Append("                                " + ParentDB + ".CONSOL_INVOICE_TBL  INV1");
                sb.Append("                          WHERE INV1.INVOICE_REF_NO = COLL.INVOICE_REF_NR(+)");
                sb.Append("                            AND INV1.INVOICE_REF_NO = INV.INVOICE_REF_NO");
                sb.Append("                            AND NVL((INV1.NET_RECEIVABLE -");
                sb.Append("                                    NVL((SELECT SUM(CTRN.RECD_AMOUNT_HDR_CURR)");
                sb.Append("                                           FROM " + ParentDB + ".COLLECTIONS_TRN_TBL CTRN");
                sb.Append("                                          WHERE CTRN.INVOICE_REF_NR LIKE");
                sb.Append("                                                INV1.INVOICE_REF_NO),");
                sb.Append("                                         0.00)),");
                sb.Append("                                    0) > 0)");
                sb.Append("          FROM " + ParentDB + ".DEM_CALC_HDR DCH  ,");
                sb.Append("               " + ParentDB + ".TRANSPORT_INST_SEA_TBL TIST,");
                sb.Append("               " + ParentDB + ".CUSTOMER_MST_TBL       CMT,");
                sb.Append("               " + ParentDB + ".CUSTOMER_CONTACT_DTLS  CCD,");
                sb.Append("               " + ParentDB + ".LOCATION_MST_TBL       LMT,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TBL     INV,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TRN_TBL INVTRN");
                sb.Append("         WHERE INVTRN.JOB_CARD_FK = DCH.DEM_CALC_HDR_PK(+)");
                sb.Append("           AND DCH.DOC_REF_FK = TIST.TRANSPORT_INST_SEA_PK");
                sb.Append("           AND INV.IS_FAC_INV <> 1");
                sb.Append("           AND INV.CUSTOMER_MST_FK = CMT.CUSTOMER_MST_PK(+)");
                sb.Append("           AND INV.PROCESS_TYPE = " + ProcessType);
                sb.Append("           AND INV.CONSOL_INVOICE_PK = INVTRN.CONSOL_INVOICE_FK");
                sb.Append("           AND CMT.CUSTOMER_MST_PK = CCD.CUSTOMER_MST_FK");
                sb.Append("           AND LMT.LOCATION_MST_PK = CCD.ADM_LOCATION_MST_FK");
                sb.Append("           AND NVL((SELECT SUM(WMT.WRITEOFF_AMOUNT)");
                sb.Append("                     FROM " + ParentDB + ".WRITEOFF_MANUAL_TBL WMT");
                sb.Append("                    WHERE WMT.CONSOL_INVOICE_FK = INV.CONSOL_INVOICE_PK),");
                sb.Append("                   0.00) = 0");
                sb.Append("          AND DCH.PROCESS_TYPE =" + ProcessType);
                if (BizType != 3)
                {
                    sb.Append("          AND DCH.Biz_Type =" + BizType);
                }

                if (CargoType != 0)
                {
                    sb.Append("     AND DCH.CARGO_TYPE=" + CargoType);
                }

                if (BizType == 1)
                {
                    if (!string.IsNullOrEmpty(FlightID))
                    {
                        sb.Append("     AND   TIST.FLIGHT_NO IN (" + FlightID + ")");
                    }
                }
                else if (BizType == 2)
                {
                    if (!string.IsNullOrEmpty(VslVoyPK))
                    {
                        sb.Append("     AND   TIST.VSL_VOY_FK=" + VslVoyPK + "");
                    }
                }
                if (Fromdate != "" & Todate != "")
                {
                    sb.Append("       AND INV.invoice_date BETWEEN TO_DATE('" + Fromdate + "') AND");
                    sb.Append("               TO_DATE('" + Todate + "')");
                }
                if (!string.IsNullOrEmpty(CountryPK) & CountryPK != "0")
                {
                    sb.Append("       AND LMT.COUNTRY_MST_FK IN(" + CountryPK + ")");
                }

                if (!string.IsNullOrEmpty(LocPK) & LocPK != "0")
                {
                    sb.Append("        AND LMT.LOCATION_MST_PK IN (" + LocPK + ") ");
                }

                if (!string.IsNullOrEmpty(CustPK) & CustPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustPK + ")");
                }

                if (!string.IsNullOrEmpty(CustGroupPK) & CustGroupPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustGroupPK + ")");
                }

                sb.Append("           AND INVTRN.JOB_TYPE = 3");
                sb.Append("           AND INV.CHK_INVOICE<>2 ");
                // '       ----DET & DEM-CBJC (Invoice)-
                sb.Append("        UNION");
                sb.Append("           SELECT DISTINCT CMT.CUSTOMER_MST_PK,");
                sb.Append("                        CCD.ADM_LOCATION_MST_FK,");
                sb.Append("                        INV.INVOICE_DATE REF_DATE,");
                sb.Append("                        (CASE");
                sb.Append("                          WHEN INV.PROCESS_TYPE = 1 THEN");
                sb.Append("                           'EXPORT'");
                sb.Append("                          ELSE");
                sb.Append("                           'IMPORT'");
                sb.Append("                        END) PROCESS,");
                sb.Append("                        'INVOICE' TRANSACTION,");
                sb.Append("                        CBJC.HBL_NO HBL_REF_NO,");
                sb.Append("                        INV.INVOICE_REF_NO DOCREFNR,");
                sb.Append("                        INV.CONSOL_INVOICE_PK REF_PK,");
                sb.Append("                        INV.BUSINESS_TYPE BIZ_TYPE,");
                sb.Append("                        DCH.CARGO_TYPE CARGO_TYPE,");
                sb.Append("                        ROUND(INV.NET_RECEIVABLE *");
                sb.Append("                              " + ParentDB + ".GET_EX_RATE(INV.CURRENCY_MST_FK,");
                sb.Append(CurrPK + ",");
                sb.Append("                                          INV.INVOICE_DATE),");
                sb.Append("                              2) DEBIT,");
                sb.Append("                        0 CREDIT,");
                sb.Append("                        0 BALANCE,");
                sb.Append("                        (SELECT DISTINCT TO_CHAR(TO_DATE(SYSDATE,");
                sb.Append("                                                         'DD/MM/YYYY') -");
                sb.Append("                                                 TO_DATE(INV.INVOICE_DATE)) OUTSTANDING_DAYS");
                sb.Append("                           FROM " + ParentDB + ".COLLECTIONS_TRN_TBL COLL,");
                sb.Append("                                " + ParentDB + ".CONSOL_INVOICE_TBL  INV1");
                sb.Append("                          WHERE INV1.INVOICE_REF_NO = COLL.INVOICE_REF_NR(+)");
                sb.Append("                            AND INV1.INVOICE_REF_NO = INV.INVOICE_REF_NO");
                sb.Append("                            AND NVL((INV1.NET_RECEIVABLE -");
                sb.Append("                                    NVL((SELECT SUM(CTRN.RECD_AMOUNT_HDR_CURR)");
                sb.Append("                                           FROM " + ParentDB + ".COLLECTIONS_TRN_TBL CTRN");
                sb.Append("                                          WHERE CTRN.INVOICE_REF_NR LIKE");
                sb.Append("                                                INV1.INVOICE_REF_NO),");
                sb.Append("                                         0.00)),");
                sb.Append("                                    0) > 0)");
                sb.Append("          FROM " + ParentDB + ".DEM_CALC_HDR DCH  ,");
                sb.Append("               " + ParentDB + ".CBJC_TBL               CBJC,");
                sb.Append("               " + ParentDB + ".CUSTOMER_MST_TBL       CMT,");
                sb.Append("               " + ParentDB + ".CUSTOMER_CONTACT_DTLS  CCD,");
                sb.Append("               " + ParentDB + ".LOCATION_MST_TBL       LMT,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TBL     INV,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TRN_TBL INVTRN");
                sb.Append("         WHERE INVTRN.JOB_CARD_FK = DCH.DEM_CALC_HDR_PK(+)");
                sb.Append("           AND DCH.DOC_REF_FK = CBJC.CBJC_PK");
                sb.Append("           AND INV.IS_FAC_INV <> 1");
                sb.Append("           AND INV.CUSTOMER_MST_FK = CMT.CUSTOMER_MST_PK(+)");
                sb.Append("           AND INV.PROCESS_TYPE = " + ProcessType);
                sb.Append("           AND INV.CONSOL_INVOICE_PK = INVTRN.CONSOL_INVOICE_FK");
                sb.Append("           AND CMT.CUSTOMER_MST_PK = CCD.CUSTOMER_MST_FK");
                sb.Append("           AND LMT.LOCATION_MST_PK = CCD.ADM_LOCATION_MST_FK");
                sb.Append("           AND NVL((SELECT SUM(WMT.WRITEOFF_AMOUNT)");
                sb.Append("                     FROM " + ParentDB + ".WRITEOFF_MANUAL_TBL WMT");
                sb.Append("                    WHERE WMT.CONSOL_INVOICE_FK = INV.CONSOL_INVOICE_PK),");
                sb.Append("                   0.00) = 0");
                sb.Append("          AND DCH.PROCESS_TYPE =" + ProcessType);
                if (BizType != 3)
                {
                    sb.Append("          AND DCH.Biz_Type =" + BizType);
                }

                if (CargoType != 0)
                {
                    sb.Append("     AND DCH.CARGO_TYPE=" + CargoType);
                }

                if (BizType == 1)
                {
                    if (!string.IsNullOrEmpty(FlightID))
                    {
                        sb.Append("         AND   CBJC.FLIGHT_NO IN (" + FlightID + ")");
                    }
                }
                else if (BizType == 2)
                {
                    if (!string.IsNullOrEmpty(VslVoyPK))
                    {
                        sb.Append("         AND   CBJC.VOYAGE_TRN_FK=" + VslVoyPK + "");
                    }
                }
                if (Fromdate != "" & Todate != "")
                {
                    sb.Append("       AND INV.invoice_date BETWEEN TO_DATE('" + Fromdate + "') AND");
                    sb.Append("               TO_DATE('" + Todate + "')");
                }
                if (!string.IsNullOrEmpty(CountryPK) & CountryPK != "0")
                {
                    sb.Append("       AND LMT.COUNTRY_MST_FK IN(" + CountryPK + ")");
                }

                if (!string.IsNullOrEmpty(LocPK) & LocPK != "0")
                {
                    sb.Append("        AND LMT.LOCATION_MST_PK IN (" + LocPK + ") ");
                }

                if (!string.IsNullOrEmpty(CustPK) & CustPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustPK + ")");
                }

                if (!string.IsNullOrEmpty(CustGroupPK) & CustGroupPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustGroupPK + ")");
                }

                sb.Append("           AND INVTRN.JOB_TYPE = 4");
                sb.Append("           AND INV.CHK_INVOICE<>2 ");
                // '          ---CBJC(Collection)---------'
                sb.Append("              UNION");
                sb.Append("              SELECT DISTINCT CMT.CUSTOMER_MST_PK,");
                sb.Append("                        CCD.ADM_LOCATION_MST_FK,");
                sb.Append("                        COL.COLLECTIONS_DATE REF_DATE,");
                sb.Append("                        (CASE");
                sb.Append("                          WHEN COL.PROCESS_TYPE = 1 THEN");
                sb.Append("                           'EXPORT'");
                sb.Append("                          ELSE");
                sb.Append("                           'IMPORT'");
                sb.Append("                        END) PROCESS,");
                sb.Append("                        'COLLECTION' TRANSACTION,");
                sb.Append("                        CBJC.HBL_NO HBL_REF_NO,");
                sb.Append("                        COL.COLLECTIONS_REF_NO DOCREFNR,");
                sb.Append("                        COL.COLLECTIONS_TBL_PK REF_PK,");
                sb.Append("                        COL.BUSINESS_TYPE BIZ_TYPE,");
                sb.Append("                        CBJC.CARGO_TYPE CARGO_TYPE,");
                sb.Append("                        0 DEBIT,");
                sb.Append("                        ROUND(CLT.RECD_AMOUNT_HDR_CURR *");
                sb.Append("                              " + ParentDB + ".GET_EX_RATE(COL.CURRENCY_MST_FK,");
                sb.Append(CurrPK + ",");
                sb.Append("                                          COL.COLLECTIONS_DATE),");
                sb.Append("                              2) CREDIT,");
                sb.Append("                        0 BALANCE,");
                sb.Append("                        '' OUTSTANDING_DAYS");
                sb.Append("          FROM ");
                sb.Append("               " + ParentDB + ".CBJC_TBL               CBJC,");
                sb.Append("               " + ParentDB + ".CUSTOMER_MST_TBL       CMT,");
                sb.Append("               " + ParentDB + ".CUSTOMER_CONTACT_DTLS  CCD,");
                sb.Append("               " + ParentDB + ".LOCATION_MST_TBL       LMT,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TBL     INV,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TRN_TBL INVTRN,");
                sb.Append("               " + ParentDB + ".COLLECTIONS_TRN_TBL    CLT,");
                sb.Append("               " + ParentDB + ".COLLECTIONS_TBL        COL");
                sb.Append("         WHERE INVTRN.JOB_CARD_FK = CBJC.CBJC_PK(+)          ");
                sb.Append("           AND INV.IS_FAC_INV <> 1");
                sb.Append("           AND INV.CUSTOMER_MST_FK = CMT.CUSTOMER_MST_PK");
                sb.Append("           AND COL.PROCESS_TYPE = " + ProcessType);
                sb.Append("           AND INV.CONSOL_INVOICE_PK = INVTRN.CONSOL_INVOICE_FK");
                sb.Append("           AND CLT.INVOICE_REF_NR(+) = INV.INVOICE_REF_NO");
                sb.Append("           AND COL.COLLECTIONS_TBL_PK(+) = CLT.COLLECTIONS_TBL_FK");
                sb.Append("           AND CMT.CUSTOMER_MST_PK = CCD.CUSTOMER_MST_FK");
                sb.Append("           AND LMT.LOCATION_MST_PK = CCD.ADM_LOCATION_MST_FK");
                sb.Append("           AND NVL((SELECT SUM(WMT.WRITEOFF_AMOUNT)");
                sb.Append("                     FROM " + ParentDB + ".WRITEOFF_MANUAL_TBL WMT");
                sb.Append("                    WHERE WMT.CONSOL_INVOICE_FK = INV.CONSOL_INVOICE_PK),");
                sb.Append("                   0.00) = 0");
                if (Fromdate != "" & Todate != "")
                {
                    sb.Append("           AND COL.COLLECTIONS_DATE  BETWEEN TO_DATE('" + Fromdate + "') AND");
                    sb.Append("               TO_DATE('" + Todate + "')");
                }
                sb.Append("          AND CBJC.PROCESS_TYPE =" + ProcessType);
                if (BizType != 3)
                {
                    sb.Append("          AND CBJC.BIZ_TYPE =" + BizType);
                }

                if (CargoType != 0)
                {
                    sb.Append("     AND CBJC.CARGO_TYPE=" + CargoType);
                }

                if (!string.IsNullOrEmpty(CountryPK) & CountryPK != "0")
                {
                    sb.Append("       AND LMT.COUNTRY_MST_FK IN(" + CountryPK + ")");
                }

                if (!string.IsNullOrEmpty(LocPK) & LocPK != "0")
                {
                    sb.Append("        AND LMT.LOCATION_MST_PK IN (" + LocPK + ") ");
                }

                if (!string.IsNullOrEmpty(CustPK) & CustPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustPK + ")");
                }

                if (!string.IsNullOrEmpty(CustGroupPK) & CustGroupPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustGroupPK + ")");
                }

                if (BizType == 1)
                {
                    if (!string.IsNullOrEmpty(FlightID))
                    {
                        sb.Append("     AND   CBJC.FLIGHT_NO IN (" + FlightID + ")");
                    }
                }
                else if (BizType == 2)
                {
                    if (!string.IsNullOrEmpty(VslVoyPK))
                    {
                        sb.Append("     AND   CBJC.VOYAGE_TRN_FK=" + VslVoyPK + "");
                    }
                }
                sb.Append("               AND INVTRN.JOB_TYPE = 2");
                sb.Append("           AND INV.CHK_INVOICE<>2 ");
                // '              ----TPT-(Collection)---------'
                sb.Append("        UNION");
                sb.Append("              SELECT DISTINCT CMT.CUSTOMER_MST_PK,");
                sb.Append("                        CCD.ADM_LOCATION_MST_FK,");
                sb.Append("                        COL.COLLECTIONS_DATE REF_DATE,");
                sb.Append("                        (CASE");
                sb.Append("                          WHEN COL.PROCESS_TYPE = 1 THEN");
                sb.Append("                           'EXPORT'");
                sb.Append("                          ELSE");
                sb.Append("                           'IMPORT'");
                sb.Append("                        END) PROCESS,");
                sb.Append("                        'COLLECTION' TRANSACTION,");
                sb.Append("                        (SELECT CON.BL_NUMBER FROM " + ParentDB + ".TRANSPORT_TRN_CONT CON WHERE CON.TRANSPORT_INST_FK = TIST.TRANSPORT_INST_SEA_PK AND ROWNUM=1) HBL_REF_NO,");
                sb.Append("                        COL.COLLECTIONS_REF_NO DOCREFNR,");
                sb.Append("                        COL.COLLECTIONS_TBL_PK REF_PK,");
                sb.Append("                        COL.BUSINESS_TYPE BIZ_TYPE,");
                sb.Append("                        TIST.CARGO_TYPE CARGO_TYPE,");
                sb.Append("                        0 DEBIT,");
                sb.Append("                        ROUND(CLT.RECD_AMOUNT_HDR_CURR *");
                sb.Append("                              " + ParentDB + ".GET_EX_RATE(COL.CURRENCY_MST_FK,");
                sb.Append(CurrPK + ",");
                sb.Append("                                          COL.COLLECTIONS_DATE),");
                sb.Append("                              2) CREDIT,");
                sb.Append("                        0 BALANCE,");
                sb.Append("                        '' OUTSTANDING_DAYS");
                sb.Append("          FROM " + ParentDB + ".TRANSPORT_INST_SEA_TBL TIST ,");
                sb.Append("               " + ParentDB + ".CUSTOMER_MST_TBL       CMT,");
                sb.Append("               " + ParentDB + ".CUSTOMER_CONTACT_DTLS  CCD,");
                sb.Append("               " + ParentDB + ".LOCATION_MST_TBL       LMT,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TBL     INV,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TRN_TBL INVTRN,");
                sb.Append("               " + ParentDB + ".COLLECTIONS_TRN_TBL    CLT,");
                sb.Append("               " + ParentDB + ".COLLECTIONS_TBL        COL");
                sb.Append("         WHERE INVTRN.JOB_CARD_FK = TIST.TRANSPORT_INST_SEA_PK(+)");
                sb.Append("           AND INV.IS_FAC_INV <> 1");
                sb.Append("           AND INV.CUSTOMER_MST_FK = CMT.CUSTOMER_MST_PK");
                sb.Append("           AND COL.PROCESS_TYPE = " + ProcessType);
                sb.Append("           AND INV.CONSOL_INVOICE_PK = INVTRN.CONSOL_INVOICE_FK");
                sb.Append("           AND CLT.INVOICE_REF_NR(+) = INV.INVOICE_REF_NO");
                sb.Append("           AND COL.COLLECTIONS_TBL_PK(+) = CLT.COLLECTIONS_TBL_FK");
                sb.Append("           AND CMT.CUSTOMER_MST_PK = CCD.CUSTOMER_MST_FK");
                sb.Append("           AND LMT.LOCATION_MST_PK = CCD.ADM_LOCATION_MST_FK");
                sb.Append("           AND NVL((SELECT SUM(WMT.WRITEOFF_AMOUNT)");
                sb.Append("                     FROM " + ParentDB + ".WRITEOFF_MANUAL_TBL WMT");
                sb.Append("                    WHERE WMT.CONSOL_INVOICE_FK = INV.CONSOL_INVOICE_PK),");
                sb.Append("                   0.00) = 0");
                if (CargoType != 0)
                {
                    sb.Append("     AND TIST.CARGO_TYPE=" + CargoType);
                }

                if (Fromdate != "" & Todate != "")
                {
                    sb.Append("           AND COL.COLLECTIONS_DATE BETWEEN TO_DATE('" + Fromdate + "') AND");
                    sb.Append("               TO_DATE('" + Todate + "')");
                }
                sb.Append("          AND TIST.PROCESS_TYPE =" + ProcessType);
                if (BizType != 3)
                {
                    sb.Append("          AND TIST.BUSINESS_TYPE =" + BizType);
                }

                if (!string.IsNullOrEmpty(CountryPK) & CountryPK != "0")
                {
                    sb.Append("       AND LMT.COUNTRY_MST_FK IN(" + CountryPK + ")");
                }

                if (!string.IsNullOrEmpty(LocPK) & LocPK != "0")
                {
                    sb.Append("        AND LMT.LOCATION_MST_PK IN (" + LocPK + ") ");
                }

                if (!string.IsNullOrEmpty(CustPK) & CustPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustPK + ")");
                }

                if (!string.IsNullOrEmpty(CustGroupPK) & CustGroupPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustGroupPK + ")");
                }

                if (BizType == 1)
                {
                    if (!string.IsNullOrEmpty(FlightID))
                    {
                        sb.Append("     AND   TIST.FLIGHT_NO IN (" + FlightID + ")");
                    }
                }
                else if (BizType == 2)
                {
                    if (!string.IsNullOrEmpty(VslVoyPK))
                    {
                        sb.Append("     AND   TIST.VSL_VOY_FK=" + VslVoyPK + "");
                    }
                }
                sb.Append("               AND INVTRN.JOB_TYPE = 3");
                sb.Append("           AND INV.CHK_INVOICE<>2 ");
                // '         ---Det & DEM TPT (Collection)'
                sb.Append("               UNION");
                sb.Append("               SELECT DISTINCT CMT.CUSTOMER_MST_PK,");
                sb.Append("                        CCD.ADM_LOCATION_MST_FK,");
                sb.Append("                        COL.COLLECTIONS_DATE REF_DATE,");
                sb.Append("                        (CASE");
                sb.Append("                          WHEN COL.PROCESS_TYPE = 1 THEN");
                sb.Append("                           'EXPORT'");
                sb.Append("                          ELSE");
                sb.Append("                           'IMPORT'");
                sb.Append("                        END) PROCESS,");
                sb.Append("                        'COLLECTION' TRANSACTION,");
                sb.Append("                        (SELECT CON.BL_NUMBER FROM " + ParentDB + ".TRANSPORT_TRN_CONT CON WHERE CON.TRANSPORT_INST_FK = TIST.TRANSPORT_INST_SEA_PK AND ROWNUM=1) HBL_REF_NO,");
                sb.Append("                        COL.COLLECTIONS_REF_NO DOCREFNR,");
                sb.Append("                        COL.COLLECTIONS_TBL_PK REF_PK,");
                sb.Append("                        COL.BUSINESS_TYPE BIZ_TYPE,");
                sb.Append("                        DCH.CARGO_TYPE CARGO_TYPE,");
                sb.Append("                        0 DEBIT,");
                sb.Append("                        ROUND(CLT.RECD_AMOUNT_HDR_CURR *");
                sb.Append("                              " + ParentDB + ".GET_EX_RATE(COL.CURRENCY_MST_FK,");
                sb.Append(CurrPK + ",");
                sb.Append("                                          COL.COLLECTIONS_DATE),");
                sb.Append("                              2) CREDIT,");
                sb.Append("                        0 BALANCE,");
                sb.Append("                        '' OUTSTANDING_DAYS");
                sb.Append("          FROM " + ParentDB + ".DEM_CALC_HDR DCH  ,");
                sb.Append("               " + ParentDB + ".TRANSPORT_INST_SEA_TBL TIST,");
                sb.Append("               " + ParentDB + ".CUSTOMER_MST_TBL       CMT,");
                sb.Append("               " + ParentDB + ".CUSTOMER_CONTACT_DTLS  CCD,");
                sb.Append("               " + ParentDB + ".LOCATION_MST_TBL       LMT,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TBL     INV,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TRN_TBL INVTRN,");
                sb.Append("               " + ParentDB + ".COLLECTIONS_TRN_TBL    CLT,");
                sb.Append("               " + ParentDB + ".COLLECTIONS_TBL        COL");
                sb.Append("         WHERE INVTRN.JOB_CARD_FK = DCH.DEM_CALC_HDR_PK(+)");
                sb.Append("           AND DCH.DOC_REF_FK =  TIST.TRANSPORT_INST_SEA_PK");
                sb.Append("           AND INV.IS_FAC_INV <> 1");
                sb.Append("           AND INV.CUSTOMER_MST_FK = CMT.CUSTOMER_MST_PK");
                sb.Append("           AND COL.PROCESS_TYPE = " + ProcessType);
                sb.Append("           AND INV.CONSOL_INVOICE_PK = INVTRN.CONSOL_INVOICE_FK");
                sb.Append("           AND CLT.INVOICE_REF_NR(+) = INV.INVOICE_REF_NO");
                sb.Append("           AND COL.COLLECTIONS_TBL_PK(+) = CLT.COLLECTIONS_TBL_FK");
                sb.Append("           AND CMT.CUSTOMER_MST_PK = CCD.CUSTOMER_MST_FK");
                sb.Append("           AND LMT.LOCATION_MST_PK = CCD.ADM_LOCATION_MST_FK");
                sb.Append("           AND NVL((SELECT SUM(WMT.WRITEOFF_AMOUNT)");
                sb.Append("                     FROM " + ParentDB + ".WRITEOFF_MANUAL_TBL WMT");
                sb.Append("                    WHERE WMT.CONSOL_INVOICE_FK = INV.CONSOL_INVOICE_PK),");
                sb.Append("                   0.00) = 0");
                sb.Append("          AND DCH.PROCESS_TYPE =" + ProcessType);
                sb.Append("               AND INVTRN.JOB_TYPE = 3");
                if (BizType != 3)
                {
                    sb.Append("          AND DCH.biz_type =" + BizType);
                }

                if (BizType == 1)
                {
                    if (!string.IsNullOrEmpty(FlightID))
                    {
                        sb.Append("     AND   TIST.FLIGHT_NO IN (" + FlightID + ")");
                    }
                }
                else if (BizType == 2)
                {
                    if (!string.IsNullOrEmpty(VslVoyPK))
                    {
                        sb.Append("     AND   TIST.VSL_VOY_FK=" + VslVoyPK + "");
                    }
                }
                if (Fromdate != "" & Todate != "")
                {
                    sb.Append("       AND  COL.COLLECTIONS_DATE  BETWEEN TO_DATE('" + Fromdate + "') AND");
                    sb.Append("               TO_DATE('" + Todate + "')");
                }
                if (!string.IsNullOrEmpty(CountryPK) & CountryPK != "0")
                {
                    sb.Append("       AND LMT.COUNTRY_MST_FK IN(" + CountryPK + ")");
                }

                if (!string.IsNullOrEmpty(LocPK) & LocPK != "0")
                {
                    sb.Append("        AND LMT.LOCATION_MST_PK IN (" + LocPK + ") ");
                }

                if (!string.IsNullOrEmpty(CustPK) & CustPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustPK + ")");
                }

                if (!string.IsNullOrEmpty(CustGroupPK) & CustGroupPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustGroupPK + ")");
                }

                if (CargoType != 0)
                {
                    sb.Append("     AND DCH.CARGO_TYPE=" + CargoType);
                }

                sb.Append("               AND INVTRN.JOB_TYPE = 3");
                sb.Append("           AND INV.CHK_INVOICE<>2 ");
                // '         ---Det & DEM CBJC (Collection)'
                sb.Append("               UNION");
                sb.Append("               SELECT DISTINCT CMT.CUSTOMER_MST_PK,");
                sb.Append("                        CCD.ADM_LOCATION_MST_FK,");
                sb.Append("                        COL.COLLECTIONS_DATE REF_DATE,");
                sb.Append("                        (CASE");
                sb.Append("                          WHEN COL.PROCESS_TYPE = 1 THEN");
                sb.Append("                           'EXPORT'");
                sb.Append("                          ELSE");
                sb.Append("                           'IMPORT'");
                sb.Append("                        END) PROCESS,");
                sb.Append("                        'COLLECTION' TRANSACTION,");
                sb.Append("                        CBJC.HBL_NO HBL_REF_NO,");
                sb.Append("                        COL.COLLECTIONS_REF_NO DOCREFNR,");
                sb.Append("                        COL.COLLECTIONS_TBL_PK REF_PK,");
                sb.Append("                        COL.BUSINESS_TYPE BIZ_TYPE,");
                sb.Append("                        DCH.CARGO_TYPE CARGO_TYPE,");
                sb.Append("                        0 DEBIT,");
                sb.Append("                        ROUND(CLT.RECD_AMOUNT_HDR_CURR *");
                sb.Append("                              " + ParentDB + ".GET_EX_RATE(COL.CURRENCY_MST_FK,");
                sb.Append(CurrPK + ",");
                sb.Append("                                          COL.COLLECTIONS_DATE),");
                sb.Append("                              2) CREDIT,");
                sb.Append("                        0 BALANCE,");
                sb.Append("                        '' OUTSTANDING_DAYS");
                sb.Append("          FROM " + ParentDB + ".DEM_CALC_HDR DCH  ,");
                sb.Append("               " + ParentDB + ".CBJC_TBL               CBJC,");
                sb.Append("               " + ParentDB + ".CUSTOMER_MST_TBL       CMT,");
                sb.Append("               " + ParentDB + ".CUSTOMER_CONTACT_DTLS  CCD,");
                sb.Append("               " + ParentDB + ".LOCATION_MST_TBL       LMT,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TBL     INV,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TRN_TBL INVTRN,");
                sb.Append("               " + ParentDB + ".COLLECTIONS_TRN_TBL    CLT,");
                sb.Append("               " + ParentDB + ".COLLECTIONS_TBL        COL");
                sb.Append("         WHERE INVTRN.JOB_CARD_FK = DCH.DEM_CALC_HDR_PK(+)");
                sb.Append("           AND DCH.DOC_REF_FK = CBJC.CBJC_PK");
                sb.Append("           AND INV.IS_FAC_INV <> 1");
                sb.Append("           AND INV.CUSTOMER_MST_FK = CMT.CUSTOMER_MST_PK");
                sb.Append("           AND COL.PROCESS_TYPE = " + ProcessType);
                sb.Append("           AND INV.CONSOL_INVOICE_PK = INVTRN.CONSOL_INVOICE_FK");
                sb.Append("           AND CLT.INVOICE_REF_NR(+) = INV.INVOICE_REF_NO");
                sb.Append("           AND COL.COLLECTIONS_TBL_PK(+) = CLT.COLLECTIONS_TBL_FK");
                sb.Append("           AND CMT.CUSTOMER_MST_PK = CCD.CUSTOMER_MST_FK");
                sb.Append("           AND LMT.LOCATION_MST_PK = CCD.ADM_LOCATION_MST_FK");
                sb.Append("           AND NVL((SELECT SUM(WMT.WRITEOFF_AMOUNT)");
                sb.Append("                     FROM " + ParentDB + ".WRITEOFF_MANUAL_TBL WMT");
                sb.Append("                    WHERE WMT.CONSOL_INVOICE_FK = INV.CONSOL_INVOICE_PK),");
                sb.Append("                   0.00) = 0");
                sb.Append("          AND DCH.PROCESS_TYPE =" + ProcessType);
                if (BizType != 3)
                {
                    sb.Append("          AND DCH.biz_type =" + BizType);
                }

                if (BizType == 1)
                {
                    if (!string.IsNullOrEmpty(FlightID))
                    {
                        sb.Append("         AND   CBJC.FLIGHT_NO IN (" + FlightID + ")");
                    }
                }
                else if (BizType == 2)
                {
                    if (!string.IsNullOrEmpty(VslVoyPK))
                    {
                        sb.Append("         AND   CBJC.VOYAGE_TRN_FK=" + VslVoyPK + "");
                    }
                }
                if (CargoType != 0)
                {
                    sb.Append("     AND DCH.CARGO_TYPE=" + CargoType);
                }

                if (Fromdate != "" & Todate != "")
                {
                    sb.Append("       AND  COL.COLLECTIONS_DATE  BETWEEN TO_DATE('" + Fromdate + "') AND");
                    sb.Append("               TO_DATE('" + Todate + "')");
                }
                if (!string.IsNullOrEmpty(CountryPK) & CountryPK != "0")
                {
                    sb.Append("       AND LMT.COUNTRY_MST_FK IN(" + CountryPK + ")");
                }

                if (!string.IsNullOrEmpty(LocPK) & LocPK != "0")
                {
                    sb.Append("        AND LMT.LOCATION_MST_PK IN (" + LocPK + ") ");
                }

                if (!string.IsNullOrEmpty(CustPK) & CustPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustPK + ")");
                }

                if (!string.IsNullOrEmpty(CustGroupPK) & CustGroupPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustGroupPK + ")");
                }

                sb.Append("               AND INVTRN.JOB_TYPE = 4");
                sb.Append("           AND INV.CHK_INVOICE<>2 ");
                // '   --------CBJC CR Note--
                sb.Append("           UNION");
                sb.Append("           SELECT DISTINCT CMT.CUSTOMER_MST_PK,");
                sb.Append("                        CCD.ADM_LOCATION_MST_FK,");
                sb.Append("                        CNT.CREDIT_NOTE_DATE REF_DATE,");
                sb.Append("                        (CASE");
                sb.Append("                          WHEN CNT.PROCESS_TYPE = 1 THEN");
                sb.Append("                           'EXPORT'");
                sb.Append("                          ELSE");
                sb.Append("                           'IMPORT'");
                sb.Append("                        END) PROCESS,");
                sb.Append("                        'CREDITNOTE' TRANSACTION,");
                sb.Append("                        CBJC.HBL_NO HBL_REF_NO,");
                sb.Append("                        CNT.CREDIT_NOTE_REF_NR DOCREFNR,");
                sb.Append("                        CNT.CRN_TBL_PK REF_PK,");
                sb.Append("                        CNT.BIZ_TYPE BIZ_TYPE,");
                sb.Append("                        CBJC.CARGO_TYPE CARGO_TYPE,");
                sb.Append("                        (SELECT  ROUND(CNT1.CRN_AMMOUNT * ");
                sb.Append("                        " + ParentDB + ".GET_EX_RATE(CNT1.CURRENCY_MST_FK,");
                sb.Append(CurrPK + ",");
                sb.Append("                                          CNT.CREDIT_NOTE_DATE),");
                sb.Append("                        2) FROM " + ParentDB + ".CREDIT_NOTE_TBL CNT1 WHERE CNT1.CRN_TBL_PK =  CNT.CRN_TBL_PK AND NVL(CNT1.CRN_STATUS,0) = 2) DEBIT,");
                sb.Append("                        (SELECT  ROUND(CNT1.CRN_AMMOUNT * ");
                sb.Append("                        " + ParentDB + ".GET_EX_RATE(CNT1.CURRENCY_MST_FK,");
                sb.Append(CurrPK + ",");
                sb.Append("                                          CNT.CREDIT_NOTE_DATE),");
                sb.Append("                        2) FROM " + ParentDB + ".CREDIT_NOTE_TBL CNT1 WHERE CNT1.CRN_TBL_PK =  CNT.CRN_TBL_PK AND NVL(CNT1.CRN_STATUS,0) <> 2) CREDIT,");
                sb.Append("                        0 BALANCE,");
                sb.Append("                        '' OUTSTANDING_DAYS");
                sb.Append("          FROM " + ParentDB + ".CBJC_TBL CBJC,");
                sb.Append("               " + ParentDB + ".CUSTOMER_MST_TBL       CMT,");
                sb.Append("               " + ParentDB + ".CUSTOMER_CONTACT_DTLS  CCD,");
                sb.Append("               " + ParentDB + ".LOCATION_MST_TBL       LMT,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TBL     INV,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TRN_TBL INVTRN,");
                sb.Append("               " + ParentDB + ".CREDIT_NOTE_TBL        CNT,");
                sb.Append("               " + ParentDB + ".CREDIT_NOTE_TRN_TBL    CNTT");
                sb.Append("         WHERE INVTRN.JOB_CARD_FK = CBJC.CBJC_PK(+)");
                sb.Append("           AND INV.IS_FAC_INV <> 1");
                sb.Append("           AND CBJC.CUSTOMER_MST_FK= CMT.CUSTOMER_MST_PK");
                sb.Append("           AND CNT.PROCESS_TYPE = " + ProcessType);
                sb.Append("           AND INV.CONSOL_INVOICE_PK = INVTRN.CONSOL_INVOICE_FK");
                sb.Append("           AND CNT.CRN_TBL_PK = CNTT.CRN_TBL_FK");
                sb.Append("           AND CNTT.CONSOL_INVOICE_TRN_FK = INVTRN.CONSOL_INVOICE_TRN_PK");
                sb.Append("           AND CMT.CUSTOMER_MST_PK = CCD.CUSTOMER_MST_FK");
                sb.Append("           AND LMT.LOCATION_MST_PK = CCD.ADM_LOCATION_MST_FK");
                sb.Append("           AND NVL((SELECT SUM(WMT.WRITEOFF_AMOUNT)");
                sb.Append("                     FROM " + ParentDB + ".WRITEOFF_MANUAL_TBL WMT");
                sb.Append("                    WHERE WMT.CONSOL_INVOICE_FK = INV.CONSOL_INVOICE_PK),");
                sb.Append("                   0.00) = 0");
                if (Fromdate != "" & Todate != "")
                {
                    sb.Append("           AND  CNT.CREDIT_NOTE_DATE  BETWEEN TO_DATE('" + Fromdate + "') AND");
                    sb.Append("               TO_DATE('" + Todate + "')");
                }
                sb.Append("          AND CBJC.PROCESS_TYPE =" + ProcessType);
                if (BizType != 3)
                {
                    sb.Append("          AND CBJC.BIZ_TYPE =" + BizType);
                }

                if (CargoType != 0)
                {
                    sb.Append("     AND CBJC.CARGO_TYPE=" + CargoType);
                }

                if (!string.IsNullOrEmpty(CountryPK) & CountryPK != "0")
                {
                    sb.Append("       AND LMT.COUNTRY_MST_FK IN(" + CountryPK + ")");
                }

                if (!string.IsNullOrEmpty(LocPK) & LocPK != "0")
                {
                    sb.Append("        AND LMT.LOCATION_MST_PK IN (" + LocPK + ") ");
                }

                if (!string.IsNullOrEmpty(CustPK) & CustPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustPK + ")");
                }

                if (!string.IsNullOrEmpty(CustGroupPK) & CustGroupPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustGroupPK + ")");
                }

                if (BizType == 1)
                {
                    if (!string.IsNullOrEmpty(FlightID))
                    {
                        sb.Append("     AND   CBJC.FLIGHT_NO IN (" + FlightID + ")");
                    }
                }
                else if (BizType == 2)
                {
                    if (!string.IsNullOrEmpty(VslVoyPK))
                    {
                        sb.Append("     AND   CBJC.VOYAGE_TRN_FK=" + VslVoyPK + "");
                    }
                }
                sb.Append("           AND INVTRN.JOB_TYPE = 2");
                sb.Append("           AND INV.CHK_INVOICE<>2 ");
                // '           ---TPT CrNote------
                sb.Append("           UNION");
                sb.Append("           SELECT DISTINCT CMT.CUSTOMER_MST_PK,");
                sb.Append("                        CCD.ADM_LOCATION_MST_FK,");
                sb.Append("                       CNT.CREDIT_NOTE_DATE REF_DATE,");
                sb.Append("                        (CASE");
                sb.Append("                          WHEN CNT.PROCESS_TYPE = 1 THEN");
                sb.Append("                           'EXPORT'");
                sb.Append("                          ELSE");
                sb.Append("                           'IMPORT'");
                sb.Append("                        END) PROCESS,");
                sb.Append("                        'CREDITNOTE' TRANSACTION,");
                sb.Append("                        (SELECT CONT.BL_NUMBER FROM " + ParentDB + ".TRANSPORT_TRN_CONT CONT WHERE CONT.TRANSPORT_INST_FK = TIST.TRANSPORT_INST_SEA_PK AND ROWNUM=1) HBL_REF_NO,");
                sb.Append("                        CNT.CREDIT_NOTE_REF_NR DOCREFNR,");
                sb.Append("                        CNT.CRN_TBL_PK REF_PK,");
                sb.Append("                        CNT.BIZ_TYPE BIZ_TYPE,");
                sb.Append("                        TIST.CARGO_TYPE CARGO_TYPE,");
                sb.Append("                        (SELECT  ROUND(CNT1.CRN_AMMOUNT * ");
                sb.Append("                        " + ParentDB + ".GET_EX_RATE(CNT1.CURRENCY_MST_FK,");
                sb.Append(CurrPK + ",");
                sb.Append("                                          CNT.CREDIT_NOTE_DATE),");
                sb.Append("                        2) FROM " + ParentDB + ".CREDIT_NOTE_TBL CNT1 WHERE CNT1.CRN_TBL_PK =  CNT.CRN_TBL_PK AND NVL(CNT1.CRN_STATUS,0) = 2) DEBIT,");
                sb.Append("                        (SELECT  ROUND(CNT1.CRN_AMMOUNT * ");
                sb.Append("                        " + ParentDB + ".GET_EX_RATE(CNT1.CURRENCY_MST_FK,");
                sb.Append(CurrPK + ",");
                sb.Append("                                          CNT.CREDIT_NOTE_DATE),");
                sb.Append("                        2) FROM " + ParentDB + ".CREDIT_NOTE_TBL CNT1 WHERE CNT1.CRN_TBL_PK =  CNT.CRN_TBL_PK AND NVL(CNT1.CRN_STATUS,0) <> 2) CREDIT,");
                sb.Append("                        0 BALANCE,");
                sb.Append("                        '' OUTSTANDING_DAYS");
                sb.Append("          FROM " + ParentDB + ".TRANSPORT_INST_SEA_TBL TIST,");
                sb.Append("               " + ParentDB + ".CUSTOMER_MST_TBL       CMT,");
                sb.Append("               " + ParentDB + ".CUSTOMER_CONTACT_DTLS  CCD,");
                sb.Append("               " + ParentDB + ".LOCATION_MST_TBL       LMT,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TBL     INV,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TRN_TBL INVTRN,");
                sb.Append("               " + ParentDB + ".CREDIT_NOTE_TBL        CNT,");
                sb.Append("               " + ParentDB + ".CREDIT_NOTE_TRN_TBL    CNTT");
                sb.Append("         WHERE INVTRN.JOB_CARD_FK = TIST.TRANSPORT_INST_SEA_PK(+)");
                sb.Append("           AND INV.IS_FAC_INV <> 1");
                sb.Append("           AND INV.CUSTOMER_MST_FK= CMT.CUSTOMER_MST_PK");
                sb.Append("           AND CNT.PROCESS_TYPE = " + ProcessType);
                sb.Append("           AND INV.CONSOL_INVOICE_PK = INVTRN.CONSOL_INVOICE_FK");
                sb.Append("           AND CNT.CRN_TBL_PK = CNTT.CRN_TBL_FK");
                sb.Append("           AND CNTT.CONSOL_INVOICE_TRN_FK = INVTRN.CONSOL_INVOICE_TRN_PK");
                sb.Append("           AND CMT.CUSTOMER_MST_PK = CCD.CUSTOMER_MST_FK");
                sb.Append("           AND LMT.LOCATION_MST_PK = CCD.ADM_LOCATION_MST_FK");
                sb.Append("           AND NVL((SELECT SUM(WMT.WRITEOFF_AMOUNT)");
                sb.Append("                     FROM " + ParentDB + ".WRITEOFF_MANUAL_TBL WMT");
                sb.Append("                    WHERE WMT.CONSOL_INVOICE_FK = INV.CONSOL_INVOICE_PK),");
                sb.Append("                   0.00) = 0");
                if (Fromdate != "" & Todate != "")
                {
                    sb.Append("           AND CNT.CREDIT_NOTE_DATE BETWEEN TO_DATE('" + Fromdate + "') AND");
                    sb.Append("               TO_DATE('" + Todate + "')");
                }
                sb.Append("          AND TIST.PROCESS_TYPE =" + ProcessType);
                if (BizType != 3)
                {
                    sb.Append("          AND TIST.BUSINESS_TYPE =" + BizType);
                }

                if (CargoType != 0)
                {
                    sb.Append("     AND TIST.CARGO_TYPE=" + CargoType);
                }

                if (!string.IsNullOrEmpty(CountryPK) & CountryPK != "0")
                {
                    sb.Append("       AND LMT.COUNTRY_MST_FK IN(" + CountryPK + ")");
                }

                if (!string.IsNullOrEmpty(LocPK) & LocPK != "0")
                {
                    sb.Append("        AND LMT.LOCATION_MST_PK IN (" + LocPK + ") ");
                }

                if (!string.IsNullOrEmpty(CustPK) & CustPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustPK + ")");
                }

                if (!string.IsNullOrEmpty(CustGroupPK) & CustGroupPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustGroupPK + ")");
                }

                if (BizType == 1)
                {
                    if (!string.IsNullOrEmpty(FlightID))
                    {
                        sb.Append("     AND   TIST.FLIGHT_NO IN (" + FlightID + ")");
                    }
                }
                else if (BizType == 2)
                {
                    if (!string.IsNullOrEmpty(VslVoyPK))
                    {
                        sb.Append("     AND   TIST.VSL_VOY_FK=" + VslVoyPK + "");
                    }
                }
                sb.Append("           AND INVTRN.JOB_TYPE = 3");
                sb.Append("           AND INV.CHK_INVOICE<>2 ");
                // '         --- DET & DEM TPT (CR Note)
                sb.Append("           UNION");
                sb.Append("           SELECT DISTINCT CMT.CUSTOMER_MST_PK,");
                sb.Append("                        CCD.ADM_LOCATION_MST_FK,");
                sb.Append("                        CNT.CREDIT_NOTE_DATE REF_DATE,");
                sb.Append("                        (CASE");
                sb.Append("                          WHEN CNT.PROCESS_TYPE = 1 THEN");
                sb.Append("                           'EXPORT'");
                sb.Append("                          ELSE");
                sb.Append("                           'IMPORT'");
                sb.Append("                        END) PROCESS,");
                sb.Append("                        'CREDITNOTE' TRANSACTION,");
                sb.Append("                        (SELECT CONT.BL_NUMBER FROM " + ParentDB + ".TRANSPORT_TRN_CONT CONT WHERE CONT.TRANSPORT_INST_FK = TIST.TRANSPORT_INST_SEA_PK AND ROWNUM=1) HBL_REF_NO,");
                sb.Append("                        CNT.CREDIT_NOTE_REF_NR DOCREFNR,");
                sb.Append("                        CNT.CRN_TBL_PK REF_PK,");
                sb.Append("                        CNT.BIZ_TYPE BIZ_TYPE,");
                sb.Append("                        DCH.CARGO_TYPE CARGO_TYPE,");
                sb.Append("                        (SELECT  ROUND(CNT1.CRN_AMMOUNT * ");
                sb.Append("                        " + ParentDB + ".GET_EX_RATE(CNT1.CURRENCY_MST_FK,");
                sb.Append(CurrPK + ",");
                sb.Append("                                          CNT.CREDIT_NOTE_DATE),");
                sb.Append("                        2) FROM " + ParentDB + ".CREDIT_NOTE_TBL CNT1 WHERE CNT1.CRN_TBL_PK =  CNT.CRN_TBL_PK AND NVL(CNT1.CRN_STATUS,0) = 2) DEBIT,");
                sb.Append("                        (SELECT  ROUND(CNT1.CRN_AMMOUNT * ");
                sb.Append("                        " + ParentDB + ".GET_EX_RATE(CNT1.CURRENCY_MST_FK,");
                sb.Append(CurrPK + ",");
                sb.Append("                                          CNT.CREDIT_NOTE_DATE),");
                sb.Append("                        2) FROM " + ParentDB + ".CREDIT_NOTE_TBL CNT1 WHERE CNT1.CRN_TBL_PK =  CNT.CRN_TBL_PK AND NVL(CNT1.CRN_STATUS,0) <> 2) CREDIT,");
                sb.Append("                        0 BALANCE,");
                sb.Append("                        '' OUTSTANDING_DAYS");
                sb.Append("          FROM " + ParentDB + ".DEM_CALC_HDR DCH  ,");
                sb.Append("               " + ParentDB + ".TRANSPORT_INST_SEA_TBL TIST,");
                sb.Append("               " + ParentDB + ".CUSTOMER_MST_TBL       CMT,");
                sb.Append("               " + ParentDB + ".CUSTOMER_CONTACT_DTLS  CCD,");
                sb.Append("               " + ParentDB + ".LOCATION_MST_TBL       LMT,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TBL     INV,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TRN_TBL INVTRN,");
                sb.Append("               " + ParentDB + ".CREDIT_NOTE_TBL        CNT,");
                sb.Append("               " + ParentDB + ".CREDIT_NOTE_TRN_TBL    CNTT");
                sb.Append("         WHERE INVTRN.JOB_CARD_FK = DCH.DEM_CALC_HDR_PK(+)");
                sb.Append("           AND DCH.DOC_REF_FK =  TIST.TRANSPORT_INST_SEA_PK");
                sb.Append("           AND INV.IS_FAC_INV <> 1");
                sb.Append("           AND INV.CUSTOMER_MST_FK= CMT.CUSTOMER_MST_PK");
                sb.Append("           AND CNT.PROCESS_TYPE = " + ProcessType);
                sb.Append("           AND INV.CONSOL_INVOICE_PK = INVTRN.CONSOL_INVOICE_FK");
                sb.Append("           AND CNT.CRN_TBL_PK = CNTT.CRN_TBL_FK");
                sb.Append("           AND CNTT.CONSOL_INVOICE_TRN_FK = INVTRN.CONSOL_INVOICE_TRN_PK");
                sb.Append("           AND CMT.CUSTOMER_MST_PK = CCD.CUSTOMER_MST_FK");
                sb.Append("           AND LMT.LOCATION_MST_PK = CCD.ADM_LOCATION_MST_FK");
                sb.Append("           AND NVL((SELECT SUM(WMT.WRITEOFF_AMOUNT)");
                sb.Append("                     FROM " + ParentDB + ".WRITEOFF_MANUAL_TBL WMT");
                sb.Append("                    WHERE WMT.CONSOL_INVOICE_FK = INV.CONSOL_INVOICE_PK),");
                sb.Append("                   0.00) = 0");
                if (BizType == 1)
                {
                    if (!string.IsNullOrEmpty(FlightID))
                    {
                        sb.Append("     AND   TIST.FLIGHT_NO IN (" + FlightID + ")");
                    }
                }
                else if (BizType == 2)
                {
                    if (!string.IsNullOrEmpty(VslVoyPK))
                    {
                        sb.Append("     AND   TIST.VSL_VOY_FK=" + VslVoyPK + "");
                    }
                }
                if (CargoType != 0)
                {
                    sb.Append("     AND DCH.CARGO_TYPE=" + CargoType);
                }

                if (Fromdate != "" & Todate != "")
                {
                    sb.Append("       AND CNT.CREDIT_NOTE_DATE BETWEEN TO_DATE('" + Fromdate + "') AND");
                    sb.Append("               TO_DATE('" + Todate + "')");
                }
                if (!string.IsNullOrEmpty(CountryPK) & CountryPK != "0")
                {
                    sb.Append("       AND LMT.COUNTRY_MST_FK IN(" + CountryPK + ")");
                }

                if (!string.IsNullOrEmpty(LocPK) & LocPK != "0")
                {
                    sb.Append("        AND LMT.LOCATION_MST_PK IN (" + LocPK + ") ");
                }

                if (!string.IsNullOrEmpty(CustPK) & CustPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustPK + ")");
                }

                if (!string.IsNullOrEmpty(CustGroupPK) & CustGroupPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustGroupPK + ")");
                }

                sb.Append("           AND INVTRN.JOB_TYPE = 3");
                sb.Append("          AND DCH.PROCESS_TYPE =" + ProcessType);
                sb.Append("           AND INV.CHK_INVOICE<>2 ");
                if (BizType != 3)
                {
                    sb.Append("          AND DCH.biz_type =" + BizType);
                }
                // '         --- DET & DEM CBJC (CR Note)
                sb.Append("           UNION");
                sb.Append("           SELECT DISTINCT CMT.CUSTOMER_MST_PK,");
                sb.Append("                        CCD.ADM_LOCATION_MST_FK,");
                sb.Append("                        CNT.CREDIT_NOTE_DATE REF_DATE,");
                sb.Append("                        (CASE");
                sb.Append("                          WHEN CNT.PROCESS_TYPE = 1 THEN");
                sb.Append("                           'EXPORT'");
                sb.Append("                          ELSE");
                sb.Append("                           'IMPORT'");
                sb.Append("                        END) PROCESS,");
                sb.Append("                        'CREDITNOTE' TRANSACTION,");
                sb.Append("                        CBJC.HBL_NO HBL_REF_NO,");
                sb.Append("                        CNT.CREDIT_NOTE_REF_NR DOCREFNR,");
                sb.Append("                        CNT.CRN_TBL_PK REF_PK,");
                sb.Append("                        CNT.BIZ_TYPE BIZ_TYPE,");
                sb.Append("                        DCH.CARGO_TYPE CARGO_TYPE,");
                sb.Append("                        (SELECT  ROUND(CNT1.CRN_AMMOUNT * ");
                sb.Append("                        " + ParentDB + ".GET_EX_RATE(CNT1.CURRENCY_MST_FK,");
                sb.Append(CurrPK + ",");
                sb.Append("                                          CNT.CREDIT_NOTE_DATE),");
                sb.Append("                        2) FROM " + ParentDB + ".CREDIT_NOTE_TBL CNT1 WHERE CNT1.CRN_TBL_PK =  CNT.CRN_TBL_PK AND NVL(CNT1.CRN_STATUS,0) = 2) DEBIT,");
                sb.Append("                        (SELECT  ROUND(CNT1.CRN_AMMOUNT * ");
                sb.Append("                        " + ParentDB + ".GET_EX_RATE(CNT1.CURRENCY_MST_FK,");
                sb.Append(CurrPK + ",");
                sb.Append("                                          CNT.CREDIT_NOTE_DATE),");
                sb.Append("                        2) FROM " + ParentDB + ".CREDIT_NOTE_TBL CNT1 WHERE CNT1.CRN_TBL_PK =  CNT.CRN_TBL_PK AND NVL(CNT1.CRN_STATUS,0) <> 2) CREDIT,");
                sb.Append("                        0 BALANCE,");
                sb.Append("                        '' OUTSTANDING_DAYS");
                sb.Append("          FROM " + ParentDB + ".DEM_CALC_HDR DCH  ,");
                sb.Append("               " + ParentDB + ".CBJC_TBL               CBJC,");
                sb.Append("               " + ParentDB + ".CUSTOMER_MST_TBL       CMT,");
                sb.Append("               " + ParentDB + ".CUSTOMER_CONTACT_DTLS  CCD,");
                sb.Append("               " + ParentDB + ".LOCATION_MST_TBL       LMT,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TBL     INV,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TRN_TBL INVTRN,");
                sb.Append("               " + ParentDB + ".CREDIT_NOTE_TBL        CNT,");
                sb.Append("               " + ParentDB + ".CREDIT_NOTE_TRN_TBL    CNTT");
                sb.Append("         WHERE INVTRN.JOB_CARD_FK = DCH.DEM_CALC_HDR_PK(+)");
                sb.Append("           AND DCH.DOC_REF_FK = CBJC.CBJC_PK");
                sb.Append("           AND INV.IS_FAC_INV <> 1");
                sb.Append("           AND INV.CUSTOMER_MST_FK= CMT.CUSTOMER_MST_PK");
                sb.Append("           AND CNT.PROCESS_TYPE = " + ProcessType);
                sb.Append("           AND INV.CONSOL_INVOICE_PK = INVTRN.CONSOL_INVOICE_FK");
                sb.Append("           AND CNT.CRN_TBL_PK = CNTT.CRN_TBL_FK");
                sb.Append("           AND CNTT.CONSOL_INVOICE_TRN_FK = INVTRN.CONSOL_INVOICE_TRN_PK");
                sb.Append("           AND CMT.CUSTOMER_MST_PK = CCD.CUSTOMER_MST_FK");
                sb.Append("           AND LMT.LOCATION_MST_PK = CCD.ADM_LOCATION_MST_FK");
                sb.Append("           AND NVL((SELECT SUM(WMT.WRITEOFF_AMOUNT)");
                sb.Append("                     FROM " + ParentDB + ".WRITEOFF_MANUAL_TBL WMT");
                sb.Append("                    WHERE WMT.CONSOL_INVOICE_FK = INV.CONSOL_INVOICE_PK),");
                sb.Append("                   0.00) = 0");
                if (BizType == 1)
                {
                    if (!string.IsNullOrEmpty(FlightID))
                    {
                        sb.Append("         AND   CBJC.FLIGHT_NO IN (" + FlightID + ")");
                    }
                }
                else if (BizType == 2)
                {
                    if (!string.IsNullOrEmpty(VslVoyPK))
                    {
                        sb.Append("         AND   CBJC.VOYAGE_TRN_FK=" + VslVoyPK + "");
                    }
                }
                if (Fromdate != "" & Todate != "")
                {
                    sb.Append("       AND CNT.CREDIT_NOTE_DATE BETWEEN TO_DATE('" + Fromdate + "') AND");
                    sb.Append("               TO_DATE('" + Todate + "')");
                }
                if (!string.IsNullOrEmpty(CountryPK) & CountryPK != "0")
                {
                    sb.Append("       AND LMT.COUNTRY_MST_FK IN(" + CountryPK + ")");
                }

                if (!string.IsNullOrEmpty(LocPK) & LocPK != "0")
                {
                    sb.Append("        AND LMT.LOCATION_MST_PK IN (" + LocPK + ") ");
                }

                sb.Append("           AND INV.CHK_INVOICE<>2 ");
                if (!string.IsNullOrEmpty(CustPK) & CustPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustPK + ")");
                }

                if (!string.IsNullOrEmpty(CustGroupPK) & CustGroupPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustGroupPK + ")");
                }

                sb.Append("           AND INVTRN.JOB_TYPE = 4");
                sb.Append("          AND DCH.PROCESS_TYPE =" + ProcessType);
                if (BizType != 3)
                {
                    sb.Append("          AND DCH.biz_type =" + BizType);
                }

                if (CargoType != 0)
                {
                    sb.Append("     AND DCH.CARGO_TYPE=" + CargoType);
                }
                // ----- End----
                sb.Append(") A,");
                sbNew2.Append("       (SELECT DISTINCT CMT.CUSTOMER_MST_PK,");
                sbNew2.Append("                        CCD.ADM_LOCATION_MST_FK,");
                sbNew2.Append("                        INV.INVOICE_DATE REF_DATE,");
                sbNew2.Append("                        (CASE");
                sbNew2.Append("                          WHEN INV.PROCESS_TYPE = 1 THEN");
                sbNew2.Append("                           'EXPORT'");
                sbNew2.Append("                          ELSE");
                sbNew2.Append("                           'IMPORT'");
                sbNew2.Append("                        END) PROCESS,");
                sbNew2.Append("                        'INVOICE' TRANSACTION,");
                sbNew2.Append("  ( CASE ");
                sbNew2.Append("    WHEN JOB.BUSINESS_TYPE = 2 AND JOB.PROCESS_TYPE = 1 THEN ");
                sbNew2.Append("      HBL.HBL_REF_NO ");
                sbNew2.Append("     WHEN JOB.BUSINESS_TYPE = 1 AND JOB.PROCESS_TYPE = 1 THEN ");
                sbNew2.Append("         HAWB.HAWB_REF_NO ");
                sbNew2.Append("        ELSE ");
                sbNew2.Append("       JOB.HBL_HAWB_REF_NO  ");
                sbNew2.Append("      END) HBL_REF_NO , ");
                sbNew2.Append("                        INV.INVOICE_REF_NO DOCREFNR,");
                sbNew2.Append("                        INV.CONSOL_INVOICE_PK  REF_PK,");
                sbNew2.Append("                        INV.BUSINESS_TYPE BIZ_TYPE,");
                if (ProcessType == 1)
                {
                    sbNew2.Append("                        BKG.CARGO_TYPE CARGO_TYPE,");
                }
                else if (BizType == 2)
                {
                    sbNew2.Append("                        JOB.CARGO_TYPE CARGO_TYPE, ");
                }
                else
                {
                    sbNew2.Append("                        0 CARGO_TYPE, ");
                }

                sbNew2.Append("                        ROUND(INV.NET_RECEIVABLE * " + ParentDB + ".GET_EX_RATE(INV.CURRENCY_MST_FK," + CurrPK + ", INV.INVOICE_DATE),2) DEBIT,");
                sbNew2.Append("                        0 CREDIT,");
                sbNew2.Append("                        0 BALANCE,");
                sbNew2.Append("(SELECT DISTINCT TO_CHAR(TO_DATE(SYSDATE,");
                sbNew2.Append("                             'DD/MM/YYYY') -");
                sbNew2.Append("                              TO_DATE(INV.INVOICE_DATE)) OUTSTANDING_DAYS");

                sbNew2.Append("                                   FROM " + ParentDB + ".COLLECTIONS_TRN_TBL COLL,");
                sbNew2.Append("                                        " + ParentDB + ".CONSOL_INVOICE_TBL  INV1");
                sbNew2.Append("                                  WHERE INV1.INVOICE_REF_NO =");
                sbNew2.Append("                                        COLL.INVOICE_REF_NR(+)");
                sbNew2.Append("                                    AND INV1.INVOICE_REF_NO =");
                sbNew2.Append("                                        INV.INVOICE_REF_NO");
                sbNew2.Append("                                    AND NVL((INV1.NET_RECEIVABLE -");
                sbNew2.Append("                                            NVL((SELECT SUM(CTRN.RECD_AMOUNT_HDR_CURR)");
                sbNew2.Append("                                                   FROM " + ParentDB + ".COLLECTIONS_TRN_TBL CTRN");
                sbNew2.Append("                                                  WHERE CTRN.INVOICE_REF_NR LIKE");
                sbNew2.Append("                                                        INV1.INVOICE_REF_NO),");
                sbNew2.Append("                                                 0.00)),");
                sbNew2.Append("                                            0) > 0)");
                sbNew2.Append("          FROM " + ParentDB + ".JOB_CARD_TRN   JOB,");
                if (ProcessType == 1)
                {
                    sbNew2.Append("               " + ParentDB + ".BOOKING_MST_TBL        BKG,");
                }

                sbNew2.Append("               " + ParentDB + ".HBL_EXP_TBL            HBL,");
                sbNew2.Append("               " + ParentDB + ".HAWB_EXP_TBL            HAWB,");
                sbNew2.Append("               " + ParentDB + ".CUSTOMER_MST_TBL       CMT,");
                sbNew2.Append("               " + ParentDB + ".CUSTOMER_CONTACT_DTLS  CCD,");
                sbNew2.Append("               " + ParentDB + ".LOCATION_MST_TBL       LMT,");
                sbNew2.Append("               " + ParentDB + ".CONSOL_INVOICE_TBL     INV,");
                sbNew2.Append("               " + ParentDB + ".CONSOL_INVOICE_TRN_TBL INVTRN");
                sbNew2.Append("         WHERE INVTRN.JOB_CARD_FK = JOB.JOB_CARD_TRN_PK(+)");
                sbNew2.Append("         AND INV.IS_FAC_INV <> 1");
                sbNew2.Append("           AND INVTRN.JOB_TYPE = 1");
                sbNew2.Append("           AND JOB.HBL_HAWB_FK = HBL.HBL_EXP_TBL_PK(+)");
                sbNew2.Append("           AND JOB.HBL_HAWB_FK = HAWB.HAWB_EXP_TBL_PK(+)");
                if (ProcessType == 1)
                {
                    sbNew2.Append("           AND CMT.CUSTOMER_MST_PK = BKG.CUST_CUSTOMER_MST_FK");
                    sbNew2.Append("           AND JOB.BOOKING_MST_FK = BKG.BOOKING_MST_PK");
                    sbNew2.Append("           AND INV.PROCESS_TYPE = " + ProcessType);
                    if (CargoType != 0)
                    {
                        sbNew2.Append("     AND BKG.CARGO_TYPE=" + CargoType);
                    }

                    if (!string.IsNullOrEmpty(POLPK) & POLPK != "0")
                    {
                        sbNew2.Append("       AND BKG.PORT_MST_POL_FK IN(" + POLPK + ")");
                    }

                    if (!string.IsNullOrEmpty(POLPK) & PODPK != "0")
                    {
                        sbNew2.Append("       AND BKG.PORT_MST_POD_FK IN( " + PODPK + ")");
                    }
                }
                else if (ProcessType == 2)
                {
                    sbNew2.Append("        AND JOB.CUST_CUSTOMER_MST_FK=CMT.CUSTOMER_MST_PK");
                    sbNew2.Append("           AND INV.PROCESS_TYPE = 2");
                    if (CargoType != 0)
                    {
                        sbNew2.Append("     AND JOB.CARGO_TYPE=" + CargoType);
                    }

                    if (!string.IsNullOrEmpty(POLPK) & POLPK != "0")
                    {
                        sbNew2.Append("         AND JOB.PORT_MST_POL_FK IN(" + POLPK + ")");
                    }

                    if (!string.IsNullOrEmpty(POLPK) & PODPK != "0")
                    {
                        sbNew2.Append("         AND JOB.PORT_MST_POD_FK IN(" + PODPK + ")");
                    }
                }
                sbNew2.Append("           AND INV.CONSOL_INVOICE_PK = INVTRN.CONSOL_INVOICE_FK");
                sbNew2.Append("           AND CMT.CUSTOMER_MST_PK = CCD.CUSTOMER_MST_FK");
                sbNew2.Append("           AND LMT.LOCATION_MST_PK = CCD.ADM_LOCATION_MST_FK");
                sbNew2.Append("           AND NVL((SELECT SUM(WMT.WRITEOFF_AMOUNT)");
                sbNew2.Append("             FROM " + ParentDB + ".WRITEOFF_MANUAL_TBL WMT");
                sbNew2.Append("            WHERE WMT.CONSOL_INVOICE_FK = INV.CONSOL_INVOICE_PK),");
                sbNew2.Append("           0.00) = 0");
                if (BizType == 1)
                {
                    sbNew2.Append("           AND INV.BUSINESS_TYPE = 1");
                    if (!string.IsNullOrEmpty(FlightID))
                    {
                        sbNew2.Append("            AND JOB.VOYAGE_FLIGHT_NO IN (" + FlightID + ")");
                    }
                }
                else if (BizType == 2)
                {
                    sbNew2.Append("           AND INV.BUSINESS_TYPE = 2");
                    if (!string.IsNullOrEmpty(VslVoyPK))
                    {
                        sbNew2.Append("            AND JOB.VOYAGE_TRN_FK=" + VslVoyPK + "");
                    }
                }
                sbNew2.Append("           AND INV.CHK_INVOICE<>2 ");
                if (!string.IsNullOrEmpty(CountryPK) & CountryPK != "0")
                {
                    sbNew2.Append("       AND LMT.COUNTRY_MST_FK IN(" + CountryPK + ")");
                }

                if (!string.IsNullOrEmpty(LocPK) & LocPK != "0")
                {
                    sbNew2.Append("        AND LMT.LOCATION_MST_PK IN (" + LocPK + ") ");
                }

                if (!string.IsNullOrEmpty(CustPK) & CustPK != "0")
                {
                    sbNew2.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustPK + ")");
                }

                if (!string.IsNullOrEmpty(CustGroupPK) & CustGroupPK != "0")
                {
                    sbNew2.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustGroupPK + ")");
                }

                if (Fromdate != "")
                {
                    sbNew2.Append("           AND INV.invoice_date <= TO_DATE('" + Fromdate + "')");
                }

                sbNew2.Append("        UNION");
                sbNew2.Append("        SELECT DISTINCT CMT.CUSTOMER_MST_PK,");
                sbNew2.Append("                        CCD.ADM_LOCATION_MST_FK,");
                sbNew2.Append("                        COL.COLLECTIONS_DATE REF_DATE,");
                sbNew2.Append("                        (CASE");
                sbNew2.Append("                          WHEN COL.PROCESS_TYPE = 1 THEN");
                sbNew2.Append("                           'EXPORT'");
                sbNew2.Append("                          ELSE");
                sbNew2.Append("                           'IMPORT'");
                sbNew2.Append("                        END) PROCESS,");
                sbNew2.Append("                        'COLLECTION' TRANSACTION,");
                sbNew2.Append("  ( CASE ");
                sbNew2.Append("    WHEN JOB.BUSINESS_TYPE = 2 AND JOB.PROCESS_TYPE = 1 THEN ");
                sbNew2.Append("      HBL.HBL_REF_NO ");
                sbNew2.Append("     WHEN JOB.BUSINESS_TYPE = 1 AND JOB.PROCESS_TYPE = 1 THEN ");
                sbNew2.Append("         HAWB.HAWB_REF_NO ");
                sbNew2.Append("        ELSE ");
                sbNew2.Append("       JOB.HBL_HAWB_REF_NO  ");
                sbNew2.Append("      END) HBL_REF_NO , ");
                sbNew2.Append("                        COL.COLLECTIONS_REF_NO DOCREFNR,");
                sbNew2.Append("                        COL.COLLECTIONS_TBL_PK REF_PK,");
                sbNew2.Append("                        COL.BUSINESS_TYPE BIZ_TYPE,");
                if (ProcessType == 1)
                {
                    sbNew2.Append("                        BKG.CARGO_TYPE CARGO_TYPE,");
                }
                else if (BizType == 2)
                {
                    sbNew2.Append("                        JOB.CARGO_TYPE CARGO_TYPE, ");
                }
                else
                {
                    sbNew2.Append("                        0 CARGO_TYPE, ");
                }

                sbNew2.Append("                        0 DEBIT,");
                sbNew2.Append("                        ROUND(CLT.RECD_AMOUNT_HDR_CURR * " + ParentDB + ".GET_EX_RATE(COL.CURRENCY_MST_FK," + CurrPK + ",COL.COLLECTIONS_DATE),2) CREDIT,");
                sbNew2.Append("                        0 BALANCE,''OUTSTANDING_DAYS");
                sbNew2.Append("          FROM " + ParentDB + ".JOB_CARD_TRN   JOB,");
                if (ProcessType == 1)
                {
                    sbNew2.Append("               " + ParentDB + ".BOOKING_MST_TBL        BKG,");
                }

                sbNew2.Append("               " + ParentDB + ".HBL_EXP_TBL            HBL,");
                sbNew2.Append("               " + ParentDB + ".HAWB_EXP_TBL            HAWB,");
                sbNew2.Append("               " + ParentDB + ".CUSTOMER_MST_TBL       CMT,");
                sbNew2.Append("               " + ParentDB + ".LOCATION_MST_TBL       LMT,");
                sbNew2.Append("               " + ParentDB + ".CUSTOMER_CONTACT_DTLS  CCD,");
                sbNew2.Append("               " + ParentDB + ".CONSOL_INVOICE_TBL     INV,");
                sbNew2.Append("               " + ParentDB + ".CONSOL_INVOICE_TRN_TBL INVTRN,");
                sbNew2.Append("               " + ParentDB + ".COLLECTIONS_TRN_TBL    CLT,");
                sbNew2.Append("               " + ParentDB + ".COLLECTIONS_TBL        COL");
                sbNew2.Append("         WHERE INVTRN.JOB_CARD_FK = JOB.JOB_CARD_TRN_PK(+)");
                sbNew2.Append("         AND INV.IS_FAC_INV <> 1");
                sbNew2.Append("           AND INVTRN.JOB_TYPE = 1");
                sbNew2.Append("           AND JOB.HBL_HAWB_FK = HBL.HBL_EXP_TBL_PK(+)");
                sbNew2.Append("           AND JOB.HBL_HAWB_FK = HAWB.HAWB_EXP_TBL_PK(+)");
                if (ProcessType == 1)
                {
                    sbNew2.Append("           AND CMT.CUSTOMER_MST_PK = BKG.CUST_CUSTOMER_MST_FK");
                    sbNew2.Append("           AND JOB.BOOKING_MST_FK = BKG.BOOKING_MST_PK");
                    sbNew2.Append("           AND COL.PROCESS_TYPE = " + ProcessType);
                    if (CargoType != 0)
                    {
                        sbNew2.Append("     AND BKG.CARGO_TYPE=" + CargoType);
                    }

                    if (!string.IsNullOrEmpty(POLPK) & POLPK != "0")
                    {
                        sbNew2.Append("       AND BKG.PORT_MST_POL_FK IN(" + POLPK + ")");
                    }

                    if (!string.IsNullOrEmpty(POLPK) & PODPK != "0")
                    {
                        sbNew2.Append("       AND BKG.PORT_MST_POD_FK IN( " + PODPK + ")");
                    }
                }
                else if (ProcessType == 2)
                {
                    sbNew2.Append("        AND JOB.CUST_CUSTOMER_MST_FK=CMT.CUSTOMER_MST_PK");
                    sbNew2.Append("           AND COL.PROCESS_TYPE = 2");
                    if (CargoType != 0)
                    {
                        sbNew2.Append("     AND JOB.CARGO_TYPE=" + CargoType);
                    }

                    if (!string.IsNullOrEmpty(POLPK) & POLPK != "0")
                    {
                        sbNew2.Append("         AND JOB.PORT_MST_POL_FK IN(" + POLPK + ")");
                    }

                    if (!string.IsNullOrEmpty(POLPK) & PODPK != "0")
                    {
                        sbNew2.Append("         AND JOB.PORT_MST_POD_FK IN(" + PODPK + ")");
                    }
                }
                sbNew2.Append("           AND INV.CONSOL_INVOICE_PK = INVTRN.CONSOL_INVOICE_FK");
                sbNew2.Append("           AND CLT.INVOICE_REF_NR = INV.INVOICE_REF_NO");
                sbNew2.Append("           AND COL.COLLECTIONS_TBL_PK = CLT.COLLECTIONS_TBL_FK");
                sbNew2.Append("           AND CMT.CUSTOMER_MST_PK = CCD.CUSTOMER_MST_FK");
                sbNew2.Append("           AND LMT.LOCATION_MST_PK = CCD.ADM_LOCATION_MST_FK");
                sbNew2.Append("           AND NVL((SELECT SUM(WMT.WRITEOFF_AMOUNT)");
                sbNew2.Append("             FROM " + ParentDB + ".WRITEOFF_MANUAL_TBL WMT");
                sbNew2.Append("            WHERE WMT.CONSOL_INVOICE_FK = INV.CONSOL_INVOICE_PK),");
                sbNew2.Append("           0.00) = 0");
                if (BizType == 1)
                {
                    sbNew2.Append("           AND COL.BUSINESS_TYPE = 1");
                    if (!string.IsNullOrEmpty(FlightID))
                    {
                        sbNew2.Append("            AND JOB.VOYAGE_FLIGHT_NO IN (" + FlightID + ")");
                    }
                }
                else if (BizType == 2)
                {
                    sbNew2.Append("           AND COL.BUSINESS_TYPE = 2");
                    if (!string.IsNullOrEmpty(VslVoyPK))
                    {
                        sbNew2.Append("            AND JOB.VOYAGE_TRN_FK=" + VslVoyPK + "");
                    }
                }
                sbNew2.Append("           AND INV.CHK_INVOICE<>2 ");
                if (!string.IsNullOrEmpty(CountryPK) & CountryPK != "0")
                {
                    sbNew2.Append("       AND LMT.COUNTRY_MST_FK IN(" + CountryPK + ")");
                }

                if (!string.IsNullOrEmpty(LocPK) & LocPK != "0")
                {
                    sbNew2.Append("        AND LMT.LOCATION_MST_PK IN (" + LocPK + ") ");
                }

                if (!string.IsNullOrEmpty(CustPK) & CustPK != "0")
                {
                    sbNew2.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustPK + ")");
                }

                if (!string.IsNullOrEmpty(CustGroupPK) & CustGroupPK != "0")
                {
                    sbNew2.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustGroupPK + ")");
                }

                if (Fromdate != "")
                {
                    sbNew2.Append("           AND COL.COLLECTIONS_DATE <= TO_DATE('" + Fromdate + "')");
                }

                sbNew2.Append("        UNION");
                sbNew2.Append("        SELECT DISTINCT CMT.CUSTOMER_MST_PK,");
                sbNew2.Append("                        CCD.ADM_LOCATION_MST_FK,");
                sbNew2.Append("                        CNT.CREDIT_NOTE_DATE REF_DATE,");
                sbNew2.Append("                        (CASE");
                sbNew2.Append("                          WHEN CNT.PROCESS_TYPE = 1 THEN");
                sbNew2.Append("                           'EXPORT'");
                sbNew2.Append("                          ELSE");
                sbNew2.Append("                           'IMPORT'");
                sbNew2.Append("                        END) PROCESS,");
                sbNew2.Append("                        'CREDITNOTE' TRANSACTION,");
                sbNew2.Append("  ( CASE ");
                sbNew2.Append("    WHEN JOB.BUSINESS_TYPE = 2 AND JOB.PROCESS_TYPE = 1 THEN ");
                sbNew2.Append("      HBL.HBL_REF_NO ");
                sbNew2.Append("     WHEN JOB.BUSINESS_TYPE = 1 AND JOB.PROCESS_TYPE = 1 THEN ");
                sbNew2.Append("         HAWB.HAWB_REF_NO ");
                sbNew2.Append("        ELSE ");
                sbNew2.Append("       JOB.HBL_HAWB_REF_NO  ");
                sbNew2.Append("      END) HBL_REF_NO , ");
                sbNew2.Append("                        CNT.CREDIT_NOTE_REF_NR DOCREFNR,");
                sbNew2.Append("                        CNT.CRN_TBL_PK REF_PK,");
                sbNew2.Append("                        CNT.BIZ_TYPE BIZ_TYPE,");
                if (ProcessType == 1)
                {
                    sbNew2.Append("                        BKG.CARGO_TYPE CARGO_TYPE,");
                }
                else if (BizType == 2)
                {
                    sbNew2.Append("                        JOB.CARGO_TYPE CARGO_TYPE, ");
                }
                else
                {
                    sbNew2.Append("                        0 CARGO_TYPE, ");
                }

                sbNew2.Append("                        0 DEBIT,");
                sbNew2.Append("                        ROUND(CNT.CRN_AMMOUNT * " + ParentDB + ".GET_EX_RATE(CNT.CURRENCY_MST_FK," + CurrPK + ",CNT.CREDIT_NOTE_DATE),2) CREDIT,");
                sbNew2.Append("                        0 BALANCE,''OUTSTANDING_DAYS");
                sbNew2.Append("          FROM " + ParentDB + ".JOB_CARD_TRN   JOB,");
                if (ProcessType == 1)
                {
                    sbNew2.Append("               " + ParentDB + ".BOOKING_MST_TBL        BKG,");
                }

                sbNew2.Append("               " + ParentDB + ".HBL_EXP_TBL            HBL,");
                sbNew2.Append("               " + ParentDB + ".HAWB_EXP_TBL            HAWB,");
                sbNew2.Append("               " + ParentDB + ".CUSTOMER_MST_TBL       CMT,");
                sbNew2.Append("               " + ParentDB + ".CUSTOMER_CONTACT_DTLS  CCD,");
                sbNew2.Append("               " + ParentDB + ".LOCATION_MST_TBL       LMT,");
                sbNew2.Append("               " + ParentDB + ".CONSOL_INVOICE_TBL     INV,");
                sbNew2.Append("               " + ParentDB + ".CONSOL_INVOICE_TRN_TBL INVTRN,");
                sbNew2.Append("               " + ParentDB + ".CREDIT_NOTE_TBL        CNT,");
                sbNew2.Append("               " + ParentDB + ".CREDIT_NOTE_TRN_TBL    CNTT");
                sbNew2.Append("         WHERE INVTRN.JOB_CARD_FK = JOB.JOB_CARD_TRN_PK(+)");
                sbNew2.Append("         AND INV.IS_FAC_INV <> 1");
                sbNew2.Append("           AND INVTRN.JOB_TYPE = 1");
                sbNew2.Append("           AND JOB.HBL_HAWB_FK = HBL.HBL_EXP_TBL_PK(+)");
                sbNew2.Append("           AND JOB.HBL_HAWB_FK = HAWB.HAWB_EXP_TBL_PK(+)");
                if (ProcessType == 1)
                {
                    sbNew2.Append("           AND CMT.CUSTOMER_MST_PK = BKG.CUST_CUSTOMER_MST_FK");
                    sbNew2.Append("           AND JOB.BOOKING_MST_FK = BKG.BOOKING_MST_PK");
                    sbNew2.Append("           AND CNT.PROCESS_TYPE = " + ProcessType);
                    if (CargoType != 0)
                    {
                        sbNew2.Append("     AND BKG.CARGO_TYPE=" + CargoType);
                    }

                    if (!string.IsNullOrEmpty(POLPK) & POLPK != "0")
                    {
                        sbNew2.Append("       AND BKG.PORT_MST_POL_FK IN(" + POLPK + ")");
                    }

                    if (!string.IsNullOrEmpty(POLPK) & PODPK != "0")
                    {
                        sbNew2.Append("       AND BKG.PORT_MST_POD_FK IN( " + PODPK + ")");
                    }
                }
                else if (ProcessType == 2)
                {
                    sbNew2.Append("        AND JOB.CUST_CUSTOMER_MST_FK=CMT.CUSTOMER_MST_PK");
                    sbNew2.Append("           AND CNT.PROCESS_TYPE = 2");
                    if (CargoType != 0)
                    {
                        sbNew2.Append("     AND JOB.CARGO_TYPE=" + CargoType);
                    }

                    if (!string.IsNullOrEmpty(POLPK) & POLPK != "0")
                    {
                        sbNew2.Append("         AND JOB.PORT_MST_POL_FK IN(" + POLPK + ")");
                    }

                    if (!string.IsNullOrEmpty(POLPK) & PODPK != "0")
                    {
                        sbNew2.Append("         AND JOB.PORT_MST_POD_FK IN(" + PODPK + ")");
                    }
                }

                sbNew2.Append("           AND INV.CONSOL_INVOICE_PK = INVTRN.CONSOL_INVOICE_FK");
                sbNew2.Append("           AND CNT.CRN_TBL_PK = CNTT.CRN_TBL_FK");
                sbNew2.Append("           AND CNTT.CONSOL_INVOICE_TRN_FK = INVTRN.CONSOL_INVOICE_TRN_PK");
                sbNew2.Append("           AND CMT.CUSTOMER_MST_PK = CCD.CUSTOMER_MST_FK");
                sbNew2.Append("           AND LMT.LOCATION_MST_PK = CCD.ADM_LOCATION_MST_FK");
                sbNew2.Append("           AND NVL((SELECT SUM(WMT.WRITEOFF_AMOUNT)");
                sbNew2.Append("             FROM " + ParentDB + ".WRITEOFF_MANUAL_TBL WMT");
                sbNew2.Append("            WHERE WMT.CONSOL_INVOICE_FK = INV.CONSOL_INVOICE_PK),");
                sbNew2.Append("           0.00) = 0");
                if (BizType == 1)
                {
                    sbNew2.Append("           AND CNT.BIZ_TYPE = 1");
                    if (!string.IsNullOrEmpty(FlightID))
                    {
                        sbNew2.Append("            AND JOB.VOYAGE_FLIGHT_NO IN (" + FlightID + ")");
                    }
                }
                else if (BizType == 2)
                {
                    sbNew2.Append("           AND CNT.BIZ_TYPE = 2");
                    if (!string.IsNullOrEmpty(VslVoyPK))
                    {
                        sbNew2.Append("            AND JOB.VOYAGE_TRN_FK=" + VslVoyPK + "");
                    }
                }
                sbNew2.Append("           AND INV.CHK_INVOICE<>2 ");
                if (!string.IsNullOrEmpty(CountryPK) & CountryPK != "0")
                {
                    sbNew2.Append("       AND LMT.COUNTRY_MST_FK IN(" + CountryPK + ")");
                }

                if (!string.IsNullOrEmpty(LocPK) & LocPK != "0")
                {
                    sbNew2.Append("        AND LMT.LOCATION_MST_PK IN (" + LocPK + ") ");
                }

                if (!string.IsNullOrEmpty(CustPK) & CustPK != "0")
                {
                    sbNew2.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustPK + ")");
                }

                if (!string.IsNullOrEmpty(CustGroupPK) & CustGroupPK != "0")
                {
                    sbNew2.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustGroupPK + ")");
                }

                if (Fromdate != "")
                {
                    sbNew2.Append("           AND CNT.CREDIT_NOTE_DATE <= TO_DATE('" + Fromdate + "')");
                }

                // -----Added By Sushama----
                // ' ----CBJC(Invoice)--'
                sb.Append(sbNew2.ToString());
                sb.Append(" UNION ");
                sb.Append("           SELECT DISTINCT CMT.CUSTOMER_MST_PK,");
                sb.Append("                        CCD.ADM_LOCATION_MST_FK,");
                sb.Append("                        INV.INVOICE_DATE REF_DATE,");
                sb.Append("                        (CASE");
                sb.Append("                          WHEN INV.PROCESS_TYPE = 1 THEN");
                sb.Append("                           'EXPORT'");
                sb.Append("                          ELSE");
                sb.Append("                           'IMPORT'");
                sb.Append("                        END) PROCESS,");
                sb.Append("                        'INVOICE' TRANSACTION,");
                sb.Append("                        CBJC.HBL_NO HBL_REF_NO,");
                sb.Append("                        INV.INVOICE_REF_NO DOCREFNR,");
                sb.Append("                        INV.CONSOL_INVOICE_PK REF_PK,");
                sb.Append("                        INV.BUSINESS_TYPE BIZ_TYPE,");
                sb.Append("                        CBJC.CARGO_TYPE CARGO_TYPE,");
                sb.Append("                        ROUND(INV.NET_RECEIVABLE *");
                sb.Append("                              " + ParentDB + ".GET_EX_RATE(INV.CURRENCY_MST_FK,");
                sb.Append(CurrPK + ",");
                sb.Append("                                          INV.INVOICE_DATE),");
                sb.Append("                              2) DEBIT,");
                sb.Append("                        0 CREDIT,");
                sb.Append("                        0 BALANCE,");
                sb.Append("                        (SELECT DISTINCT TO_CHAR(TO_DATE(SYSDATE,");
                sb.Append("                                                         'DD/MM/YYYY') -");
                sb.Append("                                                 TO_DATE(INV.INVOICE_DATE)) OUTSTANDING_DAYS");
                sb.Append("                           FROM " + ParentDB + ".COLLECTIONS_TRN_TBL COLL,");
                sb.Append("                                " + ParentDB + ".CONSOL_INVOICE_TBL  INV1");
                sb.Append("                          WHERE INV1.INVOICE_REF_NO = COLL.INVOICE_REF_NR(+)");
                sb.Append("                            AND INV1.INVOICE_REF_NO = INV.INVOICE_REF_NO");
                sb.Append("                            AND NVL((INV1.NET_RECEIVABLE -");
                sb.Append("                                    NVL((SELECT SUM(CTRN.RECD_AMOUNT_HDR_CURR)");
                sb.Append("                                           FROM " + ParentDB + ".COLLECTIONS_TRN_TBL CTRN");
                sb.Append("                                          WHERE CTRN.INVOICE_REF_NR LIKE");
                sb.Append("                                                INV1.INVOICE_REF_NO),");
                sb.Append("                                         0.00)),");
                sb.Append("                                    0) > 0)");
                sb.Append("          FROM " + ParentDB + ".CBJC_TBL   CBJC,");
                sb.Append("              " + ParentDB + ".CUSTOMER_MST_TBL       CMT,");
                sb.Append("               " + ParentDB + ".CUSTOMER_CONTACT_DTLS  CCD,");
                sb.Append("               " + ParentDB + ".LOCATION_MST_TBL       LMT,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TBL     INV,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TRN_TBL INVTRN");
                sb.Append("         WHERE INVTRN.JOB_CARD_FK = CBJC.CBJC_PK(+)");
                sb.Append("           AND INV.IS_FAC_INV <> 1");
                sb.Append("           AND INV.CUSTOMER_MST_FK = CMT.CUSTOMER_MST_PK ");
                sb.Append("           AND INV.PROCESS_TYPE = " + ProcessType);
                sb.Append("           AND INV.CONSOL_INVOICE_PK = INVTRN.CONSOL_INVOICE_FK");
                sb.Append("           AND CMT.CUSTOMER_MST_PK = CCD.CUSTOMER_MST_FK");
                sb.Append("           AND LMT.LOCATION_MST_PK = CCD.ADM_LOCATION_MST_FK");
                sb.Append("           AND NVL((SELECT SUM(WMT.WRITEOFF_AMOUNT)");
                sb.Append("                     FROM " + ParentDB + ".WRITEOFF_MANUAL_TBL WMT");
                sb.Append("                    WHERE WMT.CONSOL_INVOICE_FK = INV.CONSOL_INVOICE_PK),");
                sb.Append("                   0.00) = 0");
                if (Fromdate != "")
                {
                    sb.Append("           AND INV.invoice_date <= TO_DATE('" + Fromdate + "')");
                }

                sb.Append("          AND CBJC.PROCESS_TYPE =" + ProcessType);
                if (BizType != 3)
                {
                    sb.Append("          AND CBJC.BIZ_TYPE =" + BizType);
                }

                if (CargoType != 0)
                {
                    sb.Append("     AND CBJC.CARGO_TYPE=" + CargoType);
                }

                if (!string.IsNullOrEmpty(CountryPK) & CountryPK != "0")
                {
                    sb.Append("       AND LMT.COUNTRY_MST_FK IN(" + CountryPK + ")");
                }

                if (!string.IsNullOrEmpty(LocPK) & LocPK != "0")
                {
                    sb.Append("        AND LMT.LOCATION_MST_PK IN (" + LocPK + ") ");
                }

                if (!string.IsNullOrEmpty(CustPK) & CustPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustPK + ")");
                }

                if (!string.IsNullOrEmpty(CustGroupPK) & CustGroupPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustGroupPK + ")");
                }

                sb.Append("           AND INV.CHK_INVOICE<>2 ");
                if (BizType == 1)
                {
                    if (!string.IsNullOrEmpty(FlightID))
                    {
                        sb.Append("     AND   CBJC.FLIGHT_NO IN (" + FlightID + ")");
                    }
                }
                else if (BizType == 2)
                {
                    if (!string.IsNullOrEmpty(VslVoyPK))
                    {
                        sb.Append("     AND   CBJC.VOYAGE_TRN_FK=" + VslVoyPK + "");
                    }
                }
                sb.Append("           AND INVTRN.JOB_TYPE = 2       ");

                // '    ----TPT(Tranport Note)(Invoice)--
                sb.Append("           UNION");
                sb.Append("           SELECT DISTINCT CMT.CUSTOMER_MST_PK,");
                sb.Append("                        CCD.ADM_LOCATION_MST_FK,");
                sb.Append("                        INV.INVOICE_DATE REF_DATE,");
                sb.Append("                        (CASE");
                sb.Append("                          WHEN INV.PROCESS_TYPE = 1 THEN");
                sb.Append("                           'EXPORT'");
                sb.Append("                          ELSE");
                sb.Append("                           'IMPORT'");
                sb.Append("                        END) PROCESS,");
                sb.Append("                        'INVOICE' TRANSACTION,");
                sb.Append("                        (SELECT CONT.BL_NUMBER FROM " + ParentDB + ".TRANSPORT_TRN_CONT CONT WHERE CONT.TRANSPORT_INST_FK = TIST.TRANSPORT_INST_SEA_PK AND ROWNUM=1) HBL_REF_NO,");
                sb.Append("                        INV.INVOICE_REF_NO DOCREFNR,");
                sb.Append("                        INV.CONSOL_INVOICE_PK REF_PK,");
                sb.Append("                        INV.BUSINESS_TYPE BIZ_TYPE,");
                sb.Append("                        TIST.CARGO_TYPE CARGO_TYPE,");
                sb.Append("                        ROUND(INV.NET_RECEIVABLE *");
                sb.Append("                              " + ParentDB + ".GET_EX_RATE(INV.CURRENCY_MST_FK,");
                sb.Append(CurrPK + ",");
                sb.Append("                                          INV.INVOICE_DATE),");
                sb.Append("                              2) DEBIT,");
                sb.Append("                        0 CREDIT,");
                sb.Append("                        0 BALANCE,");
                sb.Append("                        (SELECT DISTINCT TO_CHAR(TO_DATE(SYSDATE,");
                sb.Append("                                                         'DD/MM/YYYY') -");
                sb.Append("                                                 TO_DATE(INV.INVOICE_DATE)) OUTSTANDING_DAYS");
                sb.Append("                           FROM " + ParentDB + ".COLLECTIONS_TRN_TBL COLL,");
                sb.Append("                                " + ParentDB + ".CONSOL_INVOICE_TBL  INV1");
                sb.Append("                          WHERE INV1.INVOICE_REF_NO = COLL.INVOICE_REF_NR(+)");
                sb.Append("                            AND INV1.INVOICE_REF_NO = INV.INVOICE_REF_NO");
                sb.Append("                            AND NVL((INV1.NET_RECEIVABLE -");
                sb.Append("                                    NVL((SELECT SUM(CTRN.RECD_AMOUNT_HDR_CURR)");
                sb.Append("                                           FROM " + ParentDB + ".COLLECTIONS_TRN_TBL CTRN");
                sb.Append("                                          WHERE CTRN.INVOICE_REF_NR LIKE");
                sb.Append("                                                INV1.INVOICE_REF_NO),");
                sb.Append("                                         0.00)),");
                sb.Append("                                    0) > 0)");
                sb.Append("          FROM " + ParentDB + ".TRANSPORT_INST_SEA_TBL TIST  ,");
                sb.Append("               " + ParentDB + ".CUSTOMER_MST_TBL       CMT,");
                sb.Append("               " + ParentDB + ".CUSTOMER_CONTACT_DTLS  CCD,");
                sb.Append("               " + ParentDB + ".LOCATION_MST_TBL       LMT,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TBL     INV,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TRN_TBL INVTRN");
                sb.Append("         WHERE INVTRN.JOB_CARD_FK = TIST.TRANSPORT_INST_SEA_PK(+)");
                sb.Append("           AND INV.IS_FAC_INV <> 1");
                sb.Append("           AND INV.CUSTOMER_MST_FK = CMT.CUSTOMER_MST_PK(+)");
                sb.Append("           AND INV.PROCESS_TYPE = " + ProcessType);
                sb.Append("           AND INV.CONSOL_INVOICE_PK = INVTRN.CONSOL_INVOICE_FK");
                sb.Append("           AND CMT.CUSTOMER_MST_PK = CCD.CUSTOMER_MST_FK");
                sb.Append("           AND LMT.LOCATION_MST_PK = CCD.ADM_LOCATION_MST_FK");
                sb.Append("           AND NVL((SELECT SUM(WMT.WRITEOFF_AMOUNT)");
                sb.Append("                     FROM " + ParentDB + ".WRITEOFF_MANUAL_TBL WMT");
                sb.Append("                    WHERE WMT.CONSOL_INVOICE_FK = INV.CONSOL_INVOICE_PK),");
                sb.Append("                   0.00) = 0");
                if (Fromdate != "")
                {
                    sb.Append("           AND INV.invoice_date  <= TO_DATE('" + Fromdate + "')");
                }

                sb.Append("          AND TIST.PROCESS_TYPE =" + ProcessType);
                if (BizType != 3)
                {
                    sb.Append("          AND TIST.BUSINESS_TYPE =" + BizType);
                }

                if (CargoType != 0)
                {
                    sb.Append("     AND TIST.CARGO_TYPE=" + CargoType);
                }

                if (!string.IsNullOrEmpty(CountryPK) & CountryPK != "0")
                {
                    sb.Append("       AND LMT.COUNTRY_MST_FK IN(" + CountryPK + ")");
                }

                if (!string.IsNullOrEmpty(LocPK) & LocPK != "0")
                {
                    sb.Append("        AND LMT.LOCATION_MST_PK IN (" + LocPK + ") ");
                }

                if (!string.IsNullOrEmpty(CustPK) & CustPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustPK + ")");
                }

                if (!string.IsNullOrEmpty(CustGroupPK) & CustGroupPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustGroupPK + ")");
                }

                if (BizType == 1)
                {
                    if (!string.IsNullOrEmpty(FlightID))
                    {
                        sb.Append("     AND   TIST.FLIGHT_NO IN (" + FlightID + ")");
                    }
                }
                else if (BizType == 2)
                {
                    if (!string.IsNullOrEmpty(VslVoyPK))
                    {
                        sb.Append("     AND   TIST.VSL_VOY_FK=" + VslVoyPK + "");
                    }
                }
                sb.Append("           AND INVTRN.JOB_TYPE = 3");
                sb.Append("           AND INV.CHK_INVOICE<>2 ");
                // '       ----DET & DEM TPT-(Invoice)-
                sb.Append("        UNION");
                sb.Append("           SELECT DISTINCT CMT.CUSTOMER_MST_PK,");
                sb.Append("                        CCD.ADM_LOCATION_MST_FK,");
                sb.Append("                        INV.INVOICE_DATE REF_DATE,");
                sb.Append("                        (CASE");
                sb.Append("                          WHEN INV.PROCESS_TYPE = 1 THEN");
                sb.Append("                           'EXPORT'");
                sb.Append("                          ELSE");
                sb.Append("                           'IMPORT'");
                sb.Append("                        END) PROCESS,");
                sb.Append("                        'INVOICE' TRANSACTION,");
                sb.Append("                        (SELECT CONT.BL_NUMBER FROM " + ParentDB + ".TRANSPORT_TRN_CONT CONT WHERE CONT.TRANSPORT_INST_FK = TIST.TRANSPORT_INST_SEA_PK AND ROWNUM=1) HBL_REF_NO,");
                sb.Append("                        INV.INVOICE_REF_NO DOCREFNR,");
                sb.Append("                        INV.CONSOL_INVOICE_PK REF_PK,");
                sb.Append("                        INV.BUSINESS_TYPE BIZ_TYPE,");
                sb.Append("                        DCH.CARGO_TYPE CARGO_TYPE,");
                sb.Append("                        ROUND(INV.NET_RECEIVABLE *");
                sb.Append("                              " + ParentDB + ".GET_EX_RATE(INV.CURRENCY_MST_FK,");
                sb.Append(CurrPK + ",");
                sb.Append("                                          INV.INVOICE_DATE),");
                sb.Append("                              2) DEBIT,");
                sb.Append("                        0 CREDIT,");
                sb.Append("                        0 BALANCE,");
                sb.Append("                        (SELECT DISTINCT TO_CHAR(TO_DATE(SYSDATE,");
                sb.Append("                                                         'DD/MM/YYYY') -");
                sb.Append("                                                 TO_DATE(INV.INVOICE_DATE)) OUTSTANDING_DAYS");
                sb.Append("                           FROM " + ParentDB + ".COLLECTIONS_TRN_TBL COLL,");
                sb.Append("                                " + ParentDB + ".CONSOL_INVOICE_TBL  INV1");
                sb.Append("                          WHERE INV1.INVOICE_REF_NO = COLL.INVOICE_REF_NR(+)");
                sb.Append("                            AND INV1.INVOICE_REF_NO = INV.INVOICE_REF_NO");
                sb.Append("                            AND NVL((INV1.NET_RECEIVABLE -");
                sb.Append("                                    NVL((SELECT SUM(CTRN.RECD_AMOUNT_HDR_CURR)");
                sb.Append("                                           FROM " + ParentDB + ".COLLECTIONS_TRN_TBL CTRN");
                sb.Append("                                          WHERE CTRN.INVOICE_REF_NR LIKE");
                sb.Append("                                                INV1.INVOICE_REF_NO),");
                sb.Append("                                         0.00)),");
                sb.Append("                                    0) > 0)");
                sb.Append("          FROM " + ParentDB + ".DEM_CALC_HDR DCH  ,");
                sb.Append("              " + ParentDB + ".TRANSPORT_INST_SEA_TBL TIST,");
                sb.Append("               " + ParentDB + ".CUSTOMER_MST_TBL       CMT,");
                sb.Append("               " + ParentDB + ".CUSTOMER_CONTACT_DTLS  CCD,");
                sb.Append("               " + ParentDB + ".LOCATION_MST_TBL       LMT,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TBL     INV,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TRN_TBL INVTRN");
                sb.Append("         WHERE INVTRN.JOB_CARD_FK = DCH.DEM_CALC_HDR_PK(+)");
                sb.Append("           AND DCH.DOC_REF_FK = TIST.TRANSPORT_INST_SEA_PK");
                sb.Append("           AND INV.IS_FAC_INV <> 1");
                sb.Append("           AND INV.CUSTOMER_MST_FK = CMT.CUSTOMER_MST_PK(+)");
                sb.Append("           AND INV.PROCESS_TYPE = " + ProcessType);
                sb.Append("           AND INV.CONSOL_INVOICE_PK = INVTRN.CONSOL_INVOICE_FK");
                sb.Append("           AND CMT.CUSTOMER_MST_PK = CCD.CUSTOMER_MST_FK");
                sb.Append("           AND LMT.LOCATION_MST_PK = CCD.ADM_LOCATION_MST_FK");
                sb.Append("           AND NVL((SELECT SUM(WMT.WRITEOFF_AMOUNT)");
                sb.Append("                     FROM " + ParentDB + ".WRITEOFF_MANUAL_TBL WMT");
                sb.Append("                    WHERE WMT.CONSOL_INVOICE_FK = INV.CONSOL_INVOICE_PK),");
                sb.Append("                   0.00) = 0");
                sb.Append("          AND DCH.PROCESS_TYPE =" + ProcessType);
                if (BizType != 3)
                {
                    sb.Append("          AND DCH.Biz_Type =" + BizType);
                }

                if (BizType == 1)
                {
                    if (!string.IsNullOrEmpty(FlightID))
                    {
                        sb.Append("     AND   TIST.FLIGHT_NO IN (" + FlightID + ")");
                    }
                }
                else if (BizType == 2)
                {
                    if (!string.IsNullOrEmpty(VslVoyPK))
                    {
                        sb.Append("     AND   TIST.VSL_VOY_FK=" + VslVoyPK + "");
                    }
                }
                if (Fromdate != "")
                {
                    sb.Append("       AND INV.invoice_date  <= TO_DATE('" + Fromdate + "')");
                }

                if (!string.IsNullOrEmpty(CountryPK) & CountryPK != "0")
                {
                    sb.Append("       AND LMT.COUNTRY_MST_FK IN(" + CountryPK + ")");
                }

                if (!string.IsNullOrEmpty(LocPK) & LocPK != "0")
                {
                    sb.Append("        AND LMT.LOCATION_MST_PK IN (" + LocPK + ") ");
                }

                if (!string.IsNullOrEmpty(CustPK) & CustPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustPK + ")");
                }

                if (!string.IsNullOrEmpty(CustGroupPK) & CustGroupPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustGroupPK + ")");
                }

                if (CargoType != 0)
                {
                    sb.Append("     AND DCH.CARGO_TYPE=" + CargoType);
                }

                sb.Append("           AND INVTRN.JOB_TYPE = 3");
                sb.Append("           AND INV.CHK_INVOICE<>2 ");
                // '       ----DET & DEM-CBJC(Invoice)-
                sb.Append("        UNION");
                sb.Append("           SELECT DISTINCT CMT.CUSTOMER_MST_PK,");
                sb.Append("                        CCD.ADM_LOCATION_MST_FK,");
                sb.Append("                        INV.INVOICE_DATE REF_DATE,");
                sb.Append("                        (CASE");
                sb.Append("                          WHEN INV.PROCESS_TYPE = 1 THEN");
                sb.Append("                           'EXPORT'");
                sb.Append("                          ELSE");
                sb.Append("                           'IMPORT'");
                sb.Append("                        END) PROCESS,");
                sb.Append("                        'INVOICE' TRANSACTION,");
                sb.Append("                        CBJC.HBL_NO HBL_REF_NO,");
                sb.Append("                        INV.INVOICE_REF_NO DOCREFNR,");
                sb.Append("                        INV.CONSOL_INVOICE_PK REF_PK,");
                sb.Append("                        INV.BUSINESS_TYPE BIZ_TYPE,");
                sb.Append("                        DCH.CARGO_TYPE CARGO_TYPE,");
                sb.Append("                        ROUND(INV.NET_RECEIVABLE *");
                sb.Append("                              " + ParentDB + ".GET_EX_RATE(INV.CURRENCY_MST_FK,");
                sb.Append(CurrPK + ",");
                sb.Append("                                          INV.INVOICE_DATE),");
                sb.Append("                              2) DEBIT,");
                sb.Append("                        0 CREDIT,");
                sb.Append("                        0 BALANCE,");
                sb.Append("                        (SELECT DISTINCT TO_CHAR(TO_DATE(SYSDATE,");
                sb.Append("                                                         'DD/MM/YYYY') -");
                sb.Append("                                                 TO_DATE(INV.INVOICE_DATE)) OUTSTANDING_DAYS");
                sb.Append("                           FROM " + ParentDB + ".COLLECTIONS_TRN_TBL COLL,");
                sb.Append("                                " + ParentDB + ".CONSOL_INVOICE_TBL  INV1");
                sb.Append("                          WHERE INV1.INVOICE_REF_NO = COLL.INVOICE_REF_NR(+)");
                sb.Append("                            AND INV1.INVOICE_REF_NO = INV.INVOICE_REF_NO");
                sb.Append("                            AND NVL((INV1.NET_RECEIVABLE -");
                sb.Append("                                    NVL((SELECT SUM(CTRN.RECD_AMOUNT_HDR_CURR)");
                sb.Append("                                           FROM " + ParentDB + ".COLLECTIONS_TRN_TBL CTRN");
                sb.Append("                                          WHERE CTRN.INVOICE_REF_NR LIKE");
                sb.Append("                                                INV1.INVOICE_REF_NO),");
                sb.Append("                                         0.00)),");
                sb.Append("                                    0) > 0)");
                sb.Append("          FROM " + ParentDB + ".DEM_CALC_HDR DCH  ,");
                sb.Append("               " + ParentDB + ".CBJC_TBL               CBJC,");
                sb.Append("               " + ParentDB + ".CUSTOMER_MST_TBL       CMT,");
                sb.Append("               " + ParentDB + ".CUSTOMER_CONTACT_DTLS  CCD,");
                sb.Append("               " + ParentDB + ".LOCATION_MST_TBL       LMT,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TBL     INV,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TRN_TBL INVTRN");
                sb.Append("         WHERE INVTRN.JOB_CARD_FK = DCH.DEM_CALC_HDR_PK(+)");
                sb.Append("           AND DCH.DOC_REF_FK = CBJC.CBJC_PK");
                sb.Append("           AND INV.IS_FAC_INV <> 1");
                sb.Append("           AND INV.CUSTOMER_MST_FK = CMT.CUSTOMER_MST_PK(+)");
                sb.Append("           AND INV.PROCESS_TYPE = " + ProcessType);
                sb.Append("           AND INV.CONSOL_INVOICE_PK = INVTRN.CONSOL_INVOICE_FK");
                sb.Append("           AND CMT.CUSTOMER_MST_PK = CCD.CUSTOMER_MST_FK");
                sb.Append("           AND LMT.LOCATION_MST_PK = CCD.ADM_LOCATION_MST_FK");
                sb.Append("           AND NVL((SELECT SUM(WMT.WRITEOFF_AMOUNT)");
                sb.Append("                     FROM " + ParentDB + ".WRITEOFF_MANUAL_TBL WMT");
                sb.Append("                    WHERE WMT.CONSOL_INVOICE_FK = INV.CONSOL_INVOICE_PK),");
                sb.Append("                   0.00) = 0");
                sb.Append("          AND DCH.PROCESS_TYPE =" + ProcessType);
                if (BizType != 3)
                {
                    sb.Append("          AND DCH.Biz_Type =" + BizType);
                }

                if (CargoType != 0)
                {
                    sb.Append("     AND DCH.CARGO_TYPE=" + CargoType);
                }

                if (BizType == 1)
                {
                    if (!string.IsNullOrEmpty(FlightID))
                    {
                        sb.Append("         AND   CBJC.FLIGHT_NO IN (" + FlightID + ")");
                    }
                }
                else if (BizType == 2)
                {
                    if (!string.IsNullOrEmpty(VslVoyPK))
                    {
                        sb.Append("         AND   CBJC.VOYAGE_TRN_FK=" + VslVoyPK + "");
                    }
                }
                if (Fromdate != "")
                {
                    sb.Append("       AND INV.invoice_date  <= TO_DATE('" + Fromdate + "')");
                }

                if (!string.IsNullOrEmpty(CountryPK) & CountryPK != "0")
                {
                    sb.Append("       AND LMT.COUNTRY_MST_FK IN(" + CountryPK + ")");
                }

                if (!string.IsNullOrEmpty(LocPK) & LocPK != "0")
                {
                    sb.Append("        AND LMT.LOCATION_MST_PK IN (" + LocPK + ") ");
                }

                if (!string.IsNullOrEmpty(CustPK) & CustPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustPK + ")");
                }

                if (!string.IsNullOrEmpty(CustGroupPK) & CustGroupPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustGroupPK + ")");
                }

                sb.Append("           AND INVTRN.JOB_TYPE = 4");
                sb.Append("           AND INV.CHK_INVOICE<>2 ");
                // '          ---CBJC(Collection)---------'
                sb.Append("              UNION");
                sb.Append("              SELECT DISTINCT CMT.CUSTOMER_MST_PK,");
                sb.Append("                        CCD.ADM_LOCATION_MST_FK,");
                sb.Append("                        COL.COLLECTIONS_DATE REF_DATE,");
                sb.Append("                        (CASE");
                sb.Append("                          WHEN COL.PROCESS_TYPE = 1 THEN");
                sb.Append("                           'EXPORT'");
                sb.Append("                          ELSE");
                sb.Append("                           'IMPORT'");
                sb.Append("                        END) PROCESS,");
                sb.Append("                        'COLLECTION' TRANSACTION,");
                sb.Append("                        CBJC.HBL_NO HBL_REF_NO,");
                sb.Append("                        COL.COLLECTIONS_REF_NO DOCREFNR,");
                sb.Append("                        COL.COLLECTIONS_TBL_PK REF_PK,");
                sb.Append("                        COL.BUSINESS_TYPE BIZ_TYPE,");
                sb.Append("                        CBJC.CARGO_TYPE CARGO_TYPE,");
                sb.Append("                        0 DEBIT,");
                sb.Append("                        ROUND(CLT.RECD_AMOUNT_HDR_CURR *");
                sb.Append("                              " + ParentDB + ".GET_EX_RATE(COL.CURRENCY_MST_FK,");
                sb.Append(CurrPK + ",");
                sb.Append("                                          COL.COLLECTIONS_DATE),");
                sb.Append("                              2) CREDIT,");
                sb.Append("                        0 BALANCE,");
                sb.Append("                        '' OUTSTANDING_DAYS");
                sb.Append("          FROM ");
                sb.Append("               " + ParentDB + ".CBJC_TBL               CBJC,");
                sb.Append("               " + ParentDB + ".CUSTOMER_MST_TBL       CMT,");
                sb.Append("               " + ParentDB + ".CUSTOMER_CONTACT_DTLS  CCD,");
                sb.Append("               " + ParentDB + ".LOCATION_MST_TBL       LMT,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TBL     INV,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TRN_TBL INVTRN,");
                sb.Append("               " + ParentDB + ".COLLECTIONS_TRN_TBL    CLT,");
                sb.Append("               " + ParentDB + ".COLLECTIONS_TBL        COL");
                sb.Append("         WHERE INVTRN.JOB_CARD_FK = CBJC.CBJC_PK(+)          ");
                sb.Append("           AND INV.IS_FAC_INV <> 1");
                sb.Append("           AND INV.CUSTOMER_MST_FK = CMT.CUSTOMER_MST_PK");
                sb.Append("           AND COL.PROCESS_TYPE = " + ProcessType);
                sb.Append("           AND INV.CONSOL_INVOICE_PK = INVTRN.CONSOL_INVOICE_FK");
                sb.Append("           AND CLT.INVOICE_REF_NR(+) = INV.INVOICE_REF_NO");
                sb.Append("           AND COL.COLLECTIONS_TBL_PK(+) = CLT.COLLECTIONS_TBL_FK");
                sb.Append("           AND CMT.CUSTOMER_MST_PK = CCD.CUSTOMER_MST_FK");
                sb.Append("           AND LMT.LOCATION_MST_PK = CCD.ADM_LOCATION_MST_FK");
                sb.Append("           AND NVL((SELECT SUM(WMT.WRITEOFF_AMOUNT)");
                sb.Append("                     FROM " + ParentDB + ".WRITEOFF_MANUAL_TBL WMT");
                sb.Append("                    WHERE WMT.CONSOL_INVOICE_FK = INV.CONSOL_INVOICE_PK),");
                sb.Append("                   0.00) = 0");
                if (Fromdate != "")
                {
                    sb.Append("           AND COL.COLLECTIONS_DATE  <= TO_DATE('" + Fromdate + "')");
                }

                sb.Append("          AND CBJC.PROCESS_TYPE =" + ProcessType);
                if (BizType != 3)
                {
                    sb.Append("          AND CBJC.BIZ_TYPE =" + BizType);
                }

                if (CargoType != 0)
                {
                    sb.Append("     AND CBJC.CARGO_TYPE=" + CargoType);
                }

                if (!string.IsNullOrEmpty(CountryPK) & CountryPK != "0")
                {
                    sb.Append("       AND LMT.COUNTRY_MST_FK IN(" + CountryPK + ")");
                }

                if (!string.IsNullOrEmpty(LocPK) & LocPK != "0")
                {
                    sb.Append("        AND LMT.LOCATION_MST_PK IN (" + LocPK + ") ");
                }

                if (!string.IsNullOrEmpty(CustPK) & CustPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustPK + ")");
                }

                if (!string.IsNullOrEmpty(CustGroupPK) & CustGroupPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustGroupPK + ")");
                }

                if (BizType == 1)
                {
                    if (!string.IsNullOrEmpty(FlightID))
                    {
                        sb.Append("     AND   CBJC.FLIGHT_NO IN (" + FlightID + ")");
                    }
                }
                else if (BizType == 2)
                {
                    if (!string.IsNullOrEmpty(VslVoyPK))
                    {
                        sb.Append("     AND   CBJC.VOYAGE_TRN_FK=" + VslVoyPK + "");
                    }
                }
                sb.Append("               AND INVTRN.JOB_TYPE = 2");
                sb.Append("           AND INV.CHK_INVOICE<>2 ");
                // '              ----TPT-(Collection)---------'
                sb.Append("        UNION");
                sb.Append("              SELECT DISTINCT CMT.CUSTOMER_MST_PK,");
                sb.Append("                        CCD.ADM_LOCATION_MST_FK,");
                sb.Append("                        COL.COLLECTIONS_DATE REF_DATE,");
                sb.Append("                        (CASE");
                sb.Append("                          WHEN COL.PROCESS_TYPE = 1 THEN");
                sb.Append("                           'EXPORT'");
                sb.Append("                          ELSE");
                sb.Append("                           'IMPORT'");
                sb.Append("                        END) PROCESS,");
                sb.Append("                        'COLLECTION' TRANSACTION,");
                sb.Append("                        (SELECT CON.BL_NUMBER FROM " + ParentDB + ".TRANSPORT_TRN_CONT CON WHERE CON.TRANSPORT_INST_FK = TIST.TRANSPORT_INST_SEA_PK AND ROWNUM=1) HBL_REF_NO,");
                sb.Append("                        COL.COLLECTIONS_REF_NO DOCREFNR,");
                sb.Append("                        COL.COLLECTIONS_TBL_PK REF_PK,");
                sb.Append("                        COL.BUSINESS_TYPE BIZ_TYPE,");
                sb.Append("                        TIST.CARGO_TYPE CARGO_TYPE,");
                sb.Append("                        0 DEBIT,");
                sb.Append("                        ROUND(CLT.RECD_AMOUNT_HDR_CURR *");
                sb.Append("                              " + ParentDB + ".GET_EX_RATE(COL.CURRENCY_MST_FK,");
                sb.Append(CurrPK + ",");
                sb.Append("                                          COL.COLLECTIONS_DATE),");
                sb.Append("                              2) CREDIT,");
                sb.Append("                        0 BALANCE,");
                sb.Append("                        '' OUTSTANDING_DAYS");
                sb.Append("          FROM " + ParentDB + ".TRANSPORT_INST_SEA_TBL TIST ,");
                sb.Append("               " + ParentDB + ".CUSTOMER_MST_TBL       CMT,");
                sb.Append("               " + ParentDB + ".CUSTOMER_CONTACT_DTLS  CCD,");
                sb.Append("               " + ParentDB + ".LOCATION_MST_TBL       LMT,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TBL     INV,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TRN_TBL INVTRN,");
                sb.Append("               " + ParentDB + ".COLLECTIONS_TRN_TBL    CLT,");
                sb.Append("               " + ParentDB + ".COLLECTIONS_TBL        COL");
                sb.Append("         WHERE INVTRN.JOB_CARD_FK = TIST.TRANSPORT_INST_SEA_PK(+)");
                sb.Append("           AND INV.IS_FAC_INV <> 1");
                sb.Append("           AND INV.CUSTOMER_MST_FK = CMT.CUSTOMER_MST_PK");
                sb.Append("           AND COL.PROCESS_TYPE = " + ProcessType);
                sb.Append("           AND INV.CONSOL_INVOICE_PK = INVTRN.CONSOL_INVOICE_FK");
                sb.Append("           AND CLT.INVOICE_REF_NR(+) = INV.INVOICE_REF_NO");
                sb.Append("           AND COL.COLLECTIONS_TBL_PK(+) = CLT.COLLECTIONS_TBL_FK");
                sb.Append("           AND CMT.CUSTOMER_MST_PK = CCD.CUSTOMER_MST_FK");
                sb.Append("           AND LMT.LOCATION_MST_PK = CCD.ADM_LOCATION_MST_FK");
                sb.Append("           AND NVL((SELECT SUM(WMT.WRITEOFF_AMOUNT)");
                sb.Append("                     FROM " + ParentDB + ".WRITEOFF_MANUAL_TBL WMT");
                sb.Append("                    WHERE WMT.CONSOL_INVOICE_FK = INV.CONSOL_INVOICE_PK),");
                sb.Append("                   0.00) = 0");
                if (Fromdate != "")
                {
                    sb.Append("           AND COL.COLLECTIONS_DATE  <= TO_DATE('" + Fromdate + "')");
                }

                sb.Append("          AND TIST.PROCESS_TYPE =" + ProcessType);
                if (BizType != 3)
                {
                    sb.Append("          AND TIST.BUSINESS_TYPE =" + BizType);
                }

                if (CargoType != 0)
                {
                    sb.Append("     AND TIST.CARGO_TYPE=" + CargoType);
                }

                if (!string.IsNullOrEmpty(CountryPK) & CountryPK != "0")
                {
                    sb.Append("       AND LMT.COUNTRY_MST_FK IN(" + CountryPK + ")");
                }

                if (!string.IsNullOrEmpty(LocPK) & LocPK != "0")
                {
                    sb.Append("        AND LMT.LOCATION_MST_PK IN (" + LocPK + ") ");
                }

                if (!string.IsNullOrEmpty(CustPK) & CustPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustPK + ")");
                }

                if (!string.IsNullOrEmpty(CustGroupPK) & CustGroupPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustGroupPK + ")");
                }

                if (BizType == 1)
                {
                    if (!string.IsNullOrEmpty(FlightID))
                    {
                        sb.Append("     AND   TIST.FLIGHT_NO IN (" + FlightID + ")");
                    }
                }
                else if (BizType == 2)
                {
                    if (!string.IsNullOrEmpty(VslVoyPK))
                    {
                        sb.Append("     AND   TIST.VSL_VOY_FK=" + VslVoyPK + "");
                    }
                }
                sb.Append("               AND INVTRN.JOB_TYPE = 3");
                sb.Append("           AND INV.CHK_INVOICE<>2 ");
                // '         ---Det & DEM TPT (Collection)'
                sb.Append("               UNION");
                sb.Append("               SELECT DISTINCT CMT.CUSTOMER_MST_PK,");
                sb.Append("                        CCD.ADM_LOCATION_MST_FK,");
                sb.Append("                        COL.COLLECTIONS_DATE REF_DATE,");
                sb.Append("                        (CASE");
                sb.Append("                          WHEN COL.PROCESS_TYPE = 1 THEN");
                sb.Append("                           'EXPORT'");
                sb.Append("                          ELSE");
                sb.Append("                           'IMPORT'");
                sb.Append("                        END) PROCESS,");
                sb.Append("                        'COLLECTION' TRANSACTION,");
                sb.Append("                        (SELECT CON.BL_NUMBER FROM " + ParentDB + ".TRANSPORT_TRN_CONT CON WHERE CON.TRANSPORT_INST_FK = TIST.TRANSPORT_INST_SEA_PK AND ROWNUM=1) HBL_REF_NO,");
                sb.Append("                        COL.COLLECTIONS_REF_NO DOCREFNR,");
                sb.Append("                        COL.COLLECTIONS_TBL_PK REF_PK,");
                sb.Append("                        COL.BUSINESS_TYPE BIZ_TYPE,");
                sb.Append("                        DCH.CARGO_TYPE CARGO_TYPE,");
                sb.Append("                        0 DEBIT,");
                sb.Append("                        ROUND(CLT.RECD_AMOUNT_HDR_CURR *");
                sb.Append("                              " + ParentDB + ".GET_EX_RATE(COL.CURRENCY_MST_FK,");
                sb.Append(CurrPK + ",");
                sb.Append("                                          COL.COLLECTIONS_DATE),");
                sb.Append("                              2) CREDIT,");
                sb.Append("                        0 BALANCE,");
                sb.Append("                        '' OUTSTANDING_DAYS");
                sb.Append("          FROM " + ParentDB + ".DEM_CALC_HDR DCH  ,");
                sb.Append("                " + ParentDB + ".TRANSPORT_INST_SEA_TBL TIST,");
                sb.Append("               " + ParentDB + ".CUSTOMER_MST_TBL       CMT,");
                sb.Append("               " + ParentDB + ".CUSTOMER_CONTACT_DTLS  CCD,");
                sb.Append("               " + ParentDB + ".LOCATION_MST_TBL       LMT,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TBL     INV,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TRN_TBL INVTRN,");
                sb.Append("               " + ParentDB + ".COLLECTIONS_TRN_TBL    CLT,");
                sb.Append("               " + ParentDB + ".COLLECTIONS_TBL        COL");
                sb.Append("         WHERE INVTRN.JOB_CARD_FK = DCH.DEM_CALC_HDR_PK(+)");
                sb.Append("           AND DCH.DOC_REF_FK =TIST.TRANSPORT_INST_SEA_PK");
                sb.Append("           AND INV.IS_FAC_INV <> 1");
                sb.Append("           AND INV.CUSTOMER_MST_FK = CMT.CUSTOMER_MST_PK");
                sb.Append("           AND COL.PROCESS_TYPE = " + ProcessType);
                sb.Append("           AND INV.CONSOL_INVOICE_PK = INVTRN.CONSOL_INVOICE_FK");
                sb.Append("           AND CLT.INVOICE_REF_NR(+) = INV.INVOICE_REF_NO");
                sb.Append("           AND COL.COLLECTIONS_TBL_PK(+) = CLT.COLLECTIONS_TBL_FK");
                sb.Append("           AND CMT.CUSTOMER_MST_PK = CCD.CUSTOMER_MST_FK");
                sb.Append("           AND LMT.LOCATION_MST_PK = CCD.ADM_LOCATION_MST_FK");
                sb.Append("           AND NVL((SELECT SUM(WMT.WRITEOFF_AMOUNT)");
                sb.Append("                     FROM " + ParentDB + ".WRITEOFF_MANUAL_TBL WMT");
                sb.Append("                    WHERE WMT.CONSOL_INVOICE_FK = INV.CONSOL_INVOICE_PK),");
                sb.Append("                   0.00) = 0");
                sb.Append("          AND DCH.PROCESS_TYPE =" + ProcessType);
                if (BizType != 3)
                {
                    sb.Append("          AND DCH.biz_type =" + BizType);
                }

                if (BizType == 1)
                {
                    if (!string.IsNullOrEmpty(FlightID))
                    {
                        sb.Append("     AND   TIST.FLIGHT_NO IN (" + FlightID + ")");
                    }
                }
                else if (BizType == 2)
                {
                    if (!string.IsNullOrEmpty(VslVoyPK))
                    {
                        sb.Append("     AND   TIST.VSL_VOY_FK=" + VslVoyPK + "");
                    }
                }
                if (CargoType != 0)
                {
                    sb.Append("     AND DCH.CARGO_TYPE=" + CargoType);
                }

                if (Fromdate != "")
                {
                    sb.Append("       AND  COL.COLLECTIONS_DATE   <= TO_DATE('" + Fromdate + "')");
                }

                if (!string.IsNullOrEmpty(CountryPK) & CountryPK != "0")
                {
                    sb.Append("       AND LMT.COUNTRY_MST_FK IN(" + CountryPK + ")");
                }

                if (!string.IsNullOrEmpty(LocPK) & LocPK != "0")
                {
                    sb.Append("        AND LMT.LOCATION_MST_PK IN (" + LocPK + ") ");
                }

                if (!string.IsNullOrEmpty(CustPK) & CustPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustPK + ")");
                }

                if (!string.IsNullOrEmpty(CustGroupPK) & CustGroupPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustGroupPK + ")");
                }

                sb.Append("           AND INVTRN.JOB_TYPE = 3     ");
                sb.Append("           AND INV.CHK_INVOICE<>2 ");
                // '         ---Det & DEM CBJC (Collection)'
                sb.Append("               UNION");
                sb.Append("               SELECT DISTINCT CMT.CUSTOMER_MST_PK,");
                sb.Append("                        CCD.ADM_LOCATION_MST_FK,");
                sb.Append("                        COL.COLLECTIONS_DATE REF_DATE,");
                sb.Append("                        (CASE");
                sb.Append("                          WHEN COL.PROCESS_TYPE = 1 THEN");
                sb.Append("                           'EXPORT'");
                sb.Append("                          ELSE");
                sb.Append("                           'IMPORT'");
                sb.Append("                        END) PROCESS,");
                sb.Append("                        'COLLECTION' TRANSACTION,");
                sb.Append("                        CBJC.HBL_NO HBL_REF_NO,");
                sb.Append("                        COL.COLLECTIONS_REF_NO DOCREFNR,");
                sb.Append("                        COL.COLLECTIONS_TBL_PK REF_PK,");
                sb.Append("                        COL.BUSINESS_TYPE BIZ_TYPE,");
                sb.Append("                        DCH.CARGO_TYPE CARGO_TYPE,");
                sb.Append("                        0 DEBIT,");
                sb.Append("                        ROUND(CLT.RECD_AMOUNT_HDR_CURR *");
                sb.Append("                              " + ParentDB + ".GET_EX_RATE(COL.CURRENCY_MST_FK,");
                sb.Append(CurrPK + ",");
                sb.Append("                                          COL.COLLECTIONS_DATE),");
                sb.Append("                              2) CREDIT,");
                sb.Append("                        0 BALANCE,");
                sb.Append("                        '' OUTSTANDING_DAYS");
                sb.Append("          FROM " + ParentDB + ".DEM_CALC_HDR DCH  ,");
                sb.Append("               " + ParentDB + ".CBJC_TBL               CBJC,");
                sb.Append("               " + ParentDB + ".CUSTOMER_MST_TBL       CMT,");
                sb.Append("               " + ParentDB + ".CUSTOMER_CONTACT_DTLS  CCD,");
                sb.Append("               " + ParentDB + ".LOCATION_MST_TBL       LMT,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TBL     INV,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TRN_TBL INVTRN,");
                sb.Append("               " + ParentDB + ".COLLECTIONS_TRN_TBL    CLT,");
                sb.Append("               " + ParentDB + ".COLLECTIONS_TBL        COL");
                sb.Append("         WHERE INVTRN.JOB_CARD_FK = DCH.DEM_CALC_HDR_PK(+)");
                sb.Append("           AND DCH.DOC_REF_FK = CBJC.CBJC_PK");
                sb.Append("           AND INV.IS_FAC_INV <> 1");
                sb.Append("           AND INV.CUSTOMER_MST_FK = CMT.CUSTOMER_MST_PK");
                sb.Append("           AND COL.PROCESS_TYPE = " + ProcessType);
                sb.Append("           AND INV.CONSOL_INVOICE_PK = INVTRN.CONSOL_INVOICE_FK");
                sb.Append("           AND CLT.INVOICE_REF_NR(+) = INV.INVOICE_REF_NO");
                sb.Append("           AND COL.COLLECTIONS_TBL_PK(+) = CLT.COLLECTIONS_TBL_FK");
                sb.Append("           AND CMT.CUSTOMER_MST_PK = CCD.CUSTOMER_MST_FK");
                sb.Append("           AND LMT.LOCATION_MST_PK = CCD.ADM_LOCATION_MST_FK");
                sb.Append("           AND NVL((SELECT SUM(WMT.WRITEOFF_AMOUNT)");
                sb.Append("                     FROM " + ParentDB + ".WRITEOFF_MANUAL_TBL WMT");
                sb.Append("                    WHERE WMT.CONSOL_INVOICE_FK = INV.CONSOL_INVOICE_PK),");
                sb.Append("                   0.00) = 0");
                sb.Append("          AND DCH.PROCESS_TYPE =" + ProcessType);
                if (BizType != 3)
                {
                    sb.Append("          AND DCH.biz_type =" + BizType);
                }

                if (BizType == 1)
                {
                    if (!string.IsNullOrEmpty(FlightID))
                    {
                        sb.Append("         AND   CBJC.FLIGHT_NO IN (" + FlightID + ")");
                    }
                }
                else if (BizType == 2)
                {
                    if (!string.IsNullOrEmpty(VslVoyPK))
                    {
                        sb.Append("         AND   CBJC.VOYAGE_TRN_FK=" + VslVoyPK + "");
                    }
                }
                if (Fromdate != "")
                {
                    sb.Append("       AND  COL.COLLECTIONS_DATE   <= TO_DATE('" + Fromdate + "')");
                }

                if (CargoType != 0)
                {
                    sb.Append("     AND DCH.CARGO_TYPE=" + CargoType);
                }

                if (!string.IsNullOrEmpty(CountryPK) & CountryPK != "0")
                {
                    sb.Append("       AND LMT.COUNTRY_MST_FK IN(" + CountryPK + ")");
                }

                if (!string.IsNullOrEmpty(LocPK) & LocPK != "0")
                {
                    sb.Append("        AND LMT.LOCATION_MST_PK IN (" + LocPK + ") ");
                }

                if (!string.IsNullOrEmpty(CustPK) & CustPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustPK + ")");
                }

                if (!string.IsNullOrEmpty(CustGroupPK) & CustGroupPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustGroupPK + ")");
                }

                sb.Append("           AND INVTRN.JOB_TYPE = 4     ");
                sb.Append("           AND INV.CHK_INVOICE<>2 ");
                // '   --------CBJC CR Note--
                sb.Append("           UNION");
                sb.Append("           SELECT DISTINCT CMT.CUSTOMER_MST_PK,");
                sb.Append("                        CCD.ADM_LOCATION_MST_FK,");
                sb.Append("                        CNT.CREDIT_NOTE_DATE REF_DATE,");
                sb.Append("                        (CASE");
                sb.Append("                          WHEN CNT.PROCESS_TYPE = 1 THEN");
                sb.Append("                           'EXPORT'");
                sb.Append("                          ELSE");
                sb.Append("                           'IMPORT'");
                sb.Append("                        END) PROCESS,");
                sb.Append("                        'CREDITNOTE' TRANSACTION,");
                sb.Append("                        CBJC.HBL_NO HBL_REF_NO,");
                sb.Append("                        CNT.CREDIT_NOTE_REF_NR DOCREFNR,");
                sb.Append("                        CNT.CRN_TBL_PK REF_PK,");
                sb.Append("                        CNT.BIZ_TYPE BIZ_TYPE,");
                sb.Append("                        CBJC.CARGO_TYPE CARGO_TYPE,");
                sb.Append("                        (SELECT  ROUND(CNT1.CRN_AMMOUNT * ");
                sb.Append("                        " + ParentDB + ".GET_EX_RATE(CNT1.CURRENCY_MST_FK,");
                sb.Append(CurrPK + ",");
                sb.Append("                                          CNT.CREDIT_NOTE_DATE),");
                sb.Append("                        2) FROM " + ParentDB + ".CREDIT_NOTE_TBL CNT1 WHERE CNT1.CRN_TBL_PK =  CNT.CRN_TBL_PK AND NVL(CNT1.CRN_STATUS,0) = 2) DEBIT,");
                sb.Append("                        (SELECT  ROUND(CNT1.CRN_AMMOUNT * ");
                sb.Append("                        " + ParentDB + ".GET_EX_RATE(CNT1.CURRENCY_MST_FK,");
                sb.Append(CurrPK + ",");
                sb.Append("                                          CNT.CREDIT_NOTE_DATE),");
                sb.Append("                        2) FROM " + ParentDB + ".CREDIT_NOTE_TBL CNT1 WHERE CNT1.CRN_TBL_PK =  CNT.CRN_TBL_PK AND NVL(CNT1.CRN_STATUS,0) <> 2) CREDIT,");
                sb.Append("                        0 BALANCE,");
                sb.Append("                        '' OUTSTANDING_DAYS");
                sb.Append("          FROM " + ParentDB + ".CBJC_TBL CBJC,");
                sb.Append("               " + ParentDB + ".CUSTOMER_MST_TBL       CMT,");
                sb.Append("               " + ParentDB + ".CUSTOMER_CONTACT_DTLS  CCD,");
                sb.Append("               " + ParentDB + ".LOCATION_MST_TBL       LMT,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TBL     INV,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TRN_TBL INVTRN,");
                sb.Append("               " + ParentDB + ".CREDIT_NOTE_TBL        CNT,");
                sb.Append("               " + ParentDB + ".CREDIT_NOTE_TRN_TBL    CNTT");
                sb.Append("         WHERE INVTRN.JOB_CARD_FK = CBJC.CBJC_PK(+)");
                sb.Append("           AND INV.IS_FAC_INV <> 1");
                sb.Append("           AND CBJC.CUSTOMER_MST_FK= CMT.CUSTOMER_MST_PK");
                sb.Append("           AND CNT.PROCESS_TYPE = " + ProcessType);
                sb.Append("           AND INV.CONSOL_INVOICE_PK = INVTRN.CONSOL_INVOICE_FK");
                sb.Append("           AND CNT.CRN_TBL_PK = CNTT.CRN_TBL_FK");
                sb.Append("           AND CNTT.CONSOL_INVOICE_TRN_FK = INVTRN.CONSOL_INVOICE_TRN_PK");
                sb.Append("           AND CMT.CUSTOMER_MST_PK = CCD.CUSTOMER_MST_FK");
                sb.Append("           AND LMT.LOCATION_MST_PK = CCD.ADM_LOCATION_MST_FK");
                sb.Append("           AND NVL((SELECT SUM(WMT.WRITEOFF_AMOUNT)");
                sb.Append("                     FROM " + ParentDB + ".WRITEOFF_MANUAL_TBL WMT");
                sb.Append("                    WHERE WMT.CONSOL_INVOICE_FK = INV.CONSOL_INVOICE_PK),");
                sb.Append("                   0.00) = 0");
                if (Fromdate != "")
                {
                    sb.Append("           AND  CNT.CREDIT_NOTE_DATE   <= TO_DATE('" + Fromdate + "')");
                }

                sb.Append("          AND CBJC.PROCESS_TYPE =" + ProcessType);
                if (BizType != 3)
                {
                    sb.Append("          AND CBJC.BIZ_TYPE =" + BizType);
                }

                if (CargoType != 0)
                {
                    sb.Append("     AND CBJC.CARGO_TYPE=" + CargoType);
                }

                if (!string.IsNullOrEmpty(CountryPK) & CountryPK != "0")
                {
                    sb.Append("       AND LMT.COUNTRY_MST_FK IN(" + CountryPK + ")");
                }

                if (!string.IsNullOrEmpty(LocPK) & LocPK != "0")
                {
                    sb.Append("        AND LMT.LOCATION_MST_PK IN (" + LocPK + ") ");
                }

                if (!string.IsNullOrEmpty(CustPK) & CustPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustPK + ")");
                }

                if (!string.IsNullOrEmpty(CustGroupPK) & CustGroupPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustGroupPK + ")");
                }

                if (BizType == 1)
                {
                    if (!string.IsNullOrEmpty(FlightID))
                    {
                        sb.Append("     AND   CBJC.FLIGHT_NO IN (" + FlightID + ")");
                    }
                }
                else if (BizType == 2)
                {
                    if (!string.IsNullOrEmpty(VslVoyPK))
                    {
                        sb.Append("     AND   CBJC.VOYAGE_TRN_FK=" + VslVoyPK + "");
                    }
                }
                sb.Append("           AND INVTRN.JOB_TYPE = 2");
                sb.Append("           AND INV.CHK_INVOICE<>2 ");
                // '           ---TPT CrNote------
                sb.Append("           UNION");
                sb.Append("           SELECT DISTINCT CMT.CUSTOMER_MST_PK,");
                sb.Append("                        CCD.ADM_LOCATION_MST_FK,");
                sb.Append("                        CNT.CREDIT_NOTE_DATE REF_DATE,");
                sb.Append("                        (CASE");
                sb.Append("                          WHEN CNT.PROCESS_TYPE = 1 THEN");
                sb.Append("                           'EXPORT'");
                sb.Append("                          ELSE");
                sb.Append("                           'IMPORT'");
                sb.Append("                        END) PROCESS,");
                sb.Append("                        'CREDITNOTE' TRANSACTION,");
                sb.Append("                        (SELECT CONT.BL_NUMBER FROM " + ParentDB + ".TRANSPORT_TRN_CONT CONT WHERE CONT.TRANSPORT_INST_FK = TIST.TRANSPORT_INST_SEA_PK AND ROWNUM=1) HBL_REF_NO,");
                sb.Append("                        CNT.CREDIT_NOTE_REF_NR DOCREFNR,");
                sb.Append("                        CNT.CRN_TBL_PK REF_PK,");
                sb.Append("                        CNT.BIZ_TYPE BIZ_TYPE,");
                sb.Append("                        TIST.CARGO_TYPE CARGO_TYPE,");
                sb.Append("                        (SELECT  ROUND(CNT1.CRN_AMMOUNT * ");
                sb.Append("                        " + ParentDB + ".GET_EX_RATE(CNT1.CURRENCY_MST_FK,");
                sb.Append(CurrPK + ",");
                sb.Append("                                          CNT.CREDIT_NOTE_DATE),");
                sb.Append("                        2) FROM " + ParentDB + ".CREDIT_NOTE_TBL CNT1 WHERE CNT1.CRN_TBL_PK =  CNT.CRN_TBL_PK AND NVL(CNT1.CRN_STATUS,0) = 2) DEBIT,");
                sb.Append("                        (SELECT  ROUND(CNT1.CRN_AMMOUNT * ");
                sb.Append("                        " + ParentDB + ".GET_EX_RATE(CNT1.CURRENCY_MST_FK,");
                sb.Append(CurrPK + ",");
                sb.Append("                                          CNT.CREDIT_NOTE_DATE),");
                sb.Append("                        2) FROM " + ParentDB + ".CREDIT_NOTE_TBL CNT1 WHERE CNT1.CRN_TBL_PK =  CNT.CRN_TBL_PK AND NVL(CNT1.CRN_STATUS,0) <> 2) CREDIT,");
                sb.Append("                        0 BALANCE,");
                sb.Append("                        '' OUTSTANDING_DAYS");
                sb.Append("          FROM " + ParentDB + ".TRANSPORT_INST_SEA_TBL TIST,");
                sb.Append("               " + ParentDB + ".CUSTOMER_MST_TBL       CMT,");
                sb.Append("               " + ParentDB + ".CUSTOMER_CONTACT_DTLS  CCD,");
                sb.Append("               " + ParentDB + ".LOCATION_MST_TBL       LMT,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TBL     INV,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TRN_TBL INVTRN,");
                sb.Append("               " + ParentDB + ".CREDIT_NOTE_TBL        CNT,");
                sb.Append("               " + ParentDB + ".CREDIT_NOTE_TRN_TBL    CNTT");
                sb.Append("         WHERE INVTRN.JOB_CARD_FK = TIST.TRANSPORT_INST_SEA_PK(+)");
                sb.Append("           AND INV.IS_FAC_INV <> 1");
                sb.Append("           AND INV.CUSTOMER_MST_FK= CMT.CUSTOMER_MST_PK");
                sb.Append("           AND CNT.PROCESS_TYPE = " + ProcessType);
                sb.Append("           AND INV.CONSOL_INVOICE_PK = INVTRN.CONSOL_INVOICE_FK");
                sb.Append("           AND CNT.CRN_TBL_PK = CNTT.CRN_TBL_FK");
                sb.Append("           AND CNTT.CONSOL_INVOICE_TRN_FK = INVTRN.CONSOL_INVOICE_TRN_PK");
                sb.Append("           AND CMT.CUSTOMER_MST_PK = CCD.CUSTOMER_MST_FK");
                sb.Append("           AND LMT.LOCATION_MST_PK = CCD.ADM_LOCATION_MST_FK");
                sb.Append("           AND NVL((SELECT SUM(WMT.WRITEOFF_AMOUNT)");
                sb.Append("                     FROM " + ParentDB + ".WRITEOFF_MANUAL_TBL WMT");
                sb.Append("                    WHERE WMT.CONSOL_INVOICE_FK = INV.CONSOL_INVOICE_PK),");
                sb.Append("                   0.00) = 0");
                if (Fromdate != "")
                {
                    sb.Append("           AND CNT.CREDIT_NOTE_DATE  <= TO_DATE('" + Fromdate + "')");
                }

                sb.Append("          AND TIST.PROCESS_TYPE =" + ProcessType);
                if (BizType != 3)
                {
                    sb.Append("          AND TIST.BUSINESS_TYPE =" + BizType);
                }

                if (CargoType != 0)
                {
                    sb.Append("     AND TIST.CARGO_TYPE=" + CargoType);
                }

                if (!string.IsNullOrEmpty(CountryPK) & CountryPK != "0")
                {
                    sb.Append("       AND LMT.COUNTRY_MST_FK IN(" + CountryPK + ")");
                }

                if (!string.IsNullOrEmpty(LocPK) & LocPK != "0")
                {
                    sb.Append("        AND LMT.LOCATION_MST_PK IN (" + LocPK + ") ");
                }

                if (!string.IsNullOrEmpty(CustPK) & CustPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustPK + ")");
                }

                if (!string.IsNullOrEmpty(CustGroupPK) & CustGroupPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustGroupPK + ")");
                }

                if (BizType == 1)
                {
                    if (!string.IsNullOrEmpty(FlightID))
                    {
                        sb.Append("     AND   TIST.FLIGHT_NO IN (" + FlightID + ")");
                    }
                }
                else if (BizType == 2)
                {
                    if (!string.IsNullOrEmpty(VslVoyPK))
                    {
                        sb.Append("     AND   TIST.VSL_VOY_FK=" + VslVoyPK + "");
                    }
                }
                sb.Append("           AND INVTRN.JOB_TYPE = 3");
                sb.Append("           AND INV.CHK_INVOICE<>2 ");
                // '         --- DET & DEM TPT (CR Note)
                sb.Append("           UNION");
                sb.Append("           SELECT DISTINCT CMT.CUSTOMER_MST_PK,");
                sb.Append("                        CCD.ADM_LOCATION_MST_FK,");
                sb.Append("                        CNT.CREDIT_NOTE_DATE REF_DATE,");
                sb.Append("                        (CASE");
                sb.Append("                          WHEN CNT.PROCESS_TYPE = 1 THEN");
                sb.Append("                           'EXPORT'");
                sb.Append("                          ELSE");
                sb.Append("                           'IMPORT'");
                sb.Append("                        END) PROCESS,");
                sb.Append("                        'CREDITNOTE' TRANSACTION,");
                sb.Append("                        (SELECT CONT.BL_NUMBER FROM " + ParentDB + ".TRANSPORT_TRN_CONT CONT WHERE CONT.TRANSPORT_INST_FK = TIST.TRANSPORT_INST_SEA_PK AND ROWNUM=1) HBL_REF_NO,");
                sb.Append("                        CNT.CREDIT_NOTE_REF_NR DOCREFNR,");
                sb.Append("                        CNT.CRN_TBL_PK REF_PK,");
                sb.Append("                        CNT.BIZ_TYPE BIZ_TYPE,");
                sb.Append("                        DCH.CARGO_TYPE CARGO_TYPE,");
                sb.Append("                        (SELECT  ROUND(CNT1.CRN_AMMOUNT * ");
                sb.Append("                        " + ParentDB + ".GET_EX_RATE(CNT1.CURRENCY_MST_FK,");
                sb.Append(CurrPK + ",");
                sb.Append("                                          CNT.CREDIT_NOTE_DATE),");
                sb.Append("                        2) FROM " + ParentDB + ".CREDIT_NOTE_TBL CNT1 WHERE CNT1.CRN_TBL_PK =  CNT.CRN_TBL_PK AND NVL(CNT1.CRN_STATUS,0) = 2) DEBIT,");
                sb.Append("                        (SELECT  ROUND(CNT1.CRN_AMMOUNT * ");
                sb.Append("                        " + ParentDB + ".GET_EX_RATE(CNT1.CURRENCY_MST_FK,");
                sb.Append(CurrPK + ",");
                sb.Append("                                          CNT.CREDIT_NOTE_DATE),");
                sb.Append("                        2) FROM " + ParentDB + ".CREDIT_NOTE_TBL CNT1 WHERE CNT1.CRN_TBL_PK =  CNT.CRN_TBL_PK AND NVL(CNT1.CRN_STATUS,0) <> 2) CREDIT,");
                sb.Append("                        0 BALANCE,");
                sb.Append("                        '' OUTSTANDING_DAYS");
                sb.Append("          FROM " + ParentDB + ".DEM_CALC_HDR DCH  ,");
                sb.Append("                " + ParentDB + ".TRANSPORT_INST_SEA_TBL TIST,");
                sb.Append("               " + ParentDB + ".CUSTOMER_MST_TBL       CMT,");
                sb.Append("               " + ParentDB + ".CUSTOMER_CONTACT_DTLS  CCD,");
                sb.Append("               " + ParentDB + ".LOCATION_MST_TBL       LMT,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TBL     INV,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TRN_TBL INVTRN,");
                sb.Append("               " + ParentDB + ".CREDIT_NOTE_TBL        CNT,");
                sb.Append("               " + ParentDB + ".CREDIT_NOTE_TRN_TBL    CNTT");
                sb.Append("         WHERE INVTRN.JOB_CARD_FK = DCH.DEM_CALC_HDR_PK(+)");
                sb.Append("           AND DCH.DOC_REF_FK = TIST.TRANSPORT_INST_SEA_PK");
                sb.Append("           AND INV.IS_FAC_INV <> 1");
                sb.Append("           AND INV.CUSTOMER_MST_FK= CMT.CUSTOMER_MST_PK");
                sb.Append("           AND CNT.PROCESS_TYPE = " + ProcessType);
                sb.Append("           AND INV.CONSOL_INVOICE_PK = INVTRN.CONSOL_INVOICE_FK");
                sb.Append("           AND CNT.CRN_TBL_PK = CNTT.CRN_TBL_FK");
                sb.Append("           AND CNTT.CONSOL_INVOICE_TRN_FK = INVTRN.CONSOL_INVOICE_TRN_PK");
                sb.Append("           AND CMT.CUSTOMER_MST_PK = CCD.CUSTOMER_MST_FK");
                sb.Append("           AND LMT.LOCATION_MST_PK = CCD.ADM_LOCATION_MST_FK");
                sb.Append("           AND NVL((SELECT SUM(WMT.WRITEOFF_AMOUNT)");
                sb.Append("                     FROM " + ParentDB + ".WRITEOFF_MANUAL_TBL WMT");
                sb.Append("                    WHERE WMT.CONSOL_INVOICE_FK = INV.CONSOL_INVOICE_PK),");
                sb.Append("                   0.00) = 0");
                if (BizType == 1)
                {
                    if (!string.IsNullOrEmpty(FlightID))
                    {
                        sb.Append("     AND   TIST.FLIGHT_NO IN (" + FlightID + ")");
                    }
                }
                else if (BizType == 2)
                {
                    if (!string.IsNullOrEmpty(VslVoyPK))
                    {
                        sb.Append("     AND   TIST.VSL_VOY_FK=" + VslVoyPK + "");
                    }
                }
                if (CargoType != 0)
                {
                    sb.Append("     AND DCH.CARGO_TYPE=" + CargoType);
                }

                if (Fromdate != "")
                {
                    sb.Append("       AND CNT.CREDIT_NOTE_DATE  <= TO_DATE('" + Fromdate + "')");
                }

                if (!string.IsNullOrEmpty(CountryPK) & CountryPK != "0")
                {
                    sb.Append("       AND LMT.COUNTRY_MST_FK IN(" + CountryPK + ")");
                }

                if (!string.IsNullOrEmpty(LocPK) & LocPK != "0")
                {
                    sb.Append("        AND LMT.LOCATION_MST_PK IN (" + LocPK + ") ");
                }

                if (!string.IsNullOrEmpty(CustPK) & CustPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustPK + ")");
                }

                if (!string.IsNullOrEmpty(CustGroupPK) & CustGroupPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustGroupPK + ")");
                }

                sb.Append("           AND INVTRN.JOB_TYPE = 4");
                sb.Append("          AND DCH.PROCESS_TYPE =" + ProcessType);
                if (BizType != 3)
                {
                    sb.Append("          AND DCH.biz_type =" + BizType);
                }

                sb.Append("           AND INV.CHK_INVOICE<>2 ");
                // '         --- DET & DEM CBJC (CR Note)
                sb.Append("           UNION");
                sb.Append("           SELECT DISTINCT CMT.CUSTOMER_MST_PK,");
                sb.Append("                        CCD.ADM_LOCATION_MST_FK,");
                sb.Append("                        CNT.CREDIT_NOTE_DATE REF_DATE,");
                sb.Append("                        (CASE");
                sb.Append("                          WHEN CNT.PROCESS_TYPE = 1 THEN");
                sb.Append("                           'EXPORT'");
                sb.Append("                          ELSE");
                sb.Append("                           'IMPORT'");
                sb.Append("                        END) PROCESS,");
                sb.Append("                        'CREDITNOTE' TRANSACTION,");
                sb.Append("                       CBJC.HBL_NO HBL_REF_NO,");
                sb.Append("                        CNT.CREDIT_NOTE_REF_NR DOCREFNR,");
                sb.Append("                        CNT.CRN_TBL_PK REF_PK,");
                sb.Append("                        CNT.BIZ_TYPE BIZ_TYPE,");
                sb.Append("                        DCH.CARGO_TYPE CARGO_TYPE,");
                sb.Append("                        (SELECT  ROUND(CNT1.CRN_AMMOUNT * ");
                sb.Append("                        " + ParentDB + ".GET_EX_RATE(CNT1.CURRENCY_MST_FK,");
                sb.Append(CurrPK + ",");
                sb.Append("                                          CNT.CREDIT_NOTE_DATE),");
                sb.Append("                        2) FROM " + ParentDB + ".CREDIT_NOTE_TBL CNT1 WHERE CNT1.CRN_TBL_PK =  CNT.CRN_TBL_PK AND NVL(CNT1.CRN_STATUS,0) = 2) DEBIT,");
                sb.Append("                        (SELECT  ROUND(CNT1.CRN_AMMOUNT * ");
                sb.Append("                        " + ParentDB + ".GET_EX_RATE(CNT1.CURRENCY_MST_FK,");
                sb.Append(CurrPK + ",");
                sb.Append("                                          CNT.CREDIT_NOTE_DATE),");
                sb.Append("                        2) FROM " + ParentDB + ".CREDIT_NOTE_TBL CNT1 WHERE CNT1.CRN_TBL_PK =  CNT.CRN_TBL_PK AND NVL(CNT1.CRN_STATUS,0) <> 2) CREDIT,");
                sb.Append("                        0 BALANCE,");
                sb.Append("                        '' OUTSTANDING_DAYS");
                sb.Append("          FROM " + ParentDB + ".DEM_CALC_HDR DCH  ,");
                sb.Append("               " + ParentDB + ".CBJC_TBL               CBJC,");
                sb.Append("               " + ParentDB + ".CUSTOMER_MST_TBL       CMT,");
                sb.Append("               " + ParentDB + ".CUSTOMER_CONTACT_DTLS  CCD,");
                sb.Append("               " + ParentDB + ".LOCATION_MST_TBL       LMT,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TBL     INV,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TRN_TBL INVTRN,");
                sb.Append("               " + ParentDB + ".CREDIT_NOTE_TBL        CNT,");
                sb.Append("               " + ParentDB + ".CREDIT_NOTE_TRN_TBL    CNTT");
                sb.Append("         WHERE INVTRN.JOB_CARD_FK = DCH.DEM_CALC_HDR_PK(+)");
                sb.Append("           AND DCH.DOC_REF_FK = CBJC.CBJC_PK");
                sb.Append("           AND INV.IS_FAC_INV <> 1");
                sb.Append("           AND INV.CUSTOMER_MST_FK= CMT.CUSTOMER_MST_PK");
                sb.Append("           AND CNT.PROCESS_TYPE = " + ProcessType);
                sb.Append("           AND INV.CONSOL_INVOICE_PK = INVTRN.CONSOL_INVOICE_FK");
                sb.Append("           AND CNT.CRN_TBL_PK = CNTT.CRN_TBL_FK");
                sb.Append("           AND CNTT.CONSOL_INVOICE_TRN_FK = INVTRN.CONSOL_INVOICE_TRN_PK");
                sb.Append("           AND CMT.CUSTOMER_MST_PK = CCD.CUSTOMER_MST_FK");
                sb.Append("           AND LMT.LOCATION_MST_PK = CCD.ADM_LOCATION_MST_FK");
                sb.Append("           AND NVL((SELECT SUM(WMT.WRITEOFF_AMOUNT)");
                sb.Append("                     FROM " + ParentDB + ".WRITEOFF_MANUAL_TBL WMT");
                sb.Append("                    WHERE WMT.CONSOL_INVOICE_FK = INV.CONSOL_INVOICE_PK),");
                sb.Append("                   0.00) = 0");
                if (BizType == 1)
                {
                    if (!string.IsNullOrEmpty(FlightID))
                    {
                        sb.Append("         AND   CBJC.FLIGHT_NO IN (" + FlightID + ")");
                    }
                }
                else if (BizType == 2)
                {
                    if (!string.IsNullOrEmpty(VslVoyPK))
                    {
                        sb.Append("         AND   CBJC.VOYAGE_TRN_FK=" + VslVoyPK + "");
                    }
                }
                if (Fromdate != "")
                {
                    sb.Append("       AND CNT.CREDIT_NOTE_DATE  <= TO_DATE('" + Fromdate + "')");
                }

                if (!string.IsNullOrEmpty(CountryPK) & CountryPK != "0")
                {
                    sb.Append("       AND LMT.COUNTRY_MST_FK IN(" + CountryPK + ")");
                }

                if (!string.IsNullOrEmpty(LocPK) & LocPK != "0")
                {
                    sb.Append("        AND LMT.LOCATION_MST_PK IN (" + LocPK + ") ");
                }

                if (!string.IsNullOrEmpty(CustPK) & CustPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustPK + ")");
                }

                if (!string.IsNullOrEmpty(CustGroupPK) & CustGroupPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustGroupPK + ")");
                }

                sb.Append("           AND INVTRN.JOB_TYPE = 4");
                sb.Append("          AND DCH.PROCESS_TYPE =" + ProcessType);
                if (BizType != 3)
                {
                    sb.Append("          AND DCH.biz_type =" + BizType);
                }

                if (CargoType != 0)
                {
                    sb.Append("     AND DCH.CARGO_TYPE=" + CargoType);
                }

                sb.Append("           AND INV.CHK_INVOICE<>2 ");
                sb.Append(") B");


                sb.Append(" WHERE C.CUSTOMER_MST_PK = A.CUSTOMER_MST_PK");
                sb.Append("   AND A.CUSTOMER_MST_PK = B.CUSTOMER_MST_PK(+)");
                sb.Append(" GROUP BY C.CUSTOMER_MST_PK,");
                sb.Append(" A.REF_PK,");
                sb.Append(" A.BIZ_TYPE,");
                sb.Append(" A.PROCESS,");
                sb.Append(" A.CARGO_TYPE");


                sb.Append("   UNION ");
                // '''''''''''''''''''''''''''
                sbNew3.Append("SELECT CUSTOMER_MST_PK,");
                sbNew3.Append("       ADM_LOCATION_MST_FK,");
                sbNew3.Append("       REF_DATE,");
                sbNew3.Append("       PROCESS,");
                sbNew3.Append("       TRANSACTION,");
                sbNew3.Append("       HBL_REF_NO,");
                sbNew3.Append("       DOCREFNR,");
                sbNew3.Append("       DEBIT,");
                sbNew3.Append("       CREDIT,");
                sbNew3.Append("       BALANCE,");
                sbNew3.Append("       OUTSTANDING_DAYS,");
                sbNew3.Append("       REF_PK,");
                sbNew3.Append("       BIZ_TYPE,");
                sbNew3.Append("       CARGO_TYPE");

                sbNew3.Append("     FROM ( ");
                sbNew3.Append("       SELECT CUSTOMER_MST_PK, ADM_LOCATION_MST_FK,");
                sbNew3.Append("       REF_DATE,");
                sbNew3.Append("       PROCESS,");
                sbNew3.Append("       TRANSACTION,");
                sbNew3.Append("       HBL_REF_NO,");
                sbNew3.Append("       DOCREFNR,");
                sbNew3.Append("       DEBIT,");
                sbNew3.Append("       CREDIT,");
                sbNew3.Append("       BALANCE,");
                sbNew3.Append("       OUTSTANDING_DAYS,");
                sbNew3.Append("       REF_PK,");
                sbNew3.Append("       BIZ_TYPE,");
                sbNew3.Append("       CARGO_TYPE");

                sbNew3.Append("  FROM (SELECT DISTINCT CMT.CUSTOMER_MST_PK, CCD.ADM_LOCATION_MST_FK,");
                sbNew3.Append("                        INV.INVOICE_DATE REF_DATE,");
                sbNew3.Append("                        (CASE");
                sbNew3.Append("                          WHEN INV.PROCESS_TYPE = 1 THEN");
                sbNew3.Append("                           'EXPORT'");
                sbNew3.Append("                          ELSE");
                sbNew3.Append("                           'IMPORT'");
                sbNew3.Append("                        END) PROCESS,");
                sbNew3.Append("                        'INVOICE' TRANSACTION,");
                sbNew3.Append("  ( CASE ");
                sbNew3.Append("    WHEN JOB.BUSINESS_TYPE = 2 AND JOB.PROCESS_TYPE = 1 THEN ");
                sbNew3.Append("      HBL.HBL_REF_NO ");
                sbNew3.Append("     WHEN JOB.BUSINESS_TYPE = 1 AND JOB.PROCESS_TYPE = 1 THEN ");
                sbNew3.Append("         HAWB.HAWB_REF_NO ");
                sbNew3.Append("        ELSE ");
                sbNew3.Append("       JOB.HBL_HAWB_REF_NO  ");
                sbNew3.Append("      END) HBL_REF_NO , ");
                sbNew3.Append("                        INV.INVOICE_REF_NO DOCREFNR,");
                sbNew3.Append("                        INV.CONSOL_INVOICE_PK REF_PK,");
                sbNew3.Append("                        INV.BUSINESS_TYPE BIZ_TYPE,");
                if (ProcessType == 1)
                {
                    sbNew3.Append("                        BKG.CARGO_TYPE CARGO_TYPE,");
                }
                else if (BizType == 2)
                {
                    sbNew3.Append("                        JOB.CARGO_TYPE CARGO_TYPE, ");
                }
                else
                {
                    sbNew3.Append("                        0 CARGO_TYPE, ");
                }

                sbNew3.Append("                        ROUND(INV.NET_RECEIVABLE * " + ParentDB + ".GET_EX_RATE(INV.CURRENCY_MST_FK," + CurrPK + ", INV.INVOICE_DATE),2) DEBIT,");
                sbNew3.Append("                        0 CREDIT,");
                sbNew3.Append("                        0 BALANCE,");
                sbNew3.Append("(SELECT DISTINCT TO_CHAR(TO_DATE(SYSDATE,");
                sbNew3.Append("                             'DD/MM/YYYY') -");
                sbNew3.Append("                              TO_DATE(INV.INVOICE_DATE)) OUTSTANDING_DAYS");
                sbNew3.Append("                                   FROM " + ParentDB + ".COLLECTIONS_TRN_TBL COLL,");
                sbNew3.Append("                                        " + ParentDB + ".CONSOL_INVOICE_TBL  INV1");
                sbNew3.Append("                                  WHERE INV1.INVOICE_REF_NO =");
                sbNew3.Append("                                        COLL.INVOICE_REF_NR(+)");
                sbNew3.Append("                                    AND INV1.INVOICE_REF_NO =");
                sbNew3.Append("                                        INV.INVOICE_REF_NO");
                sbNew3.Append("                                    AND NVL((INV1.NET_RECEIVABLE -");
                sbNew3.Append("                                            NVL((SELECT SUM(CTRN.RECD_AMOUNT_HDR_CURR)");
                sbNew3.Append("                                                   FROM " + ParentDB + ".COLLECTIONS_TRN_TBL CTRN");
                sbNew3.Append("                                                  WHERE CTRN.INVOICE_REF_NR LIKE");
                sbNew3.Append("                                                        INV1.INVOICE_REF_NO),");
                sbNew3.Append("                                                 0.00)),");
                sbNew3.Append("                                            0) > 0)OUTSTANDING_DAYS");
                sbNew3.Append("          FROM " + ParentDB + ".JOB_CARD_TRN   JOB,");
                if (ProcessType == 1)
                {
                    sbNew3.Append("               " + ParentDB + ".BOOKING_MST_TBL        BKG,");
                }

                sbNew3.Append("               " + ParentDB + ".HBL_EXP_TBL            HBL,");
                sbNew3.Append("               " + ParentDB + ".HAWB_EXP_TBL            HAWB,");
                sbNew3.Append("               " + ParentDB + ".CUSTOMER_MST_TBL       CMT,");
                sbNew3.Append("               " + ParentDB + ".CUSTOMER_CONTACT_DTLS CCD,");
                sbNew3.Append("               " + ParentDB + ".LOCATION_MST_TBL       LMT,");
                sbNew3.Append("               " + ParentDB + ".CONSOL_INVOICE_TBL     INV,");
                sbNew3.Append("               " + ParentDB + ".CONSOL_INVOICE_TRN_TBL INVTRN");
                sbNew3.Append("         WHERE INVTRN.JOB_CARD_FK = JOB.JOB_CARD_TRN_PK(+)");
                sbNew3.Append("         AND INV.IS_FAC_INV <> 1");
                sbNew3.Append("           AND JOB.HBL_HAWB_FK = HBL.HBL_EXP_TBL_PK(+)");
                sbNew3.Append("           AND JOB.HBL_HAWB_FK = HAWB.HAWB_EXP_TBL_PK(+)");
                if (ProcessType == 1)
                {
                    sbNew3.Append("           AND CMT.CUSTOMER_MST_PK = BKG.CUST_CUSTOMER_MST_FK");
                    sbNew3.Append("           AND JOB.BOOKING_MST_FK = BKG.BOOKING_MST_PK");
                    sbNew3.Append("           AND INV.PROCESS_TYPE = " + ProcessType);
                    if (CargoType != 0)
                    {
                        sbNew3.Append("     AND BKG.CARGO_TYPE=" + CargoType);
                    }

                    if (!string.IsNullOrEmpty(POLPK) & POLPK != "0")
                    {
                        sbNew3.Append("       AND BKG.PORT_MST_POL_FK IN(" + POLPK + ")");
                    }

                    if (!string.IsNullOrEmpty(POLPK) & PODPK != "0")
                    {
                        sbNew3.Append("       AND BKG.PORT_MST_POD_FK IN( " + PODPK + ")");
                    }
                }
                else if (ProcessType == 2)
                {
                    sbNew3.Append("        AND JOB.CUST_CUSTOMER_MST_FK=CMT.CUSTOMER_MST_PK");
                    sbNew3.Append("           AND INV.PROCESS_TYPE = 2");
                    if (CargoType != 0)
                    {
                        sbNew3.Append("     AND JOB.CARGO_TYPE=" + CargoType);
                    }

                    if (!string.IsNullOrEmpty(POLPK) & POLPK != "0")
                    {
                        sbNew3.Append("         AND JOB.PORT_MST_POL_FK IN(" + POLPK + ")");
                    }

                    if (!string.IsNullOrEmpty(POLPK) & PODPK != "0")
                    {
                        sbNew3.Append("         AND JOB.PORT_MST_POD_FK IN(" + PODPK + ")");
                    }
                }
                sbNew3.Append("           AND INV.CONSOL_INVOICE_PK = INVTRN.CONSOL_INVOICE_FK");
                sbNew3.Append("            AND CMT.CUSTOMER_MST_PK = CCD.CUSTOMER_MST_FK ");
                sbNew3.Append("           AND LMT.LOCATION_MST_PK = CCD.ADM_LOCATION_MST_FK");
                sbNew3.Append("           AND NVL((SELECT SUM(WMT.WRITEOFF_AMOUNT)");
                sbNew3.Append("             FROM " + ParentDB + ".WRITEOFF_MANUAL_TBL WMT");
                sbNew3.Append("            WHERE WMT.CONSOL_INVOICE_FK = INV.CONSOL_INVOICE_PK),");
                sbNew3.Append("           0.00) = 0");
                if (BizType == 1)
                {
                    sbNew3.Append("           AND INV.BUSINESS_TYPE = 1");
                    if (!string.IsNullOrEmpty(FlightID))
                    {
                        sbNew3.Append("            AND JOB.VOYAGE_FLIGHT_NO IN (" + FlightID + ")");
                    }
                }
                else if (BizType == 2)
                {
                    sbNew3.Append("           AND INV.BUSINESS_TYPE = 2");
                    if (!string.IsNullOrEmpty(VslVoyPK))
                    {
                        sbNew3.Append("            AND JOB.VOYAGE_TRN_FK=" + VslVoyPK + "");
                    }
                }

                if (!string.IsNullOrEmpty(CountryPK) & CountryPK != "0")
                {
                    sbNew3.Append("       AND LMT.COUNTRY_MST_FK IN(" + CountryPK + ")");
                }

                if (!string.IsNullOrEmpty(LocPK) & LocPK != "0")
                {
                    sbNew3.Append("        AND LMT.LOCATION_MST_PK IN (" + LocPK + ") ");
                }

                if (!string.IsNullOrEmpty(CustPK) & CustPK != "0")
                {
                    sbNew3.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustPK + ")");
                }

                if (!string.IsNullOrEmpty(CustGroupPK) & CustGroupPK != "0")
                {
                    sbNew3.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustGroupPK + ")");
                }

                if (Fromdate != "" & Todate != "")
                {
                    sbNew3.Append("       AND INV.invoice_date BETWEEN TO_DATE('" + Fromdate + "') AND");
                    sbNew3.Append("               TO_DATE('" + Todate + "')");
                }
                sbNew3.Append("           AND INVTRN.JOB_TYPE = 1 ");
                sbNew3.Append("           AND INV.CHK_INVOICE<>2 ");
                sbNew3.Append("        UNION");
                sbNew3.Append("        SELECT DISTINCT CMT.CUSTOMER_MST_PK, CCD.ADM_LOCATION_MST_FK,");
                sbNew3.Append("                        COL.COLLECTIONS_DATE REF_DATE,");
                sbNew3.Append("                        (CASE");
                sbNew3.Append("                          WHEN COL.PROCESS_TYPE = 1 THEN");
                sbNew3.Append("                           'EXPORT'");
                sbNew3.Append("                          ELSE");
                sbNew3.Append("                           'IMPORT'");
                sbNew3.Append("                        END) PROCESS,");
                sbNew3.Append("                        'COLLECTION' TRANSACTION,");
                sbNew3.Append("  ( CASE ");
                sbNew3.Append("    WHEN JOB.BUSINESS_TYPE = 2 AND JOB.PROCESS_TYPE = 1 THEN ");
                sbNew3.Append("      HBL.HBL_REF_NO ");
                sbNew3.Append("     WHEN JOB.BUSINESS_TYPE = 1 AND JOB.PROCESS_TYPE = 1 THEN ");
                sbNew3.Append("         HAWB.HAWB_REF_NO ");
                sbNew3.Append("        ELSE ");
                sbNew3.Append("       JOB.HBL_HAWB_REF_NO  ");
                sbNew3.Append("      END) HBL_REF_NO , ");
                sbNew3.Append("                        COL.COLLECTIONS_REF_NO DOCREFNR,");
                sbNew3.Append("                        COL.COLLECTIONS_TBL_PK REF_PK,");
                sbNew3.Append("                        COL.BUSINESS_TYPE BIZ_TYPE,");
                if (ProcessType == 1)
                {
                    sbNew3.Append("                        BKG.CARGO_TYPE CARGO_TYPE,");
                }
                else if (BizType == 2)
                {
                    sbNew3.Append("                        JOB.CARGO_TYPE CARGO_TYPE, ");
                }
                else
                {
                    sbNew3.Append("                        0 CARGO_TYPE, ");
                }

                sbNew3.Append("                        0 DEBIT,");
                sbNew3.Append("                        ROUND(CLT.RECD_AMOUNT_HDR_CURR * " + ParentDB + ".GET_EX_RATE(COL.CURRENCY_MST_FK," + CurrPK + ",COL.COLLECTIONS_DATE),2) CREDIT,");
                sbNew3.Append("                        0 BALANCE,''OUTSTANDING_DAYS");
                sbNew3.Append("          FROM " + ParentDB + ".JOB_CARD_TRN   JOB,");
                sbNew3.Append("               " + ParentDB + ".HBL_EXP_TBL            HBL,");
                sbNew3.Append("               " + ParentDB + ".HAWB_EXP_TBL            HAWB,");
                if (ProcessType == 1)
                {
                    sbNew3.Append("               " + ParentDB + ".BOOKING_MST_TBL        BKG,");
                }

                sbNew3.Append("               " + ParentDB + ".CUSTOMER_MST_TBL       CMT,");
                sbNew3.Append("               " + ParentDB + ".CUSTOMER_CONTACT_DTLS CCD,");
                sbNew3.Append("               " + ParentDB + ".LOCATION_MST_TBL       LMT,");
                sbNew3.Append("               " + ParentDB + ".CONSOL_INVOICE_TBL     INV,");
                sbNew3.Append("               " + ParentDB + ".CONSOL_INVOICE_TRN_TBL INVTRN,");
                sbNew3.Append("               " + ParentDB + ".COLLECTIONS_TRN_TBL    CLT,");
                sbNew3.Append("               " + ParentDB + ".COLLECTIONS_TBL        COL");
                sbNew3.Append("         WHERE INVTRN.JOB_CARD_FK = JOB.JOB_CARD_TRN_PK(+)");
                sbNew3.Append("         AND INV.IS_FAC_INV <> 1");
                sbNew3.Append("           AND JOB.HBL_HAWB_FK = HBL.HBL_EXP_TBL_PK(+)");
                sbNew3.Append("           AND JOB.HBL_HAWB_FK = HAWB.HAWB_EXP_TBL_PK(+)");
                if (ProcessType == 1)
                {
                    sbNew3.Append("           AND CMT.CUSTOMER_MST_PK = BKG.CUST_CUSTOMER_MST_FK");
                    sbNew3.Append("           AND JOB.BOOKING_MST_FK = BKG.BOOKING_MST_PK");
                    sbNew3.Append("           AND COL.PROCESS_TYPE = " + ProcessType);
                    if (CargoType != 0)
                    {
                        sbNew3.Append("     AND BKG.CARGO_TYPE=" + CargoType);
                    }

                    if (!string.IsNullOrEmpty(POLPK) & POLPK != "0")
                    {
                        sbNew3.Append("       AND BKG.PORT_MST_POL_FK IN(" + POLPK + ")");
                    }

                    if (!string.IsNullOrEmpty(POLPK) & PODPK != "0")
                    {
                        sbNew3.Append("       AND BKG.PORT_MST_POD_FK IN( " + PODPK + ")");
                    }
                }
                else if (ProcessType == 2)
                {
                    sbNew3.Append("        AND JOB.CUST_CUSTOMER_MST_FK=CMT.CUSTOMER_MST_PK");
                    sbNew3.Append("           AND COL.PROCESS_TYPE = 2");
                    if (CargoType != 0)
                    {
                        sbNew3.Append("     AND JOB.CARGO_TYPE=" + CargoType);
                    }

                    if (!string.IsNullOrEmpty(POLPK) & POLPK != "0")
                    {
                        sbNew3.Append("         AND JOB.PORT_MST_POL_FK IN(" + POLPK + ")");
                    }

                    if (!string.IsNullOrEmpty(POLPK) & PODPK != "0")
                    {
                        sbNew3.Append("         AND JOB.PORT_MST_POD_FK IN(" + PODPK + ")");
                    }
                }
                sbNew3.Append("           AND INV.CONSOL_INVOICE_PK = INVTRN.CONSOL_INVOICE_FK");
                sbNew3.Append("           AND CLT.INVOICE_REF_NR = INV.INVOICE_REF_NO");
                sbNew3.Append("           AND COL.COLLECTIONS_TBL_PK = CLT.COLLECTIONS_TBL_FK");
                sbNew3.Append("           AND CMT.CUSTOMER_MST_PK = CCD.CUSTOMER_MST_FK ");
                sbNew3.Append("           AND LMT.LOCATION_MST_PK = CCD.ADM_LOCATION_MST_FK");
                sbNew3.Append("           AND NVL((SELECT SUM(WMT.WRITEOFF_AMOUNT)");
                sbNew3.Append("             FROM " + ParentDB + ".WRITEOFF_MANUAL_TBL WMT");
                sbNew3.Append("            WHERE WMT.CONSOL_INVOICE_FK = INV.CONSOL_INVOICE_PK),");

                sbNew3.Append("           0.00) = 0");
                if (BizType == 1)
                {
                    sbNew3.Append("           AND COL.BUSINESS_TYPE = 1");
                    if (!string.IsNullOrEmpty(FlightID))
                    {
                        sbNew3.Append("            AND JOB.VOYAGE_FLIGHT_NO IN (" + FlightID + ")");
                    }
                }
                else if (BizType == 2)
                {
                    sbNew3.Append("           AND COL.BUSINESS_TYPE = 2");
                    if (!string.IsNullOrEmpty(VslVoyPK))
                    {
                        sbNew3.Append("            AND JOB.VOYAGE_TRN_FK=" + VslVoyPK + "");
                    }
                }
                sbNew3.Append("           AND INVTRN.JOB_TYPE = 1 ");
                if (!string.IsNullOrEmpty(CountryPK) & CountryPK != "0")
                {
                    sbNew3.Append("       AND LMT.COUNTRY_MST_FK IN(" + CountryPK + ")");
                }

                if (!string.IsNullOrEmpty(LocPK) & LocPK != "0")
                {
                    sbNew3.Append("        AND LMT.LOCATION_MST_PK IN (" + LocPK + ") ");
                }

                if (!string.IsNullOrEmpty(CustPK) & CustPK != "0")
                {
                    sbNew3.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustPK + ")");
                }

                sbNew3.Append("           AND INV.CHK_INVOICE<>2 ");
                if (!string.IsNullOrEmpty(CustGroupPK) & CustGroupPK != "0")
                {
                    sbNew3.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustGroupPK + ")");
                }

                if (Fromdate != "" & Todate != "")
                {
                    sbNew3.Append("           AND COL.COLLECTIONS_DATE BETWEEN TO_DATE('" + Fromdate + "') AND");
                    sbNew3.Append("               TO_DATE('" + Todate + "')");
                }

                sbNew3.Append("        UNION");
                sbNew3.Append("        SELECT DISTINCT CMT.CUSTOMER_MST_PK, CCD.ADM_LOCATION_MST_FK,");
                sbNew3.Append("                        CNT.CREDIT_NOTE_DATE REF_DATE,");
                sbNew3.Append("                        (CASE");
                sbNew3.Append("                          WHEN CNT.PROCESS_TYPE = 1 THEN");
                sbNew3.Append("                           'EXPORT'");
                sbNew3.Append("                          ELSE");
                sbNew3.Append("                           'IMPORT'");
                sbNew3.Append("                        END) PROCESS,");
                sbNew3.Append("                        'CREDITNOTE' TRANSACTION,");
                sbNew3.Append("  ( CASE ");
                sbNew3.Append("    WHEN JOB.BUSINESS_TYPE = 2 AND JOB.PROCESS_TYPE = 1 THEN ");
                sbNew3.Append("      HBL.HBL_REF_NO ");
                sbNew3.Append("     WHEN JOB.BUSINESS_TYPE = 1 AND JOB.PROCESS_TYPE = 1 THEN ");
                sbNew3.Append("         HAWB.HAWB_REF_NO ");
                sbNew3.Append("        ELSE ");
                sbNew3.Append("       JOB.HBL_HAWB_REF_NO  ");
                sbNew3.Append("      END) HBL_REF_NO , ");
                sbNew3.Append("                        CNT.CREDIT_NOTE_REF_NR DOCREFNR,");
                sbNew3.Append("                        CNT.CRN_TBL_PK REF_PK,");
                sbNew3.Append("                        CNT.BIZ_TYPE BIZ_TYPE,");
                if (ProcessType == 1)
                {
                    sbNew3.Append("                        BKG.CARGO_TYPE CARGO_TYPE,");
                }
                else if (BizType == 2)
                {
                    sbNew3.Append("                        JOB.CARGO_TYPE CARGO_TYPE, ");
                }
                else
                {
                    sbNew3.Append("                        0 CARGO_TYPE, ");
                }

                sbNew3.Append("                        0 DEBIT,");
                sbNew3.Append("                        ROUND(CNT.CRN_AMMOUNT * " + ParentDB + ".GET_EX_RATE(CNT.CURRENCY_MST_FK," + CurrPK + ",CNT.CREDIT_NOTE_DATE),2) CREDIT,");
                sbNew3.Append("                        0 BALANCE,''OUTSTANDING_DAYS");
                sbNew3.Append("          FROM " + ParentDB + ".JOB_CARD_TRN   JOB,");
                if (ProcessType == 1)
                {
                    sbNew3.Append("               " + ParentDB + ".BOOKING_MST_TBL        BKG,");
                }

                sbNew3.Append("               " + ParentDB + ".HBL_EXP_TBL            HBL,");
                sbNew3.Append("               " + ParentDB + ".HAWB_EXP_TBL            HAWB,");
                sbNew3.Append("               " + ParentDB + ".CUSTOMER_MST_TBL       CMT,");
                sbNew3.Append("               " + ParentDB + ".CUSTOMER_CONTACT_DTLS CCD,");
                sbNew3.Append("               " + ParentDB + ".LOCATION_MST_TBL       LMT,");
                sbNew3.Append("               " + ParentDB + ".CONSOL_INVOICE_TBL     INV,");
                sbNew3.Append("               " + ParentDB + ".CONSOL_INVOICE_TRN_TBL INVTRN,");
                sbNew3.Append("               " + ParentDB + ".CREDIT_NOTE_TBL        CNT,");
                sbNew3.Append("               " + ParentDB + ".CREDIT_NOTE_TRN_TBL    CNTT");
                sbNew3.Append("         WHERE INVTRN.JOB_CARD_FK = JOB.JOB_CARD_TRN_PK(+)");
                sbNew3.Append("         AND INV.IS_FAC_INV <> 1");
                sbNew3.Append("           AND JOB.HBL_HAWB_FK = HBL.HBL_EXP_TBL_PK(+)");
                sbNew3.Append("           AND JOB.HBL_HAWB_FK = HAWB.HAWB_EXP_TBL_PK(+)");
                if (ProcessType == 1)
                {
                    sbNew3.Append("           AND CMT.CUSTOMER_MST_PK = BKG.CUST_CUSTOMER_MST_FK");
                    sbNew3.Append("           AND JOB.BOOKING_MST_FK = BKG.BOOKING_MST_PK");
                    sbNew3.Append("           AND CNT.PROCESS_TYPE = " + ProcessType);
                    if (CargoType != 0)
                    {
                        sbNew3.Append("     AND BKG.CARGO_TYPE=" + CargoType);
                    }

                    if (!string.IsNullOrEmpty(POLPK) & POLPK != "0")
                    {
                        sbNew3.Append("       AND BKG.PORT_MST_POL_FK IN(" + POLPK + ")");
                    }

                    if (!string.IsNullOrEmpty(POLPK) & PODPK != "0")
                    {
                        sbNew3.Append("       AND BKG.PORT_MST_POD_FK IN( " + PODPK + ")");
                    }
                }
                else if (ProcessType == 2)
                {
                    sbNew3.Append("        AND JOB.CUST_CUSTOMER_MST_FK=CMT.CUSTOMER_MST_PK");
                    sbNew3.Append("           AND CNT.PROCESS_TYPE = 2");
                    if (CargoType != 0)
                    {
                        sbNew3.Append("     AND JOB.CARGO_TYPE=" + CargoType);
                    }

                    if (!string.IsNullOrEmpty(POLPK) & POLPK != "0")
                    {
                        sbNew3.Append("         AND JOB.PORT_MST_POL_FK IN(" + POLPK + ")");
                    }

                    if (!string.IsNullOrEmpty(POLPK) & PODPK != "0")
                    {
                        sbNew3.Append("         AND JOB.PORT_MST_POD_FK IN(" + PODPK + ")");
                    }
                }
                sbNew3.Append("           AND CNT.CRN_TBL_PK = CNTT.CRN_TBL_FK");
                sbNew3.Append("           AND CNTT.CONSOL_INVOICE_TRN_FK = INVTRN.CONSOL_INVOICE_TRN_PK");
                sbNew3.Append("            AND CMT.CUSTOMER_MST_PK = CCD.CUSTOMER_MST_FK ");
                sbNew3.Append("           AND LMT.LOCATION_MST_PK = CCD.ADM_LOCATION_MST_FK");
                sbNew3.Append("           AND NVL((SELECT SUM(WMT.WRITEOFF_AMOUNT)");
                sbNew3.Append("             FROM " + ParentDB + ".WRITEOFF_MANUAL_TBL WMT");
                sbNew3.Append("            WHERE WMT.CONSOL_INVOICE_FK = INV.CONSOL_INVOICE_PK),");
                sbNew3.Append("           0.00) = 0");
                sbNew3.Append("           AND INVTRN.JOB_TYPE = 1 ");
                if (BizType == 1)
                {
                    sbNew3.Append("           AND CNT.BIZ_TYPE = 1");
                    if (!string.IsNullOrEmpty(FlightID))
                    {
                        sbNew3.Append("            AND JOB.VOYAGE_FLIGHT_NO IN (" + FlightID + ")");
                    }
                }
                else if (BizType == 2)
                {
                    sbNew3.Append("           AND CNT.BIZ_TYPE = 2");
                    if (!string.IsNullOrEmpty(VslVoyPK))
                    {
                        sbNew3.Append("            AND JOB.VOYAGE_TRN_FK=" + VslVoyPK + "");
                    }
                }
                sbNew3.Append("           AND INV.CHK_INVOICE<>2 ");
                if (!string.IsNullOrEmpty(CountryPK) & CountryPK != "0")
                {
                    sbNew3.Append("       AND LMT.COUNTRY_MST_FK IN(" + CountryPK + ")");
                }

                if (!string.IsNullOrEmpty(LocPK) & LocPK != "0")
                {
                    sbNew3.Append("        AND LMT.LOCATION_MST_PK IN (" + LocPK + ") ");
                }

                if (!string.IsNullOrEmpty(CustPK) & CustPK != "0")
                {
                    sbNew3.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustPK + ")");
                }

                if (!string.IsNullOrEmpty(CustGroupPK) & CustGroupPK != "0")
                {
                    sbNew3.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustGroupPK + ")");
                }

                if (Fromdate != "" & Todate != "")
                {
                    sbNew3.Append("           AND CNT.CREDIT_NOTE_DATE BETWEEN TO_DATE('" + Fromdate + "') AND");
                    sbNew3.Append("               TO_DATE('" + Todate + "')");
                }

                // -----Added By Sushama----
                // ' ----CBJC(Invoice)--'
                sb.Append(sbNew3.ToString() + "          UNION");
                sb.Append("           SELECT DISTINCT CMT.CUSTOMER_MST_PK,");
                sb.Append("                        CCD.ADM_LOCATION_MST_FK,");
                sb.Append("                        INV.INVOICE_DATE REF_DATE,");
                sb.Append("                        (CASE");
                sb.Append("                          WHEN INV.PROCESS_TYPE = 1 THEN");
                sb.Append("                           'EXPORT'");
                sb.Append("                          ELSE");
                sb.Append("                           'IMPORT'");
                sb.Append("                        END) PROCESS,");
                sb.Append("                        'INVOICE' TRANSACTION,");
                sb.Append("                        CBJC.HBL_NO HBL_REF_NO,");
                sb.Append("                        INV.INVOICE_REF_NO DOCREFNR,");
                sb.Append("                        INV.CONSOL_INVOICE_PK REF_PK,");
                sb.Append("                        INV.BUSINESS_TYPE BIZ_TYPE,");
                sb.Append("                        CBJC.CARGO_TYPE CARGO_TYPE,");
                sb.Append("                        ROUND(INV.NET_RECEIVABLE *");
                sb.Append("                              " + ParentDB + ".GET_EX_RATE(INV.CURRENCY_MST_FK,");
                sb.Append(CurrPK + ",");
                sb.Append("                                          INV.INVOICE_DATE),");
                sb.Append("                              2) DEBIT,");
                sb.Append("                        0 CREDIT,");
                sb.Append("                        0 BALANCE,");
                sb.Append("                        (SELECT DISTINCT TO_CHAR(TO_DATE(SYSDATE,");
                sb.Append("                                                         'DD/MM/YYYY') -");
                sb.Append("                                                 TO_DATE(INV.INVOICE_DATE)) OUTSTANDING_DAYS");
                sb.Append("                           FROM " + ParentDB + ".COLLECTIONS_TRN_TBL COLL,");
                sb.Append("                                " + ParentDB + ".CONSOL_INVOICE_TBL  INV1");
                sb.Append("                          WHERE INV1.INVOICE_REF_NO = COLL.INVOICE_REF_NR(+)");
                sb.Append("                            AND INV1.INVOICE_REF_NO = INV.INVOICE_REF_NO");
                sb.Append("                            AND NVL((INV1.NET_RECEIVABLE -");
                sb.Append("                                    NVL((SELECT SUM(CTRN.RECD_AMOUNT_HDR_CURR)");
                sb.Append("                                           FROM " + ParentDB + ".COLLECTIONS_TRN_TBL CTRN");
                sb.Append("                                          WHERE CTRN.INVOICE_REF_NR LIKE");
                sb.Append("                                                INV1.INVOICE_REF_NO),");
                sb.Append("                                         0.00)),");
                sb.Append("                                    0) > 0)");
                sb.Append("          FROM " + ParentDB + ".CBJC_TBL   CBJC,");
                sb.Append("              " + ParentDB + ".CUSTOMER_MST_TBL       CMT,");
                sb.Append("               " + ParentDB + ".CUSTOMER_CONTACT_DTLS  CCD,");
                sb.Append("               " + ParentDB + ".LOCATION_MST_TBL       LMT,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TBL     INV,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TRN_TBL INVTRN");
                sb.Append("         WHERE INVTRN.JOB_CARD_FK = CBJC.CBJC_PK");
                sb.Append("           AND INV.IS_FAC_INV <> 1");
                sb.Append("           AND INV.CUSTOMER_MST_FK = CMT.CUSTOMER_MST_PK ");
                sb.Append("           AND INV.PROCESS_TYPE = " + ProcessType);
                sb.Append("           AND INV.CONSOL_INVOICE_PK = INVTRN.CONSOL_INVOICE_FK");
                sb.Append("           AND CMT.CUSTOMER_MST_PK = CCD.CUSTOMER_MST_FK");
                sb.Append("           AND LMT.LOCATION_MST_PK = CCD.ADM_LOCATION_MST_FK");
                sb.Append("           AND NVL((SELECT SUM(WMT.WRITEOFF_AMOUNT)");
                sb.Append("                     FROM " + ParentDB + ".WRITEOFF_MANUAL_TBL WMT");
                sb.Append("                    WHERE WMT.CONSOL_INVOICE_FK = INV.CONSOL_INVOICE_PK),");
                sb.Append("                   0.00) = 0");
                if (Fromdate != "" & Todate != "")
                {
                    sb.Append("           AND INV.invoice_date BETWEEN TO_DATE('" + Fromdate + "') AND");
                    sb.Append("               TO_DATE('" + Todate + "')");
                }
                sb.Append("          AND CBJC.PROCESS_TYPE =" + ProcessType);
                if (BizType != 3)
                {
                    sb.Append("          AND CBJC.BIZ_TYPE =" + BizType);
                }

                if (CargoType != 0)
                {
                    sb.Append("     AND CBJC.CARGO_TYPE=" + CargoType);
                }

                if (!string.IsNullOrEmpty(CountryPK) & CountryPK != "0")
                {
                    sb.Append("       AND LMT.COUNTRY_MST_FK IN(" + CountryPK + ")");
                }

                if (!string.IsNullOrEmpty(LocPK) & LocPK != "0")
                {
                    sb.Append("        AND LMT.LOCATION_MST_PK IN (" + LocPK + ") ");
                }

                if (!string.IsNullOrEmpty(CustPK) & CustPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustPK + ")");
                }

                if (!string.IsNullOrEmpty(CustGroupPK) & CustGroupPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustGroupPK + ")");
                }

                if (BizType == 1)
                {
                    if (!string.IsNullOrEmpty(FlightID))
                    {
                        sb.Append("     AND   CBJC.FLIGHT_NO IN (" + FlightID + ")");
                    }
                }
                else if (BizType == 2)
                {
                    if (!string.IsNullOrEmpty(VslVoyPK))
                    {
                        sb.Append("     AND   CBJC.VOYAGE_TRN_FK=" + VslVoyPK + "");
                    }
                }
                sb.Append("           AND INVTRN.JOB_TYPE = 2       ");
                sb.Append("           AND INV.CHK_INVOICE<>2 ");

                // '    ----TPT(Tranport Note)(Invoice)--
                sb.Append("           UNION");
                sb.Append("           SELECT DISTINCT CMT.CUSTOMER_MST_PK,");
                sb.Append("                        CCD.ADM_LOCATION_MST_FK,");
                sb.Append("                       INV.INVOICE_DATE REF_DATE,");
                sb.Append("                        (CASE");
                sb.Append("                          WHEN INV.PROCESS_TYPE = 1 THEN");
                sb.Append("                           'EXPORT'");
                sb.Append("                          ELSE");
                sb.Append("                           'IMPORT'");
                sb.Append("                        END) PROCESS,");
                sb.Append("                        'INVOICE' TRANSACTION,");
                sb.Append("                        (SELECT CONT.BL_NUMBER FROM " + ParentDB + ".TRANSPORT_TRN_CONT CONT WHERE CONT.TRANSPORT_INST_FK = TIST.TRANSPORT_INST_SEA_PK AND ROWNUM=1) HBL_REF_NO,");
                sb.Append("                        INV.INVOICE_REF_NO DOCREFNR,");
                sb.Append("                        INV.CONSOL_INVOICE_PK REF_PK,");
                sb.Append("                        INV.BUSINESS_TYPE BIZ_TYPE,");
                sb.Append("                        TIST.CARGO_TYPE CARGO_TYPE,");
                sb.Append("                        ROUND(INV.NET_RECEIVABLE *");
                sb.Append("                              " + ParentDB + ".GET_EX_RATE(INV.CURRENCY_MST_FK,");
                sb.Append(CurrPK + ",");
                sb.Append("                                          INV.INVOICE_DATE),");
                sb.Append("                              2) DEBIT,");
                sb.Append("                        0 CREDIT,");
                sb.Append("                        0 BALANCE,");
                sb.Append("                        (SELECT DISTINCT TO_CHAR(TO_DATE(SYSDATE,");
                sb.Append("                                                         'DD/MM/YYYY') -");
                sb.Append("                                                 TO_DATE(INV.INVOICE_DATE)) OUTSTANDING_DAYS");
                sb.Append("                           FROM " + ParentDB + ".COLLECTIONS_TRN_TBL COLL,");
                sb.Append("                                " + ParentDB + ".CONSOL_INVOICE_TBL  INV1");
                sb.Append("                          WHERE INV1.INVOICE_REF_NO = COLL.INVOICE_REF_NR(+)");
                sb.Append("                            AND INV1.INVOICE_REF_NO = INV.INVOICE_REF_NO");
                sb.Append("                            AND NVL((INV1.NET_RECEIVABLE -");
                sb.Append("                                    NVL((SELECT SUM(CTRN.RECD_AMOUNT_HDR_CURR)");
                sb.Append("                                           FROM " + ParentDB + ".COLLECTIONS_TRN_TBL CTRN");
                sb.Append("                                          WHERE CTRN.INVOICE_REF_NR LIKE");
                sb.Append("                                                INV1.INVOICE_REF_NO),");
                sb.Append("                                         0.00)),");
                sb.Append("                                    0) > 0)");
                sb.Append("          FROM " + ParentDB + ".TRANSPORT_INST_SEA_TBL TIST  ,");
                sb.Append("               " + ParentDB + ".CUSTOMER_MST_TBL       CMT,");
                sb.Append("               " + ParentDB + ".CUSTOMER_CONTACT_DTLS  CCD,");
                sb.Append("               " + ParentDB + ".LOCATION_MST_TBL       LMT,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TBL     INV,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TRN_TBL INVTRN");
                sb.Append("         WHERE INVTRN.JOB_CARD_FK = TIST.TRANSPORT_INST_SEA_PK(+)");
                sb.Append("           AND INV.IS_FAC_INV <> 1");
                sb.Append("           AND INV.CUSTOMER_MST_FK = CMT.CUSTOMER_MST_PK(+)");
                sb.Append("           AND INV.PROCESS_TYPE = " + ProcessType);
                sb.Append("           AND INV.CONSOL_INVOICE_PK = INVTRN.CONSOL_INVOICE_FK");
                sb.Append("           AND CMT.CUSTOMER_MST_PK = CCD.CUSTOMER_MST_FK");
                sb.Append("           AND LMT.LOCATION_MST_PK = CCD.ADM_LOCATION_MST_FK");
                sb.Append("           AND NVL((SELECT SUM(WMT.WRITEOFF_AMOUNT)");
                sb.Append("                     FROM " + ParentDB + ".WRITEOFF_MANUAL_TBL WMT");
                sb.Append("                    WHERE WMT.CONSOL_INVOICE_FK = INV.CONSOL_INVOICE_PK),");
                sb.Append("                   0.00) = 0");
                if (Fromdate != "" & Todate != "")
                {
                    sb.Append("           AND INV.invoice_date BETWEEN TO_DATE('" + Fromdate + "') AND");
                    sb.Append("               TO_DATE('" + Todate + "')");
                }
                sb.Append("          AND TIST.PROCESS_TYPE =" + ProcessType);
                if (BizType != 3)
                {
                    sb.Append("          AND TIST.BUSINESS_TYPE =" + BizType);
                }

                if (CargoType != 0)
                {
                    sb.Append("     AND TIST.CARGO_TYPE=" + CargoType);
                }

                if (!string.IsNullOrEmpty(CountryPK) & CountryPK != "0")
                {
                    sb.Append("       AND LMT.COUNTRY_MST_FK IN(" + CountryPK + ")");
                }

                if (!string.IsNullOrEmpty(LocPK) & LocPK != "0")
                {
                    sb.Append("        AND LMT.LOCATION_MST_PK IN (" + LocPK + ") ");
                }

                if (!string.IsNullOrEmpty(CustPK) & CustPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustPK + ")");
                }

                if (!string.IsNullOrEmpty(CustGroupPK) & CustGroupPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustGroupPK + ")");
                }

                if (BizType == 1)
                {
                    if (!string.IsNullOrEmpty(FlightID))
                    {
                        sb.Append("     AND   TIST.FLIGHT_NO IN (" + FlightID + ")");
                    }
                }
                else if (BizType == 2)
                {
                    if (!string.IsNullOrEmpty(VslVoyPK))
                    {
                        sb.Append("     AND   TIST.VSL_VOY_FK=" + VslVoyPK + "");
                    }
                }
                sb.Append("           AND INVTRN.JOB_TYPE = 3");
                sb.Append("           AND INV.CHK_INVOICE<>2 ");
                // '       ----DET & DEM-TPT(Invoice)-
                sb.Append("        UNION");
                sb.Append("           SELECT DISTINCT CMT.CUSTOMER_MST_PK,");
                sb.Append("                        CCD.ADM_LOCATION_MST_FK,");
                sb.Append("                        INV.INVOICE_DATE REF_DATE,");
                sb.Append("                        (CASE");
                sb.Append("                          WHEN INV.PROCESS_TYPE = 1 THEN");
                sb.Append("                           'EXPORT'");
                sb.Append("                          ELSE");
                sb.Append("                           'IMPORT'");
                sb.Append("                        END) PROCESS,");
                sb.Append("                        'INVOICE' TRANSACTION,");
                sb.Append("                        (SELECT CONT.BL_NUMBER FROM " + ParentDB + ".TRANSPORT_TRN_CONT CONT WHERE CONT.TRANSPORT_INST_FK = TIST.TRANSPORT_INST_SEA_PK AND ROWNUM=1) HBL_REF_NO,");
                sb.Append("                        INV.INVOICE_REF_NO DOCREFNR,");
                sb.Append("                        INV.CONSOL_INVOICE_PK REF_PK,");
                sb.Append("                        INV.BUSINESS_TYPE BIZ_TYPE,");
                sb.Append("                        DCH.CARGO_TYPE CARGO_TYPE,");
                sb.Append("                        ROUND(INV.NET_RECEIVABLE *");
                sb.Append("                              " + ParentDB + ".GET_EX_RATE(INV.CURRENCY_MST_FK,");
                sb.Append(CurrPK + ",");
                sb.Append("                                          INV.INVOICE_DATE),");
                sb.Append("                              2) DEBIT,");
                sb.Append("                        0 CREDIT,");
                sb.Append("                        0 BALANCE,");
                sb.Append("                        (SELECT DISTINCT TO_CHAR(TO_DATE(SYSDATE,");
                sb.Append("                                                         'DD/MM/YYYY') -");
                sb.Append("                                                 TO_DATE(INV.INVOICE_DATE)) OUTSTANDING_DAYS");
                sb.Append("                           FROM " + ParentDB + ".COLLECTIONS_TRN_TBL COLL,");
                sb.Append("                                " + ParentDB + ".CONSOL_INVOICE_TBL  INV1");
                sb.Append("                          WHERE INV1.INVOICE_REF_NO = COLL.INVOICE_REF_NR(+)");
                sb.Append("                            AND INV1.INVOICE_REF_NO = INV.INVOICE_REF_NO");
                sb.Append("                            AND NVL((INV1.NET_RECEIVABLE -");
                sb.Append("                                    NVL((SELECT SUM(CTRN.RECD_AMOUNT_HDR_CURR)");
                sb.Append("                                           FROM " + ParentDB + ".COLLECTIONS_TRN_TBL CTRN");
                sb.Append("                                          WHERE CTRN.INVOICE_REF_NR LIKE");
                sb.Append("                                                INV1.INVOICE_REF_NO),");
                sb.Append("                                         0.00)),");
                sb.Append("                                    0) > 0)");
                sb.Append("          FROM " + ParentDB + ".DEM_CALC_HDR DCH  ,");
                sb.Append("                " + ParentDB + ".TRANSPORT_INST_SEA_TBL TIST,");
                sb.Append("               " + ParentDB + ".CUSTOMER_MST_TBL       CMT,");
                sb.Append("               " + ParentDB + ".CUSTOMER_CONTACT_DTLS  CCD,");
                sb.Append("               " + ParentDB + ".LOCATION_MST_TBL       LMT,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TBL     INV,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TRN_TBL INVTRN");
                sb.Append("         WHERE INVTRN.JOB_CARD_FK = DCH.DEM_CALC_HDR_PK(+)");
                sb.Append("           AND DCH.DOC_REF_FK = TIST.TRANSPORT_INST_SEA_PK");
                sb.Append("           AND INV.IS_FAC_INV <> 1");
                sb.Append("           AND INV.CUSTOMER_MST_FK = CMT.CUSTOMER_MST_PK(+)");
                sb.Append("           AND INV.PROCESS_TYPE = " + ProcessType);
                sb.Append("           AND INV.CONSOL_INVOICE_PK = INVTRN.CONSOL_INVOICE_FK");
                sb.Append("           AND CMT.CUSTOMER_MST_PK = CCD.CUSTOMER_MST_FK");
                sb.Append("           AND LMT.LOCATION_MST_PK = CCD.ADM_LOCATION_MST_FK");
                sb.Append("           AND NVL((SELECT SUM(WMT.WRITEOFF_AMOUNT)");
                sb.Append("                     FROM " + ParentDB + ".WRITEOFF_MANUAL_TBL WMT");
                sb.Append("                    WHERE WMT.CONSOL_INVOICE_FK = INV.CONSOL_INVOICE_PK),");
                sb.Append("                   0.00) = 0");
                sb.Append("          AND DCH.PROCESS_TYPE =" + ProcessType);
                if (BizType != 3)
                {
                    sb.Append("          AND DCH.Biz_Type =" + BizType);
                }

                if (CargoType != 0)
                {
                    sb.Append("     AND DCH.CARGO_TYPE=" + CargoType);
                }

                if (BizType == 1)
                {
                    if (!string.IsNullOrEmpty(FlightID))
                    {
                        sb.Append("     AND   TIST.FLIGHT_NO IN (" + FlightID + ")");
                    }
                }
                else if (BizType == 2)
                {
                    if (!string.IsNullOrEmpty(VslVoyPK))
                    {
                        sb.Append("     AND   TIST.VSL_VOY_FK=" + VslVoyPK + "");
                    }
                }
                if (Fromdate != "" & Todate != "")
                {
                    sb.Append("       AND INV.invoice_date BETWEEN TO_DATE('" + Fromdate + "') AND");
                    sb.Append("               TO_DATE('" + Todate + "')");
                }
                if (!string.IsNullOrEmpty(CountryPK) & CountryPK != "0")
                {
                    sb.Append("       AND LMT.COUNTRY_MST_FK IN(" + CountryPK + ")");
                }

                if (!string.IsNullOrEmpty(LocPK) & LocPK != "0")
                {
                    sb.Append("        AND LMT.LOCATION_MST_PK IN (" + LocPK + ") ");
                }

                if (!string.IsNullOrEmpty(CustPK) & CustPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustPK + ")");
                }

                if (!string.IsNullOrEmpty(CustGroupPK) & CustGroupPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustGroupPK + ")");
                }

                sb.Append("           AND INVTRN.JOB_TYPE = 3");
                sb.Append("           AND INV.CHK_INVOICE<>2 ");
                // '       ----DET & DEM-CBJC(Invoice)-
                sb.Append("        UNION");
                sb.Append("           SELECT DISTINCT CMT.CUSTOMER_MST_PK,");
                sb.Append("                        CCD.ADM_LOCATION_MST_FK,");
                sb.Append("                        INV.INVOICE_DATE REF_DATE,");
                sb.Append("                        (CASE");
                sb.Append("                          WHEN INV.PROCESS_TYPE = 1 THEN");
                sb.Append("                           'EXPORT'");
                sb.Append("                          ELSE");
                sb.Append("                           'IMPORT'");
                sb.Append("                        END) PROCESS,");
                sb.Append("                        'INVOICE' TRANSACTION,");
                sb.Append("                        CBJC.HBL_NO HBL_REF_NO,");
                sb.Append("                        INV.INVOICE_REF_NO DOCREFNR,");
                sb.Append("                        INV.CONSOL_INVOICE_PK REF_PK,");
                sb.Append("                        INV.BUSINESS_TYPE BIZ_TYPE,");
                sb.Append("                        DCH.CARGO_TYPE CARGO_TYPE,");
                sb.Append("                        ROUND(INV.NET_RECEIVABLE *");
                sb.Append("                              " + ParentDB + ".GET_EX_RATE(INV.CURRENCY_MST_FK,");
                sb.Append(CurrPK + ",");
                sb.Append("                                          INV.INVOICE_DATE),");
                sb.Append("                              2) DEBIT,");
                sb.Append("                        0 CREDIT,");
                sb.Append("                        0 BALANCE,");
                sb.Append("                        (SELECT DISTINCT TO_CHAR(TO_DATE(SYSDATE,");
                sb.Append("                                                         'DD/MM/YYYY') -");
                sb.Append("                                                 TO_DATE(INV.INVOICE_DATE)) OUTSTANDING_DAYS");
                sb.Append("                           FROM " + ParentDB + ".COLLECTIONS_TRN_TBL COLL,");
                sb.Append("                                " + ParentDB + ".CONSOL_INVOICE_TBL  INV1");
                sb.Append("                          WHERE INV1.INVOICE_REF_NO = COLL.INVOICE_REF_NR(+)");
                sb.Append("                            AND INV1.INVOICE_REF_NO = INV.INVOICE_REF_NO");
                sb.Append("                            AND NVL((INV1.NET_RECEIVABLE -");
                sb.Append("                                    NVL((SELECT SUM(CTRN.RECD_AMOUNT_HDR_CURR)");
                sb.Append("                                           FROM " + ParentDB + ".COLLECTIONS_TRN_TBL CTRN");
                sb.Append("                                          WHERE CTRN.INVOICE_REF_NR LIKE");
                sb.Append("                                                INV1.INVOICE_REF_NO),");
                sb.Append("                                         0.00)),");
                sb.Append("                                    0) > 0)");
                sb.Append("          FROM " + ParentDB + ".DEM_CALC_HDR DCH  ,");
                sb.Append("               " + ParentDB + ".CBJC_TBL               CBJC,");
                sb.Append("               " + ParentDB + ".CUSTOMER_MST_TBL       CMT,");
                sb.Append("               " + ParentDB + ".CUSTOMER_CONTACT_DTLS  CCD,");
                sb.Append("               " + ParentDB + ".LOCATION_MST_TBL       LMT,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TBL     INV,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TRN_TBL INVTRN");
                sb.Append("         WHERE INVTRN.JOB_CARD_FK = DCH.DEM_CALC_HDR_PK(+)");
                sb.Append("           AND DCH.DOC_REF_FK = CBJC.CBJC_PK");
                sb.Append("           AND INV.IS_FAC_INV <> 1");
                sb.Append("           AND INV.CUSTOMER_MST_FK = CMT.CUSTOMER_MST_PK(+)");
                sb.Append("           AND INV.PROCESS_TYPE = " + ProcessType);
                sb.Append("           AND INV.CONSOL_INVOICE_PK = INVTRN.CONSOL_INVOICE_FK");
                sb.Append("           AND CMT.CUSTOMER_MST_PK = CCD.CUSTOMER_MST_FK");
                sb.Append("           AND LMT.LOCATION_MST_PK = CCD.ADM_LOCATION_MST_FK");
                sb.Append("           AND NVL((SELECT SUM(WMT.WRITEOFF_AMOUNT)");
                sb.Append("                     FROM " + ParentDB + ".WRITEOFF_MANUAL_TBL WMT");
                sb.Append("                    WHERE WMT.CONSOL_INVOICE_FK = INV.CONSOL_INVOICE_PK),");
                sb.Append("                   0.00) = 0");
                sb.Append("          AND DCH.PROCESS_TYPE =" + ProcessType);
                if (BizType != 3)
                {
                    sb.Append("          AND DCH.Biz_Type =" + BizType);
                }

                if (CargoType != 0)
                {
                    sb.Append("     AND DCH.CARGO_TYPE=" + CargoType);
                }

                if (BizType == 1)
                {
                    if (!string.IsNullOrEmpty(FlightID))
                    {
                        sb.Append("         AND   CBJC.FLIGHT_NO IN (" + FlightID + ")");
                    }
                }
                else if (BizType == 2)
                {
                    if (!string.IsNullOrEmpty(VslVoyPK))
                    {
                        sb.Append("         AND   CBJC.VOYAGE_TRN_FK=" + VslVoyPK + "");
                    }
                }
                if (Fromdate != "" & Todate != "")
                {
                    sb.Append("       AND INV.invoice_date BETWEEN TO_DATE('" + Fromdate + "') AND");
                    sb.Append("               TO_DATE('" + Todate + "')");
                }
                if (!string.IsNullOrEmpty(CountryPK) & CountryPK != "0")
                {
                    sb.Append("       AND LMT.COUNTRY_MST_FK IN(" + CountryPK + ")");
                }

                if (!string.IsNullOrEmpty(LocPK) & LocPK != "0")
                {
                    sb.Append("        AND LMT.LOCATION_MST_PK IN (" + LocPK + ") ");
                }

                if (!string.IsNullOrEmpty(CustPK) & CustPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustPK + ")");
                }

                if (!string.IsNullOrEmpty(CustGroupPK) & CustGroupPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustGroupPK + ")");
                }

                sb.Append("           AND INVTRN.JOB_TYPE = 4");
                sb.Append("           AND INV.CHK_INVOICE<>2 ");

                // '          ---CBJC(Collection)---------'
                sb.Append("              UNION");
                sb.Append("              SELECT DISTINCT CMT.CUSTOMER_MST_PK,");
                sb.Append("                        CCD.ADM_LOCATION_MST_FK,");
                sb.Append("                        COL.COLLECTIONS_DATE REF_DATE,");
                sb.Append("                        (CASE");
                sb.Append("                          WHEN COL.PROCESS_TYPE = 1 THEN");
                sb.Append("                           'EXPORT'");
                sb.Append("                          ELSE");
                sb.Append("                           'IMPORT'");
                sb.Append("                        END) PROCESS,");
                sb.Append("                        'COLLECTION' TRANSACTION,");
                sb.Append("                        CBJC.HBL_NO HBL_REF_NO,");
                sb.Append("                        COL.COLLECTIONS_REF_NO DOCREFNR,");
                sb.Append("                        COL.COLLECTIONS_TBL_PK REF_PK,");
                sb.Append("                        COL.BUSINESS_TYPE BIZ_TYPE,");
                sb.Append("                        CBJC.CARGO_TYPE CARGO_TYPE,");
                sb.Append("                        0 DEBIT,");
                sb.Append("                        ROUND(CLT.RECD_AMOUNT_HDR_CURR *");
                sb.Append("                              " + ParentDB + ".GET_EX_RATE(COL.CURRENCY_MST_FK,");
                sb.Append(CurrPK + ",");
                sb.Append("                                          COL.COLLECTIONS_DATE),");
                sb.Append("                              2) CREDIT,");
                sb.Append("                        0 BALANCE,");
                sb.Append("                        '' OUTSTANDING_DAYS");
                sb.Append("          FROM ");
                sb.Append("               " + ParentDB + ".CBJC_TBL               CBJC,");
                sb.Append("               " + ParentDB + ".CUSTOMER_MST_TBL       CMT,");
                sb.Append("               " + ParentDB + ".CUSTOMER_CONTACT_DTLS  CCD,");
                sb.Append("               " + ParentDB + ".LOCATION_MST_TBL       LMT,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TBL     INV,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TRN_TBL INVTRN,");
                sb.Append("               " + ParentDB + ".COLLECTIONS_TRN_TBL    CLT,");
                sb.Append("               " + ParentDB + ".COLLECTIONS_TBL        COL");
                sb.Append("         WHERE INVTRN.JOB_CARD_FK = CBJC.CBJC_PK(+)          ");
                sb.Append("           AND INV.IS_FAC_INV <> 1");
                sb.Append("           AND INV.CUSTOMER_MST_FK = CMT.CUSTOMER_MST_PK");
                sb.Append("           AND COL.PROCESS_TYPE = " + ProcessType);
                sb.Append("           AND INV.CONSOL_INVOICE_PK = INVTRN.CONSOL_INVOICE_FK");
                sb.Append("           AND CLT.INVOICE_REF_NR(+) = INV.INVOICE_REF_NO");
                sb.Append("           AND COL.COLLECTIONS_TBL_PK(+) = CLT.COLLECTIONS_TBL_FK");
                sb.Append("           AND CMT.CUSTOMER_MST_PK = CCD.CUSTOMER_MST_FK");
                sb.Append("           AND LMT.LOCATION_MST_PK = CCD.ADM_LOCATION_MST_FK");
                sb.Append("           AND NVL((SELECT SUM(WMT.WRITEOFF_AMOUNT)");
                sb.Append("                     FROM " + ParentDB + ".WRITEOFF_MANUAL_TBL WMT");
                sb.Append("                    WHERE WMT.CONSOL_INVOICE_FK = INV.CONSOL_INVOICE_PK),");
                sb.Append("                   0.00) = 0");
                if (Fromdate != "" & Todate != "")
                {
                    sb.Append("           AND COL.COLLECTIONS_DATE  BETWEEN TO_DATE('" + Fromdate + "') AND");
                    sb.Append("               TO_DATE('" + Todate + "')");
                }
                sb.Append("          AND CBJC.PROCESS_TYPE =" + ProcessType);
                if (BizType != 3)
                {
                    sb.Append("          AND CBJC.BIZ_TYPE =" + BizType);
                }

                if (CargoType != 0)
                {
                    sb.Append("     AND CBJC.CARGO_TYPE=" + CargoType);
                }

                if (!string.IsNullOrEmpty(CountryPK) & CountryPK != "0")
                {
                    sb.Append("       AND LMT.COUNTRY_MST_FK IN(" + CountryPK + ")");
                }

                if (!string.IsNullOrEmpty(LocPK) & LocPK != "0")
                {
                    sb.Append("        AND LMT.LOCATION_MST_PK IN (" + LocPK + ") ");
                }

                if (!string.IsNullOrEmpty(CustPK) & CustPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustPK + ")");
                }

                if (!string.IsNullOrEmpty(CustGroupPK) & CustGroupPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustGroupPK + ")");
                }

                if (BizType == 1)
                {
                    if (!string.IsNullOrEmpty(FlightID))
                    {
                        sb.Append("     AND   CBJC.FLIGHT_NO IN (" + FlightID + ")");
                    }
                }
                else if (BizType == 2)
                {
                    if (!string.IsNullOrEmpty(VslVoyPK))
                    {
                        sb.Append("     AND   CBJC.VOYAGE_TRN_FK=" + VslVoyPK + "");
                    }
                }
                sb.Append("               AND INVTRN.JOB_TYPE = 2");
                sb.Append("           AND INV.CHK_INVOICE<>2 ");
                // '              ----TPT-(Collection)---------'
                sb.Append("        UNION");
                sb.Append("              SELECT DISTINCT CMT.CUSTOMER_MST_PK,");
                sb.Append("                        CCD.ADM_LOCATION_MST_FK,");
                sb.Append("                        COL.COLLECTIONS_DATE REF_DATE,");
                sb.Append("                        (CASE");
                sb.Append("                          WHEN COL.PROCESS_TYPE = 1 THEN");
                sb.Append("                           'EXPORT'");
                sb.Append("                          ELSE");
                sb.Append("                           'IMPORT'");
                sb.Append("                        END) PROCESS,");
                sb.Append("                        'COLLECTION' TRANSACTION,");
                sb.Append("                        (SELECT CON.BL_NUMBER FROM " + ParentDB + ".TRANSPORT_TRN_CONT CON WHERE CON.TRANSPORT_INST_FK = TIST.TRANSPORT_INST_SEA_PK AND ROWNUM=1) HBL_REF_NO,");
                sb.Append("                        COL.COLLECTIONS_REF_NO DOCREFNR,");
                sb.Append("                        COL.COLLECTIONS_TBL_PK REF_PK,");
                sb.Append("                        COL.BUSINESS_TYPE BIZ_TYPE,");
                sb.Append("                        TIST.CARGO_TYPE CARGO_TYPE,");
                sb.Append("                        0 DEBIT,");
                sb.Append("                        ROUND(CLT.RECD_AMOUNT_HDR_CURR *");
                sb.Append("                              " + ParentDB + ".GET_EX_RATE(COL.CURRENCY_MST_FK,");
                sb.Append(CurrPK + ",");
                sb.Append("                                          COL.COLLECTIONS_DATE),");
                sb.Append("                              2) CREDIT,");
                sb.Append("                        0 BALANCE,");
                sb.Append("                        '' OUTSTANDING_DAYS");
                sb.Append("          FROM " + ParentDB + ".TRANSPORT_INST_SEA_TBL TIST ,");
                sb.Append("               " + ParentDB + ".CUSTOMER_MST_TBL       CMT,");
                sb.Append("               " + ParentDB + ".CUSTOMER_CONTACT_DTLS  CCD,");
                sb.Append("               " + ParentDB + ".LOCATION_MST_TBL       LMT,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TBL     INV,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TRN_TBL INVTRN,");
                sb.Append("               " + ParentDB + ".COLLECTIONS_TRN_TBL    CLT,");
                sb.Append("               " + ParentDB + ".COLLECTIONS_TBL        COL");
                sb.Append("         WHERE INVTRN.JOB_CARD_FK = TIST.TRANSPORT_INST_SEA_PK(+)");
                sb.Append("           AND INV.IS_FAC_INV <> 1");
                sb.Append("           AND INV.CUSTOMER_MST_FK = CMT.CUSTOMER_MST_PK");
                sb.Append("           AND COL.PROCESS_TYPE = " + ProcessType);
                sb.Append("           AND INV.CONSOL_INVOICE_PK = INVTRN.CONSOL_INVOICE_FK");
                sb.Append("           AND CLT.INVOICE_REF_NR(+) = INV.INVOICE_REF_NO");
                sb.Append("           AND COL.COLLECTIONS_TBL_PK(+) = CLT.COLLECTIONS_TBL_FK");
                sb.Append("           AND CMT.CUSTOMER_MST_PK = CCD.CUSTOMER_MST_FK");
                sb.Append("           AND LMT.LOCATION_MST_PK = CCD.ADM_LOCATION_MST_FK");
                sb.Append("           AND NVL((SELECT SUM(WMT.WRITEOFF_AMOUNT)");
                sb.Append("                     FROM " + ParentDB + ".WRITEOFF_MANUAL_TBL WMT");
                sb.Append("                    WHERE WMT.CONSOL_INVOICE_FK = INV.CONSOL_INVOICE_PK),");
                sb.Append("                   0.00) = 0");

                if (CargoType != 0)
                {
                    sb.Append("     AND TIST.CARGO_TYPE=" + CargoType);
                }

                if (Fromdate != "" & Todate != "")
                {
                    sb.Append("           AND COL.COLLECTIONS_DATE BETWEEN TO_DATE('" + Fromdate + "') AND");
                    sb.Append("               TO_DATE('" + Todate + "')");
                }
                sb.Append("          AND TIST.PROCESS_TYPE =" + ProcessType);
                if (BizType != 3)
                {
                    sb.Append("          AND TIST.BUSINESS_TYPE =" + BizType);
                }

                if (!string.IsNullOrEmpty(CountryPK) & CountryPK != "0")
                {
                    sb.Append("       AND LMT.COUNTRY_MST_FK IN(" + CountryPK + ")");
                }

                if (!string.IsNullOrEmpty(LocPK) & LocPK != "0")
                {
                    sb.Append("        AND LMT.LOCATION_MST_PK IN (" + LocPK + ") ");
                }

                if (!string.IsNullOrEmpty(CustPK) & CustPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustPK + ")");
                }

                if (!string.IsNullOrEmpty(CustGroupPK) & CustGroupPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustGroupPK + ")");
                }

                if (BizType == 1)
                {
                    if (!string.IsNullOrEmpty(FlightID))
                    {
                        sb.Append("     AND   TIST.FLIGHT_NO IN (" + FlightID + ")");
                    }
                }
                else if (BizType == 2)
                {
                    if (!string.IsNullOrEmpty(VslVoyPK))
                    {
                        sb.Append("     AND   TIST.VSL_VOY_FK=" + VslVoyPK + "");
                    }
                }
                sb.Append("               AND INVTRN.JOB_TYPE = 3");
                sb.Append("           AND INV.CHK_INVOICE<>2 ");
                // '         ---Det & DEM TPT (Collection)'
                sb.Append("               UNION");
                sb.Append("               SELECT DISTINCT CMT.CUSTOMER_MST_PK,");
                sb.Append("                        CCD.ADM_LOCATION_MST_FK,");
                sb.Append("                        COL.COLLECTIONS_DATE REF_DATE,");
                sb.Append("                        (CASE");
                sb.Append("                          WHEN COL.PROCESS_TYPE = 1 THEN");
                sb.Append("                           'EXPORT'");
                sb.Append("                          ELSE");
                sb.Append("                           'IMPORT'");
                sb.Append("                        END) PROCESS,");
                sb.Append("                        'COLLECTION' TRANSACTION,");
                sb.Append("                        (SELECT CON.BL_NUMBER FROM " + ParentDB + ".TRANSPORT_TRN_CONT CON WHERE CON.TRANSPORT_INST_FK = TIST.TRANSPORT_INST_SEA_PK AND ROWNUM=1) HBL_REF_NO,");
                sb.Append("                        COL.COLLECTIONS_REF_NO DOCREFNR,");
                sb.Append("                        COL.COLLECTIONS_TBL_PK REF_PK,");
                sb.Append("                        COL.BUSINESS_TYPE BIZ_TYPE,");
                sb.Append("                        DCH.CARGO_TYPE CARGO_TYPE,");
                sb.Append("                        0 DEBIT,");
                sb.Append("                        ROUND(CLT.RECD_AMOUNT_HDR_CURR *");
                sb.Append("                              " + ParentDB + ".GET_EX_RATE(COL.CURRENCY_MST_FK,");
                sb.Append(CurrPK + ",");
                sb.Append("                                          COL.COLLECTIONS_DATE),");
                sb.Append("                              2) CREDIT,");
                sb.Append("                        0 BALANCE,");
                sb.Append("                        '' OUTSTANDING_DAYS");
                sb.Append("          FROM " + ParentDB + ".DEM_CALC_HDR DCH  ,");
                sb.Append("               " + ParentDB + ".TRANSPORT_INST_SEA_TBL TIST,");
                sb.Append("               " + ParentDB + ".CUSTOMER_MST_TBL       CMT,");
                sb.Append("               " + ParentDB + ".CUSTOMER_CONTACT_DTLS  CCD,");
                sb.Append("               " + ParentDB + ".LOCATION_MST_TBL       LMT,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TBL     INV,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TRN_TBL INVTRN,");
                sb.Append("               " + ParentDB + ".COLLECTIONS_TRN_TBL    CLT,");
                sb.Append("               " + ParentDB + ".COLLECTIONS_TBL        COL");
                sb.Append("         WHERE INVTRN.JOB_CARD_FK = DCH.DEM_CALC_HDR_PK(+)");
                sb.Append("           AND DCH.DOC_REF_FK = TIST.TRANSPORT_INST_SEA_PK");
                sb.Append("           AND INV.IS_FAC_INV <> 1");
                sb.Append("           AND INV.CUSTOMER_MST_FK = CMT.CUSTOMER_MST_PK");
                sb.Append("           AND COL.PROCESS_TYPE = " + ProcessType);
                sb.Append("           AND INV.CONSOL_INVOICE_PK(+) = INVTRN.CONSOL_INVOICE_FK");
                sb.Append("           AND CLT.INVOICE_REF_NR(+) = INV.INVOICE_REF_NO");
                sb.Append("           AND COL.COLLECTIONS_TBL_PK = CLT.COLLECTIONS_TBL_FK");
                sb.Append("           AND CMT.CUSTOMER_MST_PK = CCD.CUSTOMER_MST_FK");
                sb.Append("           AND LMT.LOCATION_MST_PK = CCD.ADM_LOCATION_MST_FK");
                sb.Append("           AND NVL((SELECT SUM(WMT.WRITEOFF_AMOUNT)");
                sb.Append("                     FROM " + ParentDB + ".WRITEOFF_MANUAL_TBL WMT");
                sb.Append("                    WHERE WMT.CONSOL_INVOICE_FK = INV.CONSOL_INVOICE_PK),");
                sb.Append("                   0.00) = 0");
                sb.Append("          AND DCH.PROCESS_TYPE =" + ProcessType);
                if (BizType != 3)
                {
                    sb.Append("          AND DCH.BIZ_TYPE =" + BizType);
                }

                if (BizType == 1)
                {
                    if (!string.IsNullOrEmpty(FlightID))
                    {
                        sb.Append("     AND   TIST.FLIGHT_NO IN (" + FlightID + ")");
                    }
                }
                else if (BizType == 2)
                {
                    if (!string.IsNullOrEmpty(VslVoyPK))
                    {
                        sb.Append("     AND   TIST.VSL_VOY_FK=" + VslVoyPK + "");
                    }
                }
                if (CargoType != 0)
                {
                    sb.Append("     AND DCH.CARGO_TYPE=" + CargoType);
                }

                if (Fromdate != "" & Todate != "")
                {
                    sb.Append("       AND  COL.COLLECTIONS_DATE  BETWEEN TO_DATE('" + Fromdate + "') AND");
                    sb.Append("               TO_DATE('" + Todate + "')");
                }
                if (!string.IsNullOrEmpty(CountryPK) & CountryPK != "0")
                {
                    sb.Append("       AND LMT.COUNTRY_MST_FK IN(" + CountryPK + ")");
                }

                if (!string.IsNullOrEmpty(LocPK) & LocPK != "0")
                {
                    sb.Append("        AND LMT.LOCATION_MST_PK IN (" + LocPK + ") ");
                }

                if (!string.IsNullOrEmpty(CustPK) & CustPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustPK + ")");
                }

                if (!string.IsNullOrEmpty(CustGroupPK) & CustGroupPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustGroupPK + ")");
                }

                sb.Append("           AND INVTRN.JOB_TYPE = 3 ");
                sb.Append("           AND INV.CHK_INVOICE<>2 ");
                // '         ---Det & DEM CBJC (Collection)'
                sb.Append("               UNION");
                sb.Append("               SELECT DISTINCT CMT.CUSTOMER_MST_PK,");
                sb.Append("                        CCD.ADM_LOCATION_MST_FK,");
                sb.Append("                        COL.COLLECTIONS_DATE REF_DATE,");
                sb.Append("                        (CASE");
                sb.Append("                          WHEN COL.PROCESS_TYPE = 1 THEN");
                sb.Append("                           'EXPORT'");
                sb.Append("                          ELSE");
                sb.Append("                           'IMPORT'");
                sb.Append("                        END) PROCESS,");
                sb.Append("                        'COLLECTION' TRANSACTION,");
                sb.Append("                        CBJC.HBL_NO HBL_REF_NO,");
                sb.Append("                        COL.COLLECTIONS_REF_NO DOCREFNR,");
                sb.Append("                        COL.COLLECTIONS_TBL_PK REF_PK,");
                sb.Append("                        COL.BUSINESS_TYPE BIZ_TYPE,");
                sb.Append("                        DCH.CARGO_TYPE CARGO_TYPE,");
                sb.Append("                        0 DEBIT,");
                sb.Append("                        ROUND(CLT.RECD_AMOUNT_HDR_CURR *");
                sb.Append("                              " + ParentDB + ".GET_EX_RATE(COL.CURRENCY_MST_FK,");
                sb.Append(CurrPK + ",");
                sb.Append("                                          COL.COLLECTIONS_DATE),");
                sb.Append("                              2) CREDIT,");
                sb.Append("                        0 BALANCE,");
                sb.Append("                        '' OUTSTANDING_DAYS");
                sb.Append("          FROM " + ParentDB + ".DEM_CALC_HDR DCH  ,");
                sb.Append("               " + ParentDB + ".CBJC_TBL               CBJC,");
                sb.Append("               " + ParentDB + ".CUSTOMER_MST_TBL       CMT,");
                sb.Append("               " + ParentDB + ".CUSTOMER_CONTACT_DTLS  CCD,");
                sb.Append("               " + ParentDB + ".LOCATION_MST_TBL       LMT,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TBL     INV,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TRN_TBL INVTRN,");
                sb.Append("               " + ParentDB + ".COLLECTIONS_TRN_TBL    CLT,");
                sb.Append("               " + ParentDB + ".COLLECTIONS_TBL        COL");
                sb.Append("         WHERE INVTRN.JOB_CARD_FK = DCH.DEM_CALC_HDR_PK(+)");
                sb.Append("           AND DCH.DOC_REF_FK = CBJC.CBJC_PK");
                sb.Append("           AND INV.IS_FAC_INV <> 1");
                sb.Append("           AND INV.CUSTOMER_MST_FK = CMT.CUSTOMER_MST_PK");
                sb.Append("           AND COL.PROCESS_TYPE = " + ProcessType);
                sb.Append("           AND INV.CONSOL_INVOICE_PK = INVTRN.CONSOL_INVOICE_FK");
                sb.Append("           AND CLT.INVOICE_REF_NR(+) = INV.INVOICE_REF_NO");
                sb.Append("           AND COL.COLLECTIONS_TBL_PK(+) = CLT.COLLECTIONS_TBL_FK");
                sb.Append("           AND CMT.CUSTOMER_MST_PK = CCD.CUSTOMER_MST_FK");
                sb.Append("           AND LMT.LOCATION_MST_PK = CCD.ADM_LOCATION_MST_FK");
                sb.Append("           AND NVL((SELECT SUM(WMT.WRITEOFF_AMOUNT)");
                sb.Append("                     FROM " + ParentDB + ".WRITEOFF_MANUAL_TBL WMT");
                sb.Append("                    WHERE WMT.CONSOL_INVOICE_FK = INV.CONSOL_INVOICE_PK),");
                sb.Append("                   0.00) = 0");
                sb.Append("          AND DCH.PROCESS_TYPE =" + ProcessType);
                if (BizType != 3)
                {
                    sb.Append("          AND DCH.BIZ_TYPE =" + BizType);
                }

                if (CargoType != 0)
                {
                    sb.Append("     AND DCH.CARGO_TYPE=" + CargoType);
                }

                if (BizType == 1)
                {
                    if (!string.IsNullOrEmpty(FlightID))
                    {
                        sb.Append("         AND   CBJC.FLIGHT_NO IN (" + FlightID + ")");
                    }
                }
                else if (BizType == 2)
                {
                    if (!string.IsNullOrEmpty(VslVoyPK))
                    {
                        sb.Append("         AND   CBJC.VOYAGE_TRN_FK=" + VslVoyPK + "");
                    }
                }
                if (Fromdate != "" & Todate != "")
                {
                    sb.Append("       AND  COL.COLLECTIONS_DATE  BETWEEN TO_DATE('" + Fromdate + "') AND");
                    sb.Append("               TO_DATE('" + Todate + "')");
                }
                if (!string.IsNullOrEmpty(CountryPK) & CountryPK != "0")
                {
                    sb.Append("       AND LMT.COUNTRY_MST_FK IN(" + CountryPK + ")");
                }

                if (!string.IsNullOrEmpty(LocPK) & LocPK != "0")
                {
                    sb.Append("        AND LMT.LOCATION_MST_PK IN (" + LocPK + ") ");
                }

                if (!string.IsNullOrEmpty(CustPK) & CustPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustPK + ")");
                }

                if (!string.IsNullOrEmpty(CustGroupPK) & CustGroupPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustGroupPK + ")");
                }

                sb.Append("           AND INVTRN.JOB_TYPE = 4 ");
                sb.Append("           AND INV.CHK_INVOICE<>2 ");
                // '   --------CBJC CR Note--
                sb.Append("           UNION");
                sb.Append("           SELECT DISTINCT CMT.CUSTOMER_MST_PK,");
                sb.Append("                        CCD.ADM_LOCATION_MST_FK,");
                sb.Append("                        CNT.CREDIT_NOTE_DATE REF_DATE,");
                sb.Append("                        (CASE");
                sb.Append("                          WHEN CNT.PROCESS_TYPE = 1 THEN");
                sb.Append("                           'EXPORT'");
                sb.Append("                          ELSE");
                sb.Append("                           'IMPORT'");
                sb.Append("                        END) PROCESS,");
                sb.Append("                        'CREDITNOTE' TRANSACTION,");
                sb.Append("                        CBJC.HBL_NO HBL_REF_NO,");
                sb.Append("                        CNT.CREDIT_NOTE_REF_NR DOCREFNR,");
                sb.Append("                        CNT.CRN_TBL_PK REF_PK,");
                sb.Append("                        CNT.BIZ_TYPE BIZ_TYPE,");
                sb.Append("                        CBJC.CARGO_TYPE CARGO_TYPE,");
                sb.Append("                        (SELECT  ROUND(CNT1.CRN_AMMOUNT * ");
                sb.Append("                        " + ParentDB + ".GET_EX_RATE(CNT1.CURRENCY_MST_FK,");
                sb.Append(CurrPK + ",");
                sb.Append("                                          CNT.CREDIT_NOTE_DATE),");
                sb.Append("                        2) FROM " + ParentDB + ".CREDIT_NOTE_TBL CNT1 WHERE CNT1.CRN_TBL_PK =  CNT.CRN_TBL_PK AND NVL(CNT1.CRN_STATUS,0) = 2) DEBIT,");
                sb.Append("                        (SELECT  ROUND(CNT1.CRN_AMMOUNT * ");
                sb.Append("                        " + ParentDB + ".GET_EX_RATE(CNT1.CURRENCY_MST_FK,");
                sb.Append(CurrPK + ",");
                sb.Append("                                          CNT.CREDIT_NOTE_DATE),");
                sb.Append("                        2) FROM " + ParentDB + ".CREDIT_NOTE_TBL CNT1 WHERE CNT1.CRN_TBL_PK =  CNT.CRN_TBL_PK AND NVL(CNT1.CRN_STATUS,0) <> 2) CREDIT,");
                sb.Append("                        0 BALANCE,");
                sb.Append("                        '' OUTSTANDING_DAYS");
                sb.Append("          FROM " + ParentDB + ".CBJC_TBL CBJC,");
                sb.Append("               " + ParentDB + ".CUSTOMER_MST_TBL       CMT,");
                sb.Append("               " + ParentDB + ".CUSTOMER_CONTACT_DTLS  CCD,");
                sb.Append("               " + ParentDB + ".LOCATION_MST_TBL       LMT,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TBL     INV,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TRN_TBL INVTRN,");
                sb.Append("               " + ParentDB + ".CREDIT_NOTE_TBL        CNT,");
                sb.Append("               " + ParentDB + ".CREDIT_NOTE_TRN_TBL    CNTT");
                sb.Append("         WHERE INVTRN.JOB_CARD_FK = CBJC.CBJC_PK(+)");
                sb.Append("           AND INV.IS_FAC_INV <> 1");
                sb.Append("           AND CBJC.CUSTOMER_MST_FK= CMT.CUSTOMER_MST_PK");
                sb.Append("           AND CNT.PROCESS_TYPE = " + ProcessType);
                sb.Append("           AND INV.CONSOL_INVOICE_PK = INVTRN.CONSOL_INVOICE_FK");
                sb.Append("           AND CNT.CRN_TBL_PK = CNTT.CRN_TBL_FK");
                sb.Append("           AND CNTT.CONSOL_INVOICE_TRN_FK = INVTRN.CONSOL_INVOICE_TRN_PK");
                sb.Append("           AND CMT.CUSTOMER_MST_PK = CCD.CUSTOMER_MST_FK");
                sb.Append("           AND LMT.LOCATION_MST_PK = CCD.ADM_LOCATION_MST_FK");
                sb.Append("           AND NVL((SELECT SUM(WMT.WRITEOFF_AMOUNT)");
                sb.Append("                     FROM " + ParentDB + ".WRITEOFF_MANUAL_TBL WMT");
                sb.Append("                    WHERE WMT.CONSOL_INVOICE_FK = INV.CONSOL_INVOICE_PK),");
                sb.Append("                   0.00) = 0");
                if (Fromdate != "" & Todate != "")
                {
                    sb.Append("           AND  CNT.CREDIT_NOTE_DATE  BETWEEN TO_DATE('" + Fromdate + "') AND");
                    sb.Append("               TO_DATE('" + Todate + "')");
                }
                sb.Append("          AND CBJC.PROCESS_TYPE =" + ProcessType);
                if (BizType != 3)
                {
                    sb.Append("          AND CBJC.BIZ_TYPE =" + BizType);
                }

                if (CargoType != 0)
                {
                    sb.Append("     AND CBJC.CARGO_TYPE=" + CargoType);
                }

                if (!string.IsNullOrEmpty(CountryPK) & CountryPK != "0")
                {
                    sb.Append("       AND LMT.COUNTRY_MST_FK IN(" + CountryPK + ")");
                }

                if (!string.IsNullOrEmpty(LocPK) & LocPK != "0")
                {
                    sb.Append("        AND LMT.LOCATION_MST_PK IN (" + LocPK + ") ");
                }

                if (!string.IsNullOrEmpty(CustPK) & CustPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustPK + ")");
                }

                if (!string.IsNullOrEmpty(CustGroupPK) & CustGroupPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustGroupPK + ")");
                }

                if (BizType == 1)
                {
                    if (!string.IsNullOrEmpty(FlightID))
                    {
                        sb.Append("     AND   CBJC.FLIGHT_NO IN (" + FlightID + ")");
                    }
                }
                else if (BizType == 2)
                {
                    if (!string.IsNullOrEmpty(VslVoyPK))
                    {
                        sb.Append("     AND   CBJC.VOYAGE_TRN_FK=" + VslVoyPK + "");
                    }
                }
                sb.Append("           AND INVTRN.JOB_TYPE = 2");
                sb.Append("           AND INV.CHK_INVOICE<>2 ");
                // '           ---TPT CrNote------
                sb.Append("           UNION");
                sb.Append("           SELECT DISTINCT CMT.CUSTOMER_MST_PK,");
                sb.Append("                        CCD.ADM_LOCATION_MST_FK,");
                sb.Append("                        CNT.CREDIT_NOTE_DATE REF_DATE,");
                sb.Append("                        (CASE");
                sb.Append("                          WHEN CNT.PROCESS_TYPE = 1 THEN");
                sb.Append("                           'EXPORT'");
                sb.Append("                          ELSE");
                sb.Append("                           'IMPORT'");
                sb.Append("                        END) PROCESS,");
                sb.Append("                        'CREDITNOTE' TRANSACTION,");
                sb.Append("                        (SELECT CONT.BL_NUMBER FROM " + ParentDB + ".TRANSPORT_TRN_CONT CONT WHERE CONT.TRANSPORT_INST_FK = TIST.TRANSPORT_INST_SEA_PK AND ROWNUM=1) HBL_REF_NO,");
                sb.Append("                        CNT.CREDIT_NOTE_REF_NR DOCREFNR,");
                sb.Append("                        CNT.CRN_TBL_PK REF_PK,");
                sb.Append("                        CNT.BIZ_TYPE BIZ_TYPE,");
                sb.Append("                        TIST.CARGO_TYPE CARGO_TYPE,");
                sb.Append("                        (SELECT  ROUND(CNT1.CRN_AMMOUNT * ");
                sb.Append("                        " + ParentDB + ".GET_EX_RATE(CNT1.CURRENCY_MST_FK,");
                sb.Append(CurrPK + ",");
                sb.Append("                                          CNT.CREDIT_NOTE_DATE),");
                sb.Append("                        2) FROM " + ParentDB + ".CREDIT_NOTE_TBL CNT1 WHERE CNT1.CRN_TBL_PK =  CNT.CRN_TBL_PK AND NVL(CNT1.CRN_STATUS,0) = 2) DEBIT,");
                sb.Append("                        (SELECT  ROUND(CNT1.CRN_AMMOUNT * ");
                sb.Append("                        " + ParentDB + ".GET_EX_RATE(CNT1.CURRENCY_MST_FK,");
                sb.Append(CurrPK + ",");
                sb.Append("                                          CNT.CREDIT_NOTE_DATE),");
                sb.Append("                        2) FROM " + ParentDB + ".CREDIT_NOTE_TBL CNT1 WHERE CNT1.CRN_TBL_PK =  CNT.CRN_TBL_PK AND NVL(CNT1.CRN_STATUS,0) <> 2) CREDIT,");
                sb.Append("                        0 BALANCE,");
                sb.Append("                        '' OUTSTANDING_DAYS");
                sb.Append("          FROM " + ParentDB + ".TRANSPORT_INST_SEA_TBL TIST,");
                sb.Append("               " + ParentDB + ".CUSTOMER_MST_TBL       CMT,");
                sb.Append("               " + ParentDB + ".CUSTOMER_CONTACT_DTLS  CCD,");
                sb.Append("               " + ParentDB + ".LOCATION_MST_TBL       LMT,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TBL     INV,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TRN_TBL INVTRN,");
                sb.Append("               " + ParentDB + ".CREDIT_NOTE_TBL        CNT,");
                sb.Append("               " + ParentDB + ".CREDIT_NOTE_TRN_TBL    CNTT");
                sb.Append("         WHERE INVTRN.JOB_CARD_FK = TIST.TRANSPORT_INST_SEA_PK(+)");
                sb.Append("           AND INV.IS_FAC_INV <> 1");
                sb.Append("           AND INV.CUSTOMER_MST_FK= CMT.CUSTOMER_MST_PK");
                sb.Append("           AND CNT.PROCESS_TYPE = " + ProcessType);
                sb.Append("           AND INV.CONSOL_INVOICE_PK = INVTRN.CONSOL_INVOICE_FK");
                sb.Append("           AND CNT.CRN_TBL_PK = CNTT.CRN_TBL_FK");
                sb.Append("           AND CNTT.CONSOL_INVOICE_TRN_FK = INVTRN.CONSOL_INVOICE_TRN_PK");
                sb.Append("           AND CMT.CUSTOMER_MST_PK = CCD.CUSTOMER_MST_FK");
                sb.Append("           AND LMT.LOCATION_MST_PK = CCD.ADM_LOCATION_MST_FK");
                sb.Append("           AND NVL((SELECT SUM(WMT.WRITEOFF_AMOUNT)");
                sb.Append("                     FROM " + ParentDB + ".WRITEOFF_MANUAL_TBL WMT");
                sb.Append("                    WHERE WMT.CONSOL_INVOICE_FK = INV.CONSOL_INVOICE_PK),");
                sb.Append("                   0.00) = 0");
                if (Fromdate != "" & Todate != "")
                {
                    sb.Append("           AND CNT.CREDIT_NOTE_DATE BETWEEN TO_DATE('" + Fromdate + "') AND");
                    sb.Append("               TO_DATE('" + Todate + "')");
                }
                sb.Append("          AND TIST.PROCESS_TYPE =" + ProcessType);
                if (BizType != 3)
                {
                    sb.Append("          AND TIST.BUSINESS_TYPE =" + BizType);
                }

                if (CargoType != 0)
                {
                    sb.Append("     AND TIST.CARGO_TYPE=" + CargoType);
                }

                if (!string.IsNullOrEmpty(CountryPK) & CountryPK != "0")
                {
                    sb.Append("       AND LMT.COUNTRY_MST_FK IN(" + CountryPK + ")");
                }

                if (!string.IsNullOrEmpty(LocPK) & LocPK != "0")
                {
                    sb.Append("        AND LMT.LOCATION_MST_PK IN (" + LocPK + ") ");
                }

                if (!string.IsNullOrEmpty(CustPK) & CustPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustPK + ")");
                }

                if (!string.IsNullOrEmpty(CustGroupPK) & CustGroupPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustGroupPK + ")");
                }

                if (BizType == 1)
                {
                    if (!string.IsNullOrEmpty(FlightID))
                    {
                        sb.Append("     AND   TIST.FLIGHT_NO IN (" + FlightID + ")");
                    }
                }
                else if (BizType == 2)
                {
                    if (!string.IsNullOrEmpty(VslVoyPK))
                    {
                        sb.Append("     AND   TIST.VSL_VOY_FK=" + VslVoyPK + "");
                    }
                }
                sb.Append("           AND INVTRN.JOB_TYPE = 3");
                sb.Append("           AND INV.CHK_INVOICE<>2 ");
                // '         --- DET & DEM TPT (CR Note)
                sb.Append("           UNION");
                sb.Append("           SELECT DISTINCT CMT.CUSTOMER_MST_PK,");
                sb.Append("                        CCD.ADM_LOCATION_MST_FK,");
                sb.Append("                        CNT.CREDIT_NOTE_DATE REF_DATE,");
                sb.Append("                        (CASE");
                sb.Append("                          WHEN CNT.PROCESS_TYPE = 1 THEN");
                sb.Append("                           'EXPORT'");
                sb.Append("                          ELSE");
                sb.Append("                           'IMPORT'");
                sb.Append("                        END) PROCESS,");
                sb.Append("                        'CREDITNOTE' TRANSACTION,");
                sb.Append("                        (SELECT CONT.BL_NUMBER FROM " + ParentDB + ".TRANSPORT_TRN_CONT CONT WHERE CONT.TRANSPORT_INST_FK = TIST.TRANSPORT_INST_SEA_PK AND ROWNUM=1) HBL_REF_NO,");
                sb.Append("                        CNT.CREDIT_NOTE_REF_NR DOCREFNR,");
                sb.Append("                        CNT.CRN_TBL_PK REF_PK,");
                sb.Append("                        CNT.BIZ_TYPE BIZ_TYPE,");
                sb.Append("                        DCH.CARGO_TYPE CARGO_TYPE,");
                sb.Append("                        (SELECT  ROUND(CNT1.CRN_AMMOUNT * ");
                sb.Append("                        " + ParentDB + ".GET_EX_RATE(CNT1.CURRENCY_MST_FK,");
                sb.Append(CurrPK + ",");
                sb.Append("                                          CNT.CREDIT_NOTE_DATE),");
                sb.Append("                        2) FROM " + ParentDB + ".CREDIT_NOTE_TBL CNT1 WHERE CNT1.CRN_TBL_PK =  CNT.CRN_TBL_PK AND NVL(CNT1.CRN_STATUS,0) = 2) DEBIT,");
                sb.Append("                        (SELECT  ROUND(CNT1.CRN_AMMOUNT * ");
                sb.Append("                        " + ParentDB + ".GET_EX_RATE(CNT1.CURRENCY_MST_FK,");
                sb.Append(CurrPK + ",");
                sb.Append("                                          CNT.CREDIT_NOTE_DATE),");
                sb.Append("                        2) FROM " + ParentDB + ".CREDIT_NOTE_TBL CNT1 WHERE CNT1.CRN_TBL_PK =  CNT.CRN_TBL_PK AND NVL(CNT1.CRN_STATUS,0) <> 2) CREDIT,");
                sb.Append("                        0 BALANCE,");
                sb.Append("                        '' OUTSTANDING_DAYS");
                sb.Append("          FROM " + ParentDB + ".DEM_CALC_HDR DCH  ,");
                sb.Append("               " + ParentDB + ".TRANSPORT_INST_SEA_TBL TIST,");
                sb.Append("               " + ParentDB + ".CUSTOMER_MST_TBL       CMT,");
                sb.Append("               " + ParentDB + ".CUSTOMER_CONTACT_DTLS  CCD,");
                sb.Append("               " + ParentDB + ".LOCATION_MST_TBL       LMT,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TBL     INV,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TRN_TBL INVTRN,");
                sb.Append("               " + ParentDB + ".CREDIT_NOTE_TBL        CNT,");
                sb.Append("               " + ParentDB + ".CREDIT_NOTE_TRN_TBL    CNTT");
                sb.Append("         WHERE INVTRN.JOB_CARD_FK = DCH.DEM_CALC_HDR_PK(+)");
                sb.Append("           AND DCH.DOC_REF_FK = TIST.TRANSPORT_INST_SEA_PK");
                sb.Append("           AND INV.IS_FAC_INV <> 1");
                sb.Append("           AND INV.CUSTOMER_MST_FK= CMT.CUSTOMER_MST_PK");
                sb.Append("           AND CNT.PROCESS_TYPE = " + ProcessType);
                sb.Append("           AND INV.CONSOL_INVOICE_PK = INVTRN.CONSOL_INVOICE_FK");
                sb.Append("           AND CNT.CRN_TBL_PK = CNTT.CRN_TBL_FK");
                sb.Append("           AND CNTT.CONSOL_INVOICE_TRN_FK = INVTRN.CONSOL_INVOICE_TRN_PK");
                sb.Append("           AND CMT.CUSTOMER_MST_PK = CCD.CUSTOMER_MST_FK");
                sb.Append("           AND LMT.LOCATION_MST_PK = CCD.ADM_LOCATION_MST_FK");
                sb.Append("           AND NVL((SELECT SUM(WMT.WRITEOFF_AMOUNT)");
                sb.Append("                     FROM " + ParentDB + ".WRITEOFF_MANUAL_TBL WMT");
                sb.Append("                    WHERE WMT.CONSOL_INVOICE_FK = INV.CONSOL_INVOICE_PK),");
                sb.Append("                   0.00) = 0");
                if (BizType == 1)
                {
                    if (!string.IsNullOrEmpty(FlightID))
                    {
                        sb.Append("     AND   TIST.FLIGHT_NO IN (" + FlightID + ")");
                    }
                }
                else if (BizType == 2)
                {
                    if (!string.IsNullOrEmpty(VslVoyPK))
                    {
                        sb.Append("     AND   TIST.VSL_VOY_FK=" + VslVoyPK + "");
                    }
                }
                if (CargoType != 0)
                {
                    sb.Append("     AND DCH.CARGO_TYPE=" + CargoType);
                }

                if (Fromdate != "" & Todate != "")
                {
                    sb.Append("       AND CNT.CREDIT_NOTE_DATE BETWEEN TO_DATE('" + Fromdate + "') AND");
                    sb.Append("               TO_DATE('" + Todate + "')");
                }
                if (!string.IsNullOrEmpty(CountryPK) & CountryPK != "0")
                {
                    sb.Append("       AND LMT.COUNTRY_MST_FK IN(" + CountryPK + ")");
                }

                if (!string.IsNullOrEmpty(LocPK) & LocPK != "0")
                {
                    sb.Append("        AND LMT.LOCATION_MST_PK IN (" + LocPK + ") ");
                }

                if (!string.IsNullOrEmpty(CustPK) & CustPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustPK + ")");
                }

                if (!string.IsNullOrEmpty(CustGroupPK) & CustGroupPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustGroupPK + ")");
                }

                sb.Append("           AND INVTRN.JOB_TYPE = 3");
                sb.Append("          AND DCH.PROCESS_TYPE =" + ProcessType);
                if (BizType != 3)
                {
                    sb.Append("          AND DCH.BIZ_TYPE =" + BizType);
                }

                sb.Append("           AND INV.CHK_INVOICE<>2 ");
                // '         --- DET & DEM CBJC(CR Note)
                sb.Append("           UNION");
                sb.Append("           SELECT DISTINCT CMT.CUSTOMER_MST_PK,");
                sb.Append("                        CCD.ADM_LOCATION_MST_FK,");
                sb.Append("                        CNT.CREDIT_NOTE_DATE REF_DATE,");
                sb.Append("                        (CASE");
                sb.Append("                          WHEN CNT.PROCESS_TYPE = 1 THEN");
                sb.Append("                           'EXPORT'");
                sb.Append("                          ELSE");
                sb.Append("                           'IMPORT'");
                sb.Append("                        END) PROCESS,");
                sb.Append("                        'CREDITNOTE' TRANSACTION,");
                sb.Append("                        CBJC.HBL_NO HBL_REF_NO,");
                sb.Append("                        CNT.CREDIT_NOTE_REF_NR DOCREFNR,");
                sb.Append("                        CNT.CRN_TBL_PK REF_PK,");
                sb.Append("                        CNT.BIZ_TYPE BIZ_TYPE,");
                sb.Append("                        DCH.CARGO_TYPE CARGO_TYPE,");
                sb.Append("                        (SELECT  ROUND(CNT1.CRN_AMMOUNT * ");
                sb.Append("                        " + ParentDB + ".GET_EX_RATE(CNT1.CURRENCY_MST_FK,");
                sb.Append(CurrPK + ",");
                sb.Append("                                          CNT.CREDIT_NOTE_DATE),");
                sb.Append("                        2) FROM " + ParentDB + ".CREDIT_NOTE_TBL CNT1 WHERE CNT1.CRN_TBL_PK =  CNT.CRN_TBL_PK AND NVL(CNT1.CRN_STATUS,0) = 2) DEBIT,");
                sb.Append("                        (SELECT  ROUND(CNT1.CRN_AMMOUNT * ");
                sb.Append("                        " + ParentDB + ".GET_EX_RATE(CNT1.CURRENCY_MST_FK,");
                sb.Append(CurrPK + ",");
                sb.Append("                                          CNT.CREDIT_NOTE_DATE),");
                sb.Append("                        2) FROM " + ParentDB + ".CREDIT_NOTE_TBL CNT1 WHERE CNT1.CRN_TBL_PK =  CNT.CRN_TBL_PK AND NVL(CNT1.CRN_STATUS,0) <> 2) CREDIT,");
                sb.Append("                        0 BALANCE,");
                sb.Append("                        '' OUTSTANDING_DAYS");
                sb.Append("          FROM " + ParentDB + ".DEM_CALC_HDR DCH  ,");
                sb.Append("               " + ParentDB + ".CBJC_TBL               CBJC,");
                sb.Append("               " + ParentDB + ".CUSTOMER_MST_TBL       CMT,");
                sb.Append("               " + ParentDB + ".CUSTOMER_CONTACT_DTLS  CCD,");
                sb.Append("               " + ParentDB + ".LOCATION_MST_TBL       LMT,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TBL     INV,");
                sb.Append("               " + ParentDB + ".CONSOL_INVOICE_TRN_TBL INVTRN,");
                sb.Append("               " + ParentDB + ".CREDIT_NOTE_TBL        CNT,");
                sb.Append("               " + ParentDB + ".CREDIT_NOTE_TRN_TBL    CNTT");
                sb.Append("         WHERE INVTRN.JOB_CARD_FK = DCH.DEM_CALC_HDR_PK(+)");
                sb.Append("           AND DCH.DOC_REF_FK = CBJC.CBJC_PK");
                sb.Append("           AND INV.IS_FAC_INV <> 1");
                sb.Append("           AND INV.CUSTOMER_MST_FK= CMT.CUSTOMER_MST_PK");
                sb.Append("           AND CNT.PROCESS_TYPE = " + ProcessType);
                sb.Append("           AND INV.CONSOL_INVOICE_PK = INVTRN.CONSOL_INVOICE_FK");
                sb.Append("           AND CNT.CRN_TBL_PK = CNTT.CRN_TBL_FK");
                sb.Append("           AND CNTT.CONSOL_INVOICE_TRN_FK = INVTRN.CONSOL_INVOICE_TRN_PK");
                sb.Append("           AND CMT.CUSTOMER_MST_PK = CCD.CUSTOMER_MST_FK");
                sb.Append("           AND LMT.LOCATION_MST_PK = CCD.ADM_LOCATION_MST_FK");
                sb.Append("           AND NVL((SELECT SUM(WMT.WRITEOFF_AMOUNT)");
                sb.Append("                     FROM " + ParentDB + ".WRITEOFF_MANUAL_TBL WMT");
                sb.Append("                    WHERE WMT.CONSOL_INVOICE_FK = INV.CONSOL_INVOICE_PK),");
                sb.Append("                   0.00) = 0");
                if (BizType == 1)
                {
                    if (!string.IsNullOrEmpty(FlightID))
                    {
                        sb.Append("         AND   CBJC.FLIGHT_NO IN (" + FlightID + ")");
                    }
                }
                else if (BizType == 2)
                {
                    if (!string.IsNullOrEmpty(VslVoyPK))
                    {
                        sb.Append("         AND   CBJC.VOYAGE_TRN_FK=" + VslVoyPK + "");
                    }
                }
                if (CargoType != 0)
                {
                    sb.Append("     AND DCH.CARGO_TYPE=" + CargoType);
                }

                if (Fromdate != "" & Todate != "")
                {
                    sb.Append("       AND CNT.CREDIT_NOTE_DATE BETWEEN TO_DATE('" + Fromdate + "') AND");
                    sb.Append("               TO_DATE('" + Todate + "')");
                }
                if (!string.IsNullOrEmpty(CountryPK) & CountryPK != "0")
                {
                    sb.Append("       AND LMT.COUNTRY_MST_FK IN(" + CountryPK + ")");
                }

                if (!string.IsNullOrEmpty(LocPK) & LocPK != "0")
                {
                    sb.Append("        AND LMT.LOCATION_MST_PK IN (" + LocPK + ") ");
                }

                if (!string.IsNullOrEmpty(CustPK) & CustPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustPK + ")");
                }

                if (!string.IsNullOrEmpty(CustGroupPK) & CustGroupPK != "0")
                {
                    sb.Append("        AND CMT.CUSTOMER_MST_PK IN(" + CustGroupPK + ")");
                }

                sb.Append("           AND INV.CHK_INVOICE<>2 ");
                sb.Append("           AND INVTRN.JOB_TYPE = 4");
                sb.Append("          AND DCH.PROCESS_TYPE =" + ProcessType);
                if (BizType != 3)
                {
                    sb.Append("          AND DCH.BIZ_TYPE =" + BizType);
                }
                // ----- End----
                sb.Append("    ) ORDER BY to_date(REF_DATE)) ");

                List<CustomerSOAList> rec3 = entities.Database.SqlQuery<CustomerSOAList>(sb.ToString(), new object[0]).ToList();
                customerSOA.CustomerSOALists = new List<CustomerSOAList>();
                customerSOA.CustomerSOALists = rec3;
                customerSOA.CustomerSOALists.ForEach(a => a.REFDATE = a.REF_DATE.ToString("dd/MM/yyyy"));
                if (customerSOA.CustomerSOALists.Count > 0)
                {
                    if (customerSOA.CustomerSOALists.Count > 1)
                    {
                        for (int m = 0; m <= customerSOA.CustomerSOALists.Count - 2; m++)
                        {
                            if (customerSOA.CustomerSOALists[m].BALANCE == null)
                            {
                                customerSOA.CustomerSOALists[m].BALANCE = 0;
                            }
                            if (customerSOA.CustomerSOALists[m].BALANCE == 0)
                            {
                                customerSOA.CustomerSOALists[m].BALANCE = (customerSOA.CustomerSOALists[m].BALANCE + customerSOA.CustomerSOALists[m].DEBIT) - customerSOA.CustomerSOALists[m].CREDIT;
                            }

                            customerSOA.CustomerSOALists[m + 1].BALANCE = (customerSOA.CustomerSOALists[m].BALANCE + customerSOA.CustomerSOALists[m + 1].DEBIT) - customerSOA.CustomerSOALists[m + 1].CREDIT;
                        }
                    }
                    else
                    {
                        for (int m = 0; m <= customerSOA.CustomerSOALists.Count - 1; m++)
                        {
                            if (customerSOA.CustomerSOALists[m].BALANCE == 0)
                            {
                                customerSOA.CustomerSOALists[m].BALANCE = (customerSOA.CustomerSOALists[m].BALANCE + customerSOA.CustomerSOALists[m].DEBIT) - customerSOA.CustomerSOALists[m].CREDIT;
                            }
                        }
                    }
                    customerSOA.Overall_Outstanding = customerSOA.CustomerSOALists[customerSOA.CustomerSOALists.Count - 1].BALANCE;
                }
                return customerSOA;


            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}