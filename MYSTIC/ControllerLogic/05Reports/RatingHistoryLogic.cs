﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MYSTIC.Data;
using MYSTIC.Models;

namespace MYSTIC.ControllerLogic
{
    public class RatingHistoryLogic:CommonLogic
    {
        Entities entities = new Entities();

        public RatingHistory FetchRatingHistory(RatingHistory ratingHistory)
        {
            try
            {
                ratingHistory.RptRateHistory = entities.SP_RPT_RATING_HISTORY(ratingHistory.User_PK,ratingHistory.BusinessType,ratingHistory.CargoType,ratingHistory.Document,
                    ratingHistory.Customer,ratingHistory.Location_PK,ratingHistory.Currency_PK,ratingHistory.FromDate.ToString("dd/MM/yyyy"), ratingHistory.ToDate.ToString("dd/MM/yyyy"),
                    ratingHistory.POL_AOL_PK,ratingHistory.POD_AOD_PK).ToList();              
            }
            catch(Exception ex)
            {
                SaveErrorLog("RatingHistory", "FetchRatingHistory", ex.Message, ex.StackTrace);
            }
            return ratingHistory;
        }
    }
}