﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MYSTIC.Data;
using MYSTIC.Models;

namespace MYSTIC.ControllerLogic
{
    public class OutstandingReportLogic:CommonLogic
    {
        Entities entities = new Entities();

        public OutstandingReport FetchOutstandingReport(OutstandingReport outstandingReport)
        {
            try
            {
                List<string> FF = Get_FreightForwarder_List(outstandingReport.UserPK);
                foreach (string Forwarders in FF)
                {
                    string dbName = Get_FreightForwarder_DataBase(outstandingReport.UserPK, Forwarders);
                    string frDate=null;string todte=null;
                    if(outstandingReport.FromDate!=new DateTime())
                    {
                        frDate = outstandingReport.FromDate.ToString("dd/MM/yyyy");
                    }
                    if (outstandingReport.ToDate != new DateTime())
                    {
                        todte = outstandingReport.ToDate.ToString("dd/MM/yyyy");
                    }
                    outstandingReport.OutstandingResult = entities.SP_RPT_OUTSTANDING(dbName, frDate, todte, outstandingReport.Customer, outstandingReport.Location, outstandingReport.Sector, outstandingReport.Vessel,
                        outstandingReport.BusinessType, outstandingReport.ProcessType, outstandingReport.JobType, outstandingReport.BaseCurrency).ToList();
                }
            }
            catch(Exception ex)
            {
                SaveErrorLog("OutstandingReport", "FetchOutstandingReport", ex.Message, ex.StackTrace);
            }
            return outstandingReport;
        }
    }
}