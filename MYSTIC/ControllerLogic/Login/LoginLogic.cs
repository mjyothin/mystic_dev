﻿using MYSTIC.Data;
using MYSTIC.Models;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;

namespace MYSTIC.ControllerLogic
{
    public class LoginLogic : CommonLogic
    {
        private Entities entities = new Entities();
        public LoginModel IsUserValid(string UserID, string Password)
        {
            // If "0" is Returned then the User is not ther
            // If "1" is Returned then the Password is Wrong
            // IF "2" is Returned then the User is Not Active
            // If "3" is Returned then the User Do Not have the Menu Rights for the Location
            // If a String is Returned then success
            LoginModel login = new LoginModel();
            try
            {
                M_USER_MST_TBL Data = entities.M_USER_MST_TBL.Where(a => a.USER_ID.ToLower() == UserID.ToLower()).FirstOrDefault();
                //If user doesn't exist
                if (Data == null)
                {
                    login.SuccessMessage = "Invalid User";
                }
                else
                {
                    if ((Password.Length < 7))
                    {
                        login.SuccessMessage = "Incorrect Password";
                    }

                    #region password check
                    ObjectParameter PW_OUT = new ObjectParameter("PW_OUT", typeof(string));
                    var rec = entities.DECODER(Data.PASS_WORD, PW_OUT);
                    string password = PW_OUT.Value.ToString();
                    if (password != Password)
                    {
                        if (Data.WRONG_PWD_COUNT > 5)
                        {
                            login.SuccessMessage = "User ID is locked";
                        }
                        else
                        {
                            entities.M_USER_MST_TBL.Where(a => a.USER_ID == Data.USER_ID).FirstOrDefault().WRONG_PWD_COUNT = Data.WRONG_PWD_COUNT + 1;
                            entities.SaveChanges();
                            login.SuccessMessage = "Incorrect Password";
                        }
                    }
                    else
                    {
                        entities.M_USER_MST_TBL.Where(a => a.USER_ID == Data.USER_ID).FirstOrDefault().WRONG_PWD_COUNT = 0;
                        entities.SaveChanges();
                    }
                    #endregion
                    #region Check User is active
                    List<M_USER_CORPORATE_TBL> Status = entities.M_USER_CORPORATE_TBL.Where(a => a.M_USER_MST_FK == Data.M_USER_MST_PK && a.APPROVED_STATUS == 1).ToList();
                    if (Status.Count == 0)
                    {
                        login.SuccessMessage = "User has not been approved by any FF";
                    }
                    #endregion

                    if (login.SuccessMessage == null)
                    {
                        login.UserDetails = new UserMaster() { M_USER_MST_PK = Data.M_USER_MST_PK,
                            M_CORPORATE_MST_FK = Data.M_CORPORATE_MST_FK,
                            USER_ID = Data.USER_ID,
                            PASS_WORD = Data.PASS_WORD,
                            PASS_WORD_CHANGE_DT = Data.PASS_WORD_CHANGE_DT,
                            USER_NAME = Data.USER_NAME,
                            FIRST_NAME = Data.FIRST_NAME,
                            LAST_NAME = Data.LAST_NAME,
                            USER_TYPE = Data.USER_TYPE,
                            USER_ADDRESS1 = Data.USER_ADDRESS1,
                            USER_ADDRESS2 = Data.USER_ADDRESS2,
                            USER_ADDRESS3 = Data.USER_ADDRESS3,
                            CITY = Data.CITY,
                            ZIP = Data.ZIP,
                            PHONE_NO = Data.PHONE_NO,
                            MOBILE_NO= Data.MOBILE_NO};
                        if (login.UserDetails.USER_TYPE == 1)
                        {
                            login.UserCorporateDetails = FetchUserCorpDetails(login.UserDetails.M_USER_MST_PK);
                            //#region Save Activity log tbl
                           
                            //    ACTIVITY_LOG_TRN_TBL activityLog = entities.ACTIVITY_LOG_TRN_TBL.Where(a => a.USER_FK == login.UserDetails.M_USER_MST_PK && a.FF_FK == login.UserDetails.M_CORPORATE_MST_FK && a.ACTIVITY_LOG_FK == 1).FirstOrDefault();
                            //    if (activityLog == null)
                            //    {
                            //        activityLog = new ACTIVITY_LOG_TRN_TBL();
                            //        int pk = entities.Database.SqlQuery<int>("Select SEQ_ACTIVITY_LOG_TRN_TBL.Nextval from dual", new object[0]).FirstOrDefault();
                            //        activityLog.ACTIVITY_LOG_TRN_PK = pk;
                            //        activityLog.ACTIVITY_LOG_FK = 1;
                            //        activityLog.ACTIVITY_LOG_DATE = DateTime.Now;
                            //        activityLog.FF_FK = 1;
                            //        activityLog.LOG_VALUE = 1;
                            //        activityLog.RATING_STATUS = 0;
                            //        activityLog.USER_FK = login.UserDetails.M_USER_MST_PK;
                            //        entities.ACTIVITY_LOG_TRN_TBL.Add(activityLog);
                            //    }
                            //    else
                            //    {
                            //        activityLog.ACTIVITY_LOG_DATE = DateTime.Now;
                            //        activityLog.LOG_VALUE = activityLog.LOG_VALUE + 1;
                            //    }
                            //entities.SaveChanges();
                            //#endregion
                        }
                        else if (login.UserDetails.USER_TYPE == 2)
                        {
                            DataSet ds = new DataSet();
                            login.UserCustomerDetails = FetchUserCustDetails(login.UserDetails.M_USER_MST_PK);
                            login.UserFFDetails = entities.M_USER_CORPORATE_TBL.Where(a => a.M_USER_MST_FK == login.UserDetails.M_USER_MST_PK).Select(a =>new FreightForwarder { FREIGHT_FORWARDER = a.CORPORATE_NAME, FREIGHT_FORWARDER_PK = a.M_CORPORATE_MST_FK }).ToList();
                            FeedbackLogic feedbackLogic = new FeedbackLogic();
                            BookingFeedback bookingFeedback = feedbackLogic.BookingFeedbackRequired(login.UserDetails.M_USER_MST_PK);
                            if (!string.IsNullOrEmpty(bookingFeedback.BookingNumber))
                            {
                                login.BookingFeedbackRequired = true;
                            }
                            FreightForwarder FFFeedback = feedbackLogic.IsFeedBackRequiredForFF(login.UserDetails.M_USER_MST_PK);
                            if(!string.IsNullOrEmpty(FFFeedback.FREIGHT_FORWARDER))
                            {
                                login.FFFeedbackRequired = true;
                            }
                            //#region Save Activity log tbl
                            //foreach (string ff in login.UserFFDetails)
                            //{
                            //    int FF_PK = entities.M_CORPORATE_MST_TBL.Where(a => a.CORPORATE_ID == ff).FirstOrDefault().M_CORPORATE_MST_PK;
                            //    ACTIVITY_LOG_TRN_TBL activityLog = entities.ACTIVITY_LOG_TRN_TBL.Where(a => a.USER_FK == login.UserDetails.M_USER_MST_PK && a.FF_FK ==FF_PK && a.ACTIVITY_LOG_FK == 1).FirstOrDefault();
                            //    if (activityLog == null)
                            //    {
                            //        activityLog = new ACTIVITY_LOG_TRN_TBL();
                            //        int pk = entities.Database.SqlQuery<int>("Select SEQ_ACTIVITY_LOG_TRN_TBL.Nextval from dual", new object[0]).FirstOrDefault();
                            //        activityLog.ACTIVITY_LOG_TRN_PK = pk;
                            //        activityLog.ACTIVITY_LOG_FK = 1;
                            //        activityLog.ACTIVITY_LOG_DATE = DateTime.Now;
                            //        activityLog.FF_FK = 1;
                            //        activityLog.LOG_VALUE = 1;
                            //        activityLog.RATING_STATUS = 0;
                            //        activityLog.USER_FK = login.UserDetails.M_USER_MST_PK;
                            //        entities.ACTIVITY_LOG_TRN_TBL.Add(activityLog);
                            //    }
                            //    else
                            //    {
                            //        activityLog.ACTIVITY_LOG_DATE = DateTime.Now;
                            //        activityLog.LOG_VALUE = activityLog.LOG_VALUE + 1;
                            //    }
                            //    entities.SaveChanges();
                            //}
                            //#endregion
                        }

                    }

                }
                return login;
            }
            catch (Exception ex)
            {
                SaveErrorLog("Login", "IsUserValid", ex.Message, ex.StackTrace);
                login.SuccessMessage = "An error occurred.Please try again later";
                return login;
            }
        }
        public bool CheckPasswordExpiry(string pUserId, ref string errMessage)
        {
            bool CheckPasswordExpiry = false;
            DateTime passChangedDate;
            int NoOfDays;
            int expday;
            try
            {
                if (pUserId.ToUpper() == "ADMIN")
                {
                    CheckPasswordExpiry = true;
                    return CheckPasswordExpiry;
                }
                passChangedDate = ChkpasswordChangedDate(pUserId);
                expday = GetPasswordExpiry(pUserId);
                NoOfDays = (System.DateTime.Now - passChangedDate).Days;

                if (expday > 0)
                {
                    if ((NoOfDays <= expday))
                    {
                        errMessage = "Your password will expire in " + (expday - NoOfDays) + " days";
                        return true;
                    }
                    else if (NoOfDays >= expday)
                    {
                        errMessage = "Your password has expired";
                        return false;
                    }
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                errMessage = ex.Message;
            }
            return false;
        }

        public System.DateTime ChkpasswordChangedDate(string pUserID)
        {
            System.DateTime passwordChangedDate = DateTime.MinValue;

            try
            {
                M_USER_MST_TBL UserDetails = (from UPT in entities.M_USER_MST_TBL
                                              where UPT.USER_NAME == pUserID
                                              select UPT).FirstOrDefault();

                if (UserDetails.PASS_WORD_CHANGE_DT != null)
                {
                    passwordChangedDate = Convert.ToDateTime(UserDetails.PASS_WORD_CHANGE_DT);
                }
                else
                {
                    passwordChangedDate = Convert.ToDateTime(UserDetails.CREATED_DT);
                }
                return new DateTime();
            }
            catch
            {
            }
            return passwordChangedDate;
        }

        public int GetPasswordExpiry(string pUserID)
        {
            int expDay = 0;
            try
            {
                string data = (from UPT in entities.M_USER_MST_TBL
                               where UPT.USER_NAME == pUserID
                               select UPT.PASS_WORD_SEC).FirstOrDefault();
                if (data != null)
                {
                    expDay = Convert.ToInt32(data);
                }
                return expDay;
            }
            catch
            {
                throw;
            }
        }
        public string RegisterNewUser(RegisterUser registerUser)
        {
            string Message = null;
            try
            {
                M_USER_MST_TBL UserMaster = registerUser.UserDetails;
                #region Validations
                M_USER_MST_TBL IsExists = entities.M_USER_MST_TBL.Where(a => a.USER_ID == UserMaster.USER_ID).FirstOrDefault();
                if(IsExists!=null)
                {
                    return Message = "UserId already exists";
                }
                #endregion
                #region Add Default values for usermaster
                UserMaster.CREATED_DT = DateTime.Now;
                UserMaster.PASS_WORD_CHANGE_DT = DateTime.Now;
                UserMaster.CH_DT = DateTime.Now;
                #endregion
                #region Encode password
                ObjectParameter parameters = new ObjectParameter("PW_OUT", UserMaster.PASS_WORD);
                int password = entities.ENCODER(UserMaster.PASS_WORD, parameters);
                UserMaster.PASS_WORD = parameters.Value.ToString();
                #endregion

                #region Add default values to userPreference Table
                M_USER_PREFERENCE_TBL uSER_PREFERENCE_TBL = new M_USER_PREFERENCE_TBL
                {
                    ENVIRONMENT_FK = 1,
                    USER_FONT_SIZE = 1,
                    USER_DATE_FORMAT = 1,
                    USER_NUMBER_FORMAT = 1,
                    USER_STYLE_SHEET = 1
                };
                #endregion
               
                int pk = entities.Database.SqlQuery<int>("Select SEQ_M_USER_MST_TBL.Nextval from dual", new object[0]).FirstOrDefault();
                int Preferencepk = entities.Database.SqlQuery<int>("Select SEQ_M_USER_PREFERENCE_TBL.Nextval from dual", new object[0]).FirstOrDefault();
                uSER_PREFERENCE_TBL.USER_PREFERENCES_PK = Convert.ToInt32(Preferencepk);
                UserMaster.M_USER_MST_PK = Convert.ToInt32(pk);
                uSER_PREFERENCE_TBL.M_USER_MST_FK = UserMaster.M_USER_MST_PK;
                #region Add default values to User Corporate Table
                foreach (int FF in registerUser.FreightForwarderPK)
                {
                    M_CORPORATE_MST_TBL corporate = entities.M_CORPORATE_MST_TBL.Where(a => a.M_CORPORATE_MST_PK == FF).FirstOrDefault();
                    int corp_pk = entities.Database.SqlQuery<int>("Select SEQ_M_USER_CORP_TBL.Nextval from dual", new object[0]).FirstOrDefault();
                    M_USER_CORPORATE_TBL UserCorporate = new M_USER_CORPORATE_TBL
                    {
                        CREATED_DT = DateTime.Now,
                        CORPORATE_NAME = corporate.CORPORATE_ID,
                        M_CORPORATE_MST_TBL = corporate,
                        CUSTOMER = registerUser.UserDetails.USER_NAME,
                        DATABASE_NAME = corporate.DATABASE_NAME,
                        M_CORPORATE_MST_FK = corporate.M_CORPORATE_MST_PK,
                        USER_TYPE = 3,
                        M_USER_MST_FK = UserMaster.M_USER_MST_PK,
                        USER_CORPORATE_MST_PK = corp_pk
                    };
                    entities.M_USER_CORPORATE_TBL.Add(UserCorporate);
                }
                #endregion
                entities.M_USER_MST_TBL.Add(UserMaster);
                entities.M_USER_PREFERENCE_TBL.Add(uSER_PREFERENCE_TBL);
                int Save = entities.SaveChanges();
                if (Save > 0)
                {
                    foreach (int FF in registerUser.FreightForwarderPK)
                    {
                        M_CORPORATE_MST_TBL corporate = entities.M_CORPORATE_MST_TBL.Where(a => a.M_CORPORATE_MST_PK == FF).FirstOrDefault();
                        ObjectParameter ret = new ObjectParameter("RETURN_VALUE", 1);
                        string custID = null;
                        if (UserMaster.USER_NAME.Length > 4)
                        {
                            custID = UserMaster.USER_NAME.Substring(1, 4).ToUpper();
                        }
                        else
                        {
                            custID = UserMaster.USER_NAME.ToUpper();
                        }
                        int ERPSave = entities.SP_TEMP_CUSTOMER_TBL_INS(corporate.DATABASE_NAME,custID, UserMaster.USER_NAME, UserMaster.MOBILE_NO, UserMaster.PHONE_NO,
                            UserMaster.USER_ID, ret);
                    }
                        return Message="Data Saved Successfully";
                }
                else
                {
                    return Message="Error in saving Data";
                }
            }
            catch (Exception ex)
            {
                SaveErrorLog("Login", "RegisterUser", ex.Message, ex.StackTrace);
                return Message="Error in saving data";
            }
        }
        public FETCH_USER_CORP_DTL_Result FetchUserCorpDetails(int UserPK)
        {
            FETCH_USER_CORP_DTL_Result record = new FETCH_USER_CORP_DTL_Result();
            try
            {
                record = entities.FETCH_USER_CORP_DTL(UserPK.ToString()).FirstOrDefault();
            }
            catch (Exception ex)
            {
                SaveErrorLog("Login", "FetchUserCorpDetails", ex.Message, ex.StackTrace);
            }
            return record;
        }
        public FETCH_USER_CUST_DTL_Result FetchUserCustDetails(int UserPK)
        {
            FETCH_USER_CUST_DTL_Result record = new FETCH_USER_CUST_DTL_Result();
            try
            {
                record = entities.FETCH_USER_CUST_DTL(UserPK).FirstOrDefault();
            }
            catch (Exception ex)
            {
                SaveErrorLog("Login", "FetchUserCustDetails", ex.Message, ex.StackTrace);
            }
            return record;
        }
       public List<FreightForwardersDetail> FetchAllFreightForwarders()
        {
            List<FreightForwardersDetail> FF =entities.M_CORPORATE_MST_TBL.Select(a=>new FreightForwardersDetail
            {
                M_CORPORATE_MST_PK=a.M_CORPORATE_MST_PK,CORPORATE_ID=a.CORPORATE_ID,CORPORATE_NAME=a.CORPORATE_NAME,COUNTRY_MST_FK=a.COUNTRY_MST_FK,COUNTRY=a.COUNTRY,
                CONTACT_PERSON=a.CONTACT_PERSON,HOME_PAGE=a.HOME_PAGE,PHONE=a.PHONE,EMAIL=a.EMAIL
            }).ToList();
            return FF;
        }
        public List<SP_FETCH_CUSTOMERS_Result> FetchAllCustomers(List<string> CorporateName)
        {
            List<string> dbName = entities.M_CORPORATE_MST_TBL.Where(a =>CorporateName.Contains(a.CORPORATE_ID)).Select(a => a.DATABASE_NAME).ToList();
            string db= string.Join(",", dbName.ToArray());
            List <SP_FETCH_CUSTOMERS_Result> Customers = entities.SP_FETCH_CUSTOMERS(db).ToList();
            return Customers;
        }

    }
}