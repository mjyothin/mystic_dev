﻿using MYSTIC.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

namespace MYSTIC.ControllerLogic
{
    public class FCMPushNotificationLogic
    {
        public FCMPushNotification SendNotification(string deviceID,string _title, string _message, string _topic)
        {
            FCMPushNotification result = new FCMPushNotification();
            string FCM_SERVER_API_KEY = "AAAAmC7tzR8:APA91bHU_y7KYLgtm0d6pG0ofFVeY5F9PIxxN_zL1Rmz7k_AGdeayY5gEa9AYJexgjMwgDl7lGxtK2DhK6NHtRWJDCnKCxpArZFaXPVfmyWX39-fIuv9VZ0xVYGaEu9VGrCMqECQY6Zr";
            string FCM_SENDER_ID = "653622365471";
          //  string deviceID = "ejYosXU6gRc:APA91bEsWaQdOar1FxRRJQWDhx7bWR-Xs3atR0RNKVBn_5vlXD76c9BxKghNCNtJyXPWLdj5MGGeSKPXCUTbObFT7gKvT0SxlBpqFwBXttYZHYfChH0OU9s0NEYMfHv5_qhAMIa6sY5P";
            try
            {
                result.Successful = true;
                result.Error = null;
                // var value = message;
                var requestUri = "https://fcm.googleapis.com/fcm/send";

                WebRequest webRequest = WebRequest.Create(requestUri);
                webRequest.Method = "POST";
                webRequest.Headers.Add(string.Format("Authorization: key={0}", FCM_SERVER_API_KEY));
                webRequest.Headers.Add(string.Format("Sender: id={0}", FCM_SENDER_ID));
                webRequest.ContentType = "application/json";

                var data = new
                {
                     to = deviceID, //  this is for single device
                   // to = "/topics/" + _topic, // this is for topic 
                    notification = new
                    {
                        title = _title,
                        body = _message,
                        //icon="myicon"
                    }
                };

                var serializer = new JavaScriptSerializer();
                var json = serializer.Serialize(data);

                Byte[] byteArray = Encoding.UTF8.GetBytes(json);

                webRequest.ContentLength = byteArray.Length;
                using (Stream dataStream = webRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);

                    using (WebResponse webResponse = webRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = webResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                result.Response = sResponseFromServer;
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                result.Successful = false;
                result.Response = null;
                result.Error = ex;
            }
            return result;
        }
    }

}