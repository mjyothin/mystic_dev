﻿using MYSTIC.Data;
using MYSTIC.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace MYSTIC.ControllerLogic
{
    public class CommonLogic
    {
        private Entities entities = new Entities();
        private int PK = 0;
        public void SaveErrorLog(string Controller, string Action, string Message, string StackTrace)
        {
            M_ERROR_LOG_TBL m_ERROR_LOG_TBL = new M_ERROR_LOG_TBL
            {
                FORM_NAME = Controller,
                EVENT_NAME = Action,
                ERROR_MSG = Message,
                STACK_TRACE = StackTrace,
                CREATED_BY_FK = 1,
                CREATED_BY_DT = DateTime.Now,
                M_CORPORATE_MST_FK = 1
            };
            int pk = entities.Database.SqlQuery<int>("Select SEQ_M_ERROR_LOG_TBL.Nextval from dual", new object[0]).FirstOrDefault();
            m_ERROR_LOG_TBL.ERROR_LOG_TBL_PK = Convert.ToInt32(pk);
            entities.M_ERROR_LOG_TBL.Add(m_ERROR_LOG_TBL);
            entities.Entry(m_ERROR_LOG_TBL).State = EntityState.Added;
            int save = entities.SaveChanges();
        }
        public CustomerDetails GetCustomerList(int UserPK)
        {
            CustomerDetails customer = new CustomerDetails();
            try
            {
                customer.CutomerList = entities.SP_GET_CUSTOMERS(UserPK).ToList();
            }
            catch (Exception ex)
            {
                SaveErrorLog("Common", "GetCustomerList", ex.Message, ex.StackTrace);
            }
            return customer;
        }
        public Basis GetBasisList()
        {
            Basis basis = new Basis();
            try
            {
                basis.Dimensions = entities.DIMENTION_UNIT_MST_TBL.Select(a => new Dimension {
                    DIMENTION_ID = a.DIMENTION_ID,
                    DIMENTION_TYPE=a.DIMENTION_TYPE,
                    DIMENTION_UNIT_MST_PK=a.DIMENTION_UNIT_MST_PK
                }).ToList();
            }
            catch (Exception ex)
            {
                SaveErrorLog("Common", "GetBasisList", ex.Message, ex.StackTrace);
            }
            return basis;
        }
        public int Get_Place_Mst_PK(string DB_Name, int MPK, string Value = null)
        {
            ObjectParameter PK_Value = new ObjectParameter("PK", 1);
            if (Value == null)
            {
                Value = entities.PLACE_MST_TBL.Where(a => a.PLACE_PK == MPK).Select(a=>a.PLACE_CODE).FirstOrDefault();
            }
            if (Value != null)
            {
                int record = entities.FETCH_FF_TABLE_PK(DB_Name, "PLACE_MST_TBL", "PLACE_CODE", Value, "PLACE_PK", PK_Value);
                PK = Convert.ToInt32(PK_Value.Value);
            }
            return PK;
        }
        public int Get_Port_Mst_PK(string DB_Name, int MPK, string Value = null)
        {
            int PK = 0;
            ObjectParameter PK_Value = new ObjectParameter("PK", 1);
            if (Value == null)
            {
                Value = entities.PORT_MST_TBL.Where(a => a.PORT_MST_PK == MPK).Select(a=>a.PORT_ID).FirstOrDefault();
            }
            if (Value != null)
            {
                int record = entities.FETCH_FF_TABLE_PK(DB_Name, "PORT_MST_TBL", "PORT_ID", Value, "PORT_MST_PK", PK_Value);
                PK = Convert.ToInt32(PK_Value.Value);
            }
            return PK;
        }
        public int Get_Commodity_Mst_PK(string DB_Name, int MPK, string Value = null)
        {
            int PK = 0;
            ObjectParameter PK_Value = new ObjectParameter("PK", 1);
            if (Value == null)
            {
                Value = entities.COMMODITY_MST_TBL.Where(a => a.COMMODITY_MST_PK == MPK).Select(a=>a.COMMODITY_ID).FirstOrDefault();
            }
            if (Value != null)
            {
                int record = entities.FETCH_FF_TABLE_PK(DB_Name, "COMMODITY_MST_TBL", "COMMODITY_ID", Value, "COMMODITY_MST_PK", PK_Value);
                PK = Convert.ToInt32(PK_Value.Value);
            }
            return PK;
        }
        public int Get_CommodityGroup_Mst_PK(string DB_Name, int MPK, string Value = null)
        {
            int PK = 0;
            ObjectParameter PK_Value = new ObjectParameter("PK", 1);
            if (Value == null)
            {
                Value = entities.COMMODITY_GROUP_MST_TBL.Where(a => a.COMMODITY_GROUP_PK == MPK).Select(a=>a.COMMODITY_GROUP_CODE).FirstOrDefault();
            }
            if (Value != null)
            {
                int record = entities.FETCH_FF_TABLE_PK(DB_Name, "COMMODITY_GROUP_MST_TBL", "COMMODITY_GROUP_CODE", Value, "COMMODITY_GROUP_PK", PK_Value);
                PK = Convert.ToInt32(PK_Value.Value);
            }
            return PK;
        }
        public int Get_Currency_Mst_PK(string DB_Name, int MPK, string Value = null)
        {
            int PK = 0;
            ObjectParameter PK_Value = new ObjectParameter("PK", 1);
            if (Value == null)
            {
                Value = entities.CURRENCY_TYPE_MST_TBL.Where(a => a.CURRENCY_MST_PK == MPK).Select(a => a.CURRENCY_ID).FirstOrDefault();
            }
            if (Value != null)
            {
                int record = entities.FETCH_FF_TABLE_PK(DB_Name, "CURRENCY_TYPE_MST_TBL", "CURRENCY_ID", Value, "CURRENCY_MST_PK", PK_Value);
                PK = Convert.ToInt32(PK_Value.Value);
            }
            return PK;
        }
        public int Get_Pack_Mst_PK(string DB_Name, int MPK, string Value = null)
        {
            int PK = 0;
            ObjectParameter PK_Value = new ObjectParameter("PK", 1);
            if (Value == null)
            {
                Value = entities.PACK_TYPE_MST_TBL.Where(a => a.PACK_TYPE_MST_PK == MPK).Select(a=>a.PACK_TYPE_ID).FirstOrDefault();
            }
            if (Value != null)
            {
                int record = entities.FETCH_FF_TABLE_PK(DB_Name, "PACK_TYPE_MST_TBL", "PACK_TYPE_ID", Value, "PACK_TYPE_MST_PK", PK_Value);
                PK = Convert.ToInt32(PK_Value.Value);
            }
            return PK;
        }
        public int Get_Container_type_Mst_PK(string DB_Name, int MPK, string Value = null)
        {
            int PK = 0;
            ObjectParameter PK_Value = new ObjectParameter("PK", 1);
            if (Value == null)
            {
                Value = entities.CONTAINER_TYPE_MST_TBL.Where(a => a.CONTAINER_TYPE_MST_PK == MPK).Select(a => a.CONTAINER_TYPE_MST_ID).FirstOrDefault();
            }
            if (Value != null)
            {
                int record = entities.FETCH_FF_TABLE_PK(DB_Name, "CONTAINER_TYPE_MST_TBL", "CONTAINER_TYPE_MST_ID", Value, "CONTAINER_TYPE_MST_PK", PK_Value);
                PK = Convert.ToInt32(PK_Value.Value);
            }
            return PK;
        }
        public int Get_Dimension_Unit_Mst_PK(string DB_Name, int MPK, string Value = null)
        {
            int PK = 0;
            ObjectParameter PK_Value = new ObjectParameter("PK", 1);
            if (Value == null)
            {
                Value = entities.DIMENTION_UNIT_MST_TBL.Where(a => a.DIMENTION_UNIT_MST_PK == MPK).Select(a => a.DIMENTION_ID).FirstOrDefault();
            }
            if (Value != null)
            {
                int record = entities.FETCH_FF_TABLE_PK(DB_Name, "DIMENTION_UNIT_MST_TBL", "DIMENTION_ID", Value, "DIMENTION_UNIT_MST_PK", PK_Value);
                PK = Convert.ToInt32(PK_Value.Value);
            }
            return PK;
        }
        public List<string> Get_FreightForwarder_List(int UserPK)
        {
            List<string> FreightForwarder = entities.M_USER_CORPORATE_TBL.Where(a => a.M_USER_MST_FK == UserPK).Select(a =>a.CORPORATE_NAME).ToList();
            return FreightForwarder;
        }
        public List<FreightForwarder> Get_FreightForwarder(int UserPK)
        {
            List<FreightForwarder> FreightForwarder = entities.M_USER_CORPORATE_TBL.Where(a => a.M_USER_MST_FK == UserPK).Select(a => new FreightForwarder { FREIGHT_FORWARDER = a.CORPORATE_NAME, FREIGHT_FORWARDER_PK = a.M_CORPORATE_MST_FK }).ToList();
            return FreightForwarder;
        }
        public string Get_FreightForwarder_DataBase(int UserPK,string CorporateName)
        {
            string DataBaseName = entities.M_USER_CORPORATE_TBL.Where(a => a.M_USER_MST_FK == UserPK&&a.CORPORATE_NAME==CorporateName.ToUpper()).Select(a => a.DATABASE_NAME).FirstOrDefault();
            return DataBaseName;
        }
        public int Get_Dimension_Mst_PK(string DB_Name, int MPK, string Value = null)
        {
            int PK = 0;
            ObjectParameter PK_Value = new ObjectParameter("PK", 1);
            if (Value == null)
            {
                Value = entities.DIMENTION_UNIT_MST_TBL.Where(a => a.DIMENTION_UNIT_MST_PK == MPK).Select(a => a.DIMENTION_ID).FirstOrDefault();
            }
            if (Value != null)
            {
                int record = entities.FETCH_FF_TABLE_PK(DB_Name, "DIMENTION_UNIT_MST_TBL", "DIMENTION_ID", Value, "DIMENTION_UNIT_MST_PK", PK_Value);
                PK = Convert.ToInt32(PK_Value.Value);
            }
            return PK;
        }
        public int Get_SalesCall_Type_Mst_PK(string DB_Name, int MPK, string Value = null)
        {
            int PK = 0;
            ObjectParameter PK_Value = new ObjectParameter("PK", 1);
            if (Value == null)
            {
                Value = entities.S_CALL_TYPE_MST_TBL.Where(a => a.SALES_CALL_TYPE_PK == MPK).Select(a => a.SALES_CALL_TYPE_ID).FirstOrDefault();
            }
            if (Value != null)
            {
                int record = entities.FETCH_FF_TABLE_PK(DB_Name, "S_CALL_TYPE_MST_TBL", "SALES_CALL_TYPE_ID", Value, "SALES_CALL_TYPE_PK", PK_Value);
                PK = Convert.ToInt32(PK_Value.Value);
            }
            return PK;
        }
        public int Get_Call_Reason_Mst_PK(string DB_Name, int MPK, string Value = null)
        {
            int PK = 0;
            ObjectParameter PK_Value = new ObjectParameter("PK", 1);
            if (Value == null)
            {
                Value = entities.SAL_CAL_REASON_MST_TBL.Where(a => a.SAL_CAL_REASON_MST_TBL_PK == MPK).Select(a => a.SAL_CAL_ID).FirstOrDefault();
            }
            if (Value != null)
            {
                int record = entities.FETCH_FF_TABLE_PK(DB_Name, "SAL_CAL_REASON_MST_TBL", "SAL_CAL_ID", Value, "SAL_CAL_REASON_MST_TBL_PK", PK_Value);
                PK = Convert.ToInt32(PK_Value.Value);
            }
            return PK;
        }
        public int Get_Created_By_PK(string DB_Name, int MPK, string Value = null)
        {
            int PK = 0;
            ObjectParameter PK_Value = new ObjectParameter("PK", 1);
            if (Value == null)
            {
                Value = entities.M_USER_MST_TBL.Where(a => a.M_USER_MST_PK == MPK).Select(a => a.USER_ID).FirstOrDefault();
            }
            if (Value != null)
            {
                int record = entities.FETCH_FF_TABLE_PK(DB_Name, "USER_MST_TBL", "USER_ID", Value, "USER_MST_PK", PK_Value);
                PK = Convert.ToInt32(PK_Value.Value);
            }
            if(PK==0)
            {
                int record = entities.FETCH_FF_TABLE_PK(DB_Name, "USER_MST_TBL", "USER_ID", "ADMIN", "USER_MST_PK", PK_Value);
                PK = Convert.ToInt32(PK_Value.Value);
            }
            return PK;
        }

        //Smtp configuration
        public static string M_MAIL_SERVER = ConfigurationManager.AppSettings["SMTPServer"];

        public static string M_SEND_USERNAME = ConfigurationManager.AppSettings["SMTPUsername"];

        public static string M_SEND_PASSWORD = ConfigurationManager.AppSettings["SMTPPassword"];

        public static string M_SEND_DOMAIN = ConfigurationManager.AppSettings["SMTPDomain"];

        public static int M_SEND_PORT = Convert.ToInt16(ConfigurationManager.AppSettings["SMTPPort"]);
        //Till here

        public string SendMail(string MailTo, string MailBody = "", string MailSubject = "",string CustomerName="Insight Support")
        {
            try
            {
                MailMessage objMail = new MailMessage();
                Int32 index = default(Int32);

                if (string.Compare(MailTo, ";") > 0)
                {
                    MailTo = MailTo.Replace(";", ",");
                }

                index = MailTo.LastIndexOf(",");
                if (index != -1)
                {
                    if (MailTo.EndsWith(","))
                    {
                        MailTo = MailTo.Remove(index);
                    }
                }
                objMail = new MailMessage();
                objMail.From = new MailAddress(M_SEND_USERNAME,CustomerName);
                objMail.To.Add(MailTo);
                objMail.Subject = MailSubject;
                objMail.IsBodyHtml = true;
                objMail.Body = MailBody;
                objMail.BodyEncoding = Encoding.UTF8;
                //if (M_MAIL_ATTACHMENTS_COUNT > 0)
                //{
                //    Attachment ObjAttachment = default(Attachment);
                //    foreach (string M_MAIL_ATTACH_PATH_loopVariable in M_MAIL_ATTACHMENTS_PATH.Split(new string[] { "^^^" }, StringSplitOptions.None))
                //    {
                //        M_MAIL_ATTACHMENTS_PATH = M_MAIL_ATTACH_PATH_loopVariable;
                //        ObjAttachment = new Attachment(M_MAIL_ATTACHMENTS_PATH);
                //        objMail.Attachments.Add(ObjAttachment);
                //    }
                //}
                using (var smtpserver = new SmtpClient(M_MAIL_SERVER, M_SEND_PORT))
                {
                    NetworkCredential basicCredentials = new NetworkCredential(M_SEND_USERNAME, M_SEND_PASSWORD);
                    if (M_SEND_USERNAME.ToString().Contains("gmail.com") == true)
                    {
                        smtpserver.EnableSsl = true;
                    }
                    smtpserver.UseDefaultCredentials = true;
                    smtpserver.Credentials = basicCredentials;
                    smtpserver.Send(objMail);
                }
                return "Mail Sent Successfully";
            }
            catch (Exception ex)
            {
                SaveErrorLog("Common", "Send mail", ex.Message, ex.StackTrace);
                return "Error in sending mail";
            }
        }
        public string Get_FF_Email(int FF_PK)
        {
            string email = entities.M_CORPORATE_MST_TBL.Where(a => a.M_CORPORATE_MST_PK == FF_PK).FirstOrDefault().EMAIL;
            return email;
        }
    }
}