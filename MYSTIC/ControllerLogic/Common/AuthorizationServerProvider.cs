﻿using MYSTIC.ControllerLogic;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.SessionState;
using MYSTIC.Models;

namespace MYSTIC
{
    public class AuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        public override  Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
            return Task.FromResult<object>(null);
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            string allowedOrigin = context.OwinContext.Get<string>("as:clientAllowedOrigin");
            await Task.Yield();
            if (allowedOrigin == null)
            {
                allowedOrigin = "*";
            }

            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { allowedOrigin });

            // Validate your user and base on validation return claim identity or invalid_grant error
          
            LoginLogic loginLogic = new LoginLogic();

            #region Validate Users

            //Tables not available

            #endregion Validate Users
            LoginModel login= loginLogic.IsUserValid(context.UserName, context.Password);
            string strValidityCheckValue = login.SuccessMessage;


            switch (strValidityCheckValue)
            {
                case "0":
                    // Incorrect User ID
                    context.SetError("User ID doesn't exists");
                    return;

                case "1":


                    context.SetError("Incorrect Password");
                    return;
                case "2":
                    // This User ID is In-Active
                    context.SetError("User ID is in-active");
                    return;

                case "3":
                    // This User Doesnt Have Access to this Location
                    context.SetError("User Doesnt Have Access to this Location");
                    return;

                case "4":
                    // This User Doesnt Have Access to this Location
                    context.SetError("User Doesnt Have Access to this Location");
                    return;

                case "5":
                    // An Error Has Occurred While Connecting
                    context.SetError("An Error Has Occurred While Connecting");
                    return;

                case "6":
                    // User is InActive. Contact Administrator
                    context.SetError("User is InActive. Contact Administrator");
                    return;

                case "7":
                    // Employee is InActive. Contact Administrator
                    context.SetError("Employee is InActive. Contact Administrator");
                    return;
                case "8":
                    context.SetError("forgot_password", "User has entered more than 5 times wrong password.");
                    return;
                default:
                    break;
            }

            if (strValidityCheckValue.Length > 1)
            {
                try
                {

                    string errMessage = "";
                    string USERSESSION_ID = "";
                    if (loginLogic.CheckPasswordExpiry(context.UserName, ref errMessage))
                    {
                        string[] strSplitvalue = strValidityCheckValue.Split('|');                      

                        ClaimsIdentity identity = new ClaimsIdentity(context.Options.AuthenticationType);
                        identity.AddClaim(new Claim(ClaimTypes.Name, context.UserName));

                        SessionIDManager manager = new SessionIDManager();
                        USERSESSION_ID = manager.CreateSessionID(HttpContext.Current);

                        AuthenticationProperties props = new AuthenticationProperties(new Dictionary<string, string>
                            {
                                {"USER_ID", context.UserName},
                                {"USER_NAME", strSplitvalue[0]},
                                {"USER_PK", strSplitvalue[1]},
                                {"LOGED_IN_LOC_FK", strSplitvalue[2]},
                                {"DESIGNATION", strSplitvalue[3]},
                                {"LOGED_IN_LOC_NAME", strSplitvalue[4]},
                                {"GRID_RECORD_SIZE", "15"},
                                {"LIST_ONLOAD", "1"},
                                {"STYLESHEET", "StyleWGrid_8.css"},
                                {"USERSESSION_ID",  USERSESSION_ID}
                            });
                        //On success Save login details

                        AuthenticationTicket ticket = new AuthenticationTicket(identity, props);
                        if (errMessage.Length > 0)
                        {
                            context.SetError(errMessage);
                        }

                        context.Validated(ticket);
                    }
                    else
                    {
                        context.SetError(errMessage);
                        return;
                    }
                }
                catch (Exception ex)
                {
                    context.SetError(ex.Message);
                    return;
                }
            }
            else
            {
                context.SetError("invalid_grant", "The user name or password is incorrect.");
                return;
            }
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }
            return Task.FromResult<object>(null);
        }
    }
}