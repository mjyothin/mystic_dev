﻿using MYSTIC.Data;
using System;
using System.Data;
using System.Linq;
using MYSTIC.Models;
using System.Data.Entity;

namespace MYSTIC.ControllerLogic
{
    public class UserPreferenceLogic : CommonLogic
    {
        private Entities entities = new Entities();
        public bool UpdateUserPreference(M_USER_PREFERENCE_TBL UserPreference)
        {
            try
            {
                entities.Entry(UserPreference).State = EntityState.Modified;
                int save = entities.SaveChanges();
                if (save > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                SaveErrorLog("UserPreference", "UpdateUserPreference", ex.Message, ex.StackTrace);
                return false;
            }
        }
        public UserPreference  FetchUserPreferences(int UserMstPK)
        {
            UserPreference userPreference = new UserPreference();
            try
            {
                userPreference = entities.M_USER_PREFERENCE_TBL.Where(a => a.M_USER_MST_FK == UserMstPK).Select(a => new UserPreference
                {
                    ENVIRONMENT_FK =  a.ENVIRONMENT_FK,
                    USER_FONT_SIZE = a.USER_FONT_SIZE,
                    USER_DATE_FORMAT = a.USER_DATE_FORMAT,
                    USER_NUMBER_FORMAT = a.USER_NUMBER_FORMAT,
                    USER_STYLE_SHEET = a.USER_STYLE_SHEET
                }).FirstOrDefault();

            }
            catch (Exception ex)
            {
                SaveErrorLog("UserPreference", "FetchUserPreferences", ex.Message, ex.StackTrace);
            }
            return userPreference;
        }
    }
}