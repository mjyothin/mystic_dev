﻿using MYSTIC.Data;
using MYSTIC.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace MYSTIC.ControllerLogic
{
    public class CustomerProfileLogic : CommonLogic
    {
        private readonly Entities entities = new Entities();

        public CustomerProfile GetCustomerProfile(int CUSTOMERMSTPK)
        {
            CustomerProfile customerProfile = new CustomerProfile();
            customerProfile.CustomerDetails = entities.FETCH_CUSTOMER_PROFILE.Where(a => a.CUSTOMER_MST_PK == CUSTOMERMSTPK).FirstOrDefault();
            customerProfile.CustomerContactDetails = entities.FETCH_CUSTOMER_CONTACT.Where(a => a.CUSTOMER_MST_PK == CUSTOMERMSTPK).ToList();
            customerProfile.CustomerCategoryDetails = entities.FETCH_CUSTOMER_CATEGORY.Where(a => a.CUSTOMER_MST_PK == CUSTOMERMSTPK).ToList();
            return customerProfile;
        }
    }
}