﻿using MYSTIC.Data;
using MYSTIC.Models;
using System;
using System.Data.Entity.Core.Objects;
using System.Linq;

namespace MYSTIC.ControllerLogic
{
    public class PasswordManagementLogic:CommonLogic
    {
        private Entities entities = new Entities();
        public string ChangePassword(PasswordManagement password)
        {
            string Message = null;
            try
            {
                decimal UserIdPK = entities.M_USER_MST_TBL.Where(a => a.USER_ID == password.UserID).Select(a => a.M_USER_MST_PK).FirstOrDefault();
                ObjectParameter parameter = new ObjectParameter("RETURN_VALUE", "Success");
                int save = entities.CHANGE_PASSWORD(UserIdPK, password.OldPassword, password.NewPassword, "USER", parameter);
                Message = parameter.Value.ToString();
                return Message;
            }
            catch (Exception ex)
            {
                if (ex.InnerException.Message.Contains("20777"))
                {
                    SaveErrorLog("PasswordManagement", "ChangePassword", ex.Message, ex.StackTrace);
                    Message = "Old Password is Incorrect";
                }
                else if(ex.InnerException.Message.Contains("20778"))
                {
                    SaveErrorLog("PasswordManagement", "ChangePassword", ex.Message, ex.StackTrace);
                    Message = "Password Shouldnot repeat with in last three updations";
                }
                else
                {
                    SaveErrorLog("PasswordManagement", "ChangePassword", ex.Message, ex.StackTrace);
                    Message = ex.Message;
                }
                return Message;
            }
        }
    }
}