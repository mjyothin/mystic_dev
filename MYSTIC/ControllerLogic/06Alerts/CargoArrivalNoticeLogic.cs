﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MYSTIC.Data;
using MYSTIC.Models;

namespace MYSTIC.ControllerLogic
{
    public class CargoArrivalNoticeLogic:CommonLogic
    {
        Entities entities = new Entities();
        public CargoArrivalNotice FetchCargoArrival(int BusinessType)
        {
            CargoArrivalNotice cargoArrivalNotice = new CargoArrivalNotice();
            try
            {
                if(BusinessType==1)
                {
                    cargoArrivalNotice.LstCANAir = entities.ALERT_CAN_AIR.ToList();
                }
                else
                {
                    cargoArrivalNotice.LstCANSea = entities.ALERT_CAN_SEA.ToList();
                }
            }
            catch (Exception ex)
            {
                SaveErrorLog("CargoArrivalNotice", "FetchCargoArrival", ex.Message, ex.StackTrace);
                throw;
            }
            return cargoArrivalNotice;
        }
        public CargoArrivalNotice FetchCargoArrivalDetails(string CANRefNo,int BusinessType)
        {
            CargoArrivalNotice cargoArrivalNotice = new CargoArrivalNotice();
            try
            {
                if (BusinessType == 1)
                {
                    cargoArrivalNotice.CANAirDetails = entities.ALERT_CAN_AIR_DTL.Where(a=>a.CANNR==CANRefNo).ToList();
                }
                else
                {
                    cargoArrivalNotice.CANSeaDetails = entities.ALERT_CAN_SEA_DTL.Where(a => a.CANNR == CANRefNo).ToList();
                }
                cargoArrivalNotice.CANClauseDetail = entities.ALERT_CAN_CLAUSE_DTL.Where(a => a.CAN_REF_NO == CANRefNo).ToList();
            }
            catch (Exception ex)
            {
                SaveErrorLog("CargoArrivalNotice", "FetchCargoArrivalDetails", ex.Message, ex.StackTrace);
                throw;
            }
            return cargoArrivalNotice;
        }
        public CargoArrivalNotice FetchCargoArrivalClauseDetails(string CANRefNo)
        {
            CargoArrivalNotice cargoArrivalNotice = new CargoArrivalNotice();
            try
            {
                cargoArrivalNotice.CANClauseDetail = entities.ALERT_CAN_CLAUSE_DTL.Where(a => a.CAN_REF_NO == CANRefNo).ToList();
            }
            catch (Exception ex)
            {
                SaveErrorLog("CargoArrivalNotice", "FetchCargoArrivalClauseDetails", ex.Message, ex.StackTrace);
                throw;
            }
            return cargoArrivalNotice;
        }
    }
}