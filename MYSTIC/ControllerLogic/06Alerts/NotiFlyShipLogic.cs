﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MYSTIC.Data;
using MYSTIC.Models;

namespace MYSTIC.ControllerLogic
{
    public class NotiFlyShipLogic:CommonLogic
    {
        Entities entities = new Entities();
        public NotiFlyShip GetNotiFlyShip(int UserPK, string BookingNumber,string FF)
        {
            string DB = Get_FreightForwarder_DataBase(UserPK, FF);

            NotiFlyShip notiFlyShip = entities.SP_FETCH_BOOKING_CARGO_DETAILS(BookingNumber, DB).Select(a => new NotiFlyShip {
                BookingRefNo = a.BOOKING_REF_NO,
                Commodity = a.COMMODITY_NAME,
                Volume = a.VOLUME_CBM
            }).FirstOrDefault();
            var record = entities.SP_FETCH_BOOKINGS(UserPK).Where(a => a.BOOKING_NO == BookingNumber).FirstOrDefault();
            notiFlyShip.BookingDate = record.SHIPMENT_DATE.ToString("dd/MM/yyyy HH:mm");
            notiFlyShip.ShipmentDate = record.SHIPMENT_DATE.ToString("dd/MM/yyyy HH:mm");
            notiFlyShip.Customer = record.CUSTOMER_NAME;
            return notiFlyShip;
        }
    }
}