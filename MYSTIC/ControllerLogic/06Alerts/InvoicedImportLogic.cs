﻿using MYSTIC.Data;
using MYSTIC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MYSTIC.ControllerLogic
{
    public class InvoicedImportLogic
    {

        private Entities entities = new Entities();
        public InvoicedImport FetchInvoicedImport()
        {
            InvoicedImport invoicedImport = new InvoicedImport
            {
               // LstInvoiceImport = entities.ALERT_INVOICE_IMP_EXP.Where(a => a.PROCESS_TYPE == 2).ToList()
            };
            return invoicedImport;
        }
        public InvoicedImport FetchInvoicedImportDetails(string InvoiceNumber)
        {
            InvoicedImport invoicedImport = new InvoicedImport
            {
                LstInvoiceImport = entities.ALERT_INVOICE_IMP_EXP.Where(a => a.INVOICE_REF_NO == InvoiceNumber).ToList(),
                InvoiceHeader = entities.ALERT_INVOICED_HEADER.Where(a => a.INVOICE_NUMBER == InvoiceNumber).FirstOrDefault(),
                LstInvoiceDetail = entities.ALERT_INVOICE_DTL.Where(a => a.INVOICE_NUMBER == InvoiceNumber).ToList()
            };
            string JobPK = invoicedImport.LstInvoiceImport.Select(a => a.JOBPK).FirstOrDefault();
            List<int> pk = new List<int>();
            if (JobPK != null)
            {
                pk = JobPK.Split(',').Select(int.Parse).ToList<int>();
            }
            if (invoicedImport.LstInvoiceImport.Count > 0)
            {
                switch (invoicedImport.LstInvoiceImport.FirstOrDefault().JOBTYPE)
                {
                    case "JobCard":
                        invoicedImport.InvoiceJC = entities.ALERT_INVOICE_JOB_JC.Where(a => pk.Contains(a.JOBFK)).ToList();
                        break;
                    case "Customs Brokerage":
                        invoicedImport.InvoiceCBJC = entities.ALERT_INVOICE_JOB_CBJC.Where(a => pk.Contains(a.JOBFK)).ToList();
                        break;
                    case "Transport Note":
                        invoicedImport.InvoiceTPT = entities.ALERT_INVOICE_JOB_TPT.Where(a => pk.Contains(a.JOBFK)).ToList();
                        break;
                    case "DEM":
                        invoicedImport.InvoiceDEM = entities.ALERT_INVOICE_JOB_DEM.Where(a => pk.Contains(a.JOBFK)).ToList();
                        break;
                    default:
                        break;
                }
            }
            return invoicedImport;
        }
    }
}