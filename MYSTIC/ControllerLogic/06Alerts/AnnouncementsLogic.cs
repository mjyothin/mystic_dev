﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MYSTIC.Data;
using MYSTIC.Models;

namespace MYSTIC.ControllerLogic
{
    public class AnnouncementsLogic:CommonLogic
    {
        Entities entities = new Entities();
        public Announcements FetchAnnouncements(int UserPK)
        {
            Announcements announcements = new Announcements();
            try
            {
                announcements.LstAnnouncement = entities.SP_ALERT_ANNOUNCEMENT(UserPK.ToString()).Select(a=>new AnnouncementList
                {
                    Sender=a.USER_NAME,
                    Subject=a.SUBJECT,
                    Description=a.BODY
                }).ToList();
            }
            catch (Exception ex)
            {
                SaveErrorLog("Announcements", "FetchAnnouncements", ex.Message, ex.StackTrace);
            }
            return announcements;
        }
        public Announcements FetchAnnouncementDetails(string AnnouncementID)
        {
            Announcements announcements = new Announcements();
            try
            {
                announcements.AnnouncementDetails = entities.ALERT_ANNOUNCEMENT_DETAILS.Where(a => a.ANNOUNCEMENT_ID == AnnouncementID).FirstOrDefault();
                announcements.AnnouncementAttachments = entities.ALERT_ANNOUNCEMENT_ATTACHMENT.Where(a => a.ANNOUNCEMENT_ID == AnnouncementID).ToList();
            }
            catch (Exception ex)
            {
                SaveErrorLog("Announcements", "FetchAnnouncements", ex.Message, ex.StackTrace);
            }
            return announcements;
        }
    }
}