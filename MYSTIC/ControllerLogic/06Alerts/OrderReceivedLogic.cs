﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MYSTIC.Data;
using MYSTIC.Models;

namespace MYSTIC.ControllerLogic
{
    public class OrderReceivedLogic:CommonLogic
    {
        public OrderReceived FetchOrderReceived(int UserPK)
        {
            Entities entities = new Entities();
            OrderReceived orderReceived = new OrderReceived();
            try
            {
                //  orderReceived.LstOrderReceived = entities.ALERT_ORDER_RECEIVED.ToList();
                orderReceived.LstOrderReceived = entities.SP_ALERT_ORDER_RCVD(UserPK).ToList();
            }
            catch(Exception ex)
            {
                SaveErrorLog("OrderReceived", "FetchOrderReceived", ex.Message, ex.StackTrace);
            }
            return orderReceived;
        }
    }
}