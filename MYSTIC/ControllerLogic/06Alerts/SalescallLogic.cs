﻿using MYSTIC.Data;
using MYSTIC.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MYSTIC.ControllerLogic
{
    public class SalescallLogic : CommonLogic
    {
        private Entities entities = new Entities();
        public List<ALERT_SALES_CALL> FetchSalesCall()
        {

            Salescall salescall = new Salescall();
            try
            {
                salescall.SalesCall = entities.ALERT_SALES_CALL.ToList();
            }
            catch (Exception ex)
            {
                SaveErrorLog("Salescall", "FetchSalesCall", ex.Message, ex.StackTrace);
            }
            return salescall.SalesCall;
        }
        public ALERT_SALESCALL_HEADER FetchSalesCallHeader(string SalesCallID)
        {

            Salescall salescall = new Salescall();
            try
            {
                salescall.SalesCallHeader = entities.ALERT_SALESCALL_HEADER.Where(a => a.SALES_CALL_ID == SalesCallID).FirstOrDefault();
            }
            catch (Exception ex)
            {
                SaveErrorLog("Salescall", "FetchSalesCallHeader", ex.Message, ex.StackTrace);
            }
            return salescall.SalesCallHeader;
        }
        public List<ALERT_SALESCALL_COMMENT> FetchSalesCallComment(string SalesCallID)
        {
            Salescall salescall = new Salescall();
            try
            {
                salescall.SalesCallComment = entities.ALERT_SALESCALL_COMMENT.Where(a => a.SALES_CALL_ID == SalesCallID).ToList();
            }
            catch (Exception ex)
            {
                SaveErrorLog("Salescall", "FetchSalesCallComment", ex.Message, ex.StackTrace);
            }
            return salescall.SalesCallComment;
        }
        public List<ALERT_SALESCALL_HISTORY> FetchSalesCallHistory(string SalesCallID)
        {
            Salescall salescall = new Salescall();
            try
            {
                salescall.SalesCallHistory = entities.ALERT_SALESCALL_HISTORY.Where(a => a.SALES_CALL_ID == SalesCallID).ToList();
            }
            catch (Exception ex)
            {
                SaveErrorLog("Salescall", "FetchSalesCallHistory", ex.Message, ex.StackTrace);
            }
            return salescall.SalesCallHistory;
        }
        public ALERT_SALESCALL_FOLLOWUP FetchSalesCallFollowup(string SalesCallID)
        {
            Salescall salescall = new Salescall();
            try
            {
                salescall.SalesCallFollowUp = entities.ALERT_SALESCALL_FOLLOWUP.Where(a => a.SALES_CALL_ID == SalesCallID).FirstOrDefault();
            }
            catch (Exception ex)
            {
                SaveErrorLog("Salescall", "FetchSalesCallFollowup", ex.Message, ex.StackTrace);
            }
            return salescall.SalesCallFollowUp;
        }
        public ALERT_SALESCALL_ESCALATE FetchSalesCallEscalate(string SalesCallID)
        {
            Salescall salescall = new Salescall();
            try
            {
                salescall.SalesCallEscalate = entities.ALERT_SALESCALL_ESCALATE.Where(a => a.SALES_CALL_ID == SalesCallID).FirstOrDefault();
            }
            catch (Exception ex)
            {
                SaveErrorLog("Salescall", "FetchSalesCallEscalate", ex.Message, ex.StackTrace);
            }
            return salescall.SalesCallEscalate;
        }
        public ALERT_SALESCALL_DELEGATE FetchSalesCallDelegate(string SalesCallID)
        {
            Salescall salescall = new Salescall();
            try
            {
                salescall.SalesCallDelegate = entities.ALERT_SALESCALL_DELEGATE.Where(a => a.SALES_CALL_ID == SalesCallID).FirstOrDefault();
            }
            catch (Exception ex)
            {
                SaveErrorLog("Salescall", "FetchSalesCallDelegate", ex.Message, ex.StackTrace);
            }
            return salescall.SalesCallDelegate;
        }
        public ALERT_SALESCALL_REVISIT FetchSalesCallRevisit(string SalesCallID)
        {
            Salescall salescall = new Salescall();
            try
            {
                salescall.SalesCallRevisit = entities.ALERT_SALESCALL_REVISIT.Where(a => a.SALES_CALL_ID == SalesCallID).FirstOrDefault();
            }
            catch (Exception ex)
            {
                SaveErrorLog("Salescall", "FetchSalesCallRevisit", ex.Message, ex.StackTrace);
            }
            return salescall.SalesCallRevisit;
        }
    }
}