﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MYSTIC.Data;
using MYSTIC.Models;

namespace MYSTIC.ControllerLogic
{
    public class SettlementExportLogic
    {
        Entities entities = new Entities();
        public SettlementExport FetchSettlementExport()
        {
            SettlementExport settlementExport = new SettlementExport();
            //settlementExport.LstSettlement = entities.ALERT_SETTLEMENT.Where(a => a.PROCESS_TYPE == 1).ToList();
            return settlementExport;
        }
        public SettlementExport FetchSettlementExportDetails(string ColRefNo)
        {
            SettlementExport settlementExport = new SettlementExport();
            settlementExport.LstSettlement = entities.ALERT_SETTLEMENT.Where(a => a.RECDREFNR==ColRefNo).ToList();
            settlementExport.SettlementAccount = entities.ALERT_SETTLEMENT_ACCOUNT.Where(a => a.COLREFNO == ColRefNo).FirstOrDefault();
            settlementExport.SettlementCollection = entities.ALERT_SETTLEMENT_COLLECTION.Where(a => a.COLLECTIONS_TBL_FK == settlementExport.SettlementAccount.COLPK).ToList();
            settlementExport.SettlementOutstanding = entities.ALERT_SETTLEMENT_OUTSTANDING.Where(a => a.COLLECTIONS_TBL_PK == settlementExport.SettlementAccount.COLPK).ToList();
            return settlementExport;
        }
    }
}