﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MYSTIC.Models;
using MYSTIC.Data;

namespace MYSTIC.ControllerLogic
{
    public class PreArrivalAlertLogic:CommonLogic
    {
        Entities entities = new Entities();
        public PreArrivalAlert PreArrivalAlert(int UserPK,string HBLNumber,int ProcessType,string FF)
        {
            PreArrivalAlert preArrival = new PreArrivalAlert();
            string db_name = Get_FreightForwarder_DataBase(UserPK, FF);
            var record = entities.SP_UPCOMING_SHIPMENTS_DTL(db_name, HBLNumber, ProcessType).ToList(); ;
            preArrival =record.Select(a => new PreArrivalAlert
            {
                Vsl_Voy = a.VES_VOY_NO,
                HBLNr = a.SHIPMENT_NO,
                MBLNr = "",
                POL = a.POLID,
                POD = a.PODID,
                ContainerNo = "",
                DPAgent = a.DISCHARGE_AGENT
            }).FirstOrDefault();
            preArrival.ContainerNo = entities.SP_UPCOMING_SHIP_CARGO_DTL(db_name, record.FirstOrDefault().JOB_CARD_TRN_PK).FirstOrDefault().CONTAINER_NUMBER;
            return preArrival;
        }
    }
}