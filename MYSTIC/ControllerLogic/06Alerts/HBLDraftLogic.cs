﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MYSTIC.Data;
using MYSTIC.Models;

namespace MYSTIC.ControllerLogic
{
    public class HBLDraftLogic:CommonLogic
    {
        Entities entities = new Entities();
        public HBLDraft FetchHBLDraft()
        {
            HBLDraft hBLDraft = new HBLDraft();
            try
            {
                hBLDraft.LstHBLDraft = entities.ALERT_HBL_DRAFT.ToList();
            }
            catch(Exception ex)
            {
                SaveErrorLog("HBLDraft", "FetchHBLDraft", ex.Message, ex.StackTrace);
            }
            return hBLDraft;
        }
        public HBLDraft FetchHBLDraftDetails(string HBLNo)
        {
            HBLDraft hBLDraft = new HBLDraft();
            try
            {
                hBLDraft.HBLDraftDetail = entities.ALERT_HBL_DTL.Where(a=>a.HBL_REF_NO==HBLNo).FirstOrDefault();
            }
            catch (Exception ex)
            {
                SaveErrorLog("HBLDraft", "FetchHBLDraftDetails", ex.Message, ex.StackTrace);
            }
            return hBLDraft;
        }
    }
}