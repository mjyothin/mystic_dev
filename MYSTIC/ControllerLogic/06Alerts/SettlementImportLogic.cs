﻿using MYSTIC.Data;
using MYSTIC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MYSTIC.ControllerLogic
{
    public class SettlementImportLogic
    {
        Entities entities = new Entities();
        public SettlementImport FetchSettlementImport()
        {
            SettlementImport settlementImport = new SettlementImport();
           // settlementImport.LstSettlement = entities.ALERT_SETTLEMENT.Where(a => a.PROCESS_TYPE == 2).ToList();
            return settlementImport;
        }
        public SettlementImport FetchSettlementImportDetails(string ColRefNo)
        {
            SettlementImport settlementImport = new SettlementImport();
            settlementImport.LstSettlement = entities.ALERT_SETTLEMENT.Where(a => a.RECDREFNR == ColRefNo).ToList();
            settlementImport.SettlementAccount = entities.ALERT_SETTLEMENT_ACCOUNT.Where(a => a.COLREFNO == ColRefNo).FirstOrDefault();
            settlementImport.SettlementCollection = entities.ALERT_SETTLEMENT_COLLECTION.Where(a => a.COLLECTIONS_TBL_FK == settlementImport.SettlementAccount.COLPK).ToList();
            settlementImport.SettlementOutstanding = entities.ALERT_SETTLEMENT_OUTSTANDING.Where(a => a.COLLECTIONS_TBL_PK == settlementImport.SettlementAccount.COLPK).ToList();
            return settlementImport;
        }
    }
}