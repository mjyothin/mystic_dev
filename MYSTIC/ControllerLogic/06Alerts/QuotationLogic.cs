﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MYSTIC.Models;
using MYSTIC.Data;

namespace MYSTIC.ControllerLogic
{
    public class QuotationLogic:CommonLogic
    {
        Entities entities = new Entities();
        public Quotation FetchQuotations(int UserPK)
        {
            Quotation quotation = new Quotation();
            try
            {
                quotation.LstQuotationAlert = entities.SP_ALERT_QUOTATION(UserPK.ToString()).ToList();
            }
            catch(Exception ex)
            {
                SaveErrorLog("Quotation", "FetchQuotation", ex.Message, ex.StackTrace);
            }
            return quotation;
        }
        public Quotation FetchQuotationDetails(string QuotationNo)
        {
            Quotation quotation = new Quotation();
            try
            {
                quotation.QuotationDetail = entities.ALERT_QUOTATION_DTL.Where(a=>a.QUOTATION_REF_NO==QuotationNo).FirstOrDefault();
                quotation.QuotationFrtDet = entities.ALERT_QUOTATION_FRT.Where(a => a.QUOTATION_REF_NO == QuotationNo).ToList();
                quotation.QuotationOtherFrtDet = entities.ALERT_QUOTATION_OTH_FRT.Where(a => a.QUOTATION_REF_NO == QuotationNo).ToList();
            }
            catch (Exception ex)
            {
                SaveErrorLog("Quotation", "FetchQuotation", ex.Message, ex.StackTrace);
            }
            return quotation;
        }
    }
}