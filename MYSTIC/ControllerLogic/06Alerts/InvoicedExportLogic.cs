﻿using MYSTIC.Data;
using MYSTIC.Models;
using System.Collections.Generic;
using System.Linq;

namespace MYSTIC.ControllerLogic
{
    public class InvoicedExportLogic
    {
        private Entities entities = new Entities();
        public InvoicedExport FetchInvoicedExport()
        {
            InvoicedExport invoicedExport = new InvoicedExport
            {
              //  LstInvoiceExport = entities.ALERT_INVOICE_IMP_EXP.Where(a=>a.PROCESS_TYPE==1).ToList()
            };
            return invoicedExport;
        }
        public InvoicedExport FetchInvoicedExportDetails(string InvoiceNumber)
        {
            InvoicedExport invoicedExport = new InvoicedExport
            {
                LstInvoiceExport = entities.ALERT_INVOICE_IMP_EXP.Where(a => a.INVOICE_REF_NO == InvoiceNumber).ToList(),
                InvoiceHeader = entities.ALERT_INVOICED_HEADER.Where(a => a.INVOICE_NUMBER == InvoiceNumber).FirstOrDefault(),
                LstInvoiceDetail = entities.ALERT_INVOICE_DTL.Where(a => a.INVOICE_NUMBER == InvoiceNumber).ToList()
            };
            string JobPK = invoicedExport.LstInvoiceExport.Select(a => a.JOBPK).FirstOrDefault();
            List<int> pk = new List<int>();
            if (JobPK != null)
            {
                pk = JobPK.Split(',').Select(int.Parse).ToList<int>();
            }
            if (invoicedExport.LstInvoiceExport.Count > 0)
            {
                switch (invoicedExport.LstInvoiceExport.FirstOrDefault().JOBTYPE)
                {
                    case "JobCard":
                        invoicedExport.InvoiceJC = entities.ALERT_INVOICE_JOB_JC.Where(a => pk.Contains(a.JOBFK)).ToList();
                        break;
                    case "Customs Brokerage":
                        invoicedExport.InvoiceCBJC = entities.ALERT_INVOICE_JOB_CBJC.Where(a => pk.Contains(a.JOBFK)).ToList();
                        break;
                    case "Transport Note":
                        invoicedExport.InvoiceTPT = entities.ALERT_INVOICE_JOB_TPT.Where(a => pk.Contains(a.JOBFK)).ToList();
                        break;
                    case "DEM":
                        invoicedExport.InvoiceDEM = entities.ALERT_INVOICE_JOB_DEM.Where(a => pk.Contains(a.JOBFK)).ToList();
                        break;
                    default:
                        break;
                }
            }
            return invoicedExport;
        }
    }
}