﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MYSTIC.Data;
using MYSTIC.Models;

namespace MYSTIC.ControllerLogic
{
    public class VesselDelayLogic:CommonLogic
    {
        Entities entities = new Entities();
        public VesselDelay FetchVesselDelay(VesselDelaySearch VesselSearch)
        {
            VesselDelay vesselDelay = new VesselDelay();
            try
            {
                if (VesselSearch == null)
                    VesselSearch = new VesselDelaySearch();
                //vesselDelay.LstVesselDelay = entities.ALERT_VESSEL_DELAY.Where(a =>
                //  (a.VESSEL_ID == VesselSearch.VesselId || string.IsNullOrEmpty(VesselSearch.VesselId)) &&
                //  (a.VESSEL_NAME == VesselSearch.VesselName || string.IsNullOrEmpty(VesselSearch.VesselName)) &&
                //   (a.VOYAGESTATUS == VesselSearch.Status || string.IsNullOrEmpty(VesselSearch.Status)) &&
                //  (a.PROCESS_TYPE == VesselSearch.ProcessType || VesselSearch.ProcessType == 0)).ToList();
            }
            catch(Exception ex)
            {
                SaveErrorLog("VesselDelay", "FetchVesselDelay", ex.Message, ex.StackTrace);
                throw;
            }
            return vesselDelay;
        }

        public VesselDelay FetchVesselDelayDetails(VesselDelaySearch VesselSearch)
        {
            VesselDelay vesselDelay = new VesselDelay();
            try
            {
                if (VesselSearch == null)
                    VesselSearch = new VesselDelaySearch();
                vesselDelay.VesselDelayDetail = entities.ALERT_VESSEL_DELAY_DTL.Where(a =>a.VOYPK==VesselSearch.VesselPK).ToList();
            }
            catch (Exception ex)
            {
                SaveErrorLog("VesselDelay", "FetchVesselDelayDetails", ex.Message, ex.StackTrace);
                throw;
            }
            return vesselDelay;
        }
    }
}