﻿using MYSTIC.Data;
using MYSTIC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MYSTIC.ControllerLogic
{
    public class InvoiceLogic:CommonLogic
    {
        Entities entities = new Entities();
        public Invoice FetchInvoice(SearchInvoice search)
        {
            Invoice invoice = new Invoice();
            try
            {
               string Invoicedate = search.InvoiceDate != new DateTime() ? search.InvoiceDate.ToString("dd/MM/yyyy") : null;
                invoice.Invoices = entities.SP_FETCH_INVOICE(search.UserPK.ToString(), 0, 0, search.InvoiceNumber, Invoicedate, search.Customer, search.FF, search.POLID,
                    search.PODID, search.FromDate.ToString("dd/MM/yyyy"), search.ToDate.ToString("dd/MM/yyyy")).Select(a=>new InvoiceListing
                    {
                        InvoiceNumber=a.INVOICE_NUMBER,
                        InvoiceDate=a.INVOICE_DATE.ToString("dd/MM/yyyy"),
                        DueDate=a.INVOICE_DUE_DATE.HasValue?a.INVOICE_DUE_DATE.Value.ToString("dd/MM/yyyy"):"",
                        InvoiceCurrency=a.CURRENCY_ID,
                        InvoiceAmount=a.INVAMT,
                        Customer=a.CUSTOMER_NAME,
                        FF=a.FF
                    }).ToList();
            }
            catch(Exception ex)
            {

            }
            return invoice;
        }
    }
}