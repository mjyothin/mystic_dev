﻿using Microsoft.Ajax.Utilities;
using MYSTIC.Data;
using MYSTIC.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;

namespace MYSTIC.ControllerLogic
{
    public class QuotationRequestLogic : CommonLogic
    {
        private Entities entities = new Entities();
        public QuotationResponseDtl SaveQuotation(Quotation_Master Quotation, List<QuotationDetails> quotationDtl, string CommonReferenceNumber, FreightForwarder FreightForwarder, int UserPK, int Status)
        {
            //QuotationResponse quotationResponse = new QuotationResponse();
            //quotationResponse.CommonReferenceNumber = CommonReferenceNumber;
            QuotationResponseDtl responseDtl = new QuotationResponseDtl();
            try
            {
                QUOTATION_MST_TBL Quotation_mst_tbl = new QUOTATION_MST_TBL();
                List<QUOTATION_DTL_TBL> quotation_dtl_tbl = new List<QUOTATION_DTL_TBL>();
                Quotation_mst_tbl = new QUOTATION_MST_TBL()
                {
                    QUOTATION_MST_PK = Quotation.QUOTATION_MST_PK,
                    QUOTATION_REF_NO = Quotation.QUOTATION_REF_NO,
                    QUOTATION_DATE =Convert.ToDateTime( Quotation.QUOTATION_DATE),
                    EXPECTED_SHIPMENT_DT =Convert.ToDateTime( Quotation.EXPECTED_SHIPMENT_DT),
                    BIZ_TYPE = Quotation.BIZ_TYPE,
                    PROCESS_TYPE = Quotation.PROCESS_TYPE,
                    CARGO_TYPE = Quotation.CARGO_TYPE,
                    CUSTOMER_MST_FK = Quotation.CUSTOMER_MST_FK,
                    CUSTOMER_NAME = Quotation.CUSTOMER_NAME,
                    STATUS =Convert.ToByte( Status),
                    COM_REFERENCE_NO = CommonReferenceNumber,
                    COMMODITY_GROUP_MST_FK = Quotation.COMMODITY_GROUP_MST_FK,
                    CREATED_DT = DateTime.Now,
                    CREATED_BY_FK = UserPK
                };
                Quotation_mst_tbl.VALID_FOR = Convert.ToInt16(Quotation_mst_tbl.EXPECTED_SHIPMENT_DT.Subtract(Quotation_mst_tbl.QUOTATION_DATE).Days);
                quotation_dtl_tbl = quotationDtl.Select(a => new QUOTATION_DTL_TBL
                {
                    QUOTE_DTL_PK = a.QUOTE_DTL_PK,
                    QUOTATION_MST_FK = a.QUOTATION_MST_FK,
                    PORT_MST_POD_FK = a.PORT_MST_POD_FK,
                    PORT_MST_POL_FK = a.PORT_MST_POL_FK,
                    CONTAINER_TYPE_MST_FK = a.CONTAINER_TYPE_MST_FK,
                    COMMODITY_GROUP_FK = a.COMMODITY_GROUP_FK,
                    COMMODITY_MST_FK = a.COMMODITY_MST_FK,
                    BASIS = a.BASIS,
                    EXPECTED_WEIGHT = a.EXPECTED_WEIGHT,
                    CHARGEABLE_WEIGHT = a.EXPECTED_WEIGHT,
                    FRT_WEIGHT = a.EXPECTED_WEIGHT,
                    EXPECTED_VOLUME = a.EXPECTED_VOLUME,
                    CREATED_BY_FK = UserPK,
                    CREATED_DT = DateTime.Now,
                    VALID_FROM = DateTime.Now
                }).ToList();
                string DBName = Get_FreightForwarder_DataBase(UserPK, FreightForwarder.FREIGHT_FORWARDER);

                if (Quotation_mst_tbl != null)
                {
                    if (Quotation_mst_tbl.QUOTATION_REF_NO == null)
                    {
                        ObjectParameter Return_Value = new ObjectParameter("RETURN_VALUE", 1);
                        int rec = entities.GENERATE_PROTOCOL_KEY("QUOTATION", Return_Value);
                        entities.Entry(Quotation_mst_tbl).Property(a => a.QUOTATION_REF_NO).CurrentValue = Convert.ToString(Return_Value.Value);
                        Quotation_mst_tbl.QUOTATION_REF_NO = Convert.ToString(Return_Value.Value);
                    }

                    Quotation_mst_tbl.VALID_FOR = Convert.ToInt16(Quotation_mst_tbl.EXPECTED_SHIPMENT_DT.Subtract(Quotation_mst_tbl.QUOTATION_DATE).Days);
                    Quotation_mst_tbl.FREIGHT_FORWARDER = FreightForwarder.FREIGHT_FORWARDER;
                    Quotation_mst_tbl.FREIGHT_FORWARDER_PK = FreightForwarder.FREIGHT_FORWARDER_PK;
                    if (Quotation_mst_tbl.QUOTATION_MST_PK == 0)
                    {
                        int pk = entities.Database.SqlQuery<int>("Select SEQ_QUOTATION_MST_TBL.Nextval from dual", new object[0]).FirstOrDefault();
                        // Quotation_mst_tbl.QUOTATION_MST_PK = pk;
                        entities.Entry(Quotation_mst_tbl).Property(a => a.QUOTATION_MST_PK).CurrentValue = pk;
                        entities.QUOTATION_MST_TBL.Add(Quotation_mst_tbl);
                        entities.Entry(Quotation_mst_tbl).State = EntityState.Added;
                    }
                    else
                    {
                        entities.Entry(Quotation_mst_tbl).State = EntityState.Modified;
                    }
                }

                if (quotation_dtl_tbl != null)
                {
                    foreach (QUOTATION_DTL_TBL quotationdtl in quotation_dtl_tbl)
                    {
                        quotationdtl.FREIGHT_FORWARDER = FreightForwarder.FREIGHT_FORWARDER;
                        quotationdtl.FREIGHT_FORWARDER_PK = FreightForwarder.FREIGHT_FORWARDER_PK;
                        quotationdtl.CHARGEABLE_WEIGHT = quotationdtl.FRT_WEIGHT = quotationdtl.EXPECTED_WEIGHT;
                        if (quotationdtl.QUOTE_DTL_PK == 0)
                        {
                            int pk = entities.Database.SqlQuery<int>("Select SEQ_QUOTATION_DTL_TBL.Nextval from dual", new object[0]).FirstOrDefault();
                             quotationdtl.QUOTATION_MST_FK = Convert.ToInt32(Quotation_mst_tbl.QUOTATION_MST_PK);
                          //  quotationdtl.QUOTATION_MST_FK = Convert.ToInt32(entities.Entry(Quotation_mst_tbl).Property(a => a.QUOTATION_MST_PK).CurrentValue);
                            quotationdtl.QUOTE_DTL_PK = pk;
                            entities.QUOTATION_DTL_TBL.Add(quotationdtl);
                            entities.Entry(quotationdtl).State = EntityState.Added;
                        }
                        else
                        {
                            entities.Entry(quotationdtl).State = EntityState.Modified;
                        }
                    }
                }
                #region Save Activity log tbl
                ACTIVITY_LOG_TRN_TBL activityLog = entities.ACTIVITY_LOG_TRN_TBL.Where(a => a.USER_FK == UserPK && a.FF_FK == FreightForwarder.FREIGHT_FORWARDER_PK && a.ACTIVITY_LOG_FK == 4).FirstOrDefault();
                if (activityLog == null)
                {
                    activityLog = new ACTIVITY_LOG_TRN_TBL();
                    int pk = entities.Database.SqlQuery<int>("Select SEQ_ACTIVITY_LOG_TRN_TBL.Nextval from dual", new object[0]).FirstOrDefault();
                    activityLog.ACTIVITY_LOG_TRN_PK = pk;
                    activityLog.ACTIVITY_LOG_FK = 4;
                    activityLog.ACTIVITY_LOG_DATE = DateTime.Now;
                    activityLog.FF_FK = FreightForwarder.FREIGHT_FORWARDER_PK;
                    activityLog.LOG_VALUE = 1;
                    activityLog.RATING_STATUS = 0;
                    activityLog.USER_FK = UserPK;
                    entities.ACTIVITY_LOG_TRN_TBL.Add(activityLog);
                }
                else
                {
                    activityLog.ACTIVITY_LOG_DATE = DateTime.Now;
                    activityLog.LOG_VALUE = activityLog.LOG_VALUE + 1;
                }
                #endregion
                int Count = entities.SaveChanges();

              //  ReturnMessage = ReturnMessage + " Common Reference No:" + Quotation_mst_tbl.COM_REFERENCE_NO;
                if (Status > 1)
                {
                    QUOTATION_MST_TBL quote = Quotation_mst_tbl;
                    List<QUOTATION_DTL_TBL> detail = quotation_dtl_tbl;

                    ObjectParameter Return_Value = new ObjectParameter("RETURN_VALUE", 1);
                    #region Get QFOR PK values
                    quote.COL_PLACE_MST_FK = 0;// quote.COL_PLACE_MST_FK == null ? 0 : Get_Place_Mst_PK(DBName, Convert.ToInt32(quote.COL_PLACE_MST_FK));
                    quote.DEL_PLACE_MST_FK = 0;// quote.DEL_PLACE_MST_FK == null ? 0 : Get_Place_Mst_PK(DBName, Convert.ToInt32(quote.DEL_PLACE_MST_FK));
                    quote.COMMODITY_GROUP_MST_FK = quote.COMMODITY_GROUP_MST_FK == null ? 0 : Get_CommodityGroup_Mst_PK(DBName, Convert.ToInt32(quote.COMMODITY_GROUP_MST_FK));
                    quote.CREATED_BY_FK = Get_Created_By_PK(DBName, UserPK);
                    foreach (QUOTATION_DTL_TBL dtl in detail)
                    {
                        dtl.COMMODITY_GROUP_FK = quote.COMMODITY_GROUP_MST_FK;
                        dtl.PORT_MST_POL_FK = dtl.PORT_MST_POL_FK == null ? 0 : Get_Port_Mst_PK(DBName, Convert.ToInt32(dtl.PORT_MST_POL_FK));
                        dtl.PORT_MST_POD_FK = dtl.PORT_MST_POD_FK == null ? 0 : Get_Port_Mst_PK(DBName, Convert.ToInt32(dtl.PORT_MST_POD_FK));
                        dtl.PORT_MST_PLR_FK = 0;// dtl.PORT_MST_PLR_FK == null ? 0 : Get_Port_Mst_PK(DBName, Convert.ToInt32(dtl.PORT_MST_PLR_FK));
                        dtl.PORT_MST_PFD_FK = 0;// dtl.PORT_MST_PFD_FK == null ? 0 : Get_Port_Mst_PK(DBName, Convert.ToInt32(dtl.PORT_MST_PFD_FK));
                        dtl.COMMODITY_MST_FK = dtl.COMMODITY_MST_FK == null ? 0 : Get_Commodity_Mst_PK(DBName, Convert.ToInt32(dtl.COMMODITY_MST_FK));
                        dtl.CONTAINER_TYPE_MST_FK = dtl.CONTAINER_TYPE_MST_FK == 0 ? 0 : Get_Container_type_Mst_PK(DBName, Convert.ToInt32(dtl.CONTAINER_TYPE_MST_FK));
                        dtl.BASIS = dtl.BASIS == 0 ? 0 : Get_Container_type_Mst_PK(DBName, Convert.ToInt32(dtl.BASIS));
                        dtl.CREATED_BY_FK = quote.CREATED_BY_FK;
                    }
                    #endregion

                    quote.STATUS = 0;//Saved as Review from mobility

                    int Save = 0;

                    Save = entities.SP_QUOTATION_MST_TBL_INS(DBName, quote.QUOTATION_REF_NO, quote.QUOTATION_DATE.ToString("dd/MM/yyyy"), quote.CUSTOMER_MST_FK, quote.VALID_FOR,
                       quote.CARGO_TYPE, quote.BIZ_TYPE, quote.PROCESS_TYPE, quote.COL_PLACE_MST_FK, quote.COL_ADDRESS, quote.DEL_PLACE_MST_FK,
                       quote.DEL_ADDRESS, quote.STATUS, quote.COMMODITY_GROUP_MST_FK, quote.CREATED_BY_FK, quote.EXPECTED_SHIPMENT_DT.ToString("dd/MM/yyyy"), Return_Value);

                    //else
                    //{
                    //    Save = entities.SP_QUOTATION_MST_TBL_UPD(DBName, quote.QUOTATION_MST_PK, quote.QUOTATION_REF_NO, quote.QUOTATION_DATE.ToString("dd/MM/yyyy"), quote.CUSTOMER_MST_FK, quote.VALID_FOR,
                    //     quote.CARGO_TYPE, quote.BIZ_TYPE, quote.PROCESS_TYPE, quote.COL_PLACE_MST_FK, quote.COL_ADDRESS, quote.DEL_PLACE_MST_FK,
                    //     quote.DEL_ADDRESS, quote.STATUS, quote.COMMODITY_GROUP_MST_FK, quote.CREATED_BY_FK, quote.EXPECTED_SHIPMENT_DT.ToString("dd/MM/yyyy"), Return_Value);
                    //}
                    foreach (QUOTATION_DTL_TBL dtl in detail)
                    {
                        int quotation_mst_pk = Convert.ToInt32(Return_Value.Value);//Assigning PK value

                        Save = entities.SP_QUOTATION_DTL_TBL_INS(DBName, quotation_mst_pk, dtl.PORT_MST_POL_FK, dtl.PORT_MST_POD_FK, dtl.PORT_MST_PLR_FK, dtl.PORT_MST_PFD_FK,
                             dtl.COMMODITY_GROUP_FK, dtl.COMMODITY_MST_FK, dtl.COMMODITY_MST_FKS, dtl.CONTAINER_TYPE_MST_FK, dtl.BASIS, dtl.EXPECTED_VOLUME, dtl.EXPECTED_WEIGHT,
                     dtl.CREATED_BY_FK, dtl.VALID_FROM, Return_Value);
                        //}
                        //else
                        //{
                        //    Save = entities.SP_QUOTATION_DTL_TBL_UPD(DBName, dtl.QUOTE_DTL_PK, dtl.QUOTATION_MST_FK, dtl.PORT_MST_POL_FK, dtl.PORT_MST_POD_FK, dtl.PORT_MST_PLR_FK, dtl.PORT_MST_PFD_FK,
                        //        dtl.COMMODITY_GROUP_FK, dtl.COMMODITY_MST_FK, dtl.COMMODITY_MST_FKS, dtl.CONTAINER_TYPE_MST_FK, dtl.BASIS, dtl.EXPECTED_VOLUME, dtl.EXPECTED_WEIGHT,
                        //dtl.CREATED_BY_FK, dtl.VALID_FROM, Return_Value);
                        //}
                    }
                }
                responseDtl = new QuotationResponseDtl { FREIGHT_FORWARDER = FreightForwarder.FREIGHT_FORWARDER, QuotationNumber = Quotation_mst_tbl.QUOTATION_REF_NO, Status = "Review" };
               // ReturnMessage = ReturnMessage + Environment.NewLine + "Client:" + FreightForwarder.FREIGHT_FORWARDER + ",Quotation Number:" + Quotation_mst_tbl.QUOTATION_REF_NO + ",Status: Review";
            }
            catch (Exception ex)
            {
                SaveErrorLog("QuotationRequest", "SaveQuotation", ex.Message, ex.StackTrace);
            }
            return responseDtl;
        }
        public List<FreightForwarder> CheckFreightForwarders(QuotationRequest quotationRequest)
        {
            List<FreightForwarder> FreightForwarder = Get_FreightForwarder(quotationRequest.UserPK);           
            List<FreightForwarder> FFCopy = FreightForwarder.ToList();
            QUOTATION_MST_TBL quote = new QUOTATION_MST_TBL()
            {
                QUOTATION_MST_PK = quotationRequest.Quotation.QUOTATION_MST_PK,
                QUOTATION_REF_NO = quotationRequest.Quotation.QUOTATION_REF_NO,
                QUOTATION_DATE =Convert.ToDateTime( quotationRequest.Quotation.QUOTATION_DATE),
                EXPECTED_SHIPMENT_DT = Convert.ToDateTime(quotationRequest.Quotation.EXPECTED_SHIPMENT_DT),
                BIZ_TYPE = quotationRequest.Quotation.BIZ_TYPE,
                PROCESS_TYPE = quotationRequest.Quotation.PROCESS_TYPE,
                CARGO_TYPE = quotationRequest.Quotation.CARGO_TYPE,
                CUSTOMER_MST_FK = quotationRequest.Quotation.CUSTOMER_MST_FK,
                CUSTOMER_NAME = quotationRequest.Quotation.CUSTOMER_NAME,
                COMMODITY_GROUP_MST_FK = quotationRequest.Quotation.COMMODITY_GROUP_MST_FK
            }; ;
            QUOTATION_DTL_TBL dtl = quotationRequest.QuotationDetails.Select(a => new QUOTATION_DTL_TBL
            {
                QUOTE_DTL_PK = a.QUOTE_DTL_PK,
                QUOTATION_MST_FK = a.QUOTATION_MST_FK,
                PORT_MST_POD_FK = a.PORT_MST_POD_FK,
                PORT_MST_POL_FK = a.PORT_MST_POL_FK,
                CONTAINER_TYPE_MST_FK = a.CONTAINER_TYPE_MST_FK,
                COMMODITY_GROUP_FK = a.COMMODITY_GROUP_FK,
                COMMODITY_MST_FK = a.COMMODITY_MST_FK,
                BASIS = a.BASIS,
                EXPECTED_WEIGHT = a.EXPECTED_WEIGHT,
                CHARGEABLE_WEIGHT = a.EXPECTED_WEIGHT,
                FRT_WEIGHT = a.EXPECTED_WEIGHT,
                EXPECTED_VOLUME = a.EXPECTED_VOLUME
            }).FirstOrDefault();
            foreach (FreightForwarder freight in FFCopy)
            {
                string DBName = Get_FreightForwarder_DataBase(quotationRequest.UserPK, freight.FREIGHT_FORWARDER);
                if (quote.COL_PLACE_MST_FK != 0 && quote.COL_PLACE_MST_FK != null)
                {
                    quote.COL_PLACE_MST_FK = Get_Place_Mst_PK(DBName, Convert.ToInt32(quote.COL_PLACE_MST_FK));
                    if (quote.COL_PLACE_MST_FK == 0)
                    {
                        FreightForwarder.Remove(freight);
                        continue;
                    }
                }
                if (quote.DEL_PLACE_MST_FK != 0 && quote.DEL_PLACE_MST_FK != null)
                {
                    quote.DEL_PLACE_MST_FK = Get_Place_Mst_PK(DBName, Convert.ToInt32(quote.DEL_PLACE_MST_FK));
                    if (quote.DEL_PLACE_MST_FK == 0)
                    {
                        FreightForwarder.Remove(freight);
                        continue;
                    }
                }
                if (quote.COMMODITY_GROUP_MST_FK != 0 && quote.COMMODITY_GROUP_MST_FK != null)
                {
                    quote.COMMODITY_GROUP_MST_FK = Get_CommodityGroup_Mst_PK(DBName, Convert.ToInt32(quote.COMMODITY_GROUP_MST_FK));
                    if (quote.COMMODITY_GROUP_MST_FK == 0)
                    {
                        FreightForwarder.Remove(freight);
                        continue;
                    }
                }
                if (dtl.PORT_MST_POL_FK != 0 && dtl.PORT_MST_POL_FK != null)
                {
                    dtl.PORT_MST_POL_FK = Get_Port_Mst_PK(DBName, Convert.ToInt32(dtl.PORT_MST_POL_FK));
                    if (dtl.PORT_MST_POL_FK == 0)
                    {
                        FreightForwarder.Remove(freight);
                        continue;
                    }
                }
                if (dtl.PORT_MST_POD_FK != 0 && dtl.PORT_MST_POD_FK != null)
                {
                    dtl.PORT_MST_POD_FK = Get_Port_Mst_PK(DBName, Convert.ToInt32(dtl.PORT_MST_POD_FK));
                    if (dtl.PORT_MST_POD_FK == 0)
                    {
                        FreightForwarder.Remove(freight);
                        continue;
                    }
                }
                if (dtl.PORT_MST_PLR_FK != 0 && dtl.PORT_MST_PLR_FK != null)
                {
                    dtl.PORT_MST_PLR_FK = Get_Port_Mst_PK(DBName, Convert.ToInt32(dtl.PORT_MST_PLR_FK));
                    if (dtl.PORT_MST_PLR_FK == 0)
                    {
                        FreightForwarder.Remove(freight);
                        continue;
                    }
                }
                if (dtl.PORT_MST_PFD_FK != 0 && dtl.PORT_MST_PFD_FK != null)
                {
                    dtl.PORT_MST_PFD_FK = Get_Port_Mst_PK(DBName, Convert.ToInt32(dtl.PORT_MST_PFD_FK));
                    if (dtl.PORT_MST_PFD_FK == 0)
                    {
                        FreightForwarder.Remove(freight);
                        continue;
                    }
                }
                if (dtl.COMMODITY_MST_FK != 0 && dtl.COMMODITY_MST_FK != null)
                {
                    dtl.COMMODITY_MST_FK = Get_Commodity_Mst_PK(DBName, Convert.ToInt32(dtl.COMMODITY_MST_FK));
                    if (dtl.COMMODITY_MST_FK == 0)
                    {
                        FreightForwarder.Remove(freight);
                        continue;
                    }
                }
                if (dtl.CONTAINER_TYPE_MST_FK != 0 && dtl.CONTAINER_TYPE_MST_FK != null)
                {
                    dtl.CONTAINER_TYPE_MST_FK = dtl.CONTAINER_TYPE_MST_FK == 0 ? 0 : Get_Container_type_Mst_PK(DBName, Convert.ToInt32(dtl.CONTAINER_TYPE_MST_FK));
                    if (dtl.CONTAINER_TYPE_MST_FK == 0)
                    {
                        FreightForwarder.Remove(freight);
                        continue;
                    }
                }
            }

            return FreightForwarder;
        }
        public List<Quotation_List> FetchQuotationsDetails(int UserPK,int QuotationPK)
        {
            List<Quotation_List> quotationList = new List<Quotation_List>();
            var record= (from qt in entities.QUOTATION_MST_TBL
                             join qtdtl in entities.QUOTATION_DTL_TBL 
                             on qt.QUOTATION_MST_PK equals qtdtl.QUOTATION_MST_FK into grp 
                             join pol in entities.PORT_MST_TBL
                             on grp.FirstOrDefault().PORT_MST_POL_FK equals pol.PORT_MST_PK
                             join pod in entities.PORT_MST_TBL
                            on grp.FirstOrDefault().PORT_MST_POD_FK equals pod.PORT_MST_PK
                            where qt.QUOTATION_MST_PK==QuotationPK
                         select new {
                               quotation =qt,
                               quotationdtl=grp.ToList(),
                               pol,pod
                            }).ToList();
            quotationList=record.Select(a=>new Quotation_List
            {
                                 QUOTATION = new Quotation_Master()
                                 {
                                     QUOTATION_MST_PK = a.quotation.QUOTATION_MST_PK,
                                     QUOTATION_REF_NO = a.quotation.QUOTATION_REF_NO,
                                     QUOTATION_DATE = a.quotation.QUOTATION_DATE.ToString("dd/MM/yyyy"),
                                     EXPECTED_SHIPMENT_DT = a.quotation.QUOTATION_DATE.AddDays(a.quotation.VALID_FOR).ToString("dd/MM/yyyy"),
                                     BIZTYPE = a.quotation.BIZ_TYPE == 1 ? "Air" : "Sea",
                                     PROCESSTYPE = a.quotation.PROCESS_TYPE == 1 ? "Export" : "Import",
                                     CARGOTYPE = a.quotation.CARGO_TYPE == 1 ? "FCL" : a.quotation.CARGO_TYPE == 2 ? "LCL" : a.quotation.CARGO_TYPE == 3 ? "Break Bulk" : "",
                                     CUSTOMER_MST_FK = a.quotation.CUSTOMER_MST_FK,
                                     BIZ_TYPE = a.quotation.BIZ_TYPE,
                                     CARGO_TYPE = a.quotation.CARGO_TYPE,
                                     PROCESS_TYPE = a.quotation.PROCESS_TYPE,
                                     CUSTOMER_NAME = a.quotation.CUSTOMER_NAME,
                                     STATUS = a.quotation.STATUS == 1 ? "Draft" : a.quotation.STATUS == 2 ? "Pending For Acceptance" : "Accepted",
                                     POL_ID = a.pol.PORT_ID,
                                     POD_ID = a.pod.PORT_ID,
                                     PORT_OF_LOADING = a.pol.PORT_NAME,
                                     PORT_OF_DISCHARGE = a.pod.PORT_NAME,
                                     PORT_MST_POD_FK = a.pod.PORT_MST_PK,
                                     PORT_MST_POL_FK = a.pol.PORT_MST_PK,
                                     FREIGHT_FORWARDER_PK = a.quotation.FREIGHT_FORWARDER_PK,
                                     FREIGHT_FORWARDER = a.quotation.FREIGHT_FORWARDER,
                                     COM_REFERENCE_NO = a.quotation.COM_REFERENCE_NO
                                 },
                                 QUOTATIONDETAILS = a.quotationdtl.Select(b => new QuotationDetails
                                 {
                                     QUOTATION_MST_FK = b.QUOTATION_MST_FK,
                                     QUOTE_DTL_PK = b.QUOTE_DTL_PK,
                                     PORT_MST_POD_FK = b.PORT_MST_POD_FK,
                                     PORT_MST_POL_FK = b.PORT_MST_POL_FK,
                                     COMMODITY_GROUP_FK = b.COMMODITY_GROUP_FK,
                                     COMMODITY_GROUP_NAME = entities.COMMODITY_GROUP_MST_TBL.Where(c => c.COMMODITY_GROUP_PK == b.COMMODITY_GROUP_FK).Select(c => c.COMMODITY_GROUP_CODE).FirstOrDefault(),
                                     COMMODITY_MST_FK = b.COMMODITY_MST_FK,
                                     COMMODITY_NAME = entities.COMMODITY_MST_TBL.Where(c => c.COMMODITY_MST_PK == b.COMMODITY_MST_FK).Select(c => c.COMMODITY_NAME).FirstOrDefault(),
                                     BASIS = entities.DIMENTION_UNIT_MST_TBL.Where(c=> c.DIMENTION_UNIT_MST_PK == b.BASIS).Select(c => c.DIMENTION_TYPE).FirstOrDefault(),
                                     EXPECTED_VOLUME = b.EXPECTED_VOLUME,
                                     EXPECTED_WEIGHT = b.EXPECTED_WEIGHT,
                                     CONTAINER_TYPE_MST_FK = b.CONTAINER_TYPE_MST_FK,
                                     CONTAINER_TYPE = entities.CONTAINER_TYPE_MST_TBL.Where(c => c.CONTAINER_TYPE_MST_PK == b.CONTAINER_TYPE_MST_FK).Select(c => c.CONTAINER_TYPE_MST_ID).FirstOrDefault()
                                 }).ToList()
                             }).ToList();
                           
            //List < QUOTATION_MST_TBL > record = entities.QUOTATION_MST_TBL.Where(a => a.CREATED_BY_FK == UserPK && a.QUOTATION_MST_PK == QuotationPK).ToList();
            //foreach (QUOTATION_MST_TBL qt in record)
            //{
            //    QUOTATION_DTL_TBL qtDtl = entities.QUOTATION_DTL_TBL.Where(a => a.QUOTATION_MST_FK == qt.QUOTATION_MST_PK).FirstOrDefault();
            //    int POLID = 0; int PODID = 0; string POL_Name = null; string POD_Name = null;string POL_ID = null;string POD_ID = null;
            //    if (qtDtl != null)
            //    {
            //        POLID = Convert.ToInt32(qtDtl.PORT_MST_POL_FK);
            //        PODID = Convert.ToInt32(qtDtl.PORT_MST_POD_FK);
            //        POL_ID = entities.PORT_MST_TBL.Where(b => b.PORT_MST_PK == POLID).Select(b => b.PORT_ID).FirstOrDefault();
            //        POD_ID = entities.PORT_MST_TBL.Where(b => b.PORT_MST_PK == PODID).Select(b => b.PORT_ID).FirstOrDefault();
            //        POL_Name = entities.PORT_MST_TBL.Where(b => b.PORT_MST_PK == POLID).Select(b => b.PORT_NAME).FirstOrDefault();
            //        POD_Name = entities.PORT_MST_TBL.Where(b => b.PORT_MST_PK == PODID).Select(b => b.PORT_NAME).FirstOrDefault();
            //    }
            //    Quotation_List quotation = new Quotation_List
            //    {
            //        QUOTATION = new Quotation_Master()
            //        {
            //            QUOTATION_MST_PK = qt.QUOTATION_MST_PK,
            //            QUOTATION_REF_NO = qt.QUOTATION_REF_NO,
            //            QUOTATION_DATE = qt.QUOTATION_DATE.ToString("dd/MM/yyyy"),
            //            EXPECTED_SHIPMENT_DT = qt.QUOTATION_DATE.AddDays(qt.VALID_FOR).ToString("dd/MM/yyyy"),
            //            BIZTYPE = qt.BIZ_TYPE == 1 ? "Air" : "Sea",
            //            PROCESSTYPE = qt.PROCESS_TYPE == 1 ? "Export" : "Import",
            //            CARGOTYPE = qt.CARGO_TYPE == 1 ? "FCL" : qt.CARGO_TYPE == 2 ? "LCL" : qt.CARGO_TYPE == 3 ? "Break Bulk":"",
            //            CUSTOMER_MST_FK = qt.CUSTOMER_MST_FK,
            //            BIZ_TYPE=qt.BIZ_TYPE,
            //            CARGO_TYPE=qt.CARGO_TYPE,
            //            PROCESS_TYPE=qt.PROCESS_TYPE,
            //            CUSTOMER_NAME = qt.CUSTOMER_NAME,
            //            STATUS = qt.STATUS == 1 ? "Draft" : qt.STATUS == 2 ? "Pending For Acceptance" : "Accepted",
            //            POL_ID=POL_ID,
            //            POD_ID=POD_ID,
            //            PORT_OF_LOADING = POL_Name,
            //            PORT_OF_DISCHARGE = POD_Name,
            //            PORT_MST_POD_FK = PODID,
            //            PORT_MST_POL_FK = POLID,
            //            FREIGHT_FORWARDER_PK = qt.FREIGHT_FORWARDER_PK,
            //            FREIGHT_FORWARDER = qt.FREIGHT_FORWARDER,
            //            COM_REFERENCE_NO = qt.COM_REFERENCE_NO
            //        },
            //        QUOTATIONDETAILS = entities.QUOTATION_DTL_TBL.Where(a => a.QUOTATION_MST_FK == qt.QUOTATION_MST_PK).Select(a => new QuotationDetails
            //        {
            //            QUOTATION_MST_FK = a.QUOTATION_MST_FK,
            //            QUOTE_DTL_PK = a.QUOTE_DTL_PK,
            //            PORT_MST_POD_FK=a.PORT_MST_POD_FK,
            //            PORT_MST_POL_FK=a.PORT_MST_POL_FK,
            //            COMMODITY_GROUP_FK = a.COMMODITY_GROUP_FK,
            //            COMMODITY_GROUP_NAME = entities.COMMODITY_GROUP_MST_TBL.Where(b => b.COMMODITY_GROUP_PK == a.COMMODITY_GROUP_FK).Select(b => b.COMMODITY_GROUP_CODE).FirstOrDefault(),
            //            COMMODITY_MST_FK = a.COMMODITY_MST_FK,
            //            COMMODITY_NAME = entities.COMMODITY_MST_TBL.Where(b => b.COMMODITY_MST_PK == a.COMMODITY_MST_FK).Select(b => b.COMMODITY_NAME).FirstOrDefault(),
            //            BASIS = entities.DIMENTION_UNIT_MST_TBL.Where(b => b.DIMENTION_UNIT_MST_PK == a.BASIS).Select(b => b.DIMENTION_TYPE).FirstOrDefault(),
            //            EXPECTED_VOLUME = a.EXPECTED_VOLUME,
            //            EXPECTED_WEIGHT = a.EXPECTED_WEIGHT,
            //            CONTAINER_TYPE_MST_FK = a.CONTAINER_TYPE_MST_FK,
            //            CONTAINER_TYPE = entities.CONTAINER_TYPE_MST_TBL.Where(b => b.CONTAINER_TYPE_MST_PK == a.CONTAINER_TYPE_MST_FK).Select(b => b.CONTAINER_TYPE_MST_ID).FirstOrDefault(),
            //        }).ToList()
            //    };

            //    quotationList.Add(quotation);
            //}
            return quotationList;
        }
        public string GetCommonReferenceNumber()
        {

            ObjectParameter Return_Value = new ObjectParameter("RETURN_VALUE", 1);
            int record = entities.GENERATE_PROTOCOL_KEY("REFERENCE", Return_Value);
            string CommonReferenceNo = Convert.ToString(Return_Value.Value);
            return CommonReferenceNo;
        }
        public List<Quotation_Master> FetchQuotations(int UserPK)
        {
            var record = entities.QUOTATION_MST_TBL.Where(a => a.CREATED_BY_FK == UserPK).ToList();
            List<Quotation_Master> Quotations = record.Select(qt => new Quotation_Master {
                QUOTATION_MST_PK = qt.QUOTATION_MST_PK,
                QUOTATION_REF_NO = qt.QUOTATION_REF_NO,
                QUOTATION_DATE = Convert.ToString(qt.QUOTATION_DATE),
                VALID_FOR = Convert.ToString(qt.QUOTATION_DATE.AddDays(qt.VALID_FOR)),
                BIZTYPE = qt.BIZ_TYPE == 1 ? "Air" : "Sea",
                PROCESSTYPE = qt.PROCESS_TYPE == 1 ? "Export" : "Import",
                CARGOTYPE = qt.CARGO_TYPE == 1 ? "FCL" : qt.CARGO_TYPE == 2 ? "LCL" : qt.CARGO_TYPE == 3 ? "Break Bulk":" ",
                CUSTOMER_MST_FK = qt.CUSTOMER_MST_FK,
                BIZ_TYPE = qt.BIZ_TYPE,
                CARGO_TYPE = qt.CARGO_TYPE,
                PROCESS_TYPE = qt.PROCESS_TYPE,
                CUSTOMER_NAME = qt.CUSTOMER_NAME,
                STATUS = qt.STATUS == 1 ? "Draft" : qt.STATUS == 2 ? "Pending For Acceptance" : "Accepted",
                FREIGHT_FORWARDER_PK = qt.FREIGHT_FORWARDER_PK,
                FREIGHT_FORWARDER = qt.FREIGHT_FORWARDER,
                COM_REFERENCE_NO = qt.COM_REFERENCE_NO           
            }).OrderByDescending(a=>a.QUOTATION_MST_PK).ToList();
            foreach(Quotation_Master qm in Quotations)
            {
                QUOTATION_DTL_TBL dtl = entities.QUOTATION_DTL_TBL.Where(a => a.QUOTATION_MST_FK == qm.QUOTATION_MST_PK).FirstOrDefault();
                qm.PORT_MST_POD_FK = dtl.PORT_MST_POD_FK;
                qm.PORT_MST_POL_FK = dtl.PORT_MST_POL_FK;
                qm.POL_ID = entities.PORT_MST_TBL.Where(a => a.PORT_MST_PK == qm.PORT_MST_POL_FK).Select(a => a.PORT_ID).FirstOrDefault();
                qm.POD_ID = entities.PORT_MST_TBL.Where(a => a.PORT_MST_PK == qm.PORT_MST_POD_FK).Select(a => a.PORT_ID).FirstOrDefault();
                qm.PORT_OF_LOADING = entities.PORT_MST_TBL.Where(a => a.PORT_MST_PK == qm.PORT_MST_POL_FK).Select(a => a.PORT_NAME).FirstOrDefault();
                qm.PORT_OF_DISCHARGE = entities.PORT_MST_TBL.Where(a => a.PORT_MST_PK == qm.PORT_MST_POD_FK).Select(a => a.PORT_NAME).FirstOrDefault();
            }

            return Quotations;
        }
    }
}