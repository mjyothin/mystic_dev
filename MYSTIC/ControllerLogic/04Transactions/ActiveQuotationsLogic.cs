﻿using MYSTIC.Data;
using MYSTIC.Models;
using Microsoft.Ajax.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using Oracle.ManagedDataAccess.Client;

namespace MYSTIC.ControllerLogic
{
    public class ActiveQuotationsLogic : CommonLogic
    {
        private Entities entities = new Entities();
        public List<ActiveQuotations> GetDropdownForQuotation(ActiveQuotations activeQuotations)
        {
            List<ActiveQuotations> returnData = new List<ActiveQuotations>();
            try
            {
                returnData = entities.SP_FETCH_QUOTATION(activeQuotations.UserPK.ToString(), activeQuotations.BIZ_TYPE, activeQuotations.CARGO_TYPE, activeQuotations.isActive).Select(a => new ActiveQuotations
                {
                    CUSTOMER_NAME = a.CUSTOMER_NAME,
                    POLID = a.POLID,
                    POLNAME = a.POLNAME,
                    PODID = a.PODID,
                    PODNAME = a.PODNAME,
                    QUOTATION_DATE = a.QUOTATION_DATE.ToString("dd/MM/yyyy"),
                    VALID_FOR = a.QUOTATION_DATE.AddDays(Convert.ToDouble(a.VALID_FOR)).ToString("dd/MM/yyyy"),
                    FreightForwarder = a.DBNAME
                }).ToList();

            }
            catch (Exception ex)
            {
                SaveErrorLog("ActiveQuotations", "GetDropdownForQuotation", ex.Message, ex.StackTrace);
            }
            return returnData;
        }
        public List<ActiveQuotations> FetchQuotations(ActiveQuotations activeQuotations)
        {
            List<ActiveQuotations> returnData = new List<ActiveQuotations>();
            try
            {
                returnData = entities.SP_FETCH_QUOTATION(activeQuotations.UserPK.ToString(), activeQuotations.BIZ_TYPE, activeQuotations.CARGO_TYPE, activeQuotations.isActive)
                                .Where(a => (a.BIZ_TYPE == activeQuotations.BIZ_TYPE || activeQuotations.BIZ_TYPE==0)
                                && (a.CARGO_TYPE == activeQuotations.CARGO_TYPE||activeQuotations.CARGO_TYPE==0)
                                && (string.IsNullOrEmpty(activeQuotations.CUSTOMER_NAME) || a.CUSTOMER_NAME == activeQuotations.CUSTOMER_NAME)
                                && (string.IsNullOrEmpty(activeQuotations.POLID) || a.POLID == activeQuotations.POLID)
                                && (string.IsNullOrEmpty(activeQuotations.PODID) || a.PODID == activeQuotations.PODID))
                                .Select(a => new ActiveQuotations
                                {
                                    BIZ_TYPE = Convert.ToInt32(a.BIZ_TYPE),
                                    BIZTYPE = a.BIZ_TYPE == 1 ? "Air" : "Sea",
                                    CARGO_TYPE = Convert.ToInt32(a.CARGO_TYPE),
                                    CARGOTYPE = a.CARGO_TYPE == 1 ? "FCL" : a.CARGO_TYPE == 2 ? "LCL" : a.CARGO_TYPE == 3 ? "BBC":"",
                                    QUOTATION_REF_NO = a.QUOTATION_REF_NO,
                                    POLID = a.POLID,
                                    PODID = a.PODID,
                                    PODNAME = a.PODNAME,
                                    POLNAME = a.POLNAME,
                                    STATUSNAME = a.STATUS == 1 ? "Active" : (a.STATUS == 2 ? "Confirmed" : "Cancelled"),
                                    QUOTATION_DATE = a.QUOTATION_DATE.ToString("dd/MM/yyyy"),
                                    VALID_FOR = a.QUOTATION_DATE.AddDays(Convert.ToDouble(a.VALID_FOR)).ToString("dd/MM/yyyy"),
                                    CUSTOMER_ID = a.CUSTOMER_ID,
                                    CUSTOMER_NAME = a.CUSTOMER_NAME,
                                    FreightForwarder = a.DBNAME,
                                    Validity= a.QUOTATION_DATE.AddDays(Convert.ToDouble(a.VALID_FOR))
                                }).DistinctBy(a => a.QUOTATION_REF_NO).ToList();
                if(activeQuotations.isActive==1)
                {
                    returnData = returnData.OrderByDescending(a => a.QUOTATION_DATE).ToList();
                }
                else
                {
                    returnData = returnData.OrderByDescending(a => a.Validity).ToList();
                }
            }
            catch (Exception ex)
            {
                SaveErrorLog("ActiveQuotations", "FetchQuotations", ex.Message, ex.StackTrace);
            }
            return returnData;
        }
        public ActiveQuotationDetails FetchQuotationsDetails(int UserPK, string QUOTATION_REF_NO)
        {
            ActiveQuotationDetails Data = new ActiveQuotationDetails();
            try
            {
                var record = entities.SP_FETCH_QUOTATION_DTL(UserPK, QUOTATION_REF_NO).ToList();
                Data.QuotationDetails = record.Select(a => new AcQuotationDetails
                {
                    QUOTATION_MST_FK=a.QUOTATION_MST_FK,
                    QUOTE_DTL_PK=a.QUOTE_DTL_PK,
                    BIZ_TYPE = a.BIZ_TYPE,
                    CARGO_TYPE = a.CARGO_TYPE,
                    QUOTATION_REF_NO = a.QUOTATION_REF_NO,
                    QUOTATION_DATE = a.QUOTATION_DATE.Value.ToString("dd/MM/yyyy"),
                    VALID_FOR = a.QUOTATION_DATE.Value.AddDays(Convert.ToDouble(a.VALID_FOR)).ToString("dd/MM/yyyy"),
                    CUSTOMER_ID = a.CUSTOMER_ID,
                    CUSTOMER_NAME = a.CUSTOMER_NAME,
                    PODID = a.PODID,
                    PODNAME = a.PODNAME,
                    POLID = a.POLID,
                    POLNAME = a.POLNAME,
                    STATUS = a.STATUS
                }).FirstOrDefault();
                Data.QuotationCargodetails = record.Select(a => new QuotationCargodetails
                {
                    QUOTE_DTL_PK=a.QUOTE_DTL_PK,
                    QUOTATION_MST_FK=a.QUOTATION_MST_FK,
                    COMMODITY_GROUP_CODE = a.COMMODITY_GROUP_CODE,
                    COMMODITY_GROUP_DESC = a.COMMODITY_GROUP_DESC,
                    COMMODITY_ID = a.COMMODITY_ID,
                    COMMODITY_NAME = a.COMMODITY_NAME,
                    CONTAINER_TYPE_MST_ID = a.CONTAINER_TYPE_MST_ID,
                    CONTAINER_TYPE_NAME = a.CONTAINER_TYPE_NAME,
                    BASIS=a.BASIS,
                    EXPECTED_WEIGHT=a.EXPECTED_WEIGHT,
                    EXPECTED_VOLUME=a.EXPECTED_VOLUME,
                    COMMODITY_GROUP_FK=a.COMMODITY_GROUP_FK,
                    COMMODITY_MST_FK=a.COMMODITY_MST_FK,
                    CONTAINER_TYPE_MST_FK=a.CONTAINER_TYPE_MST_FK
                }).ToList();
                var FrtRecord = entities.SP_FETCH_QUOTATION_FRT_DTL(UserPK, QUOTATION_REF_NO).ToList();

                foreach (QuotationCargodetails qcd in Data.QuotationCargodetails)
                {
                     qcd.QuotationFreightDetails=FrtRecord.Where(a=>a.QUOTATION_DTL_FK==qcd.QUOTE_DTL_PK).Select(a => new QuotationFreightDetails
                     {
                         QUOTION_FREIGHT_PK=a.QUOTION_FREIGHT_PK,
                         QUOTATION_DTL_FK=a.QUOTATION_DTL_FK,
                         FREIGHT_ELEMENT_MST_FK=a.FREIGHT_ELEMENT_MST_FK,
                         CURRENCY_MST_FK=a.CURRENCY_MST_FK,
                         FREIGHT_ELEMENT_NAME = a.FREIGHT_ELEMENT_NAME,
                         CURRENCY_ID = a.CURRENCY_ID,
                         QUOTED_RATE = a.QUOTED_RATE
                     }).ToList();
                }

            }
            catch (Exception ex)
            {
                SaveErrorLog("ActiveQuotations", "FetchQuotationsDetails", ex.Message, ex.StackTrace);
            }
            return Data;
        }
    }
}