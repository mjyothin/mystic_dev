﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MYSTIC.Data;
using MYSTIC.Models;

namespace MYSTIC.ControllerLogic
{
    public class CustomQuotationLogic:CommonLogic
    {
        Entities entities = new Entities();
        public CustomQuotation FetchCustomQuotation()
        {
            CustomQuotation customQuotation = new CustomQuotation();
            try
            {
                customQuotation.lstCustomQuotation = entities.FETCH_CUSTOM_QUOTATION.ToList();
            }
            catch(Exception ex)
            {
                SaveErrorLog("CustomQuotation", "FetchCustomQuotation", ex.Message, ex.StackTrace);
            }
            return customQuotation;
        }
        public FETCH_CUSTOMQUOTE_DETAILS FetchCustomQuotationDetails( string QuotationNo)
        {
            CustomQuotation customQuotation = new CustomQuotation();
            try
            {
                customQuotation.CustomQuoteDetails = entities.FETCH_CUSTOMQUOTE_DETAILS.Where(a=>a.QUOTATION_NR==QuotationNo).FirstOrDefault();
            }
            catch (Exception ex)
            {
                SaveErrorLog("CustomQuotation", "FetchCustomQuotation", ex.Message, ex.StackTrace);
            }
            return customQuotation.CustomQuoteDetails;
        }
        public List<FETCH_CUSTOMQUOTE_CONT_DETAILS> FetchCustomQuoteContainerDetails(string QuotationNo)
        {
            CustomQuotation customQuotation = new CustomQuotation();
            try
            {
                customQuotation.CustomQuoteContDetails = entities.FETCH_CUSTOMQUOTE_CONT_DETAILS.Where(a=>a.QUOTATION_NR==QuotationNo).ToList();
            }
            catch (Exception ex)
            {
                SaveErrorLog("CustomQuotation", "FetchCustomQuotation", ex.Message, ex.StackTrace);
            }
            return customQuotation.CustomQuoteContDetails;
        }
    }
}