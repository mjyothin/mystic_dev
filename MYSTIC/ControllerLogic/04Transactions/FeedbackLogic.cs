﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MYSTIC.Data;
using MYSTIC.Models;

namespace MYSTIC.ControllerLogic
{
    public class FeedbackLogic:CommonLogic
    {
        Entities entities = new Entities();
        public FreightForwarder IsFeedBackRequiredForFF(int UserPK)
        {
            FreightForwarder freightForwarder = new FreightForwarder();
            try
            {
                var trnRecord = entities.ACTIVITY_LOG_TRN_TBL.Where(a => a.USER_FK == UserPK).ToList();
                foreach (ACTIVITY_LOG_TRN_TBL trn in trnRecord)
                {
                    var trigger = entities.ACTIVITY_LOG_MST_TBL.Where(a => a.ACTIVITY_LOG_MST_PK == trn.ACTIVITY_LOG_FK).FirstOrDefault();
                    if (trn.RATING_STATUS == 0)
                    {
                        if (trn.LOG_VALUE >= trigger.TRIGGER_POINTS)
                        {
                            return  new FreightForwarder() { FREIGHT_FORWARDER = trn.FREIGHT_FORWARDER, FREIGHT_FORWARDER_PK = trn.FF_FK };
                        }
                    }
                    else if (trn.RATING_STATUS == 2)
                    {
                        if (trn.LOG_VALUE >= (trigger.TRIGGER_POINTS + trigger.INC_PIONTS))
                        {
                            return new FreightForwarder() { FREIGHT_FORWARDER = trn.FREIGHT_FORWARDER, FREIGHT_FORWARDER_PK = trn.FF_FK };
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                SaveErrorLog("Feedback", "IsFeedBackRequiredForFF", ex.Message, ex.StackTrace);
            }
            return new FreightForwarder();
        }
        public BookingFeedback BookingFeedbackRequired(int UserPK)
        {
            BookingFeedback bookingDetails = new BookingFeedback();
            try
            {
                var record = entities.M_USER_CORPORATE_TBL.Where(a => a.M_USER_MST_FK == UserPK).ToList();
                foreach (M_USER_CORPORATE_TBL user in record)
                {
                    var trnRecord = entities.SP_RATING_BOOKING(user.DATABASE_NAME, user.CUSTOMER_MST_FK).FirstOrDefault();
                    if (trnRecord != null)
                    {
                        var ratingRecord = entities.RATE_BOOKING_TRN_TBL.Where(a => a.BOOKING_REF_NO == trnRecord.BOOKING_REF_NO).FirstOrDefault();
                        if (ratingRecord == null)
                        {
                            bookingDetails.BookingNumber = trnRecord.BOOKING_REF_NO;
                            bookingDetails.CorporateName= user.CORPORATE_NAME;
                        }
                        else
                        {
                            if (ratingRecord.RATING_STATUS == 2)
                            {
                                bookingDetails.BookingNumber = trnRecord.BOOKING_REF_NO;
                                bookingDetails.CorporateName= user.CORPORATE_NAME;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SaveErrorLog("Feedback", "BookingFeedbackRequired", ex.Message, ex.StackTrace);
            }
            return bookingDetails;
        }
        public string SaveBookingRating(RATE_BOOKING_TRN_TBL RateBooking)
        {
            try
            {
                RateBooking.RATED_DATE = DateTime.Now;
                if (RateBooking.RATE_BOOKING_TRN_PK == 0)
                {
                    int pk = entities.Database.SqlQuery<int>("Select seq_RATE_BOOKING_TRN_TBL.Nextval from dual", new object[0]).FirstOrDefault();
                    RateBooking.RATE_BOOKING_TRN_PK = pk;
                    entities.RATE_BOOKING_TRN_TBL.Add(RateBooking);
                }
                int save = entities.SaveChanges();
                if (save > 0)
                {
                    return "Data Saved Successfully";
                }
                else
                {
                    return "errorin saving data";
                }
            }
            catch (Exception ex)
            {
                SaveErrorLog("Feedback", "SaveBookingRating", ex.Message, ex.StackTrace);
            }
            return "";
        }
        public string SaveFFRating(RATE_FF_TRN_TBL RateFF)
        {
            try
            {
                RateFF.RATED_DATE = DateTime.Now;
                if (RateFF.RATE_FF_TRN_PK == 0)
                {
                    int pk = entities.Database.SqlQuery<int>("Select seq_RATE_FF_TRN_TBL.Nextval from dual", new object[0]).FirstOrDefault();
                    RateFF.RATE_FF_TRN_PK = pk;
                    entities.RATE_FF_TRN_TBL.Add(RateFF);
                }
                #region Reset trigger points
                var records = entities.ACTIVITY_LOG_TRN_TBL.Where(a => a.USER_FK == RateFF.USER_PK).ToList();
                foreach (ACTIVITY_LOG_TRN_TBL activity in records)
                {
                    activity.RATING_STATUS = RateFF.RATING_STATUS;
                    activity.LOG_VALUE = 0;
                }
                #endregion
                int save = entities.SaveChanges();
                if (save > 0)
                {
                    return "Data Saved Successfully";
                }
                else
                {
                    return "errorin saving data";
                }
            }
            catch (Exception ex)
            {
                SaveErrorLog("Feedback", "SaveFFRating", ex.Message, ex.StackTrace);
            }
            return "";
        }
        public List<RATING_REASON_MST_TBL> RatingReasonMaster()
        {
            List<RATING_REASON_MST_TBL> ratingReason = new List<RATING_REASON_MST_TBL>();
            try
            {
                ratingReason = entities.RATING_REASON_MST_TBL.Where(a=>a.ISACTIVE==1).ToList();
            }
            catch(Exception ex)
            {
                SaveErrorLog("Feedback", "RatingReasonMaster", ex.Message, ex.StackTrace);
            }
            return ratingReason;
        }
    }
}