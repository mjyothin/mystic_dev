﻿using MYSTIC.Data;
using MYSTIC.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MYSTIC.ControllerLogic
{
    public class UpcomingShipmentsLogic : CommonLogic
    {
        private Entities entities = new Entities();
        
        public List<UpcomingShipmentList> FetchShipments(UpcomingShipments upcomingShipments)
        {
                List<UpcomingShipmentList> lstUpcomingShipments = new List<UpcomingShipmentList>();
                try
                {
                lstUpcomingShipments= entities.SP_UPCOMING_SHIPMENTS(upcomingShipments.UserPk, upcomingShipments.BusinessType, upcomingShipments.CargoType, upcomingShipments.Process_Type, upcomingShipments.POL,
                        upcomingShipments.POD, upcomingShipments.CustomerPK, upcomingShipments.Freight_forwarder, upcomingShipments.ShipmentNo, upcomingShipments.VesselVoyagePK, upcomingShipments.ShippingLine,
                        null, null, upcomingShipments.IsHistory).Select(a=>new UpcomingShipmentList {
                            ARRIVAL_DATE=a.ARRIVAL_DATE.HasValue?a.ARRIVAL_DATE.Value.ToShortDateString():"",
                            BUSINESS_TYPE=a.BUSINESS_TYPE,
                            CARGO_TYPE=a.CARGO_TYPE,
                            COL_PLACE_MST_FK=a.COL_PLACE_MST_FK,
                            CONSIGNEE_NAME=a.CONSIGNEE_NAME,
                            CUSTOMER_NAME=a.CUSTOMER_NAME,
                            DEL_PLACE_MST_FK=a.DEL_PLACE_MST_FK,
                            DEPARTURE_DATE= a.DEPARTURE_DATE.HasValue ? a.DEPARTURE_DATE.Value.ToShortDateString() : "",
                            DISCHARGE_AGENT=a.DISCHARGE_AGENT,
                            ETA_DATE=a.ETA_DATE.HasValue?a.ETA_DATE.Value.ToShortDateString():"",
                            ETD_DATE= a.ETD_DATE.HasValue ? a.ETD_DATE.Value.ToShortDateString() : "",
                            FF=a.FF,JOB_CARD_TRN_PK=a.JOB_CARD_TRN_PK,
                            LOAD_AGENT=a.LOAD_AGENT,
                            PLACE_LOADING=a.PLACE_LOADING,
                            PLACE_RECEIPT=a.PLACE_RECEIPT,
                            SHIPMENT_DATE= a.SHIPMENT_DATE.HasValue ? a.SHIPMENT_DATE.Value.ToShortDateString() : "",
                            SHIPMENT_NO=a.SHIPMENT_NO,
                            SHIPPER_NAME=a.SHIPPER_NAME,
                            POD=a.POD,
                            PODID=a.PODID,
                            POL=a.POL,
                            POLID=a.POLID,
                            SHIPPING_LINE=a.SHIPPING_LINE,
                            VES_VOY_NO=a.VES_VOY_NO
                        }).ToList();
                }
                catch (Exception ex)
                {
                    SaveErrorLog("UpcomingShipments", "GetShipmentDDDetails", ex.Message, ex.StackTrace);
                }
                return lstUpcomingShipments;
          
        }      
        public ShipmentDetails FetchShipmentDetails(UpcomingShipments upcomingShipments)
        {
            ShipmentDetails shipment = new ShipmentDetails();
            try
            {
                string dbName = Get_FreightForwarder_DataBase(upcomingShipments.UserPk, upcomingShipments.Freight_forwarder);
                shipment.ShipmentDetail = entities.SP_UPCOMING_SHIPMENTS_DTL(dbName,upcomingShipments.ShipmentNo,upcomingShipments.Process_Type).FirstOrDefault();
                shipment.CargoDetails = entities.SP_UPCOMING_SHIP_CARGO_DTL(dbName, shipment.ShipmentDetail.JOB_CARD_TRN_PK).ToList();
                shipment.FreightDetails = entities.SP_UPCOMING_SHIP_FRT_DTL(dbName, shipment.ShipmentDetail.JOB_CARD_TRN_PK).ToList();
            }
            catch (Exception ex)
            {
                SaveErrorLog("UpcomingShipments", "FetchShipmentDetails", ex.Message, ex.StackTrace);
            }
            return shipment;
        }
    }
}