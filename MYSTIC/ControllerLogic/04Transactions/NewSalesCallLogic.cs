﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;
using MYSTIC.Data;
using MYSTIC.Models;

namespace MYSTIC.ControllerLogic
{
    public class NewSalesCallLogic:CommonLogic
    {
        Entities entities = new Entities();
        public string SaveSalesCall(SALES_CALL_TRN SalesCall)
        {
            string message = null;
            try
            {
                SalesCall.CREATED_DT = DateTime.Now;
                SalesCall.CALL_TREND = 1;
                if (SalesCall.SALES_CALL_ID == null)
                {
                    SalesCall.SALES_CALL_PK= entities.Database.SqlQuery<int>("Select SEQ_SALES_CALL_TRN.Nextval from dual", new object[0]).FirstOrDefault(); 
                    ObjectParameter Return_Value = new ObjectParameter("RETURN_VALUE", 1);
                    int rec = entities.GENERATE_PROTOCOL_KEY("SALES CALL", Return_Value);
                    SalesCall.SALES_CALL_ID = Convert.ToString(Return_Value.Value);
                    SalesCall.SALES_CALL_DT = DateTime.Now;
                }
                entities.SALES_CALL_TRN.Add(SalesCall);
                int save = entities.SaveChanges();
                if (save > 0)
                {
                   
                    string db_name =entities.M_CORPORATE_MST_TBL.Where(a => a.M_CORPORATE_MST_PK == SalesCall.FREIGHT_FORWARDER_PK).FirstOrDefault().DATABASE_NAME;
                    ObjectParameter obj = new ObjectParameter("RETURN_VALUE", 1);
                    SalesCall.SALES_CALL_REASON_FK = Get_Call_Reason_Mst_PK(db_name, Convert.ToInt32(SalesCall.SALES_CALL_REASON_FK));
                    SalesCall.SALES_CALL_TYPE_FK = Get_SalesCall_Type_Mst_PK(db_name, Convert.ToInt32(SalesCall.SALES_CALL_TYPE_FK));
                    SalesCall.CREATED_BY_FK = Get_Created_By_PK(db_name, SalesCall.CREATED_BY_FK);
                    var rec = entities.SP_SALES_CALL_TRN_INS(db_name, SalesCall.SALES_CALL_ID, SalesCall.SALES_CALL_DT.ToString("dd/MM/yyyy"), SalesCall.FR_TIME, SalesCall.TO_TIME, SalesCall.CUSTOMER_MST_FK,
                        SalesCall.BIZTYPE, SalesCall.SALES_CALL_TYPE_FK, SalesCall.CALL_RESULT, SalesCall.CALL_TREND, SalesCall.CALL_STATUS, SalesCall.SALES_CALL_REASON_FK, SalesCall.CREATED_BY_FK, obj);
                    message = "Data Saved Successfully";
                }
            }
            catch (Exception ex)
            {
                SaveErrorLog("SalesCallMaster", "FetchSalesCallType", ex.Message, ex.StackTrace);
                message = "Error in saving data";
            }
            return message;
        }
        public List<SP_FETCH_SALES_CALL_Result> FetchSalesCall(int UserPK)
        {
            List<M_USER_CORPORATE_TBL> corporate = entities.M_USER_CORPORATE_TBL.Where(a => a.M_USER_MST_FK == UserPK).ToList();
            List<SP_FETCH_SALES_CALL_Result> lstSalesCall = new List<SP_FETCH_SALES_CALL_Result>();
            foreach (M_USER_CORPORATE_TBL ff in corporate)
            {
                int ERP_UserPK = Get_Created_By_PK(ff.DATABASE_NAME, UserPK);
                var rec = entities.SP_FETCH_SALES_CALL(ERP_UserPK, ff.CORPORATE_NAME, ff.DATABASE_NAME, ff.USER_TYPE, ff.CUSTOMER_MST_FK).ToList();
                lstSalesCall.AddRange(rec);
            }
            return lstSalesCall;
        }
        public SP_FETCH_SALESCALL_DTL_Result FetchSalesCallDetails(int UserPK,string CorpName,string SalesCallID)
        {
            SP_FETCH_SALESCALL_DTL_Result SalesCallDetails = new SP_FETCH_SALESCALL_DTL_Result();
            string DB_Name = Get_FreightForwarder_DataBase(UserPK, CorpName);
            SalesCallDetails = entities.SP_FETCH_SALESCALL_DTL(DB_Name,CorpName,SalesCallID).FirstOrDefault();
            return SalesCallDetails;
        }
    }
}