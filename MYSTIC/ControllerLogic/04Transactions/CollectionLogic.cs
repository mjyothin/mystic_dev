﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MYSTIC.Data;
using MYSTIC.Models;

namespace MYSTIC.ControllerLogic
{
    public class CollectionLogic:CommonLogic
    {
        Entities entities = new Entities();
        public Collections FetchCollection(Collections collection)
        {
            collection.LstCollection = new List<ListCollection>();
            try
            {
                collection.LstCollection = entities.SP_FETCH_COLLECTIONS(collection.UserPK.ToString(), collection.InvoiceNumber, collection.CollectionNumber, collection.FromDate, collection.ToDate)
                    .Select(a=>new ListCollection {
                        INVOICE_REF_NO=a.INVOICE_REF_NO,
                        INVOICE_DATE=a.INVOICE_DATE.HasValue?a.INVOICE_DATE.Value.ToString("dd/MM/yyyy HH:mm"):"",
                        INVOICE_DUE_DATE= a.INVOICE_DUE_DATE.HasValue ? a.INVOICE_DUE_DATE.Value.ToString("dd/MM/yyyy HH:mm") : "",
                        INVOICE_AMOUNT=a.INVOICE_AMOUNT,
                        CURRENCY_ID=a.CURRENCY_ID,
                        COLLECTIONS_REF_NO=a.COLLECTIONS_REF_NO,
                        COLLECTIONS_DATE= a.COLLECTIONS_DATE.HasValue ? a.COLLECTIONS_DATE.Value.ToString("dd/MM/yyyy HH:mm") : "",
                        COLLECTION_AMOUNT =a.COLLECTION_AMOUNT,
                        BALANCE=a.BALANCE
                    }).ToList();
            }
            catch(Exception ex)
            {
                SaveErrorLog("Collection", "FetchCollections", ex.Message, ex.StackTrace);
            }
            return collection;
        }
    }
}