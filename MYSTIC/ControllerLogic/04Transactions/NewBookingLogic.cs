﻿using MYSTIC.Data;
using MYSTIC.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;

namespace MYSTIC.ControllerLogic
{
    public class NewBookingLogic : CommonLogic
    {
        private Entities entities = new Entities();
        public string SaveBooking(NewBooking booking)
        {
            string ReturnMessage = string.Empty;
            try
            {
                int FF_PK = entities.M_CORPORATE_MST_TBL.Where(a => a.CORPORATE_ID == booking.FreightForwarder).Select(A => A.M_CORPORATE_MST_PK).FirstOrDefault();
                string DBName = Get_FreightForwarder_DataBase(booking.UserPK, booking.FreightForwarder);
                if (booking.Booking.BOOKING_REF_NO == null)
                {
                    ObjectParameter Return_Value = new ObjectParameter("RETURN_VALUE", 1);
                    int rec = entities.GENERATE_PROTOCOL_KEY("BOOKING", Return_Value);
                    booking.Booking.BOOKING_REF_NO = Convert.ToString(Return_Value.Value);
                }
                if (booking.Booking != null)
                {
                    booking.Booking.FREIGHT_FORWARDER = booking.FreightForwarder;
                    booking.Booking.FRIEGHT_FORWARDER_PK = 1;
                    if (booking.Booking.BOOKING_MST_PK == 0)
                    {
                        int pk = entities.Database.SqlQuery<int>("Select seq_booking_mst_tbl.Nextval from dual", new object[0]).FirstOrDefault();
                        booking.Booking.BOOKING_MST_PK = pk;
                        entities.BOOKING_MST_TBL.Add(booking.Booking);
                        entities.Entry(booking.Booking).State = EntityState.Added;
                    }
                    else
                    {
                        entities.Entry(booking.Booking).State = EntityState.Modified;
                    }
                }

                if (booking.BookingDetails != null)
                {
                    foreach (BookingDetails details in booking.BookingDetails)
                    {
                        BOOKING_TRN Bookingdtl = details.BOOKING_TRN;
                        if (Bookingdtl.BOOKING_TRN_PK == 0)
                        {
                            int pk = entities.Database.SqlQuery<int>("Select SEQ_BOOKING_TRAN.Nextval from dual", new object[0]).FirstOrDefault();
                            Bookingdtl.BOOKING_MST_FK = Convert.ToInt32(booking.Booking.BOOKING_MST_PK);
                            Bookingdtl.BOOKING_TRN_PK = pk;
                            entities.BOOKING_TRN.Add(Bookingdtl);
                            entities.Entry(Bookingdtl).State = EntityState.Added;
                        }
                        else
                        {
                            entities.Entry(Bookingdtl).State = EntityState.Modified;
                        }
                        if (details.BookingFreightDetails != null)
                        {
                            foreach (BOOKING_TRN_FRT_DTLS BookingFrtdtl in details.BookingFreightDetails)
                            {
                                if (BookingFrtdtl.BOOKING_TRN_FRT_PK == 0)
                                {
                                    int pk = entities.Database.SqlQuery<int>("Select SEQ_BOOKING_TRN_FRT_DTLS.Nextval from dual", new object[0]).FirstOrDefault();
                                    BookingFrtdtl.BOOKING_TRN_FK = Convert.ToInt32(Bookingdtl.BOOKING_TRN_PK);
                                    BookingFrtdtl.BOOKING_TRN_FRT_PK = pk;
                                    entities.BOOKING_TRN_FRT_DTLS.Add(BookingFrtdtl);
                                    entities.Entry(BookingFrtdtl).State = EntityState.Added;
                                }
                                else
                                {
                                    entities.Entry(BookingFrtdtl).State = EntityState.Modified;
                                }
                            }
                        }
                    }
                }
                #region Save Activity log tbl
                ACTIVITY_LOG_TRN_TBL activityLog = entities.ACTIVITY_LOG_TRN_TBL.Where(a => a.USER_FK == booking.UserPK && a.FF_FK == FF_PK && a.ACTIVITY_LOG_FK == 2).FirstOrDefault();
                if (activityLog == null)
                {
                    activityLog = new ACTIVITY_LOG_TRN_TBL();
                    int pk = entities.Database.SqlQuery<int>("Select SEQ_ACTIVITY_LOG_TRN_TBL.Nextval from dual", new object[0]).FirstOrDefault();
                    activityLog.ACTIVITY_LOG_TRN_PK = pk;
                    activityLog.ACTIVITY_LOG_FK = 2;
                    activityLog.ACTIVITY_LOG_DATE = DateTime.Now;
                    activityLog.FF_FK = FF_PK;
                    activityLog.LOG_VALUE = 1;
                    activityLog.RATING_STATUS = 0;
                    activityLog.USER_FK = booking.UserPK;
                    entities.ACTIVITY_LOG_TRN_TBL.Add(activityLog);
                }
                else
                {
                    activityLog.ACTIVITY_LOG_DATE = DateTime.Now;
                    activityLog.LOG_VALUE = activityLog.LOG_VALUE + 1;
                }
                #endregion
                int Count = entities.SaveChanges();
                ReturnMessage = "Booking Reference No:" + booking.Booking.BOOKING_REF_NO;
                if (booking.Status > 1)
                {
                    BOOKING_MST_TBL bOOKING_MST_TBL = booking.Booking;
                    List<BookingDetails> detail = booking.BookingDetails;

                    ObjectParameter Return_Value = new ObjectParameter("RETURN_VALUE", 1);
                    #region Get QFOR PK values
                    bOOKING_MST_TBL.CREATED_BY_FK = Get_Created_By_PK(DBName, booking.UserPK);
                    #endregion

                    bOOKING_MST_TBL.COL_PLACE_MST_FK = bOOKING_MST_TBL.DEL_PLACE_MST_FK = 0;

                    bOOKING_MST_TBL.STATUS = 0;//Saved as Review from mobility                           
                    int Save = 0;
                    Save = entities.SP_BOOKING_MST_INS(DBName, bOOKING_MST_TBL.FROM_FLAG, bOOKING_MST_TBL.BUSINESS_TYPE, bOOKING_MST_TBL.CARGO_TYPE, bOOKING_MST_TBL.BOOKING_REF_NO,
                        bOOKING_MST_TBL.BOOKING_DATE.ToString("dd/MM/yyyy"), bOOKING_MST_TBL.SHIPMENT_DATE.ToString("dd/MM/yyyy"), bOOKING_MST_TBL.COL_PLACE_MST_FK, bOOKING_MST_TBL.COL_ADDRESS,
                        bOOKING_MST_TBL.DEL_PLACE_MST_FK, bOOKING_MST_TBL.DEL_ADDRESS, bOOKING_MST_TBL.PORT_MST_POL_FK, bOOKING_MST_TBL.PORT_MST_POD_FK, bOOKING_MST_TBL.CARRIER_MST_FK,
                        bOOKING_MST_TBL.VESSEL_VOYAGE_FK, bOOKING_MST_TBL.VESSEL_NAME, bOOKING_MST_TBL.VOYAGE_FLIGHT_NO, bOOKING_MST_TBL.ETA_DATE.Value.ToString("dd/MM/yyyy HH:mm"),
                        bOOKING_MST_TBL.ETD_DATE.Value.ToString("dd/MM/yyyy HH:mm"), bOOKING_MST_TBL.CUST_CUSTOMER_MST_FK, bOOKING_MST_TBL.CONS_CUSTOMER_MST_FK, bOOKING_MST_TBL.STATUS,
                        bOOKING_MST_TBL.CARGO_MOVE_FK, bOOKING_MST_TBL.CUSTOMER_MST_FK, bOOKING_MST_TBL.AIRLINE_SCHEDULE_TRN_FK, bOOKING_MST_TBL.CREATED_BY_FK, Return_Value);

                    foreach (BookingDetails detl in detail)
                    {
                        BOOKING_TRN dtl = detl.BOOKING_TRN;
                        dtl.BOOKING_MST_FK = Convert.ToInt32(Return_Value.Value);//Assigning PK value
                        dtl.QUANTITY = 0; dtl.PACK_TYPE_FK = 0;

                        Save = entities.SP_BOOKING_TRN_INS(DBName, dtl.BOOKING_MST_FK, dtl.TRANS_REFERED_FROM, dtl.TRANS_REF_NO, dtl.COMMODITY_GROUP_FK, Convert.ToString(dtl.COMMODITY_MST_FK),
                            dtl.QUANTITY, dtl.VOLUME_CBM, dtl.WEIGHT_MT, dtl.PACK_TYPE_FK, dtl.COMMODITY_MST_FKS, dtl.CONTAINER_TYPE_MST_FK, Return_Value);


                        foreach (BOOKING_TRN_FRT_DTLS Frtdtl in detl.BookingFreightDetails)
                        {
                            Frtdtl.BOOKING_TRN_FK = Convert.ToInt32(Return_Value.Value);//Assigning PK value
                            Frtdtl.MIN_BASIS_RATE = Frtdtl.TARIFF_RATE;

                            Save = entities.SP_BOOKING_TRN_FRT_DTLS_INS(DBName, Frtdtl.BOOKING_TRN_FK, Frtdtl.FREIGHT_ELEMENT_MST_FK, Frtdtl.CURRENCY_MST_FK, Frtdtl.TARIFF_RATE,
                                Frtdtl.PYMT_TYPE, Frtdtl.MIN_BASIS_RATE, Return_Value);

                        }
                    }
                    ReturnMessage = ReturnMessage + Environment.NewLine + "Client:" + booking.FreightForwarder + ",Status: Review";
                }

            }
            catch (Exception ex)
            {
                SaveErrorLog("NewBooking", "SaveBooking", ex.Message, ex.StackTrace);
            }
            return ReturnMessage;
        }
        public NewBooking FetchCarrierMaster(DateTime ShipmentDate, int BusinessType, string FF, int UserPK)
        {
            NewBooking booking = new NewBooking();
            try
            {
                string DB_Name = Get_FreightForwarder_DataBase(UserPK, FF);
                if (BusinessType == 1)
                {
                    booking.AirCarrierDetails = entities.FETCH_AIRLINE_SCHEDULE_ENQUIRY(0, DB_Name, 0, 0, 0, 0, null, null, null, null, null, null, null, null, null)
                        .Where(a => a.CUT_OFF_DATE > ShipmentDate || a.DEPARTURE > ShipmentDate).Select(a => new AirlineSchedule
                        {
                            AIRLINE_ID = a.AIRLINE_ID,
                            CARRIER_ID = a.CARRIER_ID,
                            CARRIER_MST_PK = a.CARRIER_MST_PK,
                            ARRIVAL = a.ARRIVAL.Value.ToString("dd/MM/yyyy HH:mm"),
                            ARRIVALDAY = a.ARRIVALDAY,
                            CARRIER_NAME = a.CARRIER_NAME,
                            CUT_OFF_DATE = a.CUT_OFF_DATE.Value.ToString("dd/MM/yyyy  HH:mm"),
                            DEPARTURE = a.DEPARTURE.Value.ToString("dd/MM/yyyy  HH:mm"),
                            DEPARTUREDAY = a.DEPARTUREDAY,
                            FLIGHTNUMBER = a.FLIGHTNUMBER,
                            TARNSIT_TIME = a.TARNSIT_TIME
                        }).ToList();
                }
                else
                {
                    booking.SeaCarrierDetails = entities.FETCH_SEA_SCHEDULE_ENQUIRY(0, DB_Name, 0, 0, 0, 0, null, null, null, null, null, null, null, null, null)
                        .Where(a => a.POL_CUT_OFF_DATE > ShipmentDate || a.DEPARTURE > ShipmentDate).Select(a => new SeaSchedule
                        {
                            ARRIVAL = a.ARRIVAL.HasValue ? a.ARRIVAL.Value.ToString("dd/MM/yyyy HH:mm") : "",
                            ARRIVAL_DAY = a.ARRIVAL_DAY,
                            CARRIER = a.CARRIER,
                            CARRIER_ID = a.CARRIER_ID,
                            CARRIER_MST_PK = a.CARRIER_MST_PK,
                            DEPARTURE = a.DEPARTURE.Value.ToString("dd/MM/yyyy HH:mm"),
                            DEPARTURE_DAY = a.DEPARTURE_DAY,
                            OPERATOR_MST_PK = a.OPERATOR_MST_PK,
                            POL_CUT_OFF_DATE = a.POL_CUT_OFF_DATE.Value.ToString("dd/MM/yyyy HH:mm"),
                            TARNSIT_TIME = a.TARNSIT_TIME.HasValue ? a.TARNSIT_TIME.Value.ToString() : "",
                            VESSEL = a.VESSEL,
                            VESSEL_VOYAGE_TBL_PK = a.VESSEL_VOYAGE_TBL_PK,
                            VOYAGENUMBER = a.VOYAGENUMBER,
                            VOYAGE_TRN_PK = a.VOYAGE_TRN_PK
                        }).ToList();
                }
            }
            catch (Exception ex)
            {
                SaveErrorLog("NewBooking", "FetchCarrierMaster", ex.Message, ex.StackTrace);
            }
            return booking;
        }
        public List<BookingDetails> FetchFreightDetails(NewBooking booking)
        {
            List<BookingDetails> BookingDetails = new List<BookingDetails>();
            try
            {
                string DB_Name = Get_FreightForwarder_DataBase(booking.UserPK, booking.FreightForwarder);
                foreach (BookingDetails details in booking.BookingDetails)
                {
                    string dtlGroup = null;
                    BookingDetails bookingDetails = new BookingDetails();
                    BOOKING_TRN dtl = details.BOOKING_TRN;
                    if (dtlGroup != null)
                    {
                        dtlGroup = dtlGroup + ",";
                    }
                    booking.Booking.COL_PLACE_MST_FK = 0;
                    booking.Booking.DEL_PLACE_MST_FK = 0;
                    dtlGroup = dtlGroup + "(";
                    string[] arr = new string[6];
                    arr[0] = dtl.COMMODITY_GROUP_FK == null ? "0" : dtl.COMMODITY_GROUP_FK.ToString();
                    arr[1] = dtl.COMMODITY_MST_FK == null ? "0" : dtl.COMMODITY_MST_FK.ToString();
                    arr[2] = dtl.CONTAINER_TYPE_MST_FK == null ? "0" : dtl.CONTAINER_TYPE_MST_FK.ToString();
                    arr[3] = dtl.BASIS == null ? "0" : dtl.BASIS.ToString();
                    arr[4] = dtl.WEIGHT_MT == null ? "0" : dtl.WEIGHT_MT.ToString();
                    arr[5] = dtl.VOLUME_CBM == null ? "0" : dtl.VOLUME_CBM.ToString();
                    dtlGroup = dtlGroup + string.Join(",", arr) + ")";
                    bookingDetails.FreightDetails = entities.SP_FETCH_FRT_DTL_BOOKING(booking.UserPK, DB_Name, booking.Booking.BUSINESS_TYPE, booking.Booking.FROM_FLAG, booking.Booking.CARGO_TYPE,
                   booking.Booking.COL_PLACE_MST_FK, booking.Booking.PORT_MST_POL_FK, booking.Booking.PORT_MST_POD_FK, booking.Booking.DEL_PLACE_MST_FK, dtlGroup).ToList();

                    bookingDetails.BOOKING_TRN = dtl;
                    bookingDetails.BookingFreightDetails = bookingDetails.FreightDetails.Select(a => new BOOKING_TRN_FRT_DTLS
                    {
                        FREIGHT_ELEMENT_MST_FK = Convert.ToInt32(a.FREIGHT_ELEMENT_MST_FK),
                        CURRENCY_MST_FK = Convert.ToInt32(a.CURRENCY_MST_FK),
                        TARIFF_RATE = a.QUOTED_RATE
                    }).ToList();
                    BookingDetails.Add(bookingDetails);
                }

            }
            catch (Exception ex)
            {
                SaveErrorLog("NewBooking", "FetchFreightDetails", ex.Message, ex.StackTrace);
            }
            return BookingDetails;
        }
        public NewBooking FetchShipperOrConsignee(string FF, int UserPK, int UserType, int CustomerPK, bool isShipper = false)
        {
            NewBooking booking = new NewBooking();
            try
            {
                string DB_Name = Get_FreightForwarder_DataBase(UserPK, FF);
                if (isShipper == true)
                {
                    booking.ShipperList = entities.SP_FETCH_SHIPPER_DETAILS(DB_Name, UserType, CustomerPK).ToList();
                }
                else
                {
                    booking.ConsigneeList = entities.SP_FETCH_CONSIGNEE_DETAILS(DB_Name, UserType, CustomerPK).ToList();
                }
            }
            catch (Exception ex)
            {
                SaveErrorLog("NewBooking", "FetchCarrierMaster", ex.Message, ex.StackTrace);
            }
            return booking;
        }
        public List<BookingList> FetchBookings(int UserPK)
        {
            List<BookingList> Bookings = entities.SP_FETCH_BOOKINGS(UserPK).Select(a => new BookingList
            {
                BOOKING_NO = a.BOOKING_NO,
                BUSINESS_TYPE = a.BUSINESS_TYPE,
                CARGO_TYPE = a.CARGO_TYPE,
                CONSIGNEE_NAME = a.CONSIGNEE_NAME,
                CUSTOMER_NAME = a.CUSTOMER_NAME,
                ETA_DATE = a.ETA_DATE.HasValue ? a.ETA_DATE.Value.ToShortDateString() : "",
                ETD_DATE = a.ETD_DATE.HasValue ? a.ETD_DATE.Value.ToShortDateString() : "",
                FF = a.FF,
                PLACE_LOADING = a.PLACE_LOADING,
                PLACE_RECEIPT = a.PLACE_RECEIPT,
                POD = a.POD,
                POL = a.POL,
                SHIPMENT_DATE = a.SHIPMENT_DATE.ToShortDateString(),
                SHIPMENTDATE = a.SHIPMENT_DATE,
                SHIPPER_NAME = a.SHIPPER_NAME,
                SHIPPING_LINE = a.SHIPPING_LINE,
                VES_VOY_NO = a.VES_VOY_NO
            }).OrderByDescending(a => a.SHIPMENTDATE).ToList();
            return Bookings;
        }
        public ListBooking FetchBookingDetails(string BookingNo, int UserPK, string CorporateName)
        {
            ListBooking bookingDetails = new ListBooking();
            string dbName = Get_FreightForwarder_DataBase(UserPK, CorporateName);
            List<SP_FETCH_BOOKING_CARGO_DETAILS_Result> cargodtls = entities.SP_FETCH_BOOKING_CARGO_DETAILS(BookingNo, dbName).ToList();
            bookingDetails.BookingCargoDetails = cargodtls.Select(a => new BookingCargoDetails
            {
                BOOKING_REF_NO = a.BOOKING_REF_NO,
                MOVEMENT_CODE = a.MOVEMENT_CODE,
                CONTAINER_TYPE_MST_ID = a.CONTAINER_TYPE_MST_ID,
                COMMODITY_NAME = a.COMMODITY_NAME,
                WEIGHT_MT = a.WEIGHT_MT,
                VOLUME_CBM = a.VOLUME_CBM,
                PACK_TYPE_ID = a.PACK_TYPE_ID,
                PACK_TYPE_DESC = a.PACK_TYPE_DESC,
                QUANTITY = a.QUANTITY,
                BOOKING_TRN_PK = a.BOOKING_TRN_PK
            }).ToList();
            List<SP_FETCH_BOOKING_FRT_DTL_Result> frtdtls = entities.SP_FETCH_BOOKING_FRT_DTL(BookingNo, dbName).ToList();
            foreach (BookingCargoDetails bkg in bookingDetails.BookingCargoDetails)
            {
                bkg.BookingFreightDetails = frtdtls.Where(a => a.BOOKING_TRN_PK == bkg.BOOKING_TRN_PK).Select(a => new SP_FETCH_BOOKING_FRT_DTL_Result
                {
                    BOOKING_REF_NO = a.BOOKING_REF_NO,
                    REFERENCE_NO = a.REFERENCE_NO,
                    FREIGHT_ELEMENT_ID = a.FREIGHT_ELEMENT_ID,
                    FREIGHT_ELEMENT_NAME = a.FREIGHT_ELEMENT_NAME,
                    CURRENCY = a.CURRENCY,
                    TARIFF_RATE = a.TARIFF_RATE,
                    BOOKING_TRN_PK = a.BOOKING_TRN_PK
                }).ToList();
            }
            return bookingDetails;
        }
    }
}