﻿using MYSTIC.Data;
using MYSTIC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MYSTIC.ControllerLogic
{
    public class TrackAndTraceLogic:CommonLogic
    {
        Entities entities = new Entities();
        public TrackAndTrace FetchTrackAndTrace(TrackAndTrace trackAndTrace)
        {
            try
            {
                trackAndTrace.TrackNTraceList = new List<SP_TRACK_TRACE_LIST_Result>();
                trackAndTrace.TrackNTraceList = entities.SP_TRACK_TRACE_LIST(trackAndTrace.UserPK.ToString(), trackAndTrace.BusinessType, trackAndTrace.CargoType,trackAndTrace.ContainerNumber,
                    trackAndTrace.ShipmentNumber).Distinct().ToList();
            }
            catch(Exception ex)
            {
                SaveErrorLog("TrackAndTrace", "FeytchTrackAndTrace", ex.Message, ex.StackTrace);
            }
            return trackAndTrace;
        }
        public TrackNTraceDetail FetchTrackAndTraceDetail(TrackAndTrace trackAndTrace)
        {
            TrackNTraceDetail traceDetail = new TrackNTraceDetail();
            try
            {
                string DB_Name = Get_FreightForwarder_DataBase(trackAndTrace.UserPK, trackAndTrace.Corporate);
                traceDetail = entities.SP_TRACK_TRACE_HEADER(DB_Name, trackAndTrace.ShipmentNumber, trackAndTrace.ProcessType).Select(a => new TrackNTraceDetail {
                    BUSINESS_TYPE=a.BUSINESS_TYPE,
                    CARGO_TYPE=a.CARGO_TYPE,
                    JOB_CARD_TRN_PK=a.JOB_CARD_TRN_PK,
                    CUSTOMER_NAME=a.CUSTOMER_NAME,
                    CONSIGNEE_NAME=a.CONSIGNEE_NAME,
                    ARRIVAL_DATE=a.ARRIVAL_DATE,
                    DEPARTURE_DATE=a.DEPARTURE_DATE,
                    ETA_DATE=a.ETA_DATE,
                    ETD_DATE=a.ETD_DATE,
                    PODID=a.PODID,
                    POLID=a.POLID,
                    POD=a.POD,
                    POL=a.POL,
                    SHIPMENT_DATE=trackAndTrace.ProcessType==1? a.SHIPMENT_DATE:a.IMP_SHIPMENT_DATE,
                    SHIPMENT_NO=trackAndTrace.ProcessType==1?a.SHIPMENT_NO:a.IMP_SHIPMENT_NO,
                    SHIPPER_NAME=a.SHIPPER_NAME,
                    SHIPPING_LINE=a.SHIPPING_LINE,
                    VES_VOY_NO=a.VES_VOY_NO
                }).FirstOrDefault();
                traceDetail.TrackNTraceStatus = entities.SP_TRACK_TRACE_DTL(DB_Name, traceDetail.JOB_CARD_TRN_PK).ToList();
            }
            catch (Exception ex)
            {
                SaveErrorLog("TrackAndTrace", "FeytchTrackAndTrace", ex.Message, ex.StackTrace);
            }
            return traceDetail;
        }
    }
}