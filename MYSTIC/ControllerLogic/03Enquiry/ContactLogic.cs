﻿using MYSTIC.Data;
using MYSTIC.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MYSTIC.ControllerLogic
{
    public class ContactLogic : CommonLogic
    {
        private Entities entities = new Entities();
        public string ContactMe(M_CONTACT_ME_TRN contact)
        {
            string Message = null;
            try
            {
                contact.CREATED_DATE = DateTime.Now;
                int pk = entities.Database.SqlQuery<int>("Select SEQ_M_CONTACT_ME.Nextval from dual", new object[0]).FirstOrDefault();
                contact.M_CONTACT_ME_TRN_PK = Convert.ToInt32(pk);
                entities.M_CONTACT_ME_TRN.Add(contact);
                int save = entities.SaveChanges();
                string FF_Email = Get_FF_Email(contact.FREIGHT_FORWARDER_PK);
                if (save > 0)
                {
                    Message = SendMail(FF_Email, contact.MESSAGE, contact.SUBJECT);
                }
            }
            catch (Exception ex)
            {
                SaveErrorLog("Contact", "ContactMe", ex.Message, ex.StackTrace);
            }
            return Message;
        }
        public List<SubjectMaster> SubjectDropdown()
        {
            List<SubjectMaster> Subject = entities.M_SUBJECT_MST_TBL.Where(a => a.ISACTIVE == 1).Select(a => new SubjectMaster
            {
                M_SUBJECT_MST_PK = a.M_SUBJECT_MST_PK,
                SUBJECT_DESC = a.SUBJECT_DESC,
                SUBJECT_ID = a.SUBJECT_ID
            }).ToList();
            return Subject;
        }
    }
}