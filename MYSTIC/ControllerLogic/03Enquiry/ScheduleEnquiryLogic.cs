﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MYSTIC.Data;
using MYSTIC.Models;

namespace MYSTIC.ControllerLogic
{
    public class ScheduleEnquiryLogic:CommonLogic
    {
        Entities entities = new Entities();
        public ScheduleEnquiry FetchScheduleEnquiry(ScheduleEnquiry schedule)
        {
            ScheduleEnquiry scheduleEnquiry = new ScheduleEnquiry();
            try
            {
                string db_name = Get_FreightForwarder_DataBase(schedule.USER_PK, schedule.Freight_forwarder);
                int POLPK = schedule.POL_PK==0?0: Get_Port_Mst_PK(db_name, schedule.POL_PK);
                int PODPK = schedule.POD_PK == 0 ? 0 : Get_Port_Mst_PK(db_name, schedule.POD_PK);
                int AOOPK = schedule.AOO_PK == 0 ? 0 : Get_Port_Mst_PK(db_name, schedule.AOO_PK);
                int AODPK = schedule.AOD_PK == 0 ? 0 : Get_Port_Mst_PK(db_name, schedule.AOD_PK);
                if(!string.IsNullOrEmpty(schedule.DEP_DATE))
                {
                    schedule.FLAG = 1;
                }
               else if( !string.IsNullOrEmpty(schedule.ARR_DATE))
                {
                    schedule.FLAG = 5;
                }
                else if (!string.IsNullOrEmpty(schedule.DEP_FROM_DATE) || !string.IsNullOrEmpty(schedule.DEP_TO_DATE))
                {
                    schedule.FLAG = 4;
                }
                else if( !string.IsNullOrEmpty(schedule.ARR_FROM_DATE) || !string.IsNullOrEmpty(schedule.ARR_TO_DATE))
                {
                    schedule.FLAG = 8;
                }
                else
                {
                    schedule.FLAG = 0;
                }
                if(schedule.FLAG==0)
                {
                    schedule.FLAG = 4;
                    schedule.DEP_FROM_DATE = DateTime.Now.ToShortDateString();
                }

                switch (schedule.BusinessType)
                {
                    case 1:
                        scheduleEnquiry.ScheduleAir = entities.FETCH_AIRLINE_SCHEDULE_ENQUIRY(schedule.USER_PK,db_name,schedule.CARRIER_MST_PK,AOOPK,AODPK,
                            schedule.FLAG,schedule.DEP_DATE,schedule.DEP_FROM_DATE,schedule.DEP_TO_DATE,schedule.ARR_DATE,schedule.ARR_FROM_DATE,schedule.ARR_TO_DATE,
                            schedule.TRANS_TIME_TO,schedule.TRANS_TIME_FROM,schedule.P_DAY).Select(a=>new AirlineSchedule
                            {
                                AIRLINE_ID=a.AIRLINE_ID,
                                CARRIER_ID=a.CARRIER_ID,
                                CARRIER_MST_PK=a.CARRIER_MST_PK,
                                ARRIVAL=a.ARRIVAL.Value.ToString("dd/MM/yyyy HH:mm"),
                                ARRIVALDAY=a.ARRIVALDAY,
                                CARRIER_NAME=a.CARRIER_NAME,
                                CUT_OFF_DATE=a.CUT_OFF_DATE.Value.ToString("dd/MM/yyyy HH:mm"),
                                DEPARTURE=a.DEPARTURE.Value.ToString("dd/MM/yyyy HH:mm"),
                                DEPARTUREDAY=a.DEPARTUREDAY,
                                FLIGHTNUMBER=a.FLIGHTNUMBER,
                                TARNSIT_TIME=a.TARNSIT_TIME
                            }).ToList();
                        break;
                    case 2:
                        scheduleEnquiry.ScheduleSea = entities.FETCH_SEA_SCHEDULE_ENQUIRY(schedule.USER_PK, db_name, schedule.CARRIER_MST_PK, POLPK, PODPK,
                            schedule.FLAG, schedule.DEP_DATE, schedule.DEP_FROM_DATE, schedule.DEP_TO_DATE, schedule.ARR_DATE, schedule.ARR_FROM_DATE, schedule.ARR_TO_DATE,
                            schedule.TRANS_TIME_TO, schedule.TRANS_TIME_FROM, schedule.P_DAY).Select(a=>new SeaSchedule {
                                ARRIVAL=a.ARRIVAL.HasValue?a.ARRIVAL.Value.ToString("dd/MM/yyyy HH:mm") :"",
                                ARRIVAL_DAY=a.ARRIVAL_DAY,
                                CARRIER=a.CARRIER,
                                CARRIER_ID=a.CARRIER_ID,
                                CARRIER_MST_PK=a.CARRIER_MST_PK,
                                DEPARTURE=a.DEPARTURE.Value.ToString("dd/MM/yyyy HH:mm"),
                                DEPARTURE_DAY=a.DEPARTURE_DAY,
                                OPERATOR_MST_PK=a.OPERATOR_MST_PK,
                                POL_CUT_OFF_DATE=a.POL_CUT_OFF_DATE.Value.ToString("dd/MM/yyyy HH:mm"),
                                TARNSIT_TIME=a.TARNSIT_TIME.HasValue?a.TARNSIT_TIME.Value.ToString():"",
                                VESSEL=a.VESSEL,
                                VESSEL_VOYAGE_TBL_PK=a.VESSEL_VOYAGE_TBL_PK,
                                VOYAGENUMBER=a.VOYAGENUMBER,
                                VOYAGE_TRN_PK=a.VOYAGE_TRN_PK
                            }).ToList();
                        break;
                    default:
                        break;
                }
            }
            catch(Exception ex)
            {
                SaveErrorLog("ScheduleEnquiry", "FetchScheduleEnquiry", ex.Message, ex.StackTrace);
            }
            return scheduleEnquiry;
        }
        public Carrier_Mst FetchCarrier(int User_PK,int BusinessType,string Freight_forwarder)
        {
            Carrier_Mst Carrier = new Carrier_Mst();
            try
            {
                string db_name = Get_FreightForwarder_DataBase(User_PK, Freight_forwarder);
                Carrier.lstCarrier = entities.FETCH_CARRIER_MST(User_PK, db_name, BusinessType).ToList();
                return Carrier;
            }
            catch (Exception ex)
            {
                SaveErrorLog("ScheduleEnquiry", "FetchCarrier", ex.Message, ex.StackTrace);
            }
            return Carrier;
        }
    }
}