﻿using MYSTIC.Data;
using MYSTIC.Models;
using System.Collections.Generic;
using System.Linq;

namespace MYSTIC.ControllerLogic
{
    public class CounterPartsLogic
    {
        private Entities entities = new Entities();
        public List<SP_FETCH_COUNTER_PARTS_Result> FetchCounterParts(CounterPartsSearch search)
        {
            List<SP_FETCH_COUNTER_PARTS_Result> lstCounterParts = entities.SP_FETCH_COUNTER_PARTS(search.UserPK, search.Customer_PK).Where(
                a => (a.COUNTRY == search.COUNTRY || string.IsNullOrEmpty(search.COUNTRY)) &&
                (a.CITY == search.CITY || string.IsNullOrEmpty(search.CITY)) &&
                 (a.CUSTOMER_ID == search.CUSTOMER_ID || string.IsNullOrEmpty(search.CUSTOMER_ID)) &&
                  (a.CUSTOMER_NAME == search.CUSTOMER_NAME || string.IsNullOrEmpty(search.CUSTOMER_NAME)) &&
                   (a.EMAIL == search.EMAIL || string.IsNullOrEmpty(search.EMAIL)) &&
                   (a.PHONE == search.PHONE || string.IsNullOrEmpty(search.PHONE))).ToList();
            return lstCounterParts;
        }
    }
}