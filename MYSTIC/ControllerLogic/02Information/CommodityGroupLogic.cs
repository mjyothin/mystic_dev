﻿using MYSTIC.Data;
using MYSTIC.Models;
using System;
using System.Linq;

namespace MYSTIC.ControllerLogic
{
    public class CommodityGroupLogic : CommonLogic
    {
        private Entities entities = new Entities();

        public CommodityGroup FetchCommodityGroupDetails(string CommodityGroup)
        {
            CommodityGroup commodityGroup = new CommodityGroup();
            try
            {
                commodityGroup.CommodityGroupDetails = entities.FETCH_COMMODITY_GROUP.Where(a =>
                                    (CommodityGroup == null || a.COMMODITY_GROUP_CODE.Contains(CommodityGroup) || a.COMMODITY_GROUP_DESC.Contains(CommodityGroup)
                    )).OrderBy(a=>a.COMMODITY_GROUP_CODE).ToList();
            }
            catch (Exception ex)
            {
                SaveErrorLog("CommodityGroup", "FetchCommodityGroupDetails", ex.Message, ex.StackTrace);
            }
            return commodityGroup;
        }
        public CommodityGroup FetchCommodityGroupMaster(string CommodityGroup, string DB_Name=null)
        {
            CommodityGroup commodityGroup = new CommodityGroup();
            try
            {
                commodityGroup.CommodityGroupList = entities.SP_FETCH_COMMODITY_GRP_MST(DB_Name).Where(a => (CommodityGroup == null || a.COMMODITY_GROUP_CODE.Contains(CommodityGroup) || a.COMMODITY_GROUP_DESC.Contains(CommodityGroup)
                    )).OrderBy(a=>a.COMMODITY_GROUP_CODE).ToList();
            }
            catch (Exception ex)
            {
                SaveErrorLog("CommodityGroup", "FetchCommodityGroupDetails", ex.Message, ex.StackTrace);
            }
            return commodityGroup;
        }

    }
}