﻿using MYSTIC.Data;
using MYSTIC.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MYSTIC.ControllerLogic
{
    public class ExchangeRateLogic : CommonLogic
    {
        private Entities entities = new Entities();
        public ExchangeRate FetchExchangeRate(string ExchangeRateType)
        {
            ExchangeRate exchangeRate = new ExchangeRate();
            try
            {
                exchangeRate.ExchangeRates = entities.FETCH_EXCHANGE_RATE.Where(a => a.EXRATE_TYPE == ExchangeRateType || string.IsNullOrEmpty(ExchangeRateType)).Select(
                    a => new ListExchangeRate
                    {
                        EXCHANGE_RATE_PK = a.EXCHANGE_RATE_PK,
                        EXRATE_TYPE = a.EXRATE_TYPE,
                        EXCHANGE_RATE = a.EXCHANGE_RATE,
                        FROM_CURR_ID = a.FROM_CURR_ID,
                        FROM_CURR_NAME = a.FROM_CURR_NAME,
                        TO_CURR_ID = a.TO_CURR_ID,
                        TO_CURR_NAME = a.TO_CURR_NAME,
                        ROE_BUY = a.ROE_BUY
                    }).ToList();
            }
            catch (Exception ex)
            {
                SaveErrorLog("ExchangeRate", "FetchExchangeRate", ex.Message, ex.StackTrace);
            }
            return exchangeRate;
        }
        public List<string> FetchExchangeRateType()
        {
            List<string> exchangeRateType = new List<string>();
            try
            {
                exchangeRateType = entities.FETCH_EXCHANGE_RATE.Select(a => a.EXRATE_TYPE).Distinct().ToList();
            }
            catch (Exception ex)
            {
                SaveErrorLog("ExchangeRate", "FetchExchangeRate", ex.Message, ex.StackTrace);
            }
            return exchangeRateType;
        }
    }
}