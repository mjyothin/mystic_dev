﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MYSTIC.Data;
using MYSTIC.Models;

namespace MYSTIC.ControllerLogic
{
    public class AirFreightSlabLogic:CommonLogic
    {
        Entities entities = new Entities();
        public AirFreightSlab FetchAirFreightSlab(string SearchParameter)
        {
            AirFreightSlab airFreightSlab = new AirFreightSlab();
            try
            {
                airFreightSlab.AirFreightDetails = entities.FETCH_AIRFREIGHTSLAB.Where(a => a.BREAKPOINT_ID.Contains(SearchParameter) || a.BREAKPOINT_DESC.Contains(SearchParameter)||string.IsNullOrEmpty(SearchParameter)).Select(
                    a=>new AirFreightSlabList
                    {
                        AIRFREIGHT_SLABS_TBL_PK=a.AIRFREIGHT_SLABS_TBL_PK,
                        BREAKPOINT_ID=a.BREAKPOINT_ID,
                        BREAKPOINT_DESC=a.BREAKPOINT_DESC,
                        BREAKPOINT_RANGE=a.BREAKPOINT_RANGE
                    }).ToList();
            }
            catch(Exception ex)
            {
                SaveErrorLog("AirFreight", "FetchAirFreightSlab",ex.Message, ex.StackTrace);
            }
            return airFreightSlab;
        }
    }
}