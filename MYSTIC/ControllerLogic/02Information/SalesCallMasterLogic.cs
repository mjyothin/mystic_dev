﻿using MYSTIC.Data;
using MYSTIC.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MYSTIC.ControllerLogic
{
    public class SalesCallMasterLogic : CommonLogic
    {
        private Entities entities = new Entities();
        public List<SalesCallType> FetchSalesCallType()
        {
            List<SalesCallType> lstSalesCallType = new List<SalesCallType>();
            try
            {
                lstSalesCallType = entities.S_CALL_TYPE_MST_TBL.Select(a => new SalesCallType
                {
                    SALES_CALL_TYPE_PK = a.SALES_CALL_TYPE_PK,
                    SALES_CALL_TYPE_ID = a.SALES_CALL_TYPE_ID,
                    SALES_CALL_TYPE_DESC = a.SALES_CALL_TYPE_DESC,
                    ACTIVE_FLAG = a.ACTIVE_FLAG
                }).ToList();
            }
            catch (Exception ex)
            {
                SaveErrorLog("SalesCallMaster", "FetchSalesCallType", ex.Message, ex.StackTrace);
            }
            return lstSalesCallType;
        }
        public List<SalesCallReason> FetchSalesCallReason()
        {
            List<SalesCallReason> lstSalesCallReason = new List<SalesCallReason>();
            try
            {
                lstSalesCallReason = entities.SAL_CAL_REASON_MST_TBL.Select(a => new SalesCallReason
                {
                    SAL_CAL_REASON_MST_TBL_PK = a.SAL_CAL_REASON_MST_TBL_PK,
                    SAL_CAL_ID = a.SAL_CAL_ID,
                    SAL_CAL_REASON = a.SAL_CAL_REASON
                }).ToList();
            }
            catch (Exception ex)
            {
                SaveErrorLog("SalesCallMaster", "FetchSalesCallType", ex.Message, ex.StackTrace);
            }
            return lstSalesCallReason;
        }
    }
}