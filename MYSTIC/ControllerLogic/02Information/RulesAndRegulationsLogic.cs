﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MYSTIC.Data;

namespace MYSTIC.ControllerLogic
{
    public class RulesAndRegulationsLogic
    {
        Entities entities = new Entities();
        public List<M_RULES_REGULATIONS> FetchRulesAndRegulations(string RulesID,string RulesDesc,string Country)
        {
            List<M_RULES_REGULATIONS> lstRules = entities.M_RULES_REGULATIONS.Where(a => (a.RULES_ID == RulesID || string.IsNullOrEmpty(RulesID)) &&
              (a.RULES_DESC == RulesDesc || string.IsNullOrEmpty(RulesDesc)) && (a.COUNTRY_NAME == Country || string.IsNullOrEmpty(Country))).ToList();
            return lstRules;

        }
    }
}