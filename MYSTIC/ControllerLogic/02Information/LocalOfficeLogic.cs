﻿using MYSTIC.Data;
using MYSTIC.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MYSTIC.ControllerLogic
{
    public class LocalOfficeLogic : CommonLogic
    {
        private Entities entities = new Entities();
        public List<Location> FetchLocalOfficeMaster()
        {
            List<Location> dropdowns = new List<Location>();
            try
            {
                dropdowns = entities.FETCH_LOCATION.Select(a => new Location
                {
                    LOCATION_MST_PK = a.LOCATION_MST_PK,
                    LOCATION_ID = a.LOCATION_ID,
                    LOCATION_NAME = a.LOCATION_NAME
                }).ToList();
            }
            catch (Exception ex)
            {
                SaveErrorLog("LocalOffice", "FetchLocalOfficeMaster", ex.Message, ex.StackTrace);
            }
            return dropdowns;
        }
        public LocalOffice FetchLocalOfficeDetails(int LocationPK)
        {
            LocalOffice localOffice = new LocalOffice();
            try
            {
                localOffice.Location = entities.FETCH_LOCATION.Where(a => a.LOCATION_MST_PK == LocationPK).FirstOrDefault();
                localOffice.WorkingPorts = entities.FETCH_LOCATION_WORKING_PORTS.Where(a => a.LOCATION_MST_PK == LocationPK).
                    Select(a=>new LocationWorkingPorts
                    {
                        PORT_MST_FK=a.PORT_MST_FK,
                        PORT_ID=a.PORT_ID,
                        PORT_NAME=a.PORT_NAME
                    }).ToList();
            }
            catch (Exception ex)
            {
                SaveErrorLog("LocalOffice", "FetchLocalOfficeDetails", ex.Message, ex.StackTrace);
            }
            return localOffice;
        }
    }
}