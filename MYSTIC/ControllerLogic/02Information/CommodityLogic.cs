﻿using MYSTIC.Data;
using MYSTIC.Models;
using System;
using System.Linq;

namespace MYSTIC.ControllerLogic
{
    public class CommodityLogic:CommonLogic
    {
        private Entities entities = new Entities();
       
        public Commodity FetchCommodityDetails(string CommodityID, string CommodityGroup)
        {
            Commodity commodity = new Commodity();
            try
            {
                commodity.CommodityDetails = entities.FETCH_COMMODITY.Where(a =>
                 (CommodityID == null || a.COMMODITY_ID.Contains(CommodityID) || a.COMMODITY_NAME.Contains(CommodityID))
                 && (CommodityGroup == null || a.COMMODITY_GROUP_CODE.Contains(CommodityGroup) || a.COMMODITY_GROUP_DESC.Contains(CommodityGroup)
                 )).OrderBy(a=>a.COMMODITY_NAME).ToList();
            }
            catch (Exception ex)
            {
                SaveErrorLog("Commodity", "FetchCommodityDetails", ex.Message, ex.StackTrace);
            }
            return commodity;
        }
        public Commodity FetchCommodityMaster(string CommodityID, string CommodityGroup,string DB_Name=null)
        {
            Commodity commodity = new Commodity();
            try
            {
                commodity.CommodityMaster = entities.SP_FETCH_COMMODITY_MST(DB_Name).Where(a =>
                 (CommodityID == null || a.COMMODITY_ID.Contains(CommodityID) || a.COMMODITY_NAME.Contains(CommodityID))
                 && (CommodityGroup == null || a.COMMODITY_GROUP_CODE.Contains(CommodityGroup) || a.COMMODITY_GROUP_DESC.Contains(CommodityGroup))).OrderBy(a=>a.COMMODITY_NAME).ToList();
            }
            catch (Exception ex)
            {
                SaveErrorLog("Commodity", "FetchCommodityDetails", ex.Message, ex.StackTrace);
            }
            return commodity;
        }

    }
}