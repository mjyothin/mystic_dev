﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MYSTIC.Data;
using MYSTIC.Models;

namespace MYSTIC.ControllerLogic
{
    public class UOMLogic:CommonLogic
    {
        Entities entities = new Entities();
        public UOM FetchUOM()
        {
            UOM uom = new UOM();
            try
            {
                uom.UOMMaster = entities.FETCH_UOM.ToList();
            }
            catch (Exception ex)
            {
                SaveErrorLog("UOM", "FetchUOM", ex.Message, ex.StackTrace);
            }
            return uom;

        }
    }
}