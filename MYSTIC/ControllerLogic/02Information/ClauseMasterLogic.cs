﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MYSTIC.Data;
using MYSTIC.Models;

namespace MYSTIC.ControllerLogic
{
    public class ClauseMasterLogic:CommonLogic
    {
        Entities entities = new Entities();
        public ClauseMaster FetchClauseMaster(string ClauseType)
        {
            ClauseMaster clauseMaster = new ClauseMaster();
            clauseMaster.Clauses = new List<ListClause>();
            ListClause listClause = new ListClause();
            try
            {
                List<ClauseDetails> clauseDetails = entities.FETCH_CLAUSE.Where(a => a.CLAUSE_TYPE.Contains(ClauseType)||string.IsNullOrEmpty(ClauseType)).Select(a => new ClauseDetails
                {
                    BL_CLAUSE_PK = a.BL_CLAUSE_PK,
                    BL_DESCRIPTION = a.BL_DESCRIPTION,
                    REFERENCE_NR = a.REFERENCE_NR,
                    CLAUSE_TYPE = a.CLAUSE_TYPE,
                    BIZ_TYPE = a.BIZ_TYPE
                }).OrderBy(a=>a.BL_DESCRIPTION).ToList();
                List<FETCH_CLAUSE_COMMODITY> clauseCommodities = entities.FETCH_CLAUSE_COMMODITY.Where(a => a.CLAUSE_TYPE.Contains(ClauseType) || string.IsNullOrEmpty(ClauseType)).ToList();
               List<FETCH_CLAUSE_PORT> clausePorts = entities.FETCH_CLAUSE_PORT.Where(a => a.CLAUSE_TYPE.Contains(ClauseType) || string.IsNullOrEmpty(ClauseType)).ToList();
                foreach (ClauseDetails cd in clauseDetails )
                {
                    listClause = new ListClause();
                    listClause.Clause = cd;
                    listClause.ClauseCommodity = clauseCommodities.Where(a => a.REFERENCE_NR == cd.REFERENCE_NR).Select(a => new ClauseCommodity
                    {
                        COMMODITY_ID = a.COMMODITY_ID,
                        COMMODITY_MST_PK = a.COMMODITY_MST_PK,
                        COMMODITY_NAME = a.COMMODITY_NAME
                    }).ToList();
                    listClause.ClausePort = clausePorts.Where(a => a.REFERENCE_NR == cd.REFERENCE_NR).Select(a => new ClausePort
                    {
                        PORT_ID = a.PORT_ID,
                        PORT_MST_PK = a.PORT_MST_PK,
                        PORT_NAME = a.PORT_NAME
                    }).ToList();
                    clauseMaster.Clauses.Add(listClause);
                }
               
            }
             catch(Exception ex)
            {
                SaveErrorLog("ClauseMaster", "FetchClauseMaster",ex.Message, ex.StackTrace);
            }
            return clauseMaster;
        }
        public List<string> FetchClauseType()
        {
            List<string> clauseDetails = new List<string>();
            try
            {
                clauseDetails = entities.FETCH_CLAUSE.Select(a=>a.CLAUSE_TYPE).Distinct().ToList();
            }
            catch (Exception ex)
            {
                SaveErrorLog("ClauseMaster", "FetchClauseMaster", ex.Message, ex.StackTrace);
            }
            return clauseDetails;
        }
    }
}