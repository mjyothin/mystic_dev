﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MYSTIC.Data;
using MYSTIC.Models;

namespace MYSTIC.ControllerLogic
{
    public class PlaceMasterLogic:CommonLogic
    {
        Entities entities = new Entities();
        public PlaceMaster FetchPlaceMaster(string Place)
        {
            PlaceMaster placeMaster = new PlaceMaster();
            try
            {
                placeMaster.PlaceMasters = entities.FETCH_PLACE.Where(a=>a.PLACE_CODE==Place||a.PLACE_NAME==Place||string.IsNullOrEmpty(Place)).Select(
                    a=>new Places
                    {
                        PLACE_PK=a.PLACE_PK,
                        PLACE_CODE=a.PLACE_CODE,
                        PLACE_NAME=a.PLACE_NAME
                    }
                    ).ToList();
            }
            catch (Exception ex)
            {
                SaveErrorLog("PlaceMaster", "FetchPlaceMaster", ex.Message, ex.StackTrace);
            }
            return placeMaster;
        }
    }
}