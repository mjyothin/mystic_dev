﻿using MYSTIC.Data;
using MYSTIC.Models;
using System;
using System.Linq;

namespace MYSTIC.ControllerLogic

{
    public class ContainerTypeLogic : CommonLogic
    {
        private Entities entities = new Entities();

        public ContainerType FetchContainerType(string ContainerTypeID,string DB_Name=null)
        {
            ContainerType containerType = new ContainerType();
            try
            {
                containerType.ContainerTypes = entities.FETCH_CONTAINER_TYPE.Where(a => a.CONTAINER_TYPE_MST_ID == ContainerTypeID).FirstOrDefault();                
            }
            catch (Exception ex)
            {
                SaveErrorLog("ContainerType", "FetchContainerType", ex.Message, ex.StackTrace);
            }
            return containerType;
        }
        public ContainerType FetchContainerMaster(string ContainerTypeID, string DB_Name = null)
        {
            ContainerType containerType = new ContainerType();
            try
            {
                containerType.ContainerMaster = entities.SP_FETCH_CONTAINER_MST(DB_Name).Where(a => a.CONTAINER_TYPE_MST_ID == ContainerTypeID|| string.IsNullOrEmpty(ContainerTypeID)).OrderBy(a=>a.CONTAINER_TYPE_MST_ID).ToList();
            }
            catch (Exception ex)
            {
                SaveErrorLog("ContainerType", "FetchContainerType", ex.Message, ex.StackTrace);
            }
            return containerType;
        }
    }
}