﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MYSTIC.Data;
using MYSTIC.Models;

namespace MYSTIC.ControllerLogic
{
    public class ShippingTermsLogic:CommonLogic
    {
        Entities entities = new Entities();
        public ShippingTerms FetchShippingTerms(string Terms)
        {
            ShippingTerms shippingTerms = new ShippingTerms();
            try
            {
                shippingTerms.LstShippingTerms = entities.FETCH_SHIPPINGTERMS.Where(a=>a.INCO_CODE==Terms||a.INCO_CODE_DESCRIPTION==Terms||string.IsNullOrEmpty(Terms)).ToList();
            }
            catch (Exception ex)
            {
                SaveErrorLog("ShippingTerms", "FetchShippingTerms", ex.Message, ex.StackTrace);
            }
            return shippingTerms;
        }
    }
}