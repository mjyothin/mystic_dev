﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Ajax.Utilities;
using MYSTIC.Data;
using MYSTIC.Models;

namespace MYSTIC.ControllerLogic
{
    public class PortLogic:CommonLogic
    {
        Entities entities = new Entities();
        public Port FetchPort(string PortID,string DBName,string BusinessType)
        {
            Port port = new Port();
            try
            {
                port.Ports = entities.SP_FETCH_PORT_MST(DBName,PortID).Where(a=>a.BIZ_TYPE==BusinessType||string.IsNullOrEmpty(BusinessType)).Select(
                    a=>new Ports
                    {
                        PORT_MST_PK=a.PORT_MST_PK,
                        PORT_ID=a.PORT_ID,
                        PORT_NAME=a.PORT_NAME,
                        BIZ_TYPE=a.BIZ_TYPE                      
                    }).ToList();
            }
            catch (Exception ex)
            {
                SaveErrorLog("Port", "FetchPort", ex.Message, ex.StackTrace);
            }
            return port;
        }
    }
}