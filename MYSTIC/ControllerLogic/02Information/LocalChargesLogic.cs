﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MYSTIC.Data;
using MYSTIC.Models;

namespace MYSTIC.ControllerLogic
{
    public class LocalChargeLogic:CommonLogic
    {
        Entities entities = new Entities();
        public LocalCharges FetchLocalCharges()
        {
            LocalCharges localCharges = new LocalCharges();
            try
            {
                localCharges.LocalCharge = entities.FETCH_LOCALCHARGE.ToList();
            }
            catch (Exception ex)
            {
                SaveErrorLog("LocalCharges", "FetchLocalCharges", ex.Message, ex.StackTrace);
            }
            return localCharges;

        }
    }
}