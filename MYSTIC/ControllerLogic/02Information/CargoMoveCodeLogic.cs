﻿using MYSTIC.Data;
using MYSTIC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MYSTIC.ControllerLogic
{
    public class CargoMoveCodeLogic:CommonLogic
    {
        private Entities entities = new Entities();

        public CargoMoveCode FetchCargoMoveCode(string CargoMoveCodeID,string DB_Name=null)
        {
            CargoMoveCode cargoMoveCode = new CargoMoveCode();
            try
            { 
                cargoMoveCode.CargoMoveCodeList = entities.SP_FETCH_MOVECODE_MST(DB_Name).Where(a => (a.MOVECODE_ID == CargoMoveCodeID ||string.IsNullOrEmpty( CargoMoveCodeID))).ToList();
            }
            catch (Exception ex)
            {
                SaveErrorLog("Commodity", "FetchCommodityDetails", ex.Message, ex.StackTrace);
            }
            return cargoMoveCode;
        }
    }
}