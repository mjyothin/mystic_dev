﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MYSTIC.Data;
using MYSTIC.Models;

namespace MYSTIC.ControllerLogic
{
    public class PackTypeLogic:CommonLogic
    {
        Entities entities = new Entities();
        //public PackType FetchPackType()
        //{
        //    PackType packType = new PackType();
        //    try
        //    {
        //        packType.PackTypes = entities.FETCH_PACKTYPE.ToList();
        //    }
        //    catch (Exception ex)
        //    {
        //        SaveErrorLog("PackType", "FetchPackType", ex.Message, ex.StackTrace);
        //    }
        //    return packType;
        //}
        public PackType FetchPackTypeMaster(string PackTypeID,string DB_Name=null)
        {
            PackType packType = new PackType();
            try
            {
                packType.PackTypeList = entities.SP_FETCH_PACKTYPE_MST(DB_Name).Where(a=>a.PACK_TYPE_ID==PackTypeID||a.PACK_TYPE_DESC==PackTypeID||string.IsNullOrEmpty(PackTypeID)).
                    Select(a=>new PackTypes
                    {
                        PACK_TYPE_MST_PK=a.PACK_TYPE_MST_PK,
                        PACK_TYPE_ID=a.PACK_TYPE_ID,
                        PACK_TYPE_DESC=a.PACK_TYPE_DESC,
                        UOM=a.UOM
                    }).ToList();
            }
            catch (Exception ex)
            {
                SaveErrorLog("PackType", "FetchPackTypeMaster", ex.Message, ex.StackTrace);
            }
            return packType;
        }
    }
}